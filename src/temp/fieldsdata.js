const fieldsData = [
    {
        qListObject: {
            qSize: {
                qcx: 1,
                qcy: 70
            },
            qDimensionInfo: {
                qFallbackTitle: "Product Sub Group",
                qApprMaxGlyphCount: 3,
                qCardinal: 70,
                qSortIndicator: "A",
                qGroupFallbackTitles: ["Product Sub Group"],
                qGroupPos: 0,
                qStateCounts: {
                    qLocked: 0,
                    qSelected: 1,
                    qOption: 0,
                    qDeselected: 0,
                    qAlternative: 69,
                    qExcluded: 0,
                    qSelectedExcluded: 0,
                    qLockedExcluded: 0
                },
                qTags: ["$key", "$numeric", "$integer"],
                qDimensionType: "N",
                qGrouping: "N",
                qNumFormat: {
                    qType: "U",
                    qnDec: 0,
                    qUseThou: 0
                },
                qIsAutoFormat: true,
                qGroupFieldDefs: ["Product Sub Group"],
                qMin: 0,
                qMax: 0,
                qAttrExprInfo: [],
                qAttrDimInfo: [],
                qCardinalities: {
                    qCardinal: 70,
                    qHypercubeCardinal: 0
                }
            },
            qExpressions: [],
            qDataPages: [
                {
                    qMatrix: [
                        [
                            {
                                qText: "3",
                                qNum: 3,
                                qElemNumber: 0,
                                qState: "S"
                            }
                        ],
                        [
                            {
                                qText: "1",
                                qNum: 1,
                                qElemNumber: 26,
                                qState: "X"
                            }
                        ],
                        [
                            {
                                qText: "2",
                                qNum: 2,
                                qElemNumber: 69,
                                qState: "X"
                            }
                        ],
                        [
                            {
                                qText: "4",
                                qNum: 4,
                                qElemNumber: 24,
                                qState: "X"
                            }
                        ],
                        [
                            {
                                qText: "5",
                                qNum: 5,
                                qElemNumber: 56,
                                qState: "X"
                            }
                        ],
                        [
                            {
                                qText: "6",
                                qNum: 6,
                                qElemNumber: 65,
                                qState: "X"
                            }
                        ],
                        [
                            {
                                qText: "8",
                                qNum: 8,
                                qElemNumber: 36,
                                qState: "X"
                            }
                        ],
                        [
                            {
                                qText: "9",
                                qNum: 9,
                                qElemNumber: 23,
                                qState: "X"
                            }
                        ],
                        [
                            {
                                qText: "10",
                                qNum: 10,
                                qElemNumber: 64,
                                qState: "X"
                            }
                        ],
                        [
                            {
                                qText: "11",
                                qNum: 11,
                                qElemNumber: 32,
                                qState: "X"
                            }
                        ]
                    ],
                    qTails: [],
                    qArea: {
                        qLeft: 0,
                        qTop: 0,
                        qWidth: 1,
                        qHeight: 70
                    }
                }
            ]
        }
    }
]


export default fieldsData
