(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("vue"));
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["sense-field-view"] = factory(require("vue"));
	else
		root["sense-field-view"] = factory(root["Vue"]);
})((typeof self !== 'undefined' ? self : this), function(__WEBPACK_EXTERNAL_MODULE__8bbf__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "fb15");
/******/ })
/************************************************************************/
/******/ ({

/***/ "008c":
/***/ (function(module, exports, __webpack_require__) {

var has = __webpack_require__("4276");
var toIObject = __webpack_require__("531a");
var arrayIndexOf = __webpack_require__("5464")(false);
var IE_PROTO = __webpack_require__("037a")('IE_PROTO');

module.exports = function (object, names) {
  var O = toIObject(object);
  var i = 0;
  var result = [];
  var key;
  for (key in O) if (key != IE_PROTO) has(O, key) && result.push(key);
  // Don't enum bug & hidden keys
  while (names.length > i) if (has(O, key = names[i++])) {
    ~arrayIndexOf(result, key) || result.push(key);
  }
  return result;
};


/***/ }),

/***/ "014b":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// ECMAScript 6 symbols shim
var global = __webpack_require__("e53d");
var has = __webpack_require__("07e3");
var DESCRIPTORS = __webpack_require__("8e60");
var $export = __webpack_require__("63b6");
var redefine = __webpack_require__("9138");
var META = __webpack_require__("ebfd").KEY;
var $fails = __webpack_require__("294c");
var shared = __webpack_require__("dbdb");
var setToStringTag = __webpack_require__("45f2");
var uid = __webpack_require__("62a0");
var wks = __webpack_require__("5168");
var wksExt = __webpack_require__("ccb9");
var wksDefine = __webpack_require__("6718");
var enumKeys = __webpack_require__("47ee");
var isArray = __webpack_require__("9003");
var anObject = __webpack_require__("e4ae");
var isObject = __webpack_require__("f772");
var toIObject = __webpack_require__("36c3");
var toPrimitive = __webpack_require__("1bc3");
var createDesc = __webpack_require__("aebd");
var _create = __webpack_require__("a159");
var gOPNExt = __webpack_require__("0395");
var $GOPD = __webpack_require__("bf0b");
var $DP = __webpack_require__("d9f6");
var $keys = __webpack_require__("c3a1");
var gOPD = $GOPD.f;
var dP = $DP.f;
var gOPN = gOPNExt.f;
var $Symbol = global.Symbol;
var $JSON = global.JSON;
var _stringify = $JSON && $JSON.stringify;
var PROTOTYPE = 'prototype';
var HIDDEN = wks('_hidden');
var TO_PRIMITIVE = wks('toPrimitive');
var isEnum = {}.propertyIsEnumerable;
var SymbolRegistry = shared('symbol-registry');
var AllSymbols = shared('symbols');
var OPSymbols = shared('op-symbols');
var ObjectProto = Object[PROTOTYPE];
var USE_NATIVE = typeof $Symbol == 'function';
var QObject = global.QObject;
// Don't use setters in Qt Script, https://github.com/zloirock/core-js/issues/173
var setter = !QObject || !QObject[PROTOTYPE] || !QObject[PROTOTYPE].findChild;

// fallback for old Android, https://code.google.com/p/v8/issues/detail?id=687
var setSymbolDesc = DESCRIPTORS && $fails(function () {
  return _create(dP({}, 'a', {
    get: function () { return dP(this, 'a', { value: 7 }).a; }
  })).a != 7;
}) ? function (it, key, D) {
  var protoDesc = gOPD(ObjectProto, key);
  if (protoDesc) delete ObjectProto[key];
  dP(it, key, D);
  if (protoDesc && it !== ObjectProto) dP(ObjectProto, key, protoDesc);
} : dP;

var wrap = function (tag) {
  var sym = AllSymbols[tag] = _create($Symbol[PROTOTYPE]);
  sym._k = tag;
  return sym;
};

var isSymbol = USE_NATIVE && typeof $Symbol.iterator == 'symbol' ? function (it) {
  return typeof it == 'symbol';
} : function (it) {
  return it instanceof $Symbol;
};

var $defineProperty = function defineProperty(it, key, D) {
  if (it === ObjectProto) $defineProperty(OPSymbols, key, D);
  anObject(it);
  key = toPrimitive(key, true);
  anObject(D);
  if (has(AllSymbols, key)) {
    if (!D.enumerable) {
      if (!has(it, HIDDEN)) dP(it, HIDDEN, createDesc(1, {}));
      it[HIDDEN][key] = true;
    } else {
      if (has(it, HIDDEN) && it[HIDDEN][key]) it[HIDDEN][key] = false;
      D = _create(D, { enumerable: createDesc(0, false) });
    } return setSymbolDesc(it, key, D);
  } return dP(it, key, D);
};
var $defineProperties = function defineProperties(it, P) {
  anObject(it);
  var keys = enumKeys(P = toIObject(P));
  var i = 0;
  var l = keys.length;
  var key;
  while (l > i) $defineProperty(it, key = keys[i++], P[key]);
  return it;
};
var $create = function create(it, P) {
  return P === undefined ? _create(it) : $defineProperties(_create(it), P);
};
var $propertyIsEnumerable = function propertyIsEnumerable(key) {
  var E = isEnum.call(this, key = toPrimitive(key, true));
  if (this === ObjectProto && has(AllSymbols, key) && !has(OPSymbols, key)) return false;
  return E || !has(this, key) || !has(AllSymbols, key) || has(this, HIDDEN) && this[HIDDEN][key] ? E : true;
};
var $getOwnPropertyDescriptor = function getOwnPropertyDescriptor(it, key) {
  it = toIObject(it);
  key = toPrimitive(key, true);
  if (it === ObjectProto && has(AllSymbols, key) && !has(OPSymbols, key)) return;
  var D = gOPD(it, key);
  if (D && has(AllSymbols, key) && !(has(it, HIDDEN) && it[HIDDEN][key])) D.enumerable = true;
  return D;
};
var $getOwnPropertyNames = function getOwnPropertyNames(it) {
  var names = gOPN(toIObject(it));
  var result = [];
  var i = 0;
  var key;
  while (names.length > i) {
    if (!has(AllSymbols, key = names[i++]) && key != HIDDEN && key != META) result.push(key);
  } return result;
};
var $getOwnPropertySymbols = function getOwnPropertySymbols(it) {
  var IS_OP = it === ObjectProto;
  var names = gOPN(IS_OP ? OPSymbols : toIObject(it));
  var result = [];
  var i = 0;
  var key;
  while (names.length > i) {
    if (has(AllSymbols, key = names[i++]) && (IS_OP ? has(ObjectProto, key) : true)) result.push(AllSymbols[key]);
  } return result;
};

// 19.4.1.1 Symbol([description])
if (!USE_NATIVE) {
  $Symbol = function Symbol() {
    if (this instanceof $Symbol) throw TypeError('Symbol is not a constructor!');
    var tag = uid(arguments.length > 0 ? arguments[0] : undefined);
    var $set = function (value) {
      if (this === ObjectProto) $set.call(OPSymbols, value);
      if (has(this, HIDDEN) && has(this[HIDDEN], tag)) this[HIDDEN][tag] = false;
      setSymbolDesc(this, tag, createDesc(1, value));
    };
    if (DESCRIPTORS && setter) setSymbolDesc(ObjectProto, tag, { configurable: true, set: $set });
    return wrap(tag);
  };
  redefine($Symbol[PROTOTYPE], 'toString', function toString() {
    return this._k;
  });

  $GOPD.f = $getOwnPropertyDescriptor;
  $DP.f = $defineProperty;
  __webpack_require__("6abf").f = gOPNExt.f = $getOwnPropertyNames;
  __webpack_require__("355d").f = $propertyIsEnumerable;
  __webpack_require__("9aa9").f = $getOwnPropertySymbols;

  if (DESCRIPTORS && !__webpack_require__("b8e3")) {
    redefine(ObjectProto, 'propertyIsEnumerable', $propertyIsEnumerable, true);
  }

  wksExt.f = function (name) {
    return wrap(wks(name));
  };
}

$export($export.G + $export.W + $export.F * !USE_NATIVE, { Symbol: $Symbol });

for (var es6Symbols = (
  // 19.4.2.2, 19.4.2.3, 19.4.2.4, 19.4.2.6, 19.4.2.8, 19.4.2.9, 19.4.2.10, 19.4.2.11, 19.4.2.12, 19.4.2.13, 19.4.2.14
  'hasInstance,isConcatSpreadable,iterator,match,replace,search,species,split,toPrimitive,toStringTag,unscopables'
).split(','), j = 0; es6Symbols.length > j;)wks(es6Symbols[j++]);

for (var wellKnownSymbols = $keys(wks.store), k = 0; wellKnownSymbols.length > k;) wksDefine(wellKnownSymbols[k++]);

$export($export.S + $export.F * !USE_NATIVE, 'Symbol', {
  // 19.4.2.1 Symbol.for(key)
  'for': function (key) {
    return has(SymbolRegistry, key += '')
      ? SymbolRegistry[key]
      : SymbolRegistry[key] = $Symbol(key);
  },
  // 19.4.2.5 Symbol.keyFor(sym)
  keyFor: function keyFor(sym) {
    if (!isSymbol(sym)) throw TypeError(sym + ' is not a symbol!');
    for (var key in SymbolRegistry) if (SymbolRegistry[key] === sym) return key;
  },
  useSetter: function () { setter = true; },
  useSimple: function () { setter = false; }
});

$export($export.S + $export.F * !USE_NATIVE, 'Object', {
  // 19.1.2.2 Object.create(O [, Properties])
  create: $create,
  // 19.1.2.4 Object.defineProperty(O, P, Attributes)
  defineProperty: $defineProperty,
  // 19.1.2.3 Object.defineProperties(O, Properties)
  defineProperties: $defineProperties,
  // 19.1.2.6 Object.getOwnPropertyDescriptor(O, P)
  getOwnPropertyDescriptor: $getOwnPropertyDescriptor,
  // 19.1.2.7 Object.getOwnPropertyNames(O)
  getOwnPropertyNames: $getOwnPropertyNames,
  // 19.1.2.8 Object.getOwnPropertySymbols(O)
  getOwnPropertySymbols: $getOwnPropertySymbols
});

// 24.3.2 JSON.stringify(value [, replacer [, space]])
$JSON && $export($export.S + $export.F * (!USE_NATIVE || $fails(function () {
  var S = $Symbol();
  // MS Edge converts symbol values to JSON as {}
  // WebKit converts symbol values to JSON as null
  // V8 throws on boxed symbols
  return _stringify([S]) != '[null]' || _stringify({ a: S }) != '{}' || _stringify(Object(S)) != '{}';
})), 'JSON', {
  stringify: function stringify(it) {
    var args = [it];
    var i = 1;
    var replacer, $replacer;
    while (arguments.length > i) args.push(arguments[i++]);
    $replacer = replacer = args[1];
    if (!isObject(replacer) && it === undefined || isSymbol(it)) return; // IE8 returns string on undefined
    if (!isArray(replacer)) replacer = function (key, value) {
      if (typeof $replacer == 'function') value = $replacer.call(this, key, value);
      if (!isSymbol(value)) return value;
    };
    args[1] = replacer;
    return _stringify.apply($JSON, args);
  }
});

// 19.4.3.4 Symbol.prototype[@@toPrimitive](hint)
$Symbol[PROTOTYPE][TO_PRIMITIVE] || __webpack_require__("35e8")($Symbol[PROTOTYPE], TO_PRIMITIVE, $Symbol[PROTOTYPE].valueOf);
// 19.4.3.5 Symbol.prototype[@@toStringTag]
setToStringTag($Symbol, 'Symbol');
// 20.2.1.9 Math[@@toStringTag]
setToStringTag(Math, 'Math', true);
// 24.3.3 JSON[@@toStringTag]
setToStringTag(global.JSON, 'JSON', true);


/***/ }),

/***/ "0293":
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.9 Object.getPrototypeOf(O)
var toObject = __webpack_require__("241e");
var $getPrototypeOf = __webpack_require__("53e2");

__webpack_require__("ce7e")('getPrototypeOf', function () {
  return function getPrototypeOf(it) {
    return $getPrototypeOf(toObject(it));
  };
});


/***/ }),

/***/ "037a":
/***/ (function(module, exports, __webpack_require__) {

var shared = __webpack_require__("b658")('keys');
var uid = __webpack_require__("11f1");
module.exports = function (key) {
  return shared[key] || (shared[key] = uid(key));
};


/***/ }),

/***/ "0395":
/***/ (function(module, exports, __webpack_require__) {

// fallback for IE11 buggy Object.getOwnPropertyNames with iframe and window
var toIObject = __webpack_require__("36c3");
var gOPN = __webpack_require__("6abf").f;
var toString = {}.toString;

var windowNames = typeof window == 'object' && window && Object.getOwnPropertyNames
  ? Object.getOwnPropertyNames(window) : [];

var getWindowNames = function (it) {
  try {
    return gOPN(it);
  } catch (e) {
    return windowNames.slice();
  }
};

module.exports.f = function getOwnPropertyNames(it) {
  return windowNames && toString.call(it) == '[object Window]' ? getWindowNames(it) : gOPN(toIObject(it));
};


/***/ }),

/***/ "03cc":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("e5b8");

/***/ }),

/***/ "03dd":
/***/ (function(module, exports, __webpack_require__) {

// 21.2.5.3 get RegExp.prototype.flags()
if (__webpack_require__("7e81") && /./g.flags != 'g') __webpack_require__("120e").f(RegExp.prototype, 'flags', {
  configurable: true,
  get: __webpack_require__("c26b")
});


/***/ }),

/***/ "061b":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("fa99");

/***/ }),

/***/ "07e3":
/***/ (function(module, exports) {

var hasOwnProperty = {}.hasOwnProperty;
module.exports = function (it, key) {
  return hasOwnProperty.call(it, key);
};


/***/ }),

/***/ "094c":
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__("635a");
var core = __webpack_require__("7a1e");
var hide = __webpack_require__("59c4");
var redefine = __webpack_require__("3143");
var ctx = __webpack_require__("b98f");
var PROTOTYPE = 'prototype';

var $export = function (type, name, source) {
  var IS_FORCED = type & $export.F;
  var IS_GLOBAL = type & $export.G;
  var IS_STATIC = type & $export.S;
  var IS_PROTO = type & $export.P;
  var IS_BIND = type & $export.B;
  var target = IS_GLOBAL ? global : IS_STATIC ? global[name] || (global[name] = {}) : (global[name] || {})[PROTOTYPE];
  var exports = IS_GLOBAL ? core : core[name] || (core[name] = {});
  var expProto = exports[PROTOTYPE] || (exports[PROTOTYPE] = {});
  var key, own, out, exp;
  if (IS_GLOBAL) source = name;
  for (key in source) {
    // contains in native
    own = !IS_FORCED && target && target[key] !== undefined;
    // export native or passed
    out = (own ? target : source)[key];
    // bind timers to global for call from export context
    exp = IS_BIND && own ? ctx(out, global) : IS_PROTO && typeof out == 'function' ? ctx(Function.call, out) : out;
    // extend global
    if (target) redefine(target, key, out, type & $export.U);
    // export
    if (exports[key] != out) hide(exports, key, exp);
    if (IS_PROTO && expProto[key] != out) expProto[key] = out;
  }
};
global.core = core;
// type bitmap
$export.F = 1;   // forced
$export.G = 2;   // global
$export.S = 4;   // static
$export.P = 8;   // proto
$export.B = 16;  // bind
$export.W = 32;  // wrap
$export.U = 64;  // safe
$export.R = 128; // real proto method for `library`
module.exports = $export;


/***/ }),

/***/ "09e3":
/***/ (function(module, exports, __webpack_require__) {

var def = __webpack_require__("120e").f;
var has = __webpack_require__("4276");
var TAG = __webpack_require__("fe95")('toStringTag');

module.exports = function (it, tag, stat) {
  if (it && !has(it = stat ? it : it.prototype, TAG)) def(it, TAG, { configurable: true, value: tag });
};


/***/ }),

/***/ "0b69":
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "0f30":
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.9 / 15.2.3.2 Object.getPrototypeOf(O)
var has = __webpack_require__("4276");
var toObject = __webpack_require__("9bce");
var IE_PROTO = __webpack_require__("037a")('IE_PROTO');
var ObjectProto = Object.prototype;

module.exports = Object.getPrototypeOf || function (O) {
  O = toObject(O);
  if (has(O, IE_PROTO)) return O[IE_PROTO];
  if (typeof O.constructor == 'function' && O instanceof O.constructor) {
    return O.constructor.prototype;
  } return O instanceof Object ? ObjectProto : null;
};


/***/ }),

/***/ "0f31":
/***/ (function(module, exports) {

// 7.2.1 RequireObjectCoercible(argument)
module.exports = function (it) {
  if (it == undefined) throw TypeError("Can't call method on  " + it);
  return it;
};


/***/ }),

/***/ "0fc9":
/***/ (function(module, exports, __webpack_require__) {

var toInteger = __webpack_require__("3a38");
var max = Math.max;
var min = Math.min;
module.exports = function (index, length) {
  index = toInteger(index);
  return index < 0 ? max(index + length, 0) : min(index, length);
};


/***/ }),

/***/ "1173":
/***/ (function(module, exports) {

module.exports = function (it, Constructor, name, forbiddenField) {
  if (!(it instanceof Constructor) || (forbiddenField !== undefined && forbiddenField in it)) {
    throw TypeError(name + ': incorrect invocation!');
  } return it;
};


/***/ }),

/***/ "11f1":
/***/ (function(module, exports) {

var id = 0;
var px = Math.random();
module.exports = function (key) {
  return 'Symbol('.concat(key === undefined ? '' : key, ')_', (++id + px).toString(36));
};


/***/ }),

/***/ "120e":
/***/ (function(module, exports, __webpack_require__) {

var anObject = __webpack_require__("b597");
var IE8_DOM_DEFINE = __webpack_require__("78a3");
var toPrimitive = __webpack_require__("b5ef");
var dP = Object.defineProperty;

exports.f = __webpack_require__("7e81") ? Object.defineProperty : function defineProperty(O, P, Attributes) {
  anObject(O);
  P = toPrimitive(P, true);
  anObject(Attributes);
  if (IE8_DOM_DEFINE) try {
    return dP(O, P, Attributes);
  } catch (e) { /* empty */ }
  if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported!');
  if ('value' in Attributes) O[P] = Attributes.value;
  return O;
};


/***/ }),

/***/ "1271":
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.7 Object.getOwnPropertyNames(O)
__webpack_require__("ce7e")('getOwnPropertyNames', function () {
  return __webpack_require__("0395").f;
});


/***/ }),

/***/ "1654":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var $at = __webpack_require__("71c1")(true);

// 21.1.3.27 String.prototype[@@iterator]()
__webpack_require__("30f1")(String, 'String', function (iterated) {
  this._t = String(iterated); // target
  this._i = 0;                // next index
// 21.1.5.2.1 %StringIteratorPrototype%.next()
}, function () {
  var O = this._t;
  var index = this._i;
  var point;
  if (index >= O.length) return { value: undefined, done: true };
  point = $at(O, index);
  this._i += point.length;
  return { value: point, done: false };
});


/***/ }),

/***/ "1691":
/***/ (function(module, exports) {

// IE 8- don't enum bug keys
module.exports = (
  'constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf'
).split(',');


/***/ }),

/***/ "16f9":
/***/ (function(module, exports) {

module.exports = {};


/***/ }),

/***/ "172c":
/***/ (function(module, exports, __webpack_require__) {

var toInteger = __webpack_require__("58ce");
var max = Math.max;
var min = Math.min;
module.exports = function (index, length) {
  index = toInteger(index);
  return index < 0 ? max(index + length, 0) : min(index, length);
};


/***/ }),

/***/ "1922":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_QlikFieldPlaceholder_vue_vue_type_style_index_0_id_3736f87e_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("9d06");
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_QlikFieldPlaceholder_vue_vue_type_style_index_0_id_3736f87e_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_QlikFieldPlaceholder_vue_vue_type_style_index_0_id_3736f87e_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */
 /* unused harmony default export */ var _unused_webpack_default_export = (_node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_QlikFieldPlaceholder_vue_vue_type_style_index_0_id_3736f87e_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "1af6":
/***/ (function(module, exports, __webpack_require__) {

// 22.1.2.2 / 15.4.3.2 Array.isArray(arg)
var $export = __webpack_require__("63b6");

$export($export.S, 'Array', { isArray: __webpack_require__("9003") });


/***/ }),

/***/ "1bc3":
/***/ (function(module, exports, __webpack_require__) {

// 7.1.1 ToPrimitive(input [, PreferredType])
var isObject = __webpack_require__("f772");
// instead of the ES6 spec version, we didn't implement @@toPrimitive case
// and the second argument - flag - preferred type is a string
module.exports = function (it, S) {
  if (!isObject(it)) return it;
  var fn, val;
  if (S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  if (typeof (fn = it.valueOf) == 'function' && !isObject(val = fn.call(it))) return val;
  if (!S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  throw TypeError("Can't convert object to primitive value");
};


/***/ }),

/***/ "1cb7":
/***/ (function(module, exports, __webpack_require__) {

// 26.1.6 Reflect.get(target, propertyKey [, receiver])
var gOPD = __webpack_require__("bf0b");
var getPrototypeOf = __webpack_require__("53e2");
var has = __webpack_require__("07e3");
var $export = __webpack_require__("63b6");
var isObject = __webpack_require__("f772");
var anObject = __webpack_require__("e4ae");

function get(target, propertyKey /* , receiver */) {
  var receiver = arguments.length < 3 ? target : arguments[2];
  var desc, proto;
  if (anObject(target) === receiver) return target[propertyKey];
  if (desc = gOPD.f(target, propertyKey)) return has(desc, 'value')
    ? desc.value
    : desc.get !== undefined
      ? desc.get.call(receiver)
      : undefined;
  if (isObject(proto = getPrototypeOf(target))) return get(proto, propertyKey, receiver);
}

$export($export.S, 'Reflect', { get: get });


/***/ }),

/***/ "1daf":
/***/ (function(module, exports, __webpack_require__) {

// 22.1.3.31 Array.prototype[@@unscopables]
var UNSCOPABLES = __webpack_require__("fe95")('unscopables');
var ArrayProto = Array.prototype;
if (ArrayProto[UNSCOPABLES] == undefined) __webpack_require__("59c4")(ArrayProto, UNSCOPABLES, {});
module.exports = function (key) {
  ArrayProto[UNSCOPABLES][key] = true;
};


/***/ }),

/***/ "1df8":
/***/ (function(module, exports, __webpack_require__) {

// 19.1.3.19 Object.setPrototypeOf(O, proto)
var $export = __webpack_require__("63b6");
$export($export.S, 'Object', { setPrototypeOf: __webpack_require__("ead6").set });


/***/ }),

/***/ "1ec9":
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__("f772");
var document = __webpack_require__("e53d").document;
// typeof document.createElement is 'object' in old IE
var is = isObject(document) && isObject(document.createElement);
module.exports = function (it) {
  return is ? document.createElement(it) : {};
};


/***/ }),

/***/ "1fbc":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

__webpack_require__("2177");
var redefine = __webpack_require__("3143");
var hide = __webpack_require__("59c4");
var fails = __webpack_require__("7eae");
var defined = __webpack_require__("0f31");
var wks = __webpack_require__("fe95");
var regexpExec = __webpack_require__("7f72");

var SPECIES = wks('species');

var REPLACE_SUPPORTS_NAMED_GROUPS = !fails(function () {
  // #replace needs built-in support for named groups.
  // #match works fine because it just return the exec results, even if it has
  // a "grops" property.
  var re = /./;
  re.exec = function () {
    var result = [];
    result.groups = { a: '7' };
    return result;
  };
  return ''.replace(re, '$<a>') !== '7';
});

var SPLIT_WORKS_WITH_OVERWRITTEN_EXEC = (function () {
  // Chrome 51 has a buggy "split" implementation when RegExp#exec !== nativeExec
  var re = /(?:)/;
  var originalExec = re.exec;
  re.exec = function () { return originalExec.apply(this, arguments); };
  var result = 'ab'.split(re);
  return result.length === 2 && result[0] === 'a' && result[1] === 'b';
})();

module.exports = function (KEY, length, exec) {
  var SYMBOL = wks(KEY);

  var DELEGATES_TO_SYMBOL = !fails(function () {
    // String methods call symbol-named RegEp methods
    var O = {};
    O[SYMBOL] = function () { return 7; };
    return ''[KEY](O) != 7;
  });

  var DELEGATES_TO_EXEC = DELEGATES_TO_SYMBOL ? !fails(function () {
    // Symbol-named RegExp methods call .exec
    var execCalled = false;
    var re = /a/;
    re.exec = function () { execCalled = true; return null; };
    if (KEY === 'split') {
      // RegExp[@@split] doesn't call the regex's exec method, but first creates
      // a new one. We need to return the patched regex when creating the new one.
      re.constructor = {};
      re.constructor[SPECIES] = function () { return re; };
    }
    re[SYMBOL]('');
    return !execCalled;
  }) : undefined;

  if (
    !DELEGATES_TO_SYMBOL ||
    !DELEGATES_TO_EXEC ||
    (KEY === 'replace' && !REPLACE_SUPPORTS_NAMED_GROUPS) ||
    (KEY === 'split' && !SPLIT_WORKS_WITH_OVERWRITTEN_EXEC)
  ) {
    var nativeRegExpMethod = /./[SYMBOL];
    var fns = exec(
      defined,
      SYMBOL,
      ''[KEY],
      function maybeCallNative(nativeMethod, regexp, str, arg2, forceStringMethod) {
        if (regexp.exec === regexpExec) {
          if (DELEGATES_TO_SYMBOL && !forceStringMethod) {
            // The native String method already delegates to @@method (this
            // polyfilled function), leasing to infinite recursion.
            // We avoid it by directly calling the native @@method method.
            return { done: true, value: nativeRegExpMethod.call(regexp, str, arg2) };
          }
          return { done: true, value: nativeMethod.call(str, regexp, arg2) };
        }
        return { done: false };
      }
    );
    var strfn = fns[0];
    var rxfn = fns[1];

    redefine(String.prototype, KEY, strfn);
    hide(RegExp.prototype, SYMBOL, length == 2
      // 21.2.5.8 RegExp.prototype[@@replace](string, replaceValue)
      // 21.2.5.11 RegExp.prototype[@@split](string, limit)
      ? function (string, arg) { return rxfn.call(string, this, arg); }
      // 21.2.5.6 RegExp.prototype[@@match](string)
      // 21.2.5.9 RegExp.prototype[@@search](string)
      : function (string) { return rxfn.call(string, this); }
    );
  }
};


/***/ }),

/***/ "20fd":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var $defineProperty = __webpack_require__("d9f6");
var createDesc = __webpack_require__("aebd");

module.exports = function (object, index, value) {
  if (index in object) $defineProperty.f(object, index, createDesc(0, value));
  else object[index] = value;
};


/***/ }),

/***/ "2118":
/***/ (function(module, exports) {

// IE 8- don't enum bug keys
module.exports = (
  'constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf'
).split(',');


/***/ }),

/***/ "2177":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var regexpExec = __webpack_require__("7f72");
__webpack_require__("094c")({
  target: 'RegExp',
  proto: true,
  forced: regexpExec !== /./.exec
}, {
  exec: regexpExec
});


/***/ }),

/***/ "230e":
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__("d3f4");
var document = __webpack_require__("7726").document;
// typeof document.createElement is 'object' in old IE
var is = isObject(document) && isObject(document.createElement);
module.exports = function (it) {
  return is ? document.createElement(it) : {};
};


/***/ }),

/***/ "2348":
/***/ (function(module, exports, __webpack_require__) {

var _getIterator = __webpack_require__("5d73");

__webpack_require__("28ef");

__webpack_require__("9b2f");

var _Object$getOwnPropertyNames = __webpack_require__("03cc");

var _Object$preventExtensions = __webpack_require__("4dff");

var _Object$isExtensible = __webpack_require__("a522");

var _Promise = __webpack_require__("795b");

__webpack_require__("5bf0");

var _JSON$stringify = __webpack_require__("f499");

var _Object$assign = __webpack_require__("5176");

var _isIterable = __webpack_require__("c8bb");

var _Reflect$get = __webpack_require__("8feb");

var _Object$setPrototypeOf = __webpack_require__("4d16");

var _Symbol$iterator = __webpack_require__("5d58");

var _Array$from = __webpack_require__("774e");

var _Object$getPrototypeOf = __webpack_require__("061b");

__webpack_require__("89a3");

var _Object$getOwnPropertySymbols = __webpack_require__("e265");

var _Object$defineProperties = __webpack_require__("d847");

var _Object$getOwnPropertyDescriptor = __webpack_require__("268f");

var _Array$isArray = __webpack_require__("a745");

var _Object$keys = __webpack_require__("a4bb");

__webpack_require__("9541");

__webpack_require__("c259");

__webpack_require__("a1f5");

__webpack_require__("f682");

var _Object$create = __webpack_require__("4aa6");

var _typeof2 = __webpack_require__("8993");

var _Symbol$toStringTag = __webpack_require__("d5ca");

var _Symbol = __webpack_require__("67bb");

var _Object$defineProperty = __webpack_require__("85f2");

module.exports =
/******/
function (modules) {
  // webpackBootstrap

  /******/
  // The module cache

  /******/
  var installedModules = {};
  /******/

  /******/
  // The require function

  /******/

  function __webpack_require__(moduleId) {
    /******/

    /******/
    // Check if module is in cache

    /******/
    if (installedModules[moduleId]) {
      /******/
      return installedModules[moduleId].exports;
      /******/
    }
    /******/
    // Create a new module (and put it into the cache)

    /******/


    var module = installedModules[moduleId] = {
      /******/
      i: moduleId,

      /******/
      l: false,

      /******/
      exports: {}
      /******/

    };
    /******/

    /******/
    // Execute the module function

    /******/

    modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
    /******/

    /******/
    // Flag the module as loaded

    /******/

    module.l = true;
    /******/

    /******/
    // Return the exports of the module

    /******/

    return module.exports;
    /******/
  }
  /******/

  /******/

  /******/
  // expose the modules object (__webpack_modules__)

  /******/


  __webpack_require__.m = modules;
  /******/

  /******/
  // expose the module cache

  /******/

  __webpack_require__.c = installedModules;
  /******/

  /******/
  // define getter function for harmony exports

  /******/

  __webpack_require__.d = function (exports, name, getter) {
    /******/
    if (!__webpack_require__.o(exports, name)) {
      /******/
      _Object$defineProperty(exports, name, {
        enumerable: true,
        get: getter
      });
      /******/

    }
    /******/

  };
  /******/

  /******/
  // define __esModule on exports

  /******/


  __webpack_require__.r = function (exports) {
    /******/
    if (typeof _Symbol !== 'undefined' && _Symbol$toStringTag) {
      /******/
      _Object$defineProperty(exports, _Symbol$toStringTag, {
        value: 'Module'
      });
      /******/

    }
    /******/


    Object.defineProperty(exports, '__esModule', {
      value: true
    });
    /******/
  };
  /******/

  /******/
  // create a fake namespace object

  /******/
  // mode & 1: value is a module id, require it

  /******/
  // mode & 2: merge all properties of value into the ns

  /******/
  // mode & 4: return value when already ns object

  /******/
  // mode & 8|1: behave like require

  /******/


  __webpack_require__.t = function (value, mode) {
    /******/
    if (mode & 1) value = __webpack_require__(value);
    /******/

    if (mode & 8) return value;
    /******/

    if (mode & 4 && _typeof2(value) === 'object' && value && value.__esModule) return value;
    /******/

    var ns = _Object$create(null);
    /******/


    __webpack_require__.r(ns);
    /******/


    Object.defineProperty(ns, 'default', {
      enumerable: true,
      value: value
    });
    /******/

    if (mode & 2 && typeof value != 'string') for (var key in value) {
      __webpack_require__.d(ns, key, function (key) {
        return value[key];
      }.bind(null, key));
    }
    /******/

    return ns;
    /******/
  };
  /******/

  /******/
  // getDefaultExport function for compatibility with non-harmony modules

  /******/


  __webpack_require__.n = function (module) {
    /******/
    var getter = module && module.__esModule ?
    /******/
    function getDefault() {
      return module['default'];
    } :
    /******/
    function getModuleExports() {
      return module;
    };
    /******/

    __webpack_require__.d(getter, 'a', getter);
    /******/


    return getter;
    /******/
  };
  /******/

  /******/
  // Object.prototype.hasOwnProperty.call

  /******/


  __webpack_require__.o = function (object, property) {
    return Object.prototype.hasOwnProperty.call(object, property);
  };
  /******/

  /******/
  // __webpack_public_path__

  /******/


  __webpack_require__.p = "";
  /******/

  /******/

  /******/
  // Load entry module and return exports

  /******/

  return __webpack_require__(__webpack_require__.s = "fb15");
  /******/
}(
/************************************************************************/

/******/
{
  /***/
  "0029":
  /***/
  function _(module, exports) {
    // IE 8- don't enum bug keys
    module.exports = 'constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf'.split(',');
    /***/
  },

  /***/
  "0185":
  /***/
  function _(module, exports, __webpack_require__) {
    // 7.1.13 ToObject(argument)
    var defined = __webpack_require__("e5fa");

    module.exports = function (it) {
      return Object(defined(it));
    };
    /***/

  },

  /***/
  "01f9":
  /***/
  function f9(module, exports, __webpack_require__) {
    "use strict";

    var LIBRARY = __webpack_require__("2d00");

    var $export = __webpack_require__("5ca1");

    var redefine = __webpack_require__("2aba");

    var hide = __webpack_require__("32e9");

    var Iterators = __webpack_require__("84f2");

    var $iterCreate = __webpack_require__("41a0");

    var setToStringTag = __webpack_require__("7f20");

    var getPrototypeOf = __webpack_require__("38fd");

    var ITERATOR = __webpack_require__("2b4c")('iterator');

    var BUGGY = !([].keys && 'next' in [].keys()); // Safari has buggy iterators w/o `next`

    var FF_ITERATOR = '@@iterator';
    var KEYS = 'keys';
    var VALUES = 'values';

    var returnThis = function returnThis() {
      return this;
    };

    module.exports = function (Base, NAME, Constructor, next, DEFAULT, IS_SET, FORCED) {
      $iterCreate(Constructor, NAME, next);

      var getMethod = function getMethod(kind) {
        if (!BUGGY && kind in proto) return proto[kind];

        switch (kind) {
          case KEYS:
            return function keys() {
              return new Constructor(this, kind);
            };

          case VALUES:
            return function values() {
              return new Constructor(this, kind);
            };
        }

        return function entries() {
          return new Constructor(this, kind);
        };
      };

      var TAG = NAME + ' Iterator';
      var DEF_VALUES = DEFAULT == VALUES;
      var VALUES_BUG = false;
      var proto = Base.prototype;
      var $native = proto[ITERATOR] || proto[FF_ITERATOR] || DEFAULT && proto[DEFAULT];
      var $default = $native || getMethod(DEFAULT);
      var $entries = DEFAULT ? !DEF_VALUES ? $default : getMethod('entries') : undefined;
      var $anyNative = NAME == 'Array' ? proto.entries || $native : $native;
      var methods, key, IteratorPrototype; // Fix native

      if ($anyNative) {
        IteratorPrototype = getPrototypeOf($anyNative.call(new Base()));

        if (IteratorPrototype !== Object.prototype && IteratorPrototype.next) {
          // Set @@toStringTag to native iterators
          setToStringTag(IteratorPrototype, TAG, true); // fix for some old engines

          if (!LIBRARY && typeof IteratorPrototype[ITERATOR] != 'function') hide(IteratorPrototype, ITERATOR, returnThis);
        }
      } // fix Array#{values, @@iterator}.name in V8 / FF


      if (DEF_VALUES && $native && $native.name !== VALUES) {
        VALUES_BUG = true;

        $default = function values() {
          return $native.call(this);
        };
      } // Define iterator


      if ((!LIBRARY || FORCED) && (BUGGY || VALUES_BUG || !proto[ITERATOR])) {
        hide(proto, ITERATOR, $default);
      } // Plug for library


      Iterators[NAME] = $default;
      Iterators[TAG] = returnThis;

      if (DEFAULT) {
        methods = {
          values: DEF_VALUES ? $default : getMethod(VALUES),
          keys: IS_SET ? $default : getMethod(KEYS),
          entries: $entries
        };
        if (FORCED) for (key in methods) {
          if (!(key in proto)) redefine(proto, key, methods[key]);
        } else $export($export.P + $export.F * (BUGGY || VALUES_BUG), NAME, methods);
      }

      return methods;
    };
    /***/

  },

  /***/
  "03ca":
  /***/
  function ca(module, exports, __webpack_require__) {
    "use strict"; // 25.4.1.5 NewPromiseCapability(C)

    var aFunction = __webpack_require__("f2fe");

    function PromiseCapability(C) {
      var resolve, reject;
      this.promise = new C(function ($$resolve, $$reject) {
        if (resolve !== undefined || reject !== undefined) throw TypeError('Bad Promise constructor');
        resolve = $$resolve;
        reject = $$reject;
      });
      this.resolve = aFunction(resolve);
      this.reject = aFunction(reject);
    }

    module.exports.f = function (C) {
      return new PromiseCapability(C);
    };
    /***/

  },

  /***/
  "0d58":
  /***/
  function d58(module, exports, __webpack_require__) {
    // 19.1.2.14 / 15.2.3.14 Object.keys(O)
    var $keys = __webpack_require__("ce10");

    var enumBugKeys = __webpack_require__("e11e");

    module.exports = _Object$keys || function keys(O) {
      return $keys(O, enumBugKeys);
    };
    /***/

  },

  /***/
  "0f89":
  /***/
  function f89(module, exports, __webpack_require__) {
    var isObject = __webpack_require__("6f8a");

    module.exports = function (it) {
      if (!isObject(it)) throw TypeError(it + ' is not an object!');
      return it;
    };
    /***/

  },

  /***/
  "103a":
  /***/
  function a(module, exports, __webpack_require__) {
    var document = __webpack_require__("da3c").document;

    module.exports = document && document.documentElement;
    /***/
  },

  /***/
  "1169":
  /***/
  function _(module, exports, __webpack_require__) {
    // 7.2.2 IsArray(argument)
    var cof = __webpack_require__("2d95");

    module.exports = _Array$isArray || function isArray(arg) {
      return cof(arg) == 'Array';
    };
    /***/

  },

  /***/
  "11e9":
  /***/
  function e9(module, exports, __webpack_require__) {
    var pIE = __webpack_require__("52a7");

    var createDesc = __webpack_require__("4630");

    var toIObject = __webpack_require__("6821");

    var toPrimitive = __webpack_require__("6a99");

    var has = __webpack_require__("69a8");

    var IE8_DOM_DEFINE = __webpack_require__("c69a");

    var gOPD = _Object$getOwnPropertyDescriptor;
    exports.f = __webpack_require__("9e1e") ? gOPD : function getOwnPropertyDescriptor(O, P) {
      O = toIObject(O);
      P = toPrimitive(P, true);
      if (IE8_DOM_DEFINE) try {
        return gOPD(O, P);
      } catch (e) {
        /* empty */
      }
      if (has(O, P)) return createDesc(!pIE.f.call(O, P), O[P]);
    };
    /***/
  },

  /***/
  "12fd":
  /***/
  function fd(module, exports, __webpack_require__) {
    var isObject = __webpack_require__("6f8a");

    var document = __webpack_require__("da3c").document; // typeof document.createElement is 'object' in old IE


    var is = isObject(document) && isObject(document.createElement);

    module.exports = function (it) {
      return is ? document.createElement(it) : {};
    };
    /***/

  },

  /***/
  "12fd9":
  /***/
  function fd9(module, exports) {
    /***/
  },

  /***/
  "1495":
  /***/
  function _(module, exports, __webpack_require__) {
    var dP = __webpack_require__("86cc");

    var anObject = __webpack_require__("cb7c");

    var getKeys = __webpack_require__("0d58");

    module.exports = __webpack_require__("9e1e") ? _Object$defineProperties : function defineProperties(O, Properties) {
      anObject(O);
      var keys = getKeys(Properties);
      var length = keys.length;
      var i = 0;
      var P;

      while (length > i) {
        dP.f(O, P = keys[i++], Properties[P]);
      }

      return O;
    };
    /***/
  },

  /***/
  "196c":
  /***/
  function c(module, exports) {
    // fast apply, http://jsperf.lnkit.com/fast-apply/5
    module.exports = function (fn, args, that) {
      var un = that === undefined;

      switch (args.length) {
        case 0:
          return un ? fn() : fn.call(that);

        case 1:
          return un ? fn(args[0]) : fn.call(that, args[0]);

        case 2:
          return un ? fn(args[0], args[1]) : fn.call(that, args[0], args[1]);

        case 3:
          return un ? fn(args[0], args[1], args[2]) : fn.call(that, args[0], args[1], args[2]);

        case 4:
          return un ? fn(args[0], args[1], args[2], args[3]) : fn.call(that, args[0], args[1], args[2], args[3]);
      }

      return fn.apply(that, args);
    };
    /***/

  },

  /***/
  "1b55":
  /***/
  function b55(module, exports, __webpack_require__) {
    var store = __webpack_require__("7772")('wks');

    var uid = __webpack_require__("7b00");

    var _Symbol2 = __webpack_require__("da3c").Symbol;

    var USE_SYMBOL = typeof _Symbol2 == 'function';

    var $exports = module.exports = function (name) {
      return store[name] || (store[name] = USE_SYMBOL && _Symbol2[name] || (USE_SYMBOL ? _Symbol2 : uid)('Symbol.' + name));
    };

    $exports.store = store;
    /***/
  },

  /***/
  "1b8f":
  /***/
  function b8f(module, exports, __webpack_require__) {
    var toInteger = __webpack_require__("a812");

    var max = Math.max;
    var min = Math.min;

    module.exports = function (index, length) {
      index = toInteger(index);
      return index < 0 ? max(index + length, 0) : min(index, length);
    };
    /***/

  },

  /***/
  "1be4":
  /***/
  function be4(module, exports, __webpack_require__) {
    "use strict";

    var global = __webpack_require__("da3c");

    var core = __webpack_require__("a7d3");

    var dP = __webpack_require__("3adc");

    var DESCRIPTORS = __webpack_require__("7d95");

    var SPECIES = __webpack_require__("1b55")('species');

    module.exports = function (KEY) {
      var C = typeof core[KEY] == 'function' ? core[KEY] : global[KEY];
      if (DESCRIPTORS && C && !C[SPECIES]) dP.f(C, SPECIES, {
        configurable: true,
        get: function get() {
          return this;
        }
      });
    };
    /***/

  },

  /***/
  "230e":
  /***/
  function e(module, exports, __webpack_require__) {
    var isObject = __webpack_require__("d3f4");

    var document = __webpack_require__("7726").document; // typeof document.createElement is 'object' in old IE


    var is = isObject(document) && isObject(document.createElement);

    module.exports = function (it) {
      return is ? document.createElement(it) : {};
    };
    /***/

  },

  /***/
  "2312":
  /***/
  function _(module, exports, __webpack_require__) {
    module.exports = __webpack_require__("8ce0");
    /***/
  },

  /***/
  "2418":
  /***/
  function _(module, exports, __webpack_require__) {
    // false -> Array#indexOf
    // true  -> Array#includes
    var toIObject = __webpack_require__("6a9b");

    var toLength = __webpack_require__("a5ab");

    var toAbsoluteIndex = __webpack_require__("1b8f");

    module.exports = function (IS_INCLUDES) {
      return function ($this, el, fromIndex) {
        var O = toIObject($this);
        var length = toLength(O.length);
        var index = toAbsoluteIndex(fromIndex, length);
        var value; // Array#includes uses SameValueZero equality algorithm
        // eslint-disable-next-line no-self-compare

        if (IS_INCLUDES && el != el) while (length > index) {
          value = O[index++]; // eslint-disable-next-line no-self-compare

          if (value != value) return true; // Array#indexOf ignores holes, Array#includes - not
        } else for (; length > index; index++) {
          if (IS_INCLUDES || index in O) {
            if (O[index] === el) return IS_INCLUDES || index || 0;
          }
        }
        return !IS_INCLUDES && -1;
      };
    };
    /***/

  },

  /***/
  "245b":
  /***/
  function b(module, exports) {
    module.exports = function (done, value) {
      return {
        value: value,
        done: !!done
      };
    };
    /***/

  },

  /***/
  "2621":
  /***/
  function _(module, exports) {
    exports.f = _Object$getOwnPropertySymbols;
    /***/
  },

  /***/
  "2695":
  /***/
  function _(module, exports, __webpack_require__) {
    var has = __webpack_require__("43c8");

    var toIObject = __webpack_require__("6a9b");

    var arrayIndexOf = __webpack_require__("2418")(false);

    var IE_PROTO = __webpack_require__("5d8f")('IE_PROTO');

    module.exports = function (object, names) {
      var O = toIObject(object);
      var i = 0;
      var result = [];
      var key;

      for (key in O) {
        if (key != IE_PROTO) has(O, key) && result.push(key);
      } // Don't enum bug & hidden keys


      while (names.length > i) {
        if (has(O, key = names[i++])) {
          ~arrayIndexOf(result, key) || result.push(key);
        }
      }

      return result;
    };
    /***/

  },

  /***/
  "2a4e":
  /***/
  function a4e(module, exports, __webpack_require__) {
    var toInteger = __webpack_require__("a812");

    var defined = __webpack_require__("e5fa"); // true  -> String#at
    // false -> String#codePointAt


    module.exports = function (TO_STRING) {
      return function (that, pos) {
        var s = String(defined(that));
        var i = toInteger(pos);
        var l = s.length;
        var a, b;
        if (i < 0 || i >= l) return TO_STRING ? '' : undefined;
        a = s.charCodeAt(i);
        return a < 0xd800 || a > 0xdbff || i + 1 === l || (b = s.charCodeAt(i + 1)) < 0xdc00 || b > 0xdfff ? TO_STRING ? s.charAt(i) : a : TO_STRING ? s.slice(i, i + 2) : (a - 0xd800 << 10) + (b - 0xdc00) + 0x10000;
      };
    };
    /***/

  },

  /***/
  "2aba":
  /***/
  function aba(module, exports, __webpack_require__) {
    var global = __webpack_require__("7726");

    var hide = __webpack_require__("32e9");

    var has = __webpack_require__("69a8");

    var SRC = __webpack_require__("ca5a")('src');

    var TO_STRING = 'toString';
    var $toString = Function[TO_STRING];
    var TPL = ('' + $toString).split(TO_STRING);

    __webpack_require__("8378").inspectSource = function (it) {
      return $toString.call(it);
    };

    (module.exports = function (O, key, val, safe) {
      var isFunction = typeof val == 'function';
      if (isFunction) has(val, 'name') || hide(val, 'name', key);
      if (O[key] === val) return;
      if (isFunction) has(val, SRC) || hide(val, SRC, O[key] ? '' + O[key] : TPL.join(String(key)));

      if (O === global) {
        O[key] = val;
      } else if (!safe) {
        delete O[key];
        hide(O, key, val);
      } else if (O[key]) {
        O[key] = val;
      } else {
        hide(O, key, val);
      } // add fake Function#toString for correct work wrapped methods / constructors with methods like LoDash isNative

    })(Function.prototype, TO_STRING, function toString() {
      return typeof this == 'function' && this[SRC] || $toString.call(this);
    });
    /***/
  },

  /***/
  "2aeb":
  /***/
  function aeb(module, exports, __webpack_require__) {
    // 19.1.2.2 / 15.2.3.5 Object.create(O [, Properties])
    var anObject = __webpack_require__("cb7c");

    var dPs = __webpack_require__("1495");

    var enumBugKeys = __webpack_require__("e11e");

    var IE_PROTO = __webpack_require__("613b")('IE_PROTO');

    var Empty = function Empty() {
      /* empty */
    };

    var PROTOTYPE = 'prototype'; // Create object with fake `null` prototype: use iframe Object with cleared prototype

    var _createDict = function createDict() {
      // Thrash, waste and sodomy: IE GC bug
      var iframe = __webpack_require__("230e")('iframe');

      var i = enumBugKeys.length;
      var lt = '<';
      var gt = '>';
      var iframeDocument;
      iframe.style.display = 'none';

      __webpack_require__("fab2").appendChild(iframe);

      iframe.src = 'javascript:'; // eslint-disable-line no-script-url
      // createDict = iframe.contentWindow.Object;
      // html.removeChild(iframe);

      iframeDocument = iframe.contentWindow.document;
      iframeDocument.open();
      iframeDocument.write(lt + 'script' + gt + 'document.F=Object' + lt + '/script' + gt);
      iframeDocument.close();
      _createDict = iframeDocument.F;

      while (i--) {
        delete _createDict[PROTOTYPE][enumBugKeys[i]];
      }

      return _createDict();
    };

    module.exports = _Object$create || function create(O, Properties) {
      var result;

      if (O !== null) {
        Empty[PROTOTYPE] = anObject(O);
        result = new Empty();
        Empty[PROTOTYPE] = null; // add "__proto__" for Object.getPrototypeOf polyfill

        result[IE_PROTO] = O;
      } else result = _createDict();

      return Properties === undefined ? result : dPs(result, Properties);
    };
    /***/

  },

  /***/
  "2b4c":
  /***/
  function b4c(module, exports, __webpack_require__) {
    var store = __webpack_require__("5537")('wks');

    var uid = __webpack_require__("ca5a");

    var _Symbol3 = __webpack_require__("7726").Symbol;

    var USE_SYMBOL = typeof _Symbol3 == 'function';

    var $exports = module.exports = function (name) {
      return store[name] || (store[name] = USE_SYMBOL && _Symbol3[name] || (USE_SYMBOL ? _Symbol3 : uid)('Symbol.' + name));
    };

    $exports.store = store;
    /***/
  },

  /***/
  "2b5f":
  /***/
  function b5f(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    /* harmony import */

    var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_QlikField_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("8ffe");
    /* harmony import */


    var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_QlikField_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default =
    /*#__PURE__*/
    __webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_QlikField_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
    /* unused harmony reexport * */

    /* unused harmony default export */


    var _unused_webpack_default_export = _node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_QlikField_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a;
    /***/
  },

  /***/
  "2d00":
  /***/
  function d00(module, exports) {
    module.exports = false;
    /***/
  },

  /***/
  "2d95":
  /***/
  function d95(module, exports) {
    var toString = {}.toString;

    module.exports = function (it) {
      return toString.call(it).slice(8, -1);
    };
    /***/

  },

  /***/
  "2ea1":
  /***/
  function ea1(module, exports, __webpack_require__) {
    // 7.1.1 ToPrimitive(input [, PreferredType])
    var isObject = __webpack_require__("6f8a"); // instead of the ES6 spec version, we didn't implement @@toPrimitive case
    // and the second argument - flag - preferred type is a string


    module.exports = function (it, S) {
      if (!isObject(it)) return it;
      var fn, val;
      if (S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
      if (typeof (fn = it.valueOf) == 'function' && !isObject(val = fn.call(it))) return val;
      if (!S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
      throw TypeError("Can't convert object to primitive value");
    };
    /***/

  },

  /***/
  "2ef0":
  /***/
  function ef0(module, exports, __webpack_require__) {// extracted by mini-css-extract-plugin

    /***/
  },

  /***/
  "302f":
  /***/
  function f(module, exports, __webpack_require__) {
    // 7.3.20 SpeciesConstructor(O, defaultConstructor)
    var anObject = __webpack_require__("0f89");

    var aFunction = __webpack_require__("f2fe");

    var SPECIES = __webpack_require__("1b55")('species');

    module.exports = function (O, D) {
      var C = anObject(O).constructor;
      var S;
      return C === undefined || (S = anObject(C)[SPECIES]) == undefined ? D : aFunction(S);
    };
    /***/

  },

  /***/
  "32e9":
  /***/
  function e9(module, exports, __webpack_require__) {
    var dP = __webpack_require__("86cc");

    var createDesc = __webpack_require__("4630");

    module.exports = __webpack_require__("9e1e") ? function (object, key, value) {
      return dP.f(object, key, createDesc(1, value));
    } : function (object, key, value) {
      object[key] = value;
      return object;
    };
    /***/
  },

  /***/
  "36dc":
  /***/
  function dc(module, exports, __webpack_require__) {
    var global = __webpack_require__("da3c");

    var macrotask = __webpack_require__("df0a").set;

    var Observer = global.MutationObserver || global.WebKitMutationObserver;
    var process = global.process;
    var Promise = global.Promise;
    var isNode = __webpack_require__("6e1f")(process) == 'process';

    module.exports = function () {
      var head, last, notify;

      var flush = function flush() {
        var parent, fn;
        if (isNode && (parent = process.domain)) parent.exit();

        while (head) {
          fn = head.fn;
          head = head.next;

          try {
            fn();
          } catch (e) {
            if (head) notify();else last = undefined;
            throw e;
          }
        }

        last = undefined;
        if (parent) parent.enter();
      }; // Node.js


      if (isNode) {
        notify = function notify() {
          process.nextTick(flush);
        }; // browsers with MutationObserver, except iOS Safari - https://github.com/zloirock/core-js/issues/339

      } else if (Observer && !(global.navigator && global.navigator.standalone)) {
        var toggle = true;
        var node = document.createTextNode('');
        new Observer(flush).observe(node, {
          characterData: true
        }); // eslint-disable-line no-new

        notify = function notify() {
          node.data = toggle = !toggle;
        }; // environments with maybe non-completely correct, but existent Promise

      } else if (Promise && Promise.resolve) {
        // Promise.resolve without an argument throws an error in LG WebOS 2
        var promise = Promise.resolve(undefined);

        notify = function notify() {
          promise.then(flush);
        }; // for other environments - macrotask based on:
        // - setImmediate
        // - MessageChannel
        // - window.postMessag
        // - onreadystatechange
        // - setTimeout

      } else {
        notify = function notify() {
          // strange IE + webpack dev server bug - use .call(global)
          macrotask.call(global, flush);
        };
      }

      return function (fn) {
        var task = {
          fn: fn,
          next: undefined
        };
        if (last) last.next = task;

        if (!head) {
          head = task;
          notify();
        }

        last = task;
      };
    };
    /***/

  },

  /***/
  "37c8":
  /***/
  function c8(module, exports, __webpack_require__) {
    exports.f = __webpack_require__("2b4c");
    /***/
  },

  /***/
  "38fc":
  /***/
  function fc(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    /* harmony import */

    var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_FieldValue_vue_vue_type_style_index_0_id_bfa4efde_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("2ef0");
    /* harmony import */


    var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_FieldValue_vue_vue_type_style_index_0_id_bfa4efde_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default =
    /*#__PURE__*/
    __webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_FieldValue_vue_vue_type_style_index_0_id_bfa4efde_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
    /* unused harmony reexport * */

    /* unused harmony default export */


    var _unused_webpack_default_export = _node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_FieldValue_vue_vue_type_style_index_0_id_bfa4efde_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a;
    /***/
  },

  /***/
  "38fd":
  /***/
  function fd(module, exports, __webpack_require__) {
    // 19.1.2.9 / 15.2.3.2 Object.getPrototypeOf(O)
    var has = __webpack_require__("69a8");

    var toObject = __webpack_require__("4bf8");

    var IE_PROTO = __webpack_require__("613b")('IE_PROTO');

    var ObjectProto = Object.prototype;

    module.exports = _Object$getPrototypeOf || function (O) {
      O = toObject(O);
      if (has(O, IE_PROTO)) return O[IE_PROTO];

      if (typeof O.constructor == 'function' && O instanceof O.constructor) {
        return O.constructor.prototype;
      }

      return O instanceof Object ? ObjectProto : null;
    };
    /***/

  },

  /***/
  "3904":
  /***/
  function _(module, exports, __webpack_require__) {
    var hide = __webpack_require__("8ce0");

    module.exports = function (target, src, safe) {
      for (var key in src) {
        if (safe && target[key]) target[key] = src[key];else hide(target, key, src[key]);
      }

      return target;
    };
    /***/

  },

  /***/
  "3a72":
  /***/
  function a72(module, exports, __webpack_require__) {
    var global = __webpack_require__("7726");

    var core = __webpack_require__("8378");

    var LIBRARY = __webpack_require__("2d00");

    var wksExt = __webpack_require__("37c8");

    var defineProperty = __webpack_require__("86cc").f;

    module.exports = function (name) {
      var $Symbol = core.Symbol || (core.Symbol = LIBRARY ? {} : global.Symbol || {});
      if (name.charAt(0) != '_' && !(name in $Symbol)) defineProperty($Symbol, name, {
        value: wksExt.f(name)
      });
    };
    /***/

  },

  /***/
  "3adc":
  /***/
  function adc(module, exports, __webpack_require__) {
    var anObject = __webpack_require__("0f89");

    var IE8_DOM_DEFINE = __webpack_require__("a47f");

    var toPrimitive = __webpack_require__("2ea1");

    var dP = _Object$defineProperty;
    exports.f = __webpack_require__("7d95") ? _Object$defineProperty : function defineProperty(O, P, Attributes) {
      anObject(O);
      P = toPrimitive(P, true);
      anObject(Attributes);
      if (IE8_DOM_DEFINE) try {
        return dP(O, P, Attributes);
      } catch (e) {
        /* empty */
      }
      if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported!');
      if ('value' in Attributes) O[P] = Attributes.value;
      return O;
    };
    /***/
  },

  /***/
  "41a0":
  /***/
  function a0(module, exports, __webpack_require__) {
    "use strict";

    var create = __webpack_require__("2aeb");

    var descriptor = __webpack_require__("4630");

    var setToStringTag = __webpack_require__("7f20");

    var IteratorPrototype = {}; // 25.1.2.1.1 %IteratorPrototype%[@@iterator]()

    __webpack_require__("32e9")(IteratorPrototype, __webpack_require__("2b4c")('iterator'), function () {
      return this;
    });

    module.exports = function (Constructor, NAME, next) {
      Constructor.prototype = create(IteratorPrototype, {
        next: descriptor(1, next)
      });
      setToStringTag(Constructor, NAME + ' Iterator');
    };
    /***/

  },

  /***/
  "436c":
  /***/
  function c(module, exports, __webpack_require__) {
    var ITERATOR = __webpack_require__("1b55")('iterator');

    var SAFE_CLOSING = false;

    try {
      var riter = [7][ITERATOR]();

      riter['return'] = function () {
        SAFE_CLOSING = true;
      }; // eslint-disable-next-line no-throw-literal


      _Array$from(riter, function () {
        throw 2;
      });
    } catch (e) {
      /* empty */
    }

    module.exports = function (exec, skipClosing) {
      if (!skipClosing && !SAFE_CLOSING) return false;
      var safe = false;

      try {
        var arr = [7];
        var iter = arr[ITERATOR]();

        iter.next = function () {
          return {
            done: safe = true
          };
        };

        arr[ITERATOR] = function () {
          return iter;
        };

        exec(arr);
      } catch (e) {
        /* empty */
      }

      return safe;
    };
    /***/

  },

  /***/
  "43c8":
  /***/
  function c8(module, exports) {
    var hasOwnProperty = {}.hasOwnProperty;

    module.exports = function (it, key) {
      return hasOwnProperty.call(it, key);
    };
    /***/

  },

  /***/
  "4588":
  /***/
  function _(module, exports) {
    // 7.1.4 ToInteger
    var ceil = Math.ceil;
    var floor = Math.floor;

    module.exports = function (it) {
      return isNaN(it = +it) ? 0 : (it > 0 ? floor : ceil)(it);
    };
    /***/

  },

  /***/
  "4630":
  /***/
  function _(module, exports) {
    module.exports = function (bitmap, value) {
      return {
        enumerable: !(bitmap & 1),
        configurable: !(bitmap & 2),
        writable: !(bitmap & 4),
        value: value
      };
    };
    /***/

  },

  /***/
  "4bf8":
  /***/
  function bf8(module, exports, __webpack_require__) {
    // 7.1.13 ToObject(argument)
    var defined = __webpack_require__("be13");

    module.exports = function (it) {
      return Object(defined(it));
    };
    /***/

  },

  /***/
  "52a7":
  /***/
  function a7(module, exports) {
    exports.f = {}.propertyIsEnumerable;
    /***/
  },

  /***/
  "5537":
  /***/
  function _(module, exports, __webpack_require__) {
    var core = __webpack_require__("8378");

    var global = __webpack_require__("7726");

    var SHARED = '__core-js_shared__';
    var store = global[SHARED] || (global[SHARED] = {});
    (module.exports = function (key, value) {
      return store[key] || (store[key] = value !== undefined ? value : {});
    })('versions', []).push({
      version: core.version,
      mode: __webpack_require__("2d00") ? 'pure' : 'global',
      copyright: '© 2018 Denis Pushkarev (zloirock.ru)'
    });
    /***/
  },

  /***/
  "560b":
  /***/
  function b(module, exports, __webpack_require__) {
    var ctx = __webpack_require__("bc25");

    var call = __webpack_require__("9c93");

    var isArrayIter = __webpack_require__("c227");

    var anObject = __webpack_require__("0f89");

    var toLength = __webpack_require__("a5ab");

    var getIterFn = __webpack_require__("f159");

    var BREAK = {};
    var RETURN = {};

    var exports = module.exports = function (iterable, entries, fn, that, ITERATOR) {
      var iterFn = ITERATOR ? function () {
        return iterable;
      } : getIterFn(iterable);
      var f = ctx(fn, that, entries ? 2 : 1);
      var index = 0;
      var length, step, iterator, result;
      if (typeof iterFn != 'function') throw TypeError(iterable + ' is not iterable!'); // fast case for arrays with default iterator

      if (isArrayIter(iterFn)) for (length = toLength(iterable.length); length > index; index++) {
        result = entries ? f(anObject(step = iterable[index])[0], step[1]) : f(iterable[index]);
        if (result === BREAK || result === RETURN) return result;
      } else for (iterator = iterFn.call(iterable); !(step = iterator.next()).done;) {
        result = call(iterator, f, step.value, entries);
        if (result === BREAK || result === RETURN) return result;
      }
    };

    exports.BREAK = BREAK;
    exports.RETURN = RETURN;
    /***/
  },

  /***/
  "5987":
  /***/
  function _(module, exports, __webpack_require__) {
    /**
     * enigma.js v2.4.0
     * Copyright (c) 2018 QlikTech International AB
     * This library is licensed under MIT - See the LICENSE file for full details
     */
    (function (global, factory) {
      true ? module.exports = factory() : undefined;
    })(this, function () {
      'use strict';

      function _typeof(obj) {
        if (typeof _Symbol === "function" && _typeof2(_Symbol$iterator) === "symbol") {
          _typeof = function _typeof(obj) {
            return _typeof2(obj);
          };
        } else {
          _typeof = function _typeof(obj) {
            return obj && typeof _Symbol === "function" && obj.constructor === _Symbol && obj !== _Symbol.prototype ? "symbol" : _typeof2(obj);
          };
        }

        return _typeof(obj);
      }

      function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
          throw new TypeError("Cannot call a class as a function");
        }
      }

      function _defineProperties(target, props) {
        for (var i = 0; i < props.length; i++) {
          var descriptor = props[i];
          descriptor.enumerable = descriptor.enumerable || false;
          descriptor.configurable = true;
          if ("value" in descriptor) descriptor.writable = true;

          _Object$defineProperty(target, descriptor.key, descriptor);
        }
      }

      function _createClass(Constructor, protoProps, staticProps) {
        if (protoProps) _defineProperties(Constructor.prototype, protoProps);
        if (staticProps) _defineProperties(Constructor, staticProps);
        return Constructor;
      }

      function _inherits(subClass, superClass) {
        if (typeof superClass !== "function" && superClass !== null) {
          throw new TypeError("Super expression must either be null or a function");
        }

        subClass.prototype = _Object$create(superClass && superClass.prototype, {
          constructor: {
            value: subClass,
            writable: true,
            configurable: true
          }
        });
        if (superClass) _setPrototypeOf(subClass, superClass);
      }

      function _getPrototypeOf(o) {
        _getPrototypeOf = _Object$setPrototypeOf ? _Object$getPrototypeOf : function _getPrototypeOf(o) {
          return o.__proto__ || _Object$getPrototypeOf(o);
        };
        return _getPrototypeOf(o);
      }

      function _setPrototypeOf(o, p) {
        _setPrototypeOf = _Object$setPrototypeOf || function _setPrototypeOf(o, p) {
          o.__proto__ = p;
          return o;
        };

        return _setPrototypeOf(o, p);
      }

      function _assertThisInitialized(self) {
        if (self === void 0) {
          throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
        }

        return self;
      }

      function _possibleConstructorReturn(self, call) {
        if (call && (_typeof2(call) === "object" || typeof call === "function")) {
          return call;
        }

        return _assertThisInitialized(self);
      }

      function _superPropBase(object, property) {
        while (!Object.prototype.hasOwnProperty.call(object, property)) {
          object = _getPrototypeOf(object);
          if (object === null) break;
        }

        return object;
      }

      function _get(target, property, receiver) {
        if (typeof Reflect !== "undefined" && _Reflect$get) {
          _get = _Reflect$get;
        } else {
          _get = function _get(target, property, receiver) {
            var base = _superPropBase(target, property);

            if (!base) return;

            var desc = _Object$getOwnPropertyDescriptor(base, property);

            if (desc.get) {
              return desc.get.call(receiver);
            }

            return desc.value;
          };
        }

        return _get(target, property, receiver || target);
      }

      function _toConsumableArray(arr) {
        return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread();
      }

      function _arrayWithoutHoles(arr) {
        if (_Array$isArray(arr)) {
          for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) {
            arr2[i] = arr[i];
          }

          return arr2;
        }
      }

      function _iterableToArray(iter) {
        if (_isIterable(Object(iter)) || Object.prototype.toString.call(iter) === "[object Arguments]") return _Array$from(iter);
      }

      function _nonIterableSpread() {
        throw new TypeError("Invalid attempt to spread non-iterable instance");
      }
      /**
       * Utility functions
       */


      var util = {};

      util.isObject = function isObject(arg) {
        return _typeof2(arg) === 'object' && arg !== null;
      };

      util.isNumber = function isNumber(arg) {
        return typeof arg === 'number';
      };

      util.isUndefined = function isUndefined(arg) {
        return arg === void 0;
      };

      util.isFunction = function isFunction(arg) {
        return typeof arg === 'function';
      };
      /**
       * EventEmitter class
       */


      function EventEmitter() {
        EventEmitter.init.call(this);
      }

      var nodeEventEmitter = EventEmitter; // Backwards-compat with node 0.10.x

      EventEmitter.EventEmitter = EventEmitter;
      EventEmitter.prototype._events = undefined;
      EventEmitter.prototype._maxListeners = undefined; // By default EventEmitters will print a warning if more than 10 listeners are
      // added to it. This is a useful default which helps finding memory leaks.

      EventEmitter.defaultMaxListeners = 10;

      EventEmitter.init = function () {
        this._events = this._events || {};
        this._maxListeners = this._maxListeners || undefined;
      }; // Obviously not all Emitters should be limited to 10. This function allows
      // that to be increased. Set to zero for unlimited.


      EventEmitter.prototype.setMaxListeners = function (n) {
        if (!util.isNumber(n) || n < 0 || isNaN(n)) throw TypeError('n must be a positive number');
        this._maxListeners = n;
        return this;
      };

      EventEmitter.prototype.emit = function (type) {
        var er, handler, len, args, i, listeners;
        if (!this._events) this._events = {}; // If there is no 'error' event listener then throw.

        if (type === 'error' && !this._events.error) {
          er = arguments[1];

          if (er instanceof Error) {
            throw er; // Unhandled 'error' event
          } else {
            throw Error('Uncaught, unspecified "error" event.');
          }

          return false;
        }

        handler = this._events[type];
        if (util.isUndefined(handler)) return false;

        if (util.isFunction(handler)) {
          switch (arguments.length) {
            // fast cases
            case 1:
              handler.call(this);
              break;

            case 2:
              handler.call(this, arguments[1]);
              break;

            case 3:
              handler.call(this, arguments[1], arguments[2]);
              break;
            // slower

            default:
              len = arguments.length;
              args = new Array(len - 1);

              for (i = 1; i < len; i++) {
                args[i - 1] = arguments[i];
              }

              handler.apply(this, args);
          }
        } else if (util.isObject(handler)) {
          len = arguments.length;
          args = new Array(len - 1);

          for (i = 1; i < len; i++) {
            args[i - 1] = arguments[i];
          }

          listeners = handler.slice();
          len = listeners.length;

          for (i = 0; i < len; i++) {
            listeners[i].apply(this, args);
          }
        }

        return true;
      };

      EventEmitter.prototype.addListener = function (type, listener) {
        var m;
        if (!util.isFunction(listener)) throw TypeError('listener must be a function');
        if (!this._events) this._events = {}; // To avoid recursion in the case that type === "newListener"! Before
        // adding it to the listeners, first emit "newListener".

        if (this._events.newListener) this.emit('newListener', type, util.isFunction(listener.listener) ? listener.listener : listener);
        if (!this._events[type]) // Optimize the case of one listener. Don't need the extra array object.
          this._events[type] = listener;else if (util.isObject(this._events[type])) // If we've already got an array, just append.
          this._events[type].push(listener);else // Adding the second element, need to change to array.
          this._events[type] = [this._events[type], listener]; // Check for listener leak

        if (util.isObject(this._events[type]) && !this._events[type].warned) {
          var m;

          if (!util.isUndefined(this._maxListeners)) {
            m = this._maxListeners;
          } else {
            m = EventEmitter.defaultMaxListeners;
          }

          if (m && m > 0 && this._events[type].length > m) {
            this._events[type].warned = true;

            if (util.isFunction(console.error)) {
              console.error('(node) warning: possible EventEmitter memory ' + 'leak detected. %d listeners added. ' + 'Use emitter.setMaxListeners() to increase limit.', this._events[type].length);
            }

            if (util.isFunction(console.trace)) console.trace();
          }
        }

        return this;
      };

      EventEmitter.prototype.on = EventEmitter.prototype.addListener;

      EventEmitter.prototype.once = function (type, listener) {
        if (!util.isFunction(listener)) throw TypeError('listener must be a function');
        var fired = false;

        function g() {
          this.removeListener(type, g);

          if (!fired) {
            fired = true;
            listener.apply(this, arguments);
          }
        }

        g.listener = listener;
        this.on(type, g);
        return this;
      }; // emits a 'removeListener' event iff the listener was removed


      EventEmitter.prototype.removeListener = function (type, listener) {
        var list, position, length, i;
        if (!util.isFunction(listener)) throw TypeError('listener must be a function');
        if (!this._events || !this._events[type]) return this;
        list = this._events[type];
        length = list.length;
        position = -1;

        if (list === listener || util.isFunction(list.listener) && list.listener === listener) {
          delete this._events[type];
          if (this._events.removeListener) this.emit('removeListener', type, listener);
        } else if (util.isObject(list)) {
          for (i = length; i-- > 0;) {
            if (list[i] === listener || list[i].listener && list[i].listener === listener) {
              position = i;
              break;
            }
          }

          if (position < 0) return this;

          if (list.length === 1) {
            list.length = 0;
            delete this._events[type];
          } else {
            list.splice(position, 1);
          }

          if (this._events.removeListener) this.emit('removeListener', type, listener);
        }

        return this;
      };

      EventEmitter.prototype.removeAllListeners = function (type) {
        var key, listeners;
        if (!this._events) return this; // not listening for removeListener, no need to emit

        if (!this._events.removeListener) {
          if (arguments.length === 0) this._events = {};else if (this._events[type]) delete this._events[type];
          return this;
        } // emit removeListener for all listeners on all events


        if (arguments.length === 0) {
          for (key in this._events) {
            if (key === 'removeListener') continue;
            this.removeAllListeners(key);
          }

          this.removeAllListeners('removeListener');
          this._events = {};
          return this;
        }

        listeners = this._events[type];

        if (util.isFunction(listeners)) {
          this.removeListener(type, listeners);
        } else if (_Array$isArray(listeners)) {
          // LIFO order
          while (listeners.length) {
            this.removeListener(type, listeners[listeners.length - 1]);
          }
        }

        delete this._events[type];
        return this;
      };

      EventEmitter.prototype.listeners = function (type) {
        var ret;
        if (!this._events || !this._events[type]) ret = [];else if (util.isFunction(this._events[type])) ret = [this._events[type]];else ret = this._events[type].slice();
        return ret;
      };

      EventEmitter.listenerCount = function (emitter, type) {
        var ret;
        if (!emitter._events || !emitter._events[type]) ret = 0;else if (util.isFunction(emitter._events[type])) ret = 1;else ret = emitter._events[type].length;
        return ret;
      };
      /**
      * @module EventEmitter
      * @private
      */


      var Events = {
        /**
        * Function used to add event handling to objects passed in.
        * @param {Object} obj Object instance that will get event handling.
        */
        mixin: function mixin(obj) {
          _Object$keys(nodeEventEmitter.prototype).forEach(function (key) {
            obj[key] = nodeEventEmitter.prototype[key];
          });

          nodeEventEmitter.init(obj);
        }
      };
      var RPC_CLOSE_NORMAL = 1000;
      var RPC_CLOSE_MANUAL_SUSPEND = 4000;
      var cacheId = 0;
      /**
       * The QIX Engine session object
       */

      var Session =
      /*#__PURE__*/
      function () {
        /**
         * Handle opened state. This event is triggered whenever the websocket is connected and ready for
         * communication.
         * @event Session#opened
         * @type {Object}
         */

        /**
         * Handle closed state. This event is triggered when the underlying websocket is closed and
         * config.suspendOnClose is false.
         * @event Session#closed
         * @type {Object}
         */

        /**
         * Handle suspended state. This event is triggered in two cases (listed below). It is useful
         * in scenarios where you for example want to block interaction in your application until you
         * are resumed again. If config.suspendOnClose is true and there was a network disconnect
         * (socked closed) or if you ran session.suspend().
         * @event Session#suspended
         * @type {Object}
         * @param {Object} evt Event object.
         * @param {String} evt.initiator String indication what triggered the suspended state. Possible
         * values network, manual.
         */

        /**
         * Handle resumed state. This event is triggered when the session was properly resumed. It is
         * useful in scenarios where you for example can close blocking modal dialogs and allow the user
         * to interact with your application again.
         * @event Session#resumed
         * @type {Object}
         */

        /**
         * Handle all JSON-RPC notification event, 'notification:*. Or handle a specific JSON-RPC
         * notification event, 'notification:OnConnected'. These events depend on the product you use QIX
         * Engine from.
         * @event Session#notification
         * @type {Object}
         */

        /**
        * Handle websocket messages. Generally used in debugging purposes. traffic:* will handle all
        * websocket messages, traffic:sent will handle outgoing messages and traffic:received will handle
        * incoming messages.
        * @event Session#traffic
        * @type {Object}
        */
        function Session(options) {
          _classCallCheck(this, Session);

          var session = this;

          _Object$assign(session, options);

          this.Promise = this.config.Promise;
          this.definition = this.config.definition;
          Events.mixin(session);
          cacheId += 1;
          session.id = cacheId;
          session.rpc.on('socket-error', session.onRpcError.bind(session));
          session.rpc.on('closed', session.onRpcClosed.bind(session));
          session.rpc.on('message', session.onRpcMessage.bind(session));
          session.rpc.on('notification', session.onRpcNotification.bind(session));
          session.rpc.on('traffic', session.onRpcTraffic.bind(session));
          session.on('closed', function () {
            return session.onSessionClosed();
          });
        }
        /**
        * Event handler for re-triggering error events from RPC.
        * @private
        * @emits socket-error
        * @param {Error} err Webocket error event.
        */


        _createClass(Session, [{
          key: "onRpcError",
          value: function onRpcError(err) {
            if (this.suspendResume.isSuspended) {
              return;
            }

            this.emit('socket-error', err);
          }
          /**
          * Event handler for the RPC close event.
          * @private
          * @emits Session#suspended
          * @emits Session#closed
          * @param {Event} evt WebSocket close event.
          */

        }, {
          key: "onRpcClosed",
          value: function onRpcClosed(evt) {
            var _this = this;

            if (this.suspendResume.isSuspended) {
              return;
            }

            if (evt.code === RPC_CLOSE_NORMAL || evt.code === RPC_CLOSE_MANUAL_SUSPEND) {
              return;
            }

            if (this.config.suspendOnClose) {
              this.suspendResume.suspend().then(function () {
                return _this.emit('suspended', {
                  initiator: 'network'
                });
              });
            } else {
              this.emit('closed', evt);
            }
          }
          /**
          * Event handler for the RPC message event.
          * @private
          * @param {Object} response JSONRPC response.
          */

        }, {
          key: "onRpcMessage",
          value: function onRpcMessage(response) {
            var _this2 = this;

            if (this.suspendResume.isSuspended) {
              return;
            }

            if (response.change) {
              response.change.forEach(function (handle) {
                return _this2.emitHandleChanged(handle);
              });
            }

            if (response.close) {
              response.close.forEach(function (handle) {
                return _this2.emitHandleClosed(handle);
              });
            }
          }
          /**
          * Event handler for the RPC notification event.
          * @private
          * @emits Session#notification
          * @param {Object} response The JSONRPC notification.
          */

        }, {
          key: "onRpcNotification",
          value: function onRpcNotification(response) {
            this.emit('notification:*', response.method, response.params);
            this.emit("notification:".concat(response.method), response.params);
          }
          /**
          * Event handler for the RPC traffic event.
          * @private
          * @emits Session#traffic
          * @param {String} dir The traffic direction, sent or received.
          * @param {Object} data JSONRPC request/response/WebSocket message.
          * @param {Number} handle The associated handle.
          */

        }, {
          key: "onRpcTraffic",
          value: function onRpcTraffic(dir, data, handle) {
            this.emit('traffic:*', dir, data);
            this.emit("traffic:".concat(dir), data);
            var api = this.apis.getApi(handle);

            if (api) {
              api.emit('traffic:*', dir, data);
              api.emit("traffic:".concat(dir), data);
            }
          }
          /**
          * Event handler for cleaning up API instances when a session has been closed.
          * @private
          * @emits API#closed
          */

        }, {
          key: "onSessionClosed",
          value: function onSessionClosed() {
            this.apis.getApis().forEach(function (entry) {
              entry.api.emit('closed');
              entry.api.removeAllListeners();
            });
            this.apis.clear();
          }
          /**
           * Function used to get an API for a backend object.
           * @private
           * @param {Object} args Arguments used to create object API.
           * @param {Number} args.handle Handle of the backend object.
           * @param {String} args.id ID of the backend object.
           * @param {String} args.type QIX type of the backend object. Can for example
           *                           be "Doc" or "GenericVariable".
           * @param {String} args.genericType Custom type of the backend object, if defined in qInfo.
           * @returns {*} Returns the generated and possibly augmented API.
           */

        }, {
          key: "getObjectApi",
          value: function getObjectApi(args) {
            var handle = args.handle,
                id = args.id,
                type = args.type,
                genericType = args.genericType;
            var api = this.apis.getApi(handle);

            if (api) {
              return api;
            }

            var factory = this.definition.generate(type);
            api = factory(this, handle, id, genericType);
            this.apis.add(handle, api);
            return api;
          }
          /**
          * Establishes the websocket against the configured URL and returns the Global instance.
          * @emits Session#opened
          * @returns {Promise<Object>} Eventually resolved if the connection was successful.
          */

        }, {
          key: "open",
          value: function open() {
            var _this3 = this;

            if (!this.globalPromise) {
              var args = {
                handle: -1,
                id: 'Global',
                type: 'Global',
                genericType: 'Global'
              };
              this.globalPromise = this.rpc.open().then(function () {
                return _this3.getObjectApi(args);
              }).then(function (global) {
                _this3.emit('opened');

                return global;
              });
            }

            return this.globalPromise;
          }
          /**
          * Function used to send data on the RPC socket.
          * @param {Object} request The request to be sent. (data and some meta info)
          * @returns {Object} Returns a promise instance.
          */

        }, {
          key: "send",
          value: function send(request) {
            var _this4 = this;

            if (this.suspendResume.isSuspended) {
              return this.Promise.reject(new Error('Session suspended'));
            }

            request.id = this.rpc.createRequestId();
            var promise = this.intercept.executeRequests(this, this.Promise.resolve(request)).then(function (augmentedRequest) {
              var data = _Object$assign({}, _this4.config.protocol, augmentedRequest); // the outKey value is used by multiple-out interceptor, at some point
              // we need to refactor that implementation and figure out how to transport
              // this value without hijacking the JSONRPC request object:


              delete data.outKey;

              var response = _this4.rpc.send(data);

              augmentedRequest.retry = function () {
                return _this4.send(request);
              };

              return _this4.intercept.executeResponses(_this4, response, augmentedRequest);
            });
            Session.addToPromiseChain(promise, 'requestId', request.id);
            return promise;
          }
          /**
          * Suspends the enigma.js session by closing the websocket and rejecting all method calls
          * until is has been resumed again.
          * @emits Session#suspended
          * @returns {Promise<Object>} Eventually resolved when the websocket has been closed.
          */

        }, {
          key: "suspend",
          value: function suspend() {
            var _this5 = this;

            return this.suspendResume.suspend().then(function () {
              return _this5.emit('suspended', {
                initiator: 'manual'
              });
            });
          }
          /**
          * Resumes a previously suspended enigma.js session by re-creating the websocket and,
          * if possible, re-open the document as well as refreshing the internal cashes. If successful,
          * changed events will be triggered on all generated APIs, and on the ones it was unable to
          * restore, the closed event will be triggered.
          * @emits Session#resumed
          * @param {Boolean} onlyIfAttached If true, resume only if the session was re-attached properly.
          * @returns {Promise<Object>} Eventually resolved when the websocket (and potentially the
          * previously opened document, and generated APIs) has been restored, rejected when it fails any
          * of those steps, or when onlyIfAttached is true and a new session was created.
          */

        }, {
          key: "resume",
          value: function resume(onlyIfAttached) {
            var _this6 = this;

            return this.suspendResume.resume(onlyIfAttached).then(function (value) {
              _this6.emit('resumed');

              return value;
            });
          }
          /**
          * Closes the websocket and cleans up internal caches, also triggers the closed event
          * on all generated APIs. Note that you have to manually invoke this when you want to
          * close a session and config.suspendOnClose is true.
          * @emits Session#closed
          * @returns {Promise<Object>} Eventually resolved when the websocket has been closed.
          */

        }, {
          key: "close",
          value: function close() {
            var _this7 = this;

            this.globalPromise = undefined;
            return this.rpc.close().then(function (evt) {
              return _this7.emit('closed', evt);
            });
          }
          /**
          * Given a handle, this function will emit the 'changed' event on the
          * corresponding API instance.
          * @private
          * @param {Number} handle The handle of the API instance.
          * @emits API#changed
          */

        }, {
          key: "emitHandleChanged",
          value: function emitHandleChanged(handle) {
            var api = this.apis.getApi(handle);

            if (api) {
              api.emit('changed');
            }
          }
          /**
          * Given a handle, this function will emit the 'closed' event on the
          * corresponding API instance.
          * @private
          * @param {Number} handle The handle of the API instance.
          * @emits API#closed
          */

        }, {
          key: "emitHandleClosed",
          value: function emitHandleClosed(handle) {
            var api = this.apis.getApi(handle);

            if (api) {
              api.emit('closed');
              api.removeAllListeners();
            }
          }
          /**
          * Function used to add info on the promise chain.
          * @private
          * @param {Promise<Object>} promise The promise to add info on.
          * @param {String} name The property to add info on.
          * @param {Any} value The info to add.
          */

        }], [{
          key: "addToPromiseChain",
          value: function addToPromiseChain(promise, name, value) {
            promise[name] = value;
            var then = promise.then;

            promise.then = function patchedThen() {
              for (var _len = arguments.length, params = new Array(_len), _key = 0; _key < _len; _key++) {
                params[_key] = arguments[_key];
              }

              var chain = then.apply(this, params);
              Session.addToPromiseChain(chain, name, value);
              return chain;
            };
          }
        }]);

        return Session;
      }();
      /**
      * Key-value cache
      * @private
      */


      var KeyValueCache =
      /*#__PURE__*/
      function () {
        function KeyValueCache() {
          _classCallCheck(this, KeyValueCache);

          this.entries = {};
        }
        /**
        * Adds an entry.
        * @private
        * @function KeyValueCache#add
        * @param {String} key The key representing an entry.
        * @param {*} entry The entry to be added.
        */


        _createClass(KeyValueCache, [{
          key: "add",
          value: function add(key, entry) {
            key += '';

            if (typeof this.entries[key] !== 'undefined') {
              throw new Error("Entry already defined with key ".concat(key));
            }

            this.entries[key] = entry;
          }
          /**
          * Sets an entry.
          * @private
          * @function KeyValueCache#set
          * @param {String} key The key representing an entry.
          * @param {*} entry The entry.
          */

        }, {
          key: "set",
          value: function set(key, entry) {
            key += '';
            this.entries[key] = entry;
          }
          /**
          * Removes an entry.
          * @private
          * @function KeyValueCache#remove
          * @param {String} key The key representing an entry.
          */

        }, {
          key: "remove",
          value: function remove(key) {
            delete this.entries[key];
          }
          /**
          * Gets an entry.
          * @private
          * @function KeyValueCache#get
          * @param {String} key The key representing an entry.
          * @returns {*} The entry for the key.
          */

        }, {
          key: "get",
          value: function get(key) {
            return this.entries[key];
          }
          /**
          * Gets a list of all entries.
          * @private
          * @function KeyValueCache#getAll
          * @returns {Array} The list of entries including its `key` and `value` properties.
          */

        }, {
          key: "getAll",
          value: function getAll() {
            var _this = this;

            return _Object$keys(this.entries).map(function (key) {
              return {
                key: key,
                value: _this.entries[key]
              };
            });
          }
          /**
          * Gets a key for an entry.
          * @private
          * @function KeyValueCache#getKey
          * @param {*} entry The entry to locate the key for.
          * @returns {String} The key representing an entry.
          */

        }, {
          key: "getKey",
          value: function getKey(entry) {
            var _this2 = this;

            return _Object$keys(this.entries).filter(function (key) {
              return _this2.entries[key] === entry;
            })[0];
          }
          /**
          * Clears the cache of all entries.
          * @private
          * @function KeyValueCache#clear
          */

        }, {
          key: "clear",
          value: function clear() {
            this.entries = {};
          }
        }]);

        return KeyValueCache;
      }();

      var hasOwnProperty$1 = Object.prototype.hasOwnProperty;
      /**
      * Returns the camelCase counterpart of a symbol.
      * @private
      * @param {String} symbol The symbol.
      * @return the camelCase counterpart.
      */

      function toCamelCase(symbol) {
        return symbol.substring(0, 1).toLowerCase() + symbol.substring(1);
      }
      /**
       * A facade function that allows parameters to be passed either by name
       * (through an object), or by position (through an array).
       * @private
       * @param {Function} base The function that is being overriden. Will be
       *                        called with parameters in array-form.
       * @param {Object} defaults Parameter list and it's default values.
       * @param {*} params The parameters.
       */


      function namedParamFacade(base, defaults) {
        for (var _len = arguments.length, params = new Array(_len > 2 ? _len - 2 : 0), _key = 2; _key < _len; _key++) {
          params[_key - 2] = arguments[_key];
        }

        if (params.length === 1 && _typeof(params[0]) === 'object' && !_Array$isArray(params[0])) {
          var valid = _Object$keys(params[0]).every(function (key) {
            return hasOwnProperty$1.call(defaults, key);
          });

          if (valid) {
            params = _Object$keys(defaults).map(function (key) {
              return params[0][key] || defaults[key];
            });
          }
        }

        return base.apply(this, params);
      }
      /**
      * Qix schema definition.
      * @private
      */


      var Schema =
      /*#__PURE__*/
      function () {
        /**
        * Create a new schema instance.
        * @private
        * @param {Configuration} config The configuration for QIX.
        */
        function Schema(config) {
          _classCallCheck(this, Schema);

          this.config = config;
          this.Promise = config.Promise;
          this.schema = config.schema;
          this.mixins = new KeyValueCache();
          this.types = new KeyValueCache();
        }

        _createClass(Schema, [{
          key: "registerMixin",
          value: function registerMixin(_ref) {
            var _this = this;

            var types = _ref.types,
                type = _ref.type,
                extend = _ref.extend,
                override = _ref.override,
                init = _ref.init;

            if (!_Array$isArray(types)) {
              types = [types];
            } // to support a single type


            if (type) {
              types.push(type);
            }

            var cached = {
              extend: extend,
              override: override,
              init: init
            };
            types.forEach(function (typeKey) {
              var entryList = _this.mixins.get(typeKey);

              if (entryList) {
                entryList.push(cached);
              } else {
                _this.mixins.add(typeKey, [cached]);
              }
            });
          }
          /**
          * Function used to generate a type definition.
          * @private
          * @param {String} type The type.
          * @returns {{create: Function, def: Object}} Returns an object with a definition
          *          of the type and a create factory.
          */

        }, {
          key: "generate",
          value: function generate(type) {
            var entry = this.types.get(type);

            if (entry) {
              return entry;
            }

            if (!this.schema.structs[type]) {
              throw new Error("".concat(type, " not found"));
            }

            var factory = this.generateApi(type, this.schema.structs[type]);
            this.types.add(type, factory);
            return factory;
          }
          /**
          * Function used to generate an API definition for a given type.
          * @private
          * @param {String} type The type to generate.
          * @param {Object} schema The schema describing the type.
          * @returns {{create: (function(session:Object, handle:Number, id:String,
          *          customKey:String)), def: Object}} Returns the API definition.
          */

        }, {
          key: "generateApi",
          value: function generateApi(type, schema) {
            var api = _Object$create({});

            this.generateDefaultApi(api, schema); // Generate default

            this.mixinType(type, api); // Mixin default type

            this.mixinNamedParamFacade(api, schema); // Mixin named parameter support

            return function create(session, handle, id, customKey) {
              var _this2 = this;

              var instance = _Object$create(api);

              Events.mixin(instance); // Always mixin event-emitter per instance

              _Object$defineProperties(instance, {
                session: {
                  enumerable: true,
                  value: session
                },
                handle: {
                  enumerable: true,
                  value: handle,
                  writable: true
                },
                id: {
                  enumerable: true,
                  value: id
                },
                type: {
                  enumerable: true,
                  value: type
                },
                genericType: {
                  enumerable: true,
                  value: customKey
                }
              });

              var mixinList = this.mixins.get(type) || [];

              if (customKey !== type) {
                this.mixinType(customKey, instance); // Mixin custom types

                mixinList = mixinList.concat(this.mixins.get(customKey) || []);
              }

              mixinList.forEach(function (mixin) {
                if (typeof mixin.init === 'function') {
                  mixin.init({
                    config: _this2.config,
                    api: instance
                  });
                }
              });
              return instance;
            }.bind(this);
          }
          /**
          * Function used to generate the methods with the right handlers to the object
          * API that is being generated.
          * @private
          * @param {Object} api The object API that is currently being generated.
          * @param {Object} schema The API definition.
          */

        }, {
          key: "generateDefaultApi",
          value: function generateDefaultApi(api, schema) {
            _Object$keys(schema).forEach(function (method) {
              var out = schema[method].Out;
              var outKey = out.length === 1 ? out[0].Name : -1;
              var fnName = toCamelCase(method);

              api[fnName] = function generatedMethod() {
                for (var _len2 = arguments.length, params = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
                  params[_key2] = arguments[_key2];
                }

                return this.session.send({
                  handle: this.handle,
                  method: method,
                  params: params,
                  outKey: outKey
                });
              };
            });
          }
          /**
          * Function used to add mixin methods to a specified API.
          * @private
          * @param {String} type Used to specify which mixin should be woven in.
          * @param {Object} api The object that will be woven.
          */

        }, {
          key: "mixinType",
          value: function mixinType(type, api) {
            var mixinList = this.mixins.get(type);

            if (mixinList) {
              mixinList.forEach(function (_ref2) {
                var _ref2$extend = _ref2.extend,
                    extend = _ref2$extend === void 0 ? {} : _ref2$extend,
                    _ref2$override = _ref2.override,
                    override = _ref2$override === void 0 ? {} : _ref2$override;

                _Object$keys(override).forEach(function (key) {
                  if (typeof api[key] === 'function' && typeof override[key] === 'function') {
                    var baseFn = api[key];

                    api[key] = function wrappedFn() {
                      for (var _len3 = arguments.length, args = new Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
                        args[_key3] = arguments[_key3];
                      }

                      return override[key].apply(this, [baseFn.bind(this)].concat(args));
                    };
                  } else {
                    throw new Error("No function to override. Type: ".concat(type, " function: ").concat(key));
                  }
                });

                _Object$keys(extend).forEach(function (key) {
                  // handle overrides
                  if (typeof api[key] === 'function' && typeof extend[key] === 'function') {
                    throw new Error("Extend is not allowed for this mixin. Type: ".concat(type, " function: ").concat(key));
                  } else {
                    api[key] = extend[key];
                  }
                });
              });
            }
          }
          /**
          * Function used to mixin the named parameter facade.
          * @private
          * @param {Object} api The object API that is currently being generated.
          * @param {Object} schema The API definition.
          */

        }, {
          key: "mixinNamedParamFacade",
          value: function mixinNamedParamFacade(api, schema) {
            _Object$keys(schema).forEach(function (key) {
              var fnName = toCamelCase(key);
              var base = api[fnName];
              var defaults = schema[key].In.reduce(function (result, item) {
                result[item.Name] = item.DefaultValue;
                return result;
              }, {});

              api[fnName] = function namedParamWrapper() {
                for (var _len4 = arguments.length, params = new Array(_len4), _key4 = 0; _key4 < _len4; _key4++) {
                  params[_key4] = arguments[_key4];
                }

                return namedParamFacade.apply(this, [base, defaults].concat(params));
              };
            });
          }
        }]);

        return Schema;
      }();
      /**
       * Helper class for handling RPC calls
       * @private
       */


      var RPCResolver =
      /*#__PURE__*/
      function () {
        function RPCResolver(id, handle, resolve, reject) {
          _classCallCheck(this, RPCResolver);

          Events.mixin(this);
          this.id = id;
          this.handle = handle;
          this.resolve = resolve;
          this.reject = reject;
        }

        _createClass(RPCResolver, [{
          key: "resolveWith",
          value: function resolveWith(data) {
            this.resolve(data);
            this.emit('resolved', this.id);
          }
        }, {
          key: "rejectWith",
          value: function rejectWith(err) {
            this.reject(err);
            this.emit('rejected', this.id);
          }
        }]);

        return RPCResolver;
      }();
      /**
      * This class handles remote procedure calls on a web socket.
      * @private
      */


      var RPC =
      /*#__PURE__*/
      function () {
        /**
        * Create a new RPC instance.
        * @private
        * @param {Object} options The configuration options for this class.
        * @param {Function} options.Promise The promise constructor to use.
        * @param {String} options.url The complete websocket URL used to connect.
        * @param {Function} options.createSocket The function callback to create a WebSocket.
        */
        function RPC(options) {
          _classCallCheck(this, RPC);

          _Object$assign(this, options);

          Events.mixin(this);
          this.resolvers = {};
          this.requestId = 0;
          this.openedPromise = undefined;
        }
        /**
        * Opens a connection to the configured endpoint.
        * @private
        * @param {Boolean} force - ignores all previous and outstanding open calls if set to true.
        * @returns {Object} A promise instance.
        */


        _createClass(RPC, [{
          key: "open",
          value: function open() {
            var _this = this;

            var force = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

            if (!force && this.openedPromise) {
              return this.openedPromise;
            }

            try {
              this.socket = this.createSocket(this.url);
            } catch (err) {
              return this.Promise.reject(err);
            }

            this.socket.onopen = this.onOpen.bind(this);
            this.socket.onclose = this.onClose.bind(this);
            this.socket.onerror = this.onError.bind(this);
            this.socket.onmessage = this.onMessage.bind(this);
            this.openedPromise = new this.Promise(function (resolve, reject) {
              return _this.registerResolver('opened', null, resolve, reject);
            });
            this.closedPromise = new this.Promise(function (resolve, reject) {
              return _this.registerResolver('closed', null, resolve, reject);
            });
            return this.openedPromise;
          }
          /**
          * Resolves the open promise when a connection is successfully established.
          * @private
          */

        }, {
          key: "onOpen",
          value: function onOpen() {
            var _this2 = this;

            this.resolvers.opened.resolveWith(function () {
              return _this2.closedPromise;
            });
          }
          /**
          * Resolves the close promise when a connection is closed.
          * @private
          * @param {Object} event - The event describing close.
          */

        }, {
          key: "onClose",
          value: function onClose(event) {
            this.emit('closed', event);
            this.resolvers.closed.resolveWith(event);
            this.rejectAllOutstandingResolvers({
              code: -1,
              message: 'Socket closed'
            });
          }
          /**
          * Closes a connection.
          * @private
          * @param {Number} [code=1000] - The reason code for closing the connection.
          * @param {String} [reason=""] - The human readable string describing why the connection is closed.
          * @returns {Object} Returns a promise instance.
          */

        }, {
          key: "close",
          value: function close() {
            var code = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1000;
            var reason = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';

            if (this.socket) {
              this.socket.close(code, reason);
              this.socket = null;
            }

            return this.closedPromise;
          }
          /**
          * Emits an error event and rejects the open promise if an error is raised on the connection.
          * @private
          * @param {Object} event - The event describing the error.
          */

        }, {
          key: "onError",
          value: function onError(event) {
            if (this.resolvers.opened) {
              this.resolvers.opened.rejectWith(event);
            } else {
              // only emit errors after the initial open promise has been resolved,
              // this makes it possible to catch early websocket errors as well
              // as run-time ones:
              this.emit('socket-error', event);
            }

            this.rejectAllOutstandingResolvers({
              code: -1,
              message: 'Socket error'
            });
          }
          /**
          * Parses the onMessage event on the connection and resolve the promise for the request.
          * @private
          * @param {Object} event - The event describing the message.
          */

        }, {
          key: "onMessage",
          value: function onMessage(event) {
            var data = JSON.parse(event.data);
            var resolver = this.resolvers[data.id] || {};
            this.emit('traffic', 'received', data, resolver.handle);

            if (typeof data.id !== 'undefined') {
              this.emit('message', data);
              this.resolvers[data.id].resolveWith(data);
            } else {
              this.emit(data.params ? 'notification' : 'message', data);
            }
          }
          /**
          * Rejects all outstanding resolvers.
          * @private
          * @param {Object} reason - The reject reason.
          */

        }, {
          key: "rejectAllOutstandingResolvers",
          value: function rejectAllOutstandingResolvers(reason) {
            var _this3 = this;

            _Object$keys(this.resolvers).forEach(function (id) {
              if (id === 'opened' || id === 'closed') {
                return; // "opened" and "closed" should not be handled here
              }

              var resolver = _this3.resolvers[id];
              resolver.rejectWith(reason);
            });
          }
          /**
          * Unregisters a resolver.
          * @private
          * @param {Number|String} id - The ID to unregister the resolver with.
          */

        }, {
          key: "unregisterResolver",
          value: function unregisterResolver(id) {
            var resolver = this.resolvers[id];
            resolver.removeAllListeners();
            delete this.resolvers[id];
          }
          /**
          * Registers a resolver.
          * @private
          * @param {Number|String} id - The ID to register the resolver with.
          * @param {Number} handle - The associated handle.
          * @returns {Function} The promise executor function.
          */

        }, {
          key: "registerResolver",
          value: function registerResolver(id, handle, resolve, reject) {
            var _this4 = this;

            var resolver = new RPCResolver(id, handle, resolve, reject);
            this.resolvers[id] = resolver;
            resolver.on('resolved', function (resolvedId) {
              return _this4.unregisterResolver(resolvedId);
            });
            resolver.on('rejected', function (rejectedId) {
              return _this4.unregisterResolver(rejectedId);
            });
          }
          /**
          * Sends data on the socket.
          * @private
          * @param {Object} data - The data to send.
          * @returns {Object} A promise instance.
          */

        }, {
          key: "send",
          value: function send(data) {
            var _this5 = this;

            if (!this.socket || this.socket.readyState !== this.socket.OPEN) {
              var error = new Error('Not connected');
              error.code = -1;
              return this.Promise.reject(error);
            }

            if (!data.id) {
              data.id = this.createRequestId();
            }

            data.jsonrpc = '2.0';
            return new this.Promise(function (resolve, reject) {
              _this5.socket.send(_JSON$stringify(data));

              _this5.emit('traffic', 'sent', data, data.handle);

              return _this5.registerResolver(data.id, data.handle, resolve, reject);
            });
          }
        }, {
          key: "createRequestId",
          value: function createRequestId() {
            this.requestId += 1;
            return this.requestId;
          }
        }]);

        return RPC;
      }();

      var ON_ATTACHED_TIMEOUT_MS = 5000;
      var RPC_CLOSE_MANUAL_SUSPEND$1 = 4000;

      var SuspendResume =
      /*#__PURE__*/
      function () {
        /**
        * Creates a new SuspendResume instance.
        * @private
        * @param {Object} options The configuration option for this class.
        * @param {Promise<Object>} options.Promise The promise constructor to use.
        * @param {RPC} options.rpc The RPC instance to use when communicating towards Engine.
        * @param {ApiCache} options.apis The ApiCache instance to use.
        */
        function SuspendResume(options) {
          var _this = this;

          _classCallCheck(this, SuspendResume);

          _Object$assign(this, options);

          this.isSuspended = false;
          this.rpc.on('traffic', function (dir, data) {
            if (dir === 'sent' && data.method === 'OpenDoc') {
              _this.openDocParams = data.params;
            }
          });
        }
        /**
        * Function used to restore the rpc connection.
        * @private
        * @param {Boolean} onlyIfAttached - if true, the returned promise will resolve
        *                                   only if the session can be re-attached.
        * @returns {Object} Returns a promise instance.
        */


        _createClass(SuspendResume, [{
          key: "restoreRpcConnection",
          value: function restoreRpcConnection(onlyIfAttached) {
            var _this2 = this;

            return this.reopen(ON_ATTACHED_TIMEOUT_MS).then(function (sessionState) {
              if (sessionState === 'SESSION_CREATED' && onlyIfAttached) {
                return _this2.Promise.reject(new Error('Not attached'));
              }

              return _this2.Promise.resolve();
            });
          }
          /**
          * Function used to restore the global API.
          * @private
          * @param {Object} changed - A list where the restored APIs will be added.
          * @returns {Object} Returns a promise instance.
          */

        }, {
          key: "restoreGlobal",
          value: function restoreGlobal(changed) {
            var global = this.apis.getApisByType('Global').pop();
            changed.push(global.api);
            return this.Promise.resolve();
          }
          /**
          * Function used to restore the doc API.
          * @private
          * @param {String} sessionState - The state of the session, attached or created.
          * @param {Array} closed - A list where the closed of APIs APIs will be added.
          * @param {Object} changed - A list where the restored APIs will be added.
          * @returns {Object} Returns a promise instance.
          */

        }, {
          key: "restoreDoc",
          value: function restoreDoc(closed, changed) {
            var _this3 = this;

            var doc = this.apis.getApisByType('Doc').pop();

            if (!doc) {
              return this.Promise.resolve();
            }

            return this.rpc.send({
              method: 'GetActiveDoc',
              handle: -1,
              params: []
            }).then(function (response) {
              if (response.error && _this3.openDocParams) {
                return _this3.rpc.send({
                  method: 'OpenDoc',
                  handle: -1,
                  params: _this3.openDocParams
                });
              }

              return response;
            }).then(function (response) {
              if (response.error) {
                closed.push(doc.api);
                return _this3.Promise.resolve();
              }

              var handle = response.result.qReturn.qHandle;
              doc.api.handle = handle;
              changed.push(doc.api);
              return _this3.Promise.resolve(doc.api);
            });
          }
          /**
          * Function used to restore the APIs on the doc.
          * @private
          * @param {Object} doc - The doc API on which the APIs we want to restore exist.
          * @param {Array} closed - A list where the closed of APIs APIs will be added.
          * @param {Object} changed - A list where the restored APIs will be added.
          * @returns {Object} Returns a promise instance.
          */

        }, {
          key: "restoreDocObjects",
          value: function restoreDocObjects(doc, closed, changed) {
            var _this4 = this;

            var tasks = [];
            var apis = this.apis.getApis().map(function (entry) {
              return entry.api;
            }).filter(function (api) {
              return api.type !== 'Global' && api.type !== 'Doc';
            });

            if (!doc) {
              apis.forEach(function (api) {
                return closed.push(api);
              });
              return this.Promise.resolve();
            }

            apis.forEach(function (api) {
              var method = SuspendResume.buildGetMethodName(api.type);

              if (!method) {
                closed.push(api);
              } else {
                var request = _this4.rpc.send({
                  method: method,
                  handle: doc.handle,
                  params: [api.id]
                }).then(function (response) {
                  if (response.error || !response.result.qReturn.qHandle) {
                    closed.push(api);
                  } else {
                    api.handle = response.result.qReturn.qHandle;
                    changed.push(api);
                  }
                });

                tasks.push(request);
              }
            });
            return this.Promise.all(tasks);
          }
          /**
          * Set the instance as suspended.
          * @private
          */

        }, {
          key: "suspend",
          value: function suspend() {
            this.isSuspended = true;
            return this.rpc.close(RPC_CLOSE_MANUAL_SUSPEND$1);
          }
          /**
          * Resumes a previously suspended RPC connection, and refreshes the API cache.
          *                                APIs unabled to be restored has their 'closed'
          *                                event triggered, otherwise 'changed'.
          * @private
          * @emits API#changed
          * @emits APIfunction@#closed
          * @param {Boolean} onlyIfAttached if true, resume only if the session was re-attached.
          * @returns {Promise<Object>} Eventually resolved if the RPC connection was successfully resumed,
          *                    otherwise rejected.
          */

        }, {
          key: "resume",
          value: function resume(onlyIfAttached) {
            var _this5 = this;

            var changed = [];
            var closed = [];
            return this.restoreRpcConnection(onlyIfAttached).then(function () {
              return _this5.restoreGlobal(changed);
            }).then(function () {
              return _this5.restoreDoc(closed, changed);
            }).then(function (doc) {
              return _this5.restoreDocObjects(doc, closed, changed);
            }).then(function () {
              _this5.isSuspended = false;

              _this5.apis.clear();

              closed.forEach(function (api) {
                api.emit('closed');
                api.removeAllListeners();
              });
              changed.forEach(function (api) {
                _this5.apis.add(api.handle, api);

                if (api.type !== 'Global') {
                  api.emit('changed');
                }
              });
            }).catch(function (err) {
              return _this5.rpc.close().then(function () {
                return _this5.Promise.reject(err);
              });
            });
          }
          /**
          * Reopens the connection and waits for the OnConnected notification.
          * @private
          * @param {Number} timeout - The time to wait for the OnConnected notification.
          * @returns {Object} A promise containing the session state (SESSION_CREATED or SESSION_ATTACHED).
          */

        }, {
          key: "reopen",
          value: function reopen(timeout) {
            var _this6 = this;

            var timer;
            var notificationResolve;
            var notificationReceived = false;
            var notificationPromise = new this.Promise(function (resolve) {
              notificationResolve = resolve;
            });

            var waitForNotification = function waitForNotification() {
              if (!notificationReceived) {
                timer = setTimeout(function () {
                  return notificationResolve('SESSION_CREATED');
                }, timeout);
              }

              return notificationPromise;
            };

            var onNotification = function onNotification(data) {
              if (data.method !== 'OnConnected') return;
              clearTimeout(timer);
              notificationResolve(data.params.qSessionState);
              notificationReceived = true;
            };

            this.rpc.on('notification', onNotification);
            return this.rpc.open(true).then(waitForNotification).then(function (state) {
              _this6.rpc.removeListener('notification', onNotification);

              return state;
            }).catch(function (err) {
              _this6.rpc.removeListener('notification', onNotification);

              return _this6.Promise.reject(err);
            });
          }
          /**
          * Function used to build the get method names for Doc APIs.
          * @private
          * @param {String} type - The API type.
          * @returns {String} Returns the get method name, or undefined if the type cannot be restored.
          */

        }], [{
          key: "buildGetMethodName",
          value: function buildGetMethodName(type) {
            if (type === 'Field' || type === 'Variable') {
              return null;
            }

            if (type === 'GenericVariable') {
              return 'GetVariableById';
            }

            return type.replace('Generic', 'Get');
          }
        }]);

        return SuspendResume;
      }();

      var SUCCESS_KEY = 'qSuccess';

      function deltaRequestInterceptor(session, request) {
        var delta = session.config.protocol.delta && request.outKey !== -1 && request.outKey !== SUCCESS_KEY;

        if (delta) {
          request.delta = delta;
        }

        return request;
      }
      /**
      * Response interceptor for generating APIs. Handles the quirks of engine not
      * returning an error when an object is missing.
      * @private
      * @param {Session} session - The session the intercept is being executed on.
      * @param {Object} request - The JSON-RPC request.
      * @param {Object} response - The response.
      * @returns {Object} - Returns the generated API
      */


      function apiInterceptor(session, request, response) {
        if (response.qHandle && response.qType) {
          return session.getObjectApi({
            handle: response.qHandle,
            type: response.qType,
            id: response.qGenericId,
            genericType: response.qGenericType
          });
        }

        if (response.qHandle === null && response.qType === null) {
          return session.config.Promise.reject(new Error('Object not found'));
        }

        return response;
      }

      var hasOwn = Object.prototype.hasOwnProperty;
      var toStr = Object.prototype.toString;
      var defineProperty = _Object$defineProperty;
      var gOPD = _Object$getOwnPropertyDescriptor;

      var isArray = function isArray(arr) {
        if (typeof _Array$isArray === 'function') {
          return _Array$isArray(arr);
        }

        return toStr.call(arr) === '[object Array]';
      };

      var isPlainObject = function isPlainObject(obj) {
        if (!obj || toStr.call(obj) !== '[object Object]') {
          return false;
        }

        var hasOwnConstructor = hasOwn.call(obj, 'constructor');
        var hasIsPrototypeOf = obj.constructor && obj.constructor.prototype && hasOwn.call(obj.constructor.prototype, 'isPrototypeOf'); // Not own constructor property must be Object

        if (obj.constructor && !hasOwnConstructor && !hasIsPrototypeOf) {
          return false;
        } // Own properties are enumerated firstly, so to speed up,
        // if last one is own, then all properties are own.


        var key;

        for (key in obj) {
          /**/
        }

        return typeof key === 'undefined' || hasOwn.call(obj, key);
      }; // If name is '__proto__', and Object.defineProperty is available, define __proto__ as an own property on target


      var setProperty = function setProperty(target, options) {
        if (defineProperty && options.name === '__proto__') {
          defineProperty(target, options.name, {
            enumerable: true,
            configurable: true,
            value: options.newValue,
            writable: true
          });
        } else {
          target[options.name] = options.newValue;
        }
      }; // Return undefined instead of __proto__ if '__proto__' is not an own property


      var getProperty = function getProperty(obj, name) {
        if (name === '__proto__') {
          if (!hasOwn.call(obj, name)) {
            return void 0;
          } else if (gOPD) {
            // In early versions of node, obj['__proto__'] is buggy when obj has
            // __proto__ as an own property. Object.getOwnPropertyDescriptor() works.
            return gOPD(obj, name).value;
          }
        }

        return obj[name];
      };

      var extend = function extend() {
        var options, name, src, copy, copyIsArray, clone;
        var target = arguments[0];
        var i = 1;
        var length = arguments.length;
        var deep = false; // Handle a deep copy situation

        if (typeof target === 'boolean') {
          deep = target;
          target = arguments[1] || {}; // skip the boolean and the target

          i = 2;
        }

        if (target == null || _typeof2(target) !== 'object' && typeof target !== 'function') {
          target = {};
        }

        for (; i < length; ++i) {
          options = arguments[i]; // Only deal with non-null/undefined values

          if (options != null) {
            // Extend the base object
            for (name in options) {
              src = getProperty(target, name);
              copy = getProperty(options, name); // Prevent never-ending loop

              if (target !== copy) {
                // Recurse if we're merging plain objects or arrays
                if (deep && copy && (isPlainObject(copy) || (copyIsArray = isArray(copy)))) {
                  if (copyIsArray) {
                    copyIsArray = false;
                    clone = src && isArray(src) ? src : [];
                  } else {
                    clone = src && isPlainObject(src) ? src : {};
                  } // Never move original objects, clone them


                  setProperty(target, {
                    name: name,
                    newValue: extend(deep, clone, copy)
                  }); // Don't bring in undefined values
                } else if (typeof copy !== 'undefined') {
                  setProperty(target, {
                    name: name,
                    newValue: copy
                  });
                }
              }
            }
          }
        } // Return the modified object


        return target;
      };

      var extend$1 = extend.bind(null, true);
      var JSONPatch = {};
      var isArray$1 = _Array$isArray;

      function isObject(v) {
        return v != null && !_Array$isArray(v) && _typeof(v) === 'object';
      }

      function isUndef(v) {
        return typeof v === 'undefined';
      }

      function isFunction(v) {
        return typeof v === 'function';
      }
      /**
      * Generate an exact duplicate (with no references) of a specific value.
      *
      * @private
      * @param {Object} The value to duplicate
      * @returns {Object} a unique, duplicated value
      */


      function generateValue(val) {
        if (val) {
          return extend$1({}, {
            val: val
          }).val;
        }

        return val;
      }
      /**
      * An additional type checker used to determine if the property is of internal
      * use or not a type that can be translated into JSON (like functions).
      *
      * @private
      * @param {Object} obj The object which has the property to check
      * @param {String} The property name to check
      * @returns {Boolean} Whether the property is deemed special or not
      */


      function isSpecialProperty(obj, key) {
        return isFunction(obj[key]) || key.substring(0, 2) === '$$' || key.substring(0, 1) === '_';
      }
      /**
      * Finds the parent object from a JSON-Pointer ("/foo/bar/baz" = "bar" is "baz" parent),
      * also creates the object structure needed.
      *
      * @private
      * @param {Object} data The root object to traverse through
      * @param {String} The JSON-Pointer string to use when traversing
      * @returns {Object} The parent object
      */


      function getParent(data, str) {
        var seperator = '/';
        var parts = str.substring(1).split(seperator).slice(0, -1);
        var numPart;
        parts.forEach(function (part, i) {
          if (i === parts.length) {
            return;
          }

          numPart = +part;
          var newPart = !isNaN(numPart) ? [] : {};
          data[numPart || part] = isUndef(data[numPart || part]) ? newPart : data[part];
          data = data[numPart || part];
        });
        return data;
      }
      /**
      * Cleans an object of all its properties, unless they're deemed special or
      * cannot be removed by configuration.
      *
      * @private
      * @param {Object} obj The object to clean
      */


      function emptyObject(obj) {
        _Object$keys(obj).forEach(function (key) {
          var config = _Object$getOwnPropertyDescriptor(obj, key);

          if (config.configurable && !isSpecialProperty(obj, key)) {
            delete obj[key];
          }
        });
      }
      /**
      * Compare an object with another, could be object, array, number, string, bool.
      * @private
      * @param {Object} a The first object to compare
      * @param {Object} a The second object to compare
      * @returns {Boolean} Whether the objects are identical
      */


      function compare(a, b) {
        var isIdentical = true;

        if (isObject(a) && isObject(b)) {
          if (_Object$keys(a).length !== _Object$keys(b).length) {
            return false;
          }

          _Object$keys(a).forEach(function (key) {
            if (!compare(a[key], b[key])) {
              isIdentical = false;
            }
          });

          return isIdentical;
        }

        if (isArray$1(a) && isArray$1(b)) {
          if (a.length !== b.length) {
            return false;
          }

          for (var i = 0, l = a.length; i < l; i += 1) {
            if (!compare(a[i], b[i])) {
              return false;
            }
          }

          return true;
        }

        return a === b;
      }
      /**
      * Generates patches by comparing two arrays.
      *
      * @private
      * @param {Array} oldA The old (original) array, which will be patched
      * @param {Array} newA The new array, which will be used to compare against
      * @returns {Array} An array of patches (if any)
      */


      function patchArray(original, newA, basePath) {
        var patches = [];
        var oldA = original.slice();
        var tmpIdx = -1;

        function findIndex(a, id, idx) {
          if (a[idx] && isUndef(a[idx].qInfo)) {
            return null;
          }

          if (a[idx] && a[idx].qInfo.qId === id) {
            // shortcut if identical
            return idx;
          }

          for (var ii = 0, ll = a.length; ii < ll; ii += 1) {
            if (a[ii] && a[ii].qInfo.qId === id) {
              return ii;
            }
          }

          return -1;
        }

        if (compare(newA, oldA)) {
          // array is unchanged
          return patches;
        }

        if (!isUndef(newA[0]) && isUndef(newA[0].qInfo)) {
          // we cannot create patches without unique identifiers, replace array...
          patches.push({
            op: 'replace',
            path: basePath,
            value: newA
          });
          return patches;
        }

        for (var i = oldA.length - 1; i >= 0; i -= 1) {
          tmpIdx = findIndex(newA, oldA[i].qInfo && oldA[i].qInfo.qId, i);

          if (tmpIdx === -1) {
            patches.push({
              op: 'remove',
              path: "".concat(basePath, "/").concat(i)
            });
            oldA.splice(i, 1);
          } else {
            patches = patches.concat(JSONPatch.generate(oldA[i], newA[tmpIdx], "".concat(basePath, "/").concat(i)));
          }
        }

        for (var _i = 0, l = newA.length; _i < l; _i += 1) {
          tmpIdx = findIndex(oldA, newA[_i].qInfo && newA[_i].qInfo.qId);

          if (tmpIdx === -1) {
            patches.push({
              op: 'add',
              path: "".concat(basePath, "/").concat(_i),
              value: newA[_i]
            });
            oldA.splice(_i, 0, newA[_i]);
          } else if (tmpIdx !== _i) {
            patches.push({
              op: 'move',
              path: "".concat(basePath, "/").concat(_i),
              from: "".concat(basePath, "/").concat(tmpIdx)
            });
            oldA.splice(_i, 0, oldA.splice(tmpIdx, 1)[0]);
          }
        }

        return patches;
      }
      /**
      * Generate an array of JSON-Patch:es following the JSON-Patch Specification Draft.
      *
      * See [specification draft](http://tools.ietf.org/html/draft-ietf-appsawg-json-patch-10)
      *
      * Does NOT currently generate patches for arrays (will replace them)
      * @private
      * @param {Object} original The object to patch to
      * @param {Object} newData The object to patch from
      * @param {String} [basePath] The base path to use when generating the paths for
      *                            the patches (normally not used)
      * @returns {Array} An array of patches
      */


      JSONPatch.generate = function generate(original, newData, basePath) {
        basePath = basePath || '';
        var patches = [];

        _Object$keys(newData).forEach(function (key) {
          var val = generateValue(newData[key]);
          var oldVal = original[key];
          var tmpPath = "".concat(basePath, "/").concat(key);

          if (compare(val, oldVal) || isSpecialProperty(newData, key)) {
            return;
          }

          if (isUndef(oldVal)) {
            // property does not previously exist
            patches.push({
              op: 'add',
              path: tmpPath,
              value: val
            });
          } else if (isObject(val) && isObject(oldVal)) {
            // we need to generate sub-patches for this, since it already exist
            patches = patches.concat(JSONPatch.generate(oldVal, val, tmpPath));
          } else if (isArray$1(val) && isArray$1(oldVal)) {
            patches = patches.concat(patchArray(oldVal, val, tmpPath));
          } else {
            // it's a simple property (bool, string, number)
            patches.push({
              op: 'replace',
              path: "".concat(basePath, "/").concat(key),
              value: val
            });
          }
        });

        _Object$keys(original).forEach(function (key) {
          if (isUndef(newData[key]) && !isSpecialProperty(original, key)) {
            // this property does not exist anymore
            patches.push({
              op: 'remove',
              path: "".concat(basePath, "/").concat(key)
            });
          }
        });

        return patches;
      };
      /**
      * Apply a list of patches to an object.
      * @private
      * @param {Object} original The object to patch
      * @param {Array} patches The list of patches to apply
      */


      JSONPatch.apply = function apply(original, patches) {
        patches.forEach(function (patch) {
          var parent = getParent(original, patch.path);
          var key = patch.path.split('/').splice(-1)[0];
          var target = key && isNaN(+key) ? parent[key] : parent[+key] || parent;
          var from = patch.from ? patch.from.split('/').splice(-1)[0] : null;

          if (patch.path === '/') {
            parent = null;
            target = original;
          }

          if (patch.op === 'add' || patch.op === 'replace') {
            if (isArray$1(parent)) {
              // trust indexes from patches, so don't replace the index if it's an add
              if (key === '-') {
                key = parent.length;
              }

              parent.splice(+key, patch.op === 'add' ? 0 : 1, patch.value);
            } else if (isArray$1(target) && isArray$1(patch.value)) {
              var _target;

              var newValues = patch.value.slice(); // keep array reference if possible...

              target.length = 0;

              (_target = target).push.apply(_target, _toConsumableArray(newValues));
            } else if (isObject(target) && isObject(patch.value)) {
              // keep object reference if possible...
              emptyObject(target);
              extend$1(target, patch.value);
            } else if (!parent) {
              throw new Error('Patchee is not an object we can patch');
            } else {
              // simple value
              parent[key] = patch.value;
            }
          } else if (patch.op === 'move') {
            var oldParent = getParent(original, patch.from);

            if (isArray$1(parent)) {
              parent.splice(+key, 0, oldParent.splice(+from, 1)[0]);
            } else {
              parent[key] = oldParent[from];
              delete oldParent[from];
            }
          } else if (patch.op === 'remove') {
            if (isArray$1(parent)) {
              parent.splice(+key, 1);
            } else {
              delete parent[key];
            }
          }
        });
      };
      /**
      * Deep clone an object.
      * @private
      * @param {Object} obj The object to clone
      * @returns {Object} A new object identical to the `obj`
      */


      JSONPatch.clone = function clone(obj) {
        return extend$1({}, obj);
      };
      /**
      * Creates a JSON-patch.
      * @private
      * @param {String} op The operation of the patch. Available values: "add", "remove", "move"
      * @param {Object} [val] The value to set the `path` to. If `op` is `move`, `val`
      *                       is the "from JSON-path" path
      * @param {String} path The JSON-path for the property to change (e.g. "/qHyperCubeDef/columnOrder")
      * @returns {Object} A patch following the JSON-patch specification
      */


      JSONPatch.createPatch = function createPatch(op, val, path) {
        var patch = {
          op: op.toLowerCase(),
          path: path
        };

        if (patch.op === 'move') {
          patch.from = val;
        } else if (typeof val !== 'undefined') {
          patch.value = val;
        }

        return patch;
      };
      /**
      * Apply the differences of two objects (keeping references if possible).
      * Identical to running `JSONPatch.apply(original, JSONPatch.generate(original, newData));`
      * @private
      * @param {Object} original The object to update/patch
      * @param {Object} newData the object to diff against
      *
      * @example
      * var obj1 = { foo: [1,2,3], bar: { baz: true, qux: 1 } };
      * var obj2 = { foo: [4,5,6], bar: { baz: false } };
      * JSONPatch.updateObject(obj1, obj2);
      * // => { foo: [4,5,6], bar: { baz: false } };
      */


      JSONPatch.updateObject = function updateObject(original, newData) {
        if (!_Object$keys(original).length) {
          extend$1(original, newData);
          return;
        }

        JSONPatch.apply(original, JSONPatch.generate(original, newData));
      };

      var sessions = {};
      /**
      * Function to make sure we release handle caches when they are closed.
      * @private
      * @param {Session} session The session instance to listen on.
      */

      var bindSession = function bindSession(session) {
        if (!sessions[session.id]) {
          var cache = {};
          sessions[session.id] = cache;
          session.on('traffic:received', function (data) {
            return data.close && data.close.forEach(function (handle) {
              return delete cache[handle];
            });
          });
          session.on('closed', function () {
            return delete sessions[session.id];
          });
        }
      };
      /**
      * Simple function that ensures the session events has been bound, and returns
      * either an existing key-value cache or creates one for the specified handle.
      * @private
      * @param {Session} session The session that owns the handle.
      * @param {Number} handle The object handle to retrieve the cache for.
      * @returns {KeyValueCache} The cache instance.
      */


      var getHandleCache = function getHandleCache(session, handle) {
        bindSession(session);
        var cache = sessions[session.id];

        if (!cache[handle]) {
          cache[handle] = new KeyValueCache();
        }

        return cache[handle];
      };
      /**
      * Function used to apply a list of patches and return the patched value.
      * @private
      * @param {Session} session The session.
      * @param {Number} handle The object handle.
      * @param {String} cacheId The cacheId.
      * @param {Array} patches The patches.
      * @returns {Object} Returns the patched value.
      */


      var patchValue = function patchValue(session, handle, cacheId, patches) {
        var cache = getHandleCache(session, handle);
        var entry = cache.get(cacheId);

        if (typeof entry === 'undefined') {
          entry = _Array$isArray(patches[0].value) ? [] : {};
        }

        if (patches.length) {
          if (patches[0].path === '/' && _typeof(patches[0].value) !== 'object') {
            // 'plain' values on root path is not supported (no object reference),
            // so we simply store the value directly:
            entry = patches[0].value;
          } else {
            JSONPatch.apply(entry, patches);
          }

          cache.set(cacheId, entry);
        }

        return entry;
      };
      /**
      * Process delta interceptor.
      * @private
      * @param {Session} session The session the intercept is being executed on.
      * @param {Object} request The JSON-RPC request.
      * @param {Object} response The response.
      * @returns {Object} Returns the patched response
      */


      function deltaInterceptor(session, request, response) {
        var delta = response.delta,
            result = response.result;

        if (delta) {
          // when delta is on the response data is expected to be an array of patches:
          _Object$keys(result).forEach(function (key) {
            if (!_Array$isArray(result[key])) {
              throw new Error('Unexpected RPC response, expected array of patches');
            }

            result[key] = patchValue(session, request.handle, "".concat(request.method, "-").concat(key), result[key]);
          }); // return a cloned response object to avoid patched object references:


          return JSON.parse(_JSON$stringify(response));
        }

        return response;
      } // export object reference for testing purposes:


      deltaInterceptor.sessions = sessions;
      /**
      * Process error interceptor.
      * @private
      * @param {Session} session - The session the intercept is being executed on.
      * @param {Object} request - The JSON-RPC request.
      * @param {Object} response - The response.
      * @returns {Object} - Returns the defined error for an error, else the response.
      */

      function errorInterceptor(session, request, response) {
        if (typeof response.error !== 'undefined') {
          var data = response.error;
          var error = new Error(data.message);
          error.code = data.code;
          error.parameter = data.parameter;
          return session.config.Promise.reject(error);
        }

        return response;
      }

      var RETURN_KEY = 'qReturn';
      /**
      * Picks out the result "out" parameter based on the QIX method+schema, with
      * some specific handling for some methods that breaks the predictable protocol.
      * @private
      * @param {Session} session - The session the intercept is being executed on.
      * @param {Object} request - The JSON-RPC request.
      * @param {Object} response - The response.
      * @returns {Object} - Returns the result property on the response
      */

      function outParamInterceptor(session, request, response) {
        if (request.method === 'CreateSessionApp' || request.method === 'CreateSessionAppFromApp') {
          // this method returns multiple out params that we need
          // to normalize before processing the response further:
          response[RETURN_KEY].qGenericId = response[RETURN_KEY].qGenericId || response.qSessionAppId;
        } else if (request.method === 'GetInteract') {
          // this method returns a qReturn value when it should only return
          // meta.outKey:
          delete response[RETURN_KEY];
        }

        if (hasOwnProperty.call(response, RETURN_KEY)) {
          return response[RETURN_KEY];
        }

        if (request.outKey !== -1) {
          return response[request.outKey];
        }

        return response;
      }
      /**
      * Process result interceptor.
      * @private
      * @param {Session} session - The session the intercept is being executed on.
      * @param {Object} request - The JSON-RPC request.
      * @param {Object} response - The response.
      * @returns {Object} - Returns the result property on the response
      */


      function resultInterceptor(session, request, response) {
        return response.result;
      }
      /**
       * Interceptors is a concept similar to mixins, but run on a lower level. The interceptor concept
       * can augment either the requests (i.e. before sent to QIX Engine), or the responses (i.e. after
       * QIX Engine has sent a response). The interceptor promises runs in parallel to the regular
       * promises used in enigma.js, which means that it can be really useful when you want to normalize
       * behaviors in your application.
       * @interface Interceptor
       */

      /**
        * @class InterceptorRequest
        * @implements {Interceptor}
        */

      /**
       * @class InterceptorResponse
       * @implements {Interceptor}
       */

      /**
       * This method is invoked when a request is about to be sent to QIX Engine.
       * @function InterceptorRequest#onFulfilled
       * @param {Session} session The session executing the interceptor.
       * @param {Object} request The JSON-RPC request that will be sent.
       */

      /**
       * This method is invoked when a previous interceptor has rejected the
       * promise, use this to handle for example errors before they are sent into mixins.
       * @function InterceptorResponse#onRejected
       * @param {Session} session The session executing the interceptor. You may use .retry() to retry
       * sending it to QIX Engine.
       * @param {Object} request The JSON-RPC request resulting in this error.
       * @param {Object} error Whatever the previous interceptor is rejected with.
       */

      /**
       * This method is invoked when a promise has been successfully resolved,
       * use this to modify the result or reject the promise chain before it is sent
       * to mixins.
       * @function InterceptorResponse#onFulfilled
       * @param {Session} session The session executing the interceptor.
       * @param {Object} request The JSON-RPC request resulting in this response.
       * @param {Object} result Whatever the previous interceptor is resolved with.
       */


      var Intercept =
      /*#__PURE__*/
      function () {
        /**
        * Create a new Intercept instance.
        * @private
        * @param {Object} options The configuration options for this class.
        * @param {Promise<Object>} options.Promise The promise constructor to use.
        * @param {ApiCache} options.apis The ApiCache instance to use.
        * @param {Array<Object>} [options.request] The additional request interceptors to use.
        * @param {Array<Object>} [options.response] The additional response interceptors to use.
        */
        function Intercept(options) {
          _classCallCheck(this, Intercept);

          _Object$assign(this, options);

          this.request = [{
            onFulfilled: deltaRequestInterceptor
          }].concat(_toConsumableArray(this.request || []));
          this.response = [{
            onFulfilled: errorInterceptor
          }, {
            onFulfilled: deltaInterceptor
          }, {
            onFulfilled: resultInterceptor
          }, {
            onFulfilled: outParamInterceptor
          }].concat(_toConsumableArray(this.response || []), [{
            onFulfilled: apiInterceptor
          }]);
        }
        /**
        * Execute the request interceptor queue, each interceptor will get the result from
        * the previous interceptor.
        * @private
        * @param {Session} session The session instance to execute against.
        * @param {Promise<Object>} promise The promise to chain on to.
        * @returns {Promise<Object>}
        */


        _createClass(Intercept, [{
          key: "executeRequests",
          value: function executeRequests(session, promise) {
            var _this = this;

            return this.request.reduce(function (interception, interceptor) {
              var intercept = interceptor.onFulfilled && interceptor.onFulfilled.bind(_this, session);
              return interception.then(intercept);
            }, promise);
          }
          /**
          * Execute the response interceptor queue, each interceptor will get the result from
          * the previous interceptor.
          * @private
          * @param {Session} session The session instance to execute against.
          * @param {Promise<Object>} promise The promise to chain on to.
          * @param {Object} request The JSONRPC request object for the intercepted response.
          * @returns {Promise<Object>}
          */

        }, {
          key: "executeResponses",
          value: function executeResponses(session, promise, request) {
            var _this2 = this;

            return this.response.reduce(function (interception, interceptor) {
              return interception.then(interceptor.onFulfilled && interceptor.onFulfilled.bind(_this2, session, request), interceptor.onRejected && interceptor.onRejected.bind(_this2, session, request));
            }, promise);
          }
        }]);

        return Intercept;
      }();
      /**
      * API cache for instances of QIX types, e.g. GenericObject.
      * @private
      * @extends KeyValueCache
      */


      var ApiCache =
      /*#__PURE__*/
      function (_KeyValueCache) {
        _inherits(ApiCache, _KeyValueCache);

        function ApiCache() {
          _classCallCheck(this, ApiCache);

          return _possibleConstructorReturn(this, _getPrototypeOf(ApiCache).apply(this, arguments));
        }

        _createClass(ApiCache, [{
          key: "add",

          /**
          * Adds an API.
          * @private
          * @function ApiCache#add
          * @param {Number} handle - The handle for the API.
          * @param {*} api - The API.
          * @returns {{api: *}} The entry.
          */
          value: function add(handle, api) {
            var _this = this;

            var entry = {
              api: api
            };

            _get(_getPrototypeOf(ApiCache.prototype), "add", this).call(this, handle.toString(), entry);

            api.on('closed', function () {
              return _this.remove(handle);
            });
            return entry;
          }
          /**
          * Gets an API.
          * @private
          * @function ApiCache#getApi
          * @param {Number} handle - The handle for the API.
          * @returns {*} The API for the handle.
          */

        }, {
          key: "getApi",
          value: function getApi(handle) {
            var entry = typeof handle !== 'undefined' ? this.get(handle.toString()) : undefined;
            return entry && entry.api;
          }
          /**
          * Gets a list of APIs.
          * @private
          * @function ApiCache#getApis
          * @returns {Array} The list of entries including `handle` and `api` properties for each entry.
          */

        }, {
          key: "getApis",
          value: function getApis() {
            return _get(_getPrototypeOf(ApiCache.prototype), "getAll", this).call(this).map(function (entry) {
              return {
                handle: entry.key,
                api: entry.value.api
              };
            });
          }
          /**
          * Gets a list of APIs with a given type.
          * @private
          * @function ApiCache#getApisByType
          * @param {String} type - The type of APIs to get.
          * @returns {Array} The list of entries including `handle` and `api` properties for each entry.
          */

        }, {
          key: "getApisByType",
          value: function getApisByType(type) {
            return this.getApis().filter(function (entry) {
              return entry.api.type === type;
            });
          }
        }]);

        return ApiCache;
      }(KeyValueCache);
      /**
       * The enigma.js configuration object.
       * @interface Configuration
       * @property {Object} schema Object containing the specification for the API to generate.
       * Corresponds to a specific version of the QIX Engine API.
       * @property {String} url String containing a proper websocker URL to QIX Engine.
       * @property {Function} [createSocket] A function to use when instantiating the WebSocket,
       * mandatory for Node.js.
       * @property {Object} [Promise] ES6-compatible Promise library.
       * @property {Boolean} [suspendOnClose=false] Set to true if the session should be suspended
       * instead of closed when the websocket is closed.
       * @property {Array<Mixin>} [mixins=[]] Mixins to extend/augment the QIX Engine API. Mixins
       * are applied in the array order.
       * @property {Array} [requestInterceptors=[]] Interceptors for augmenting requests before they
       * are sent to QIX Engine. Interceptors are applied in the array order.
       * @property {Array} [responseInterceptors=[]] Interceptors for augmenting responses before they
       * are passed into mixins and end-users. Interceptors are applied in the array order.
       * @property {Object} [protocol={}] An object containing additional JSON-RPC request parameters.
       * @property {Boolean} [protocol.delta=true] Set to false to disable the use of the
       * bandwidth-reducing delta protocol.
       */

      /**
       * Mixin object to extend/augment the QIX Engine API
       * @interface Mixin
       * @property {String|Array<String>} types String or array of strings containing the API-types that
       * will be mixed in.
       * @property {Object} [extend] Object literal containing the methods that will be extended on the
       * specified API.
       * @property {Object} [override] Object literal containing the methods to override existing methods.
       * @property {Function} [init] Init function that, if defined, will run when an API is instantiated.
       * It runs with Promise and API object as parameters
       */

      /**
       * The API for generated APIs depends on the QIX Engine schema you pass into your Configuration,
       * and on what QIX struct the API has.
       * @interface API
       * @property {String} id Contains the unique identifier for this API.
       * @property {String} type Contains the schema class name for this API.
       * @property {String} genericType Corresponds to the qInfo.qType property on the generic object's
       * properties object.
       * @property {Session} session Contains a reference to the session that this API belongs to.
       * @property {Number} handle Contains the handle QIX Engine assigned to the API. Used interally in
       * enigma.js for caches and JSON-RPC requests.
       */

      /**
       * Handle changes on the API. The changed event is triggered whenever enigma.js or QIX Engine has
       * identified potential changes on the underlying properties or hypercubes and you should re-fetch
       * your data.
       * @event API#changed
       * @type {Object}
       */

      /**
       * Handle closed API. The closed event is triggered whenever QIX Engine considers an API closed.
       * It usually means that it no longer exist in the QIX Engine document or session.
       * @event API#closed
       * @type {Object}
       */

      /**
       * Handle JSON-RPC requests/responses for this API. Generally used in debugging purposes.
       * traffic:* will handle all websocket messages, traffic:sent will handle outgoing messages
       * and traffic:received will handle incoming messages.
       * @event API#traffic
       * @type {Object}
       */

      /**
      * Qix service.
      */


      var Qix =
      /*#__PURE__*/
      function () {
        function Qix() {
          _classCallCheck(this, Qix);
        }

        _createClass(Qix, null, [{
          key: "getSession",

          /**
          * Function used to get a session.
          * @private
          * @param {Configuration} config The configuration object for this session.
          * @returns {Session} Returns a session instance.
          */
          value: function getSession(config) {
            var createSocket = config.createSocket,
                Promise = config.Promise,
                requestInterceptors = config.requestInterceptors,
                responseInterceptors = config.responseInterceptors,
                url = config.url;
            var apis = new ApiCache();
            var intercept = new Intercept({
              apis: apis,
              Promise: Promise,
              request: requestInterceptors,
              response: responseInterceptors
            });
            var rpc = new RPC({
              createSocket: createSocket,
              Promise: Promise,
              url: url
            });
            var suspendResume = new SuspendResume({
              apis: apis,
              Promise: Promise,
              rpc: rpc
            });
            var session = new Session({
              apis: apis,
              config: config,
              intercept: intercept,
              rpc: rpc,
              suspendResume: suspendResume
            });
            return session;
          }
          /**
          * Function used to create a QIX session.
          * @param {Configuration} config The configuration object for the QIX session.
          * @returns {Session} Returns a new QIX session.
          */

        }, {
          key: "create",
          value: function create(config) {
            Qix.configureDefaults(config);
            config.mixins.forEach(function (mixin) {
              config.definition.registerMixin(mixin);
            });
            return Qix.getSession(config);
          }
          /**
          * Function used to configure defaults.
          * @private
          * @param {Configuration} config The configuration object for how to connect
          *                               and retrieve end QIX APIs.
          */

        }, {
          key: "configureDefaults",
          value: function configureDefaults(config) {
            if (!config) {
              throw new Error('You need to supply a configuration.');
            } // eslint-disable-next-line no-restricted-globals


            if (!config.Promise && typeof _Promise === 'undefined') {
              throw new Error('Your environment has no Promise implementation. You must provide a Promise implementation in the config.');
            }

            if (typeof config.createSocket !== 'function' && typeof WebSocket === 'function') {
              // eslint-disable-next-line no-undef
              config.createSocket = function (url) {
                return new WebSocket(url);
              };
            }

            if (typeof config.suspendOnClose === 'undefined') {
              config.suspendOnClose = false;
            }

            config.protocol = config.protocol || {};
            config.protocol.delta = typeof config.protocol.delta !== 'undefined' ? config.protocol.delta : true; // eslint-disable-next-line no-restricted-globals

            config.Promise = config.Promise || _Promise;
            config.mixins = config.mixins || [];
            config.definition = config.definition || new Schema(config);
          }
        }]);

        return Qix;
      }();

      return Qix;
    });
    /***/

  },

  /***/
  "5b5f":
  /***/
  function b5f(module, exports, __webpack_require__) {
    "use strict";

    var LIBRARY = __webpack_require__("b457");

    var global = __webpack_require__("da3c");

    var ctx = __webpack_require__("bc25");

    var classof = __webpack_require__("7d8a");

    var $export = __webpack_require__("d13f");

    var isObject = __webpack_require__("6f8a");

    var aFunction = __webpack_require__("f2fe");

    var anInstance = __webpack_require__("b0bc");

    var forOf = __webpack_require__("560b");

    var speciesConstructor = __webpack_require__("302f");

    var task = __webpack_require__("df0a").set;

    var microtask = __webpack_require__("36dc")();

    var newPromiseCapabilityModule = __webpack_require__("03ca");

    var perform = __webpack_require__("75c9");

    var userAgent = __webpack_require__("8a12");

    var promiseResolve = __webpack_require__("decf");

    var PROMISE = 'Promise';
    var TypeError = global.TypeError;
    var process = global.process;
    var versions = process && process.versions;
    var v8 = versions && versions.v8 || '';
    var $Promise = global[PROMISE];
    var isNode = classof(process) == 'process';

    var empty = function empty() {
      /* empty */
    };

    var Internal, newGenericPromiseCapability, OwnPromiseCapability, Wrapper;
    var newPromiseCapability = newGenericPromiseCapability = newPromiseCapabilityModule.f;
    var USE_NATIVE = !!function () {
      try {
        // correct subclassing with @@species support
        var promise = $Promise.resolve(1);

        var FakePromise = (promise.constructor = {})[__webpack_require__("1b55")('species')] = function (exec) {
          exec(empty, empty);
        }; // unhandled rejections tracking support, NodeJS Promise without it fails @@species test


        return (isNode || typeof PromiseRejectionEvent == 'function') && promise.then(empty) instanceof FakePromise // v8 6.6 (Node 10 and Chrome 66) have a bug with resolving custom thenables
        // https://bugs.chromium.org/p/chromium/issues/detail?id=830565
        // we can't detect it synchronously, so just check versions
        && v8.indexOf('6.6') !== 0 && userAgent.indexOf('Chrome/66') === -1;
      } catch (e) {
        /* empty */
      }
    }(); // helpers

    var isThenable = function isThenable(it) {
      var then;
      return isObject(it) && typeof (then = it.then) == 'function' ? then : false;
    };

    var notify = function notify(promise, isReject) {
      if (promise._n) return;
      promise._n = true;
      var chain = promise._c;
      microtask(function () {
        var value = promise._v;
        var ok = promise._s == 1;
        var i = 0;

        var run = function run(reaction) {
          var handler = ok ? reaction.ok : reaction.fail;
          var resolve = reaction.resolve;
          var reject = reaction.reject;
          var domain = reaction.domain;
          var result, then, exited;

          try {
            if (handler) {
              if (!ok) {
                if (promise._h == 2) onHandleUnhandled(promise);
                promise._h = 1;
              }

              if (handler === true) result = value;else {
                if (domain) domain.enter();
                result = handler(value); // may throw

                if (domain) {
                  domain.exit();
                  exited = true;
                }
              }

              if (result === reaction.promise) {
                reject(TypeError('Promise-chain cycle'));
              } else if (then = isThenable(result)) {
                then.call(result, resolve, reject);
              } else resolve(result);
            } else reject(value);
          } catch (e) {
            if (domain && !exited) domain.exit();
            reject(e);
          }
        };

        while (chain.length > i) {
          run(chain[i++]);
        } // variable length - can't use forEach


        promise._c = [];
        promise._n = false;
        if (isReject && !promise._h) onUnhandled(promise);
      });
    };

    var onUnhandled = function onUnhandled(promise) {
      task.call(global, function () {
        var value = promise._v;
        var unhandled = isUnhandled(promise);
        var result, handler, console;

        if (unhandled) {
          result = perform(function () {
            if (isNode) {
              process.emit('unhandledRejection', value, promise);
            } else if (handler = global.onunhandledrejection) {
              handler({
                promise: promise,
                reason: value
              });
            } else if ((console = global.console) && console.error) {
              console.error('Unhandled promise rejection', value);
            }
          }); // Browsers should not trigger `rejectionHandled` event if it was handled here, NodeJS - should

          promise._h = isNode || isUnhandled(promise) ? 2 : 1;
        }

        promise._a = undefined;
        if (unhandled && result.e) throw result.v;
      });
    };

    var isUnhandled = function isUnhandled(promise) {
      return promise._h !== 1 && (promise._a || promise._c).length === 0;
    };

    var onHandleUnhandled = function onHandleUnhandled(promise) {
      task.call(global, function () {
        var handler;

        if (isNode) {
          process.emit('rejectionHandled', promise);
        } else if (handler = global.onrejectionhandled) {
          handler({
            promise: promise,
            reason: promise._v
          });
        }
      });
    };

    var $reject = function $reject(value) {
      var promise = this;
      if (promise._d) return;
      promise._d = true;
      promise = promise._w || promise; // unwrap

      promise._v = value;
      promise._s = 2;
      if (!promise._a) promise._a = promise._c.slice();
      notify(promise, true);
    };

    var $resolve = function $resolve(value) {
      var promise = this;
      var then;
      if (promise._d) return;
      promise._d = true;
      promise = promise._w || promise; // unwrap

      try {
        if (promise === value) throw TypeError("Promise can't be resolved itself");

        if (then = isThenable(value)) {
          microtask(function () {
            var wrapper = {
              _w: promise,
              _d: false
            }; // wrap

            try {
              then.call(value, ctx($resolve, wrapper, 1), ctx($reject, wrapper, 1));
            } catch (e) {
              $reject.call(wrapper, e);
            }
          });
        } else {
          promise._v = value;
          promise._s = 1;
          notify(promise, false);
        }
      } catch (e) {
        $reject.call({
          _w: promise,
          _d: false
        }, e); // wrap
      }
    }; // constructor polyfill


    if (!USE_NATIVE) {
      // 25.4.3.1 Promise(executor)
      $Promise = function Promise(executor) {
        anInstance(this, $Promise, PROMISE, '_h');
        aFunction(executor);
        Internal.call(this);

        try {
          executor(ctx($resolve, this, 1), ctx($reject, this, 1));
        } catch (err) {
          $reject.call(this, err);
        }
      }; // eslint-disable-next-line no-unused-vars


      Internal = function Promise(executor) {
        this._c = []; // <- awaiting reactions

        this._a = undefined; // <- checked in isUnhandled reactions

        this._s = 0; // <- state

        this._d = false; // <- done

        this._v = undefined; // <- value

        this._h = 0; // <- rejection state, 0 - default, 1 - handled, 2 - unhandled

        this._n = false; // <- notify
      };

      Internal.prototype = __webpack_require__("3904")($Promise.prototype, {
        // 25.4.5.3 Promise.prototype.then(onFulfilled, onRejected)
        then: function then(onFulfilled, onRejected) {
          var reaction = newPromiseCapability(speciesConstructor(this, $Promise));
          reaction.ok = typeof onFulfilled == 'function' ? onFulfilled : true;
          reaction.fail = typeof onRejected == 'function' && onRejected;
          reaction.domain = isNode ? process.domain : undefined;

          this._c.push(reaction);

          if (this._a) this._a.push(reaction);
          if (this._s) notify(this, false);
          return reaction.promise;
        },
        // 25.4.5.1 Promise.prototype.catch(onRejected)
        'catch': function _catch(onRejected) {
          return this.then(undefined, onRejected);
        }
      });

      OwnPromiseCapability = function OwnPromiseCapability() {
        var promise = new Internal();
        this.promise = promise;
        this.resolve = ctx($resolve, promise, 1);
        this.reject = ctx($reject, promise, 1);
      };

      newPromiseCapabilityModule.f = newPromiseCapability = function newPromiseCapability(C) {
        return C === $Promise || C === Wrapper ? new OwnPromiseCapability(C) : newGenericPromiseCapability(C);
      };
    }

    $export($export.G + $export.W + $export.F * !USE_NATIVE, {
      Promise: $Promise
    });

    __webpack_require__("c0d8")($Promise, PROMISE);

    __webpack_require__("1be4")(PROMISE);

    Wrapper = __webpack_require__("a7d3")[PROMISE]; // statics

    $export($export.S + $export.F * !USE_NATIVE, PROMISE, {
      // 25.4.4.5 Promise.reject(r)
      reject: function reject(r) {
        var capability = newPromiseCapability(this);
        var $$reject = capability.reject;
        $$reject(r);
        return capability.promise;
      }
    });
    $export($export.S + $export.F * (LIBRARY || !USE_NATIVE), PROMISE, {
      // 25.4.4.6 Promise.resolve(x)
      resolve: function resolve(x) {
        return promiseResolve(LIBRARY && this === Wrapper ? $Promise : this, x);
      }
    });
    $export($export.S + $export.F * !(USE_NATIVE && __webpack_require__("436c")(function (iter) {
      $Promise.all(iter)['catch'](empty);
    })), PROMISE, {
      // 25.4.4.1 Promise.all(iterable)
      all: function all(iterable) {
        var C = this;
        var capability = newPromiseCapability(C);
        var resolve = capability.resolve;
        var reject = capability.reject;
        var result = perform(function () {
          var values = [];
          var index = 0;
          var remaining = 1;
          forOf(iterable, false, function (promise) {
            var $index = index++;
            var alreadyCalled = false;
            values.push(undefined);
            remaining++;
            C.resolve(promise).then(function (value) {
              if (alreadyCalled) return;
              alreadyCalled = true;
              values[$index] = value;
              --remaining || resolve(values);
            }, reject);
          });
          --remaining || resolve(values);
        });
        if (result.e) reject(result.v);
        return capability.promise;
      },
      // 25.4.4.4 Promise.race(iterable)
      race: function race(iterable) {
        var C = this;
        var capability = newPromiseCapability(C);
        var reject = capability.reject;
        var result = perform(function () {
          forOf(iterable, false, function (promise) {
            C.resolve(promise).then(capability.resolve, reject);
          });
        });
        if (result.e) reject(result.v);
        return capability.promise;
      }
    });
    /***/
  },

  /***/
  "5ca1":
  /***/
  function ca1(module, exports, __webpack_require__) {
    var global = __webpack_require__("7726");

    var core = __webpack_require__("8378");

    var hide = __webpack_require__("32e9");

    var redefine = __webpack_require__("2aba");

    var ctx = __webpack_require__("9b43");

    var PROTOTYPE = 'prototype';

    var $export = function $export(type, name, source) {
      var IS_FORCED = type & $export.F;
      var IS_GLOBAL = type & $export.G;
      var IS_STATIC = type & $export.S;
      var IS_PROTO = type & $export.P;
      var IS_BIND = type & $export.B;
      var target = IS_GLOBAL ? global : IS_STATIC ? global[name] || (global[name] = {}) : (global[name] || {})[PROTOTYPE];
      var exports = IS_GLOBAL ? core : core[name] || (core[name] = {});
      var expProto = exports[PROTOTYPE] || (exports[PROTOTYPE] = {});
      var key, own, out, exp;
      if (IS_GLOBAL) source = name;

      for (key in source) {
        // contains in native
        own = !IS_FORCED && target && target[key] !== undefined; // export native or passed

        out = (own ? target : source)[key]; // bind timers to global for call from export context

        exp = IS_BIND && own ? ctx(out, global) : IS_PROTO && typeof out == 'function' ? ctx(Function.call, out) : out; // extend global

        if (target) redefine(target, key, out, type & $export.U); // export

        if (exports[key] != out) hide(exports, key, exp);
        if (IS_PROTO && expProto[key] != out) expProto[key] = out;
      }
    };

    global.core = core; // type bitmap

    $export.F = 1; // forced

    $export.G = 2; // global

    $export.S = 4; // static

    $export.P = 8; // proto

    $export.B = 16; // bind

    $export.W = 32; // wrap

    $export.U = 64; // safe

    $export.R = 128; // real proto method for `library`

    module.exports = $export;
    /***/
  },

  /***/
  "5ce7":
  /***/
  function ce7(module, exports, __webpack_require__) {
    "use strict";

    var create = __webpack_require__("7108");

    var descriptor = __webpack_require__("f845");

    var setToStringTag = __webpack_require__("c0d8");

    var IteratorPrototype = {}; // 25.1.2.1.1 %IteratorPrototype%[@@iterator]()

    __webpack_require__("8ce0")(IteratorPrototype, __webpack_require__("1b55")('iterator'), function () {
      return this;
    });

    module.exports = function (Constructor, NAME, next) {
      Constructor.prototype = create(IteratorPrototype, {
        next: descriptor(1, next)
      });
      setToStringTag(Constructor, NAME + ' Iterator');
    };
    /***/

  },

  /***/
  "5d8f":
  /***/
  function d8f(module, exports, __webpack_require__) {
    var shared = __webpack_require__("7772")('keys');

    var uid = __webpack_require__("7b00");

    module.exports = function (key) {
      return shared[key] || (shared[key] = uid(key));
    };
    /***/

  },

  /***/
  "613b":
  /***/
  function b(module, exports, __webpack_require__) {
    var shared = __webpack_require__("5537")('keys');

    var uid = __webpack_require__("ca5a");

    module.exports = function (key) {
      return shared[key] || (shared[key] = uid(key));
    };
    /***/

  },

  /***/
  "626a":
  /***/
  function a(module, exports, __webpack_require__) {
    // fallback for non-array-like ES3 and non-enumerable old V8 strings
    var cof = __webpack_require__("2d95"); // eslint-disable-next-line no-prototype-builtins


    module.exports = Object('z').propertyIsEnumerable(0) ? Object : function (it) {
      return cof(it) == 'String' ? it.split('') : Object(it);
    };
    /***/
  },

  /***/
  "67ab":
  /***/
  function ab(module, exports, __webpack_require__) {
    var META = __webpack_require__("ca5a")('meta');

    var isObject = __webpack_require__("d3f4");

    var has = __webpack_require__("69a8");

    var setDesc = __webpack_require__("86cc").f;

    var id = 0;

    var isExtensible = _Object$isExtensible || function () {
      return true;
    };

    var FREEZE = !__webpack_require__("79e5")(function () {
      return isExtensible(_Object$preventExtensions({}));
    });

    var setMeta = function setMeta(it) {
      setDesc(it, META, {
        value: {
          i: 'O' + ++id,
          // object ID
          w: {} // weak collections IDs

        }
      });
    };

    var fastKey = function fastKey(it, create) {
      // return primitive with prefix
      if (!isObject(it)) return _typeof2(it) == 'symbol' ? it : (typeof it == 'string' ? 'S' : 'P') + it;

      if (!has(it, META)) {
        // can't set metadata to uncaught frozen object
        if (!isExtensible(it)) return 'F'; // not necessary to add metadata

        if (!create) return 'E'; // add missing metadata

        setMeta(it); // return object ID
      }

      return it[META].i;
    };

    var getWeak = function getWeak(it, create) {
      if (!has(it, META)) {
        // can't set metadata to uncaught frozen object
        if (!isExtensible(it)) return true; // not necessary to add metadata

        if (!create) return false; // add missing metadata

        setMeta(it); // return hash weak collections IDs
      }

      return it[META].w;
    }; // add metadata on freeze-family methods calling


    var onFreeze = function onFreeze(it) {
      if (FREEZE && meta.NEED && isExtensible(it) && !has(it, META)) setMeta(it);
      return it;
    };

    var meta = module.exports = {
      KEY: META,
      NEED: false,
      fastKey: fastKey,
      getWeak: getWeak,
      onFreeze: onFreeze
    };
    /***/
  },

  /***/
  "6821":
  /***/
  function _(module, exports, __webpack_require__) {
    // to indexed object, toObject with fallback for non-array-like ES3 strings
    var IObject = __webpack_require__("626a");

    var defined = __webpack_require__("be13");

    module.exports = function (it) {
      return IObject(defined(it));
    };
    /***/

  },

  /***/
  "69a8":
  /***/
  function a8(module, exports) {
    var hasOwnProperty = {}.hasOwnProperty;

    module.exports = function (it, key) {
      return hasOwnProperty.call(it, key);
    };
    /***/

  },

  /***/
  "6a99":
  /***/
  function a99(module, exports, __webpack_require__) {
    // 7.1.1 ToPrimitive(input [, PreferredType])
    var isObject = __webpack_require__("d3f4"); // instead of the ES6 spec version, we didn't implement @@toPrimitive case
    // and the second argument - flag - preferred type is a string


    module.exports = function (it, S) {
      if (!isObject(it)) return it;
      var fn, val;
      if (S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
      if (typeof (fn = it.valueOf) == 'function' && !isObject(val = fn.call(it))) return val;
      if (!S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
      throw TypeError("Can't convert object to primitive value");
    };
    /***/

  },

  /***/
  "6a9b":
  /***/
  function a9b(module, exports, __webpack_require__) {
    // to indexed object, toObject with fallback for non-array-like ES3 strings
    var IObject = __webpack_require__("8bab");

    var defined = __webpack_require__("e5fa");

    module.exports = function (it) {
      return IObject(defined(it));
    };
    /***/

  },

  /***/
  "6be5":
  /***/
  function be5(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    /* harmony import */

    var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_FieldHeader_vue_vue_type_style_index_0_id_341e5fe8_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("917c");
    /* harmony import */


    var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_FieldHeader_vue_vue_type_style_index_0_id_341e5fe8_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default =
    /*#__PURE__*/
    __webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_FieldHeader_vue_vue_type_style_index_0_id_341e5fe8_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
    /* unused harmony reexport * */

    /* unused harmony default export */


    var _unused_webpack_default_export = _node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_FieldHeader_vue_vue_type_style_index_0_id_341e5fe8_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a;
    /***/
  },

  /***/
  "6e1f":
  /***/
  function e1f(module, exports) {
    var toString = {}.toString;

    module.exports = function (it) {
      return toString.call(it).slice(8, -1);
    };
    /***/

  },

  /***/
  "6f8a":
  /***/
  function f8a(module, exports) {
    module.exports = function (it) {
      return _typeof2(it) === 'object' ? it !== null : typeof it === 'function';
    };
    /***/

  },

  /***/
  "7108":
  /***/
  function _(module, exports, __webpack_require__) {
    // 19.1.2.2 / 15.2.3.5 Object.create(O [, Properties])
    var anObject = __webpack_require__("0f89");

    var dPs = __webpack_require__("f568");

    var enumBugKeys = __webpack_require__("0029");

    var IE_PROTO = __webpack_require__("5d8f")('IE_PROTO');

    var Empty = function Empty() {
      /* empty */
    };

    var PROTOTYPE = 'prototype'; // Create object with fake `null` prototype: use iframe Object with cleared prototype

    var _createDict2 = function createDict() {
      // Thrash, waste and sodomy: IE GC bug
      var iframe = __webpack_require__("12fd")('iframe');

      var i = enumBugKeys.length;
      var lt = '<';
      var gt = '>';
      var iframeDocument;
      iframe.style.display = 'none';

      __webpack_require__("103a").appendChild(iframe);

      iframe.src = 'javascript:'; // eslint-disable-line no-script-url
      // createDict = iframe.contentWindow.Object;
      // html.removeChild(iframe);

      iframeDocument = iframe.contentWindow.document;
      iframeDocument.open();
      iframeDocument.write(lt + 'script' + gt + 'document.F=Object' + lt + '/script' + gt);
      iframeDocument.close();
      _createDict2 = iframeDocument.F;

      while (i--) {
        delete _createDict2[PROTOTYPE][enumBugKeys[i]];
      }

      return _createDict2();
    };

    module.exports = _Object$create || function create(O, Properties) {
      var result;

      if (O !== null) {
        Empty[PROTOTYPE] = anObject(O);
        result = new Empty();
        Empty[PROTOTYPE] = null; // add "__proto__" for Object.getPrototypeOf polyfill

        result[IE_PROTO] = O;
      } else result = _createDict2();

      return Properties === undefined ? result : dPs(result, Properties);
    };
    /***/

  },

  /***/
  "75c9":
  /***/
  function c9(module, exports) {
    module.exports = function (exec) {
      try {
        return {
          e: false,
          v: exec()
        };
      } catch (e) {
        return {
          e: true,
          v: e
        };
      }
    };
    /***/

  },

  /***/
  "7633":
  /***/
  function _(module, exports, __webpack_require__) {
    // 19.1.2.14 / 15.2.3.14 Object.keys(O)
    var $keys = __webpack_require__("2695");

    var enumBugKeys = __webpack_require__("0029");

    module.exports = _Object$keys || function keys(O) {
      return $keys(O, enumBugKeys);
    };
    /***/

  },

  /***/
  "7726":
  /***/
  function _(module, exports) {
    // https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
    var global = module.exports = typeof window != 'undefined' && window.Math == Math ? window : typeof self != 'undefined' && self.Math == Math ? self // eslint-disable-next-line no-new-func
    : Function('return this')();
    if (typeof __g == 'number') __g = global; // eslint-disable-line no-undef

    /***/
  },

  /***/
  "7772":
  /***/
  function _(module, exports, __webpack_require__) {
    var core = __webpack_require__("a7d3");

    var global = __webpack_require__("da3c");

    var SHARED = '__core-js_shared__';
    var store = global[SHARED] || (global[SHARED] = {});
    (module.exports = function (key, value) {
      return store[key] || (store[key] = value !== undefined ? value : {});
    })('versions', []).push({
      version: core.version,
      mode: __webpack_require__("b457") ? 'pure' : 'global',
      copyright: '© 2019 Denis Pushkarev (zloirock.ru)'
    });
    /***/
  },

  /***/
  "77f1":
  /***/
  function f1(module, exports, __webpack_require__) {
    var toInteger = __webpack_require__("4588");

    var max = Math.max;
    var min = Math.min;

    module.exports = function (index, length) {
      index = toInteger(index);
      return index < 0 ? max(index + length, 0) : min(index, length);
    };
    /***/

  },

  /***/
  "795b":
  /***/
  function b(module, exports, __webpack_require__) {
    module.exports = __webpack_require__("dd04");
    /***/
  },

  /***/
  "79e5":
  /***/
  function e5(module, exports) {
    module.exports = function (exec) {
      try {
        return !!exec();
      } catch (e) {
        return true;
      }
    };
    /***/

  },

  /***/
  "7b00":
  /***/
  function b00(module, exports) {
    var id = 0;
    var px = Math.random();

    module.exports = function (key) {
      return 'Symbol('.concat(key === undefined ? '' : key, ')_', (++id + px).toString(36));
    };
    /***/

  },

  /***/
  "7bbc":
  /***/
  function bbc(module, exports, __webpack_require__) {
    // fallback for IE11 buggy Object.getOwnPropertyNames with iframe and window
    var toIObject = __webpack_require__("6821");

    var gOPN = __webpack_require__("9093").f;

    var toString = {}.toString;
    var windowNames = (typeof window === "undefined" ? "undefined" : _typeof2(window)) == 'object' && window && _Object$getOwnPropertyNames ? _Object$getOwnPropertyNames(window) : [];

    var getWindowNames = function getWindowNames(it) {
      try {
        return gOPN(it);
      } catch (e) {
        return windowNames.slice();
      }
    };

    module.exports.f = function getOwnPropertyNames(it) {
      return windowNames && toString.call(it) == '[object Window]' ? getWindowNames(it) : gOPN(toIObject(it));
    };
    /***/

  },

  /***/
  "7d8a":
  /***/
  function d8a(module, exports, __webpack_require__) {
    // getting tag from 19.1.3.6 Object.prototype.toString()
    var cof = __webpack_require__("6e1f");

    var TAG = __webpack_require__("1b55")('toStringTag'); // ES3 wrong here


    var ARG = cof(function () {
      return arguments;
    }()) == 'Arguments'; // fallback for IE11 Script Access Denied error

    var tryGet = function tryGet(it, key) {
      try {
        return it[key];
      } catch (e) {
        /* empty */
      }
    };

    module.exports = function (it) {
      var O, T, B;
      return it === undefined ? 'Undefined' : it === null ? 'Null' // @@toStringTag case
      : typeof (T = tryGet(O = Object(it), TAG)) == 'string' ? T // builtinTag case
      : ARG ? cof(O) // ES3 arguments fallback
      : (B = cof(O)) == 'Object' && typeof O.callee == 'function' ? 'Arguments' : B;
    };
    /***/

  },

  /***/
  "7d95":
  /***/
  function d95(module, exports, __webpack_require__) {
    // Thank's IE8 for his funny defineProperty
    module.exports = !__webpack_require__("d782")(function () {
      return Object.defineProperty({}, 'a', {
        get: function get() {
          return 7;
        }
      }).a != 7;
    });
    /***/
  },

  /***/
  "7f20":
  /***/
  function f20(module, exports, __webpack_require__) {
    var def = __webpack_require__("86cc").f;

    var has = __webpack_require__("69a8");

    var TAG = __webpack_require__("2b4c")('toStringTag');

    module.exports = function (it, tag, stat) {
      if (it && !has(it = stat ? it : it.prototype, TAG)) def(it, TAG, {
        configurable: true,
        value: tag
      });
    };
    /***/

  },

  /***/
  "8378":
  /***/
  function _(module, exports) {
    var core = module.exports = {
      version: '2.6.1'
    };
    if (typeof __e == 'number') __e = core; // eslint-disable-line no-undef

    /***/
  },

  /***/
  "8496":
  /***/
  function _(module, exports, __webpack_require__) {// extracted by mini-css-extract-plugin

    /***/
  },

  /***/
  "84f2":
  /***/
  function f2(module, exports) {
    module.exports = {};
    /***/
  },

  /***/
  "85fe":
  /***/
  function fe(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    /* WEBPACK VAR INJECTION */

    (function (global) {
      /* harmony export (binding) */
      __webpack_require__.d(__webpack_exports__, "a", function () {
        return ObserveVisibility;
      });
      /* unused harmony export install */


      function _typeof(obj) {
        if (typeof _Symbol === "function" && _typeof2(_Symbol$iterator) === "symbol") {
          _typeof = function _typeof(obj) {
            return _typeof2(obj);
          };
        } else {
          _typeof = function _typeof(obj) {
            return obj && typeof _Symbol === "function" && obj.constructor === _Symbol && obj !== _Symbol.prototype ? "symbol" : _typeof2(obj);
          };
        }

        return _typeof(obj);
      }

      function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
          throw new TypeError("Cannot call a class as a function");
        }
      }

      function _defineProperties(target, props) {
        for (var i = 0; i < props.length; i++) {
          var descriptor = props[i];
          descriptor.enumerable = descriptor.enumerable || false;
          descriptor.configurable = true;
          if ("value" in descriptor) descriptor.writable = true;

          _Object$defineProperty(target, descriptor.key, descriptor);
        }
      }

      function _createClass(Constructor, protoProps, staticProps) {
        if (protoProps) _defineProperties(Constructor.prototype, protoProps);
        if (staticProps) _defineProperties(Constructor, staticProps);
        return Constructor;
      }

      function _toConsumableArray(arr) {
        return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread();
      }

      function _arrayWithoutHoles(arr) {
        if (_Array$isArray(arr)) {
          for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) {
            arr2[i] = arr[i];
          }

          return arr2;
        }
      }

      function _iterableToArray(iter) {
        if (_isIterable(Object(iter)) || Object.prototype.toString.call(iter) === "[object Arguments]") return _Array$from(iter);
      }

      function _nonIterableSpread() {
        throw new TypeError("Invalid attempt to spread non-iterable instance");
      }

      function processOptions(value) {
        var options;

        if (typeof value === 'function') {
          // Simple options (callback-only)
          options = {
            callback: value
          };
        } else {
          // Options object
          options = value;
        }

        return options;
      }

      function throttle(callback, delay) {
        var timeout;
        var lastState;
        var currentArgs;

        var throttled = function throttled(state) {
          for (var _len = arguments.length, args = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
            args[_key - 1] = arguments[_key];
          }

          currentArgs = args;
          if (timeout && state === lastState) return;
          lastState = state;
          clearTimeout(timeout);
          timeout = setTimeout(function () {
            callback.apply(void 0, [state].concat(_toConsumableArray(currentArgs)));
            timeout = 0;
          }, delay);
        };

        throttled._clear = function () {
          clearTimeout(timeout);
        };

        return throttled;
      }

      function deepEqual(val1, val2) {
        if (val1 === val2) return true;

        if (_typeof(val1) === 'object') {
          for (var key in val1) {
            if (!deepEqual(val1[key], val2[key])) {
              return false;
            }
          }

          return true;
        }

        return false;
      }

      var VisibilityState =
      /*#__PURE__*/
      function () {
        function VisibilityState(el, options, vnode) {
          _classCallCheck(this, VisibilityState);

          this.el = el;
          this.observer = null;
          this.frozen = false;
          this.createObserver(options, vnode);
        }

        _createClass(VisibilityState, [{
          key: "createObserver",
          value: function createObserver(options, vnode) {
            var _this = this;

            if (this.observer) {
              this.destroyObserver();
            }

            if (this.frozen) return;
            this.options = processOptions(options);

            this.callback = function (result, entry) {
              _this.options.callback(result, entry);

              if (result && _this.options.once) {
                _this.frozen = true;

                _this.destroyObserver();
              }
            }; // Throttle


            if (this.callback && this.options.throttle) {
              this.callback = throttle(this.callback, this.options.throttle);
            }

            this.oldResult = undefined;
            this.observer = new IntersectionObserver(function (entries) {
              var entry = entries[0];

              if (_this.callback) {
                // Use isIntersecting if possible because browsers can report isIntersecting as true, but intersectionRatio as 0, when something very slowly enters the viewport.
                var result = entry.isIntersecting && entry.intersectionRatio >= _this.threshold;
                if (result === _this.oldResult) return;
                _this.oldResult = result;

                _this.callback(result, entry);
              }
            }, this.options.intersection); // Wait for the element to be in document

            vnode.context.$nextTick(function () {
              if (_this.observer) {
                _this.observer.observe(_this.el);
              }
            });
          }
        }, {
          key: "destroyObserver",
          value: function destroyObserver() {
            if (this.observer) {
              this.observer.disconnect();
              this.observer = null;
            } // Cancel throttled call


            if (this.callback && this.callback._clear) {
              this.callback._clear();

              this.callback = null;
            }
          }
        }, {
          key: "threshold",
          get: function get() {
            return this.options.intersection && this.options.intersection.threshold || 0;
          }
        }]);

        return VisibilityState;
      }();

      function bind(el, _ref, vnode) {
        var value = _ref.value;
        if (!value) return;

        if (typeof IntersectionObserver === 'undefined') {
          console.warn('[vue-observe-visibility] IntersectionObserver API is not available in your browser. Please install this polyfill: https://github.com/w3c/IntersectionObserver/tree/master/polyfill');
        } else {
          var state = new VisibilityState(el, value, vnode);
          el._vue_visibilityState = state;
        }
      }

      function update(el, _ref2, vnode) {
        var value = _ref2.value,
            oldValue = _ref2.oldValue;
        if (deepEqual(value, oldValue)) return;
        var state = el._vue_visibilityState;

        if (!value) {
          unbind(el);
          return;
        }

        if (state) {
          state.createObserver(value, vnode);
        } else {
          bind(el, {
            value: value
          }, vnode);
        }
      }

      function unbind(el) {
        var state = el._vue_visibilityState;

        if (state) {
          state.destroyObserver();
          delete el._vue_visibilityState;
        }
      }

      var ObserveVisibility = {
        bind: bind,
        update: update,
        unbind: unbind
      };

      function install(Vue) {
        Vue.directive('observe-visibility', ObserveVisibility);
        /* -- Add more components here -- */
      }
      /* -- Plugin definition & Auto-install -- */

      /* You shouldn't have to modify the code below */
      // Plugin


      var plugin = {
        // eslint-disable-next-line no-undef
        version: "0.4.4",
        install: install
      };
      var GlobalVue = null;

      if (typeof window !== 'undefined') {
        GlobalVue = window.Vue;
      } else if (typeof global !== 'undefined') {
        GlobalVue = global.Vue;
      }

      if (GlobalVue) {
        GlobalVue.use(plugin);
      }
      /* unused harmony default export */


      var _unused_webpack_default_export = plugin;
      /* WEBPACK VAR INJECTION */
    }).call(this, __webpack_require__("c8ba"));
    /***/
  },

  /***/
  "86cc":
  /***/
  function cc(module, exports, __webpack_require__) {
    var anObject = __webpack_require__("cb7c");

    var IE8_DOM_DEFINE = __webpack_require__("c69a");

    var toPrimitive = __webpack_require__("6a99");

    var dP = _Object$defineProperty;
    exports.f = __webpack_require__("9e1e") ? _Object$defineProperty : function defineProperty(O, P, Attributes) {
      anObject(O);
      P = toPrimitive(P, true);
      anObject(Attributes);
      if (IE8_DOM_DEFINE) try {
        return dP(O, P, Attributes);
      } catch (e) {
        /* empty */
      }
      if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported!');
      if ('value' in Attributes) O[P] = Attributes.value;
      return O;
    };
    /***/
  },

  /***/
  "8a12":
  /***/
  function a12(module, exports, __webpack_require__) {
    var global = __webpack_require__("da3c");

    var navigator = global.navigator;
    module.exports = navigator && navigator.userAgent || '';
    /***/
  },

  /***/
  "8a81":
  /***/
  function a81(module, exports, __webpack_require__) {
    "use strict"; // ECMAScript 6 symbols shim

    var global = __webpack_require__("7726");

    var has = __webpack_require__("69a8");

    var DESCRIPTORS = __webpack_require__("9e1e");

    var $export = __webpack_require__("5ca1");

    var redefine = __webpack_require__("2aba");

    var META = __webpack_require__("67ab").KEY;

    var $fails = __webpack_require__("79e5");

    var shared = __webpack_require__("5537");

    var setToStringTag = __webpack_require__("7f20");

    var uid = __webpack_require__("ca5a");

    var wks = __webpack_require__("2b4c");

    var wksExt = __webpack_require__("37c8");

    var wksDefine = __webpack_require__("3a72");

    var enumKeys = __webpack_require__("d4c0");

    var isArray = __webpack_require__("1169");

    var anObject = __webpack_require__("cb7c");

    var isObject = __webpack_require__("d3f4");

    var toIObject = __webpack_require__("6821");

    var toPrimitive = __webpack_require__("6a99");

    var createDesc = __webpack_require__("4630");

    var _create = __webpack_require__("2aeb");

    var gOPNExt = __webpack_require__("7bbc");

    var $GOPD = __webpack_require__("11e9");

    var $DP = __webpack_require__("86cc");

    var $keys = __webpack_require__("0d58");

    var gOPD = $GOPD.f;
    var dP = $DP.f;
    var gOPN = gOPNExt.f;
    var $Symbol = global.Symbol;
    var $JSON = global.JSON;

    var _stringify = $JSON && $JSON.stringify;

    var PROTOTYPE = 'prototype';
    var HIDDEN = wks('_hidden');
    var TO_PRIMITIVE = wks('toPrimitive');
    var isEnum = {}.propertyIsEnumerable;
    var SymbolRegistry = shared('symbol-registry');
    var AllSymbols = shared('symbols');
    var OPSymbols = shared('op-symbols');
    var ObjectProto = Object[PROTOTYPE];
    var USE_NATIVE = typeof $Symbol == 'function';
    var QObject = global.QObject; // Don't use setters in Qt Script, https://github.com/zloirock/core-js/issues/173

    var setter = !QObject || !QObject[PROTOTYPE] || !QObject[PROTOTYPE].findChild; // fallback for old Android, https://code.google.com/p/v8/issues/detail?id=687

    var setSymbolDesc = DESCRIPTORS && $fails(function () {
      return _create(dP({}, 'a', {
        get: function get() {
          return dP(this, 'a', {
            value: 7
          }).a;
        }
      })).a != 7;
    }) ? function (it, key, D) {
      var protoDesc = gOPD(ObjectProto, key);
      if (protoDesc) delete ObjectProto[key];
      dP(it, key, D);
      if (protoDesc && it !== ObjectProto) dP(ObjectProto, key, protoDesc);
    } : dP;

    var wrap = function wrap(tag) {
      var sym = AllSymbols[tag] = _create($Symbol[PROTOTYPE]);

      sym._k = tag;
      return sym;
    };

    var isSymbol = USE_NATIVE && _typeof2($Symbol.iterator) == 'symbol' ? function (it) {
      return _typeof2(it) == 'symbol';
    } : function (it) {
      return it instanceof $Symbol;
    };

    var $defineProperty = function defineProperty(it, key, D) {
      if (it === ObjectProto) $defineProperty(OPSymbols, key, D);
      anObject(it);
      key = toPrimitive(key, true);
      anObject(D);

      if (has(AllSymbols, key)) {
        if (!D.enumerable) {
          if (!has(it, HIDDEN)) dP(it, HIDDEN, createDesc(1, {}));
          it[HIDDEN][key] = true;
        } else {
          if (has(it, HIDDEN) && it[HIDDEN][key]) it[HIDDEN][key] = false;
          D = _create(D, {
            enumerable: createDesc(0, false)
          });
        }

        return setSymbolDesc(it, key, D);
      }

      return dP(it, key, D);
    };

    var $defineProperties = function defineProperties(it, P) {
      anObject(it);
      var keys = enumKeys(P = toIObject(P));
      var i = 0;
      var l = keys.length;
      var key;

      while (l > i) {
        $defineProperty(it, key = keys[i++], P[key]);
      }

      return it;
    };

    var $create = function create(it, P) {
      return P === undefined ? _create(it) : $defineProperties(_create(it), P);
    };

    var $propertyIsEnumerable = function propertyIsEnumerable(key) {
      var E = isEnum.call(this, key = toPrimitive(key, true));
      if (this === ObjectProto && has(AllSymbols, key) && !has(OPSymbols, key)) return false;
      return E || !has(this, key) || !has(AllSymbols, key) || has(this, HIDDEN) && this[HIDDEN][key] ? E : true;
    };

    var $getOwnPropertyDescriptor = function getOwnPropertyDescriptor(it, key) {
      it = toIObject(it);
      key = toPrimitive(key, true);
      if (it === ObjectProto && has(AllSymbols, key) && !has(OPSymbols, key)) return;
      var D = gOPD(it, key);
      if (D && has(AllSymbols, key) && !(has(it, HIDDEN) && it[HIDDEN][key])) D.enumerable = true;
      return D;
    };

    var $getOwnPropertyNames = function getOwnPropertyNames(it) {
      var names = gOPN(toIObject(it));
      var result = [];
      var i = 0;
      var key;

      while (names.length > i) {
        if (!has(AllSymbols, key = names[i++]) && key != HIDDEN && key != META) result.push(key);
      }

      return result;
    };

    var $getOwnPropertySymbols = function getOwnPropertySymbols(it) {
      var IS_OP = it === ObjectProto;
      var names = gOPN(IS_OP ? OPSymbols : toIObject(it));
      var result = [];
      var i = 0;
      var key;

      while (names.length > i) {
        if (has(AllSymbols, key = names[i++]) && (IS_OP ? has(ObjectProto, key) : true)) result.push(AllSymbols[key]);
      }

      return result;
    }; // 19.4.1.1 Symbol([description])


    if (!USE_NATIVE) {
      $Symbol = function _Symbol4() {
        if (this instanceof $Symbol) throw TypeError('Symbol is not a constructor!');
        var tag = uid(arguments.length > 0 ? arguments[0] : undefined);

        var $set = function $set(value) {
          if (this === ObjectProto) $set.call(OPSymbols, value);
          if (has(this, HIDDEN) && has(this[HIDDEN], tag)) this[HIDDEN][tag] = false;
          setSymbolDesc(this, tag, createDesc(1, value));
        };

        if (DESCRIPTORS && setter) setSymbolDesc(ObjectProto, tag, {
          configurable: true,
          set: $set
        });
        return wrap(tag);
      };

      redefine($Symbol[PROTOTYPE], 'toString', function toString() {
        return this._k;
      });
      $GOPD.f = $getOwnPropertyDescriptor;
      $DP.f = $defineProperty;
      __webpack_require__("9093").f = gOPNExt.f = $getOwnPropertyNames;
      __webpack_require__("52a7").f = $propertyIsEnumerable;
      __webpack_require__("2621").f = $getOwnPropertySymbols;

      if (DESCRIPTORS && !__webpack_require__("2d00")) {
        redefine(ObjectProto, 'propertyIsEnumerable', $propertyIsEnumerable, true);
      }

      wksExt.f = function (name) {
        return wrap(wks(name));
      };
    }

    $export($export.G + $export.W + $export.F * !USE_NATIVE, {
      Symbol: $Symbol
    });

    for (var es6Symbols = // 19.4.2.2, 19.4.2.3, 19.4.2.4, 19.4.2.6, 19.4.2.8, 19.4.2.9, 19.4.2.10, 19.4.2.11, 19.4.2.12, 19.4.2.13, 19.4.2.14
    'hasInstance,isConcatSpreadable,iterator,match,replace,search,species,split,toPrimitive,toStringTag,unscopables'.split(','), j = 0; es6Symbols.length > j;) {
      wks(es6Symbols[j++]);
    }

    for (var wellKnownSymbols = $keys(wks.store), k = 0; wellKnownSymbols.length > k;) {
      wksDefine(wellKnownSymbols[k++]);
    }

    $export($export.S + $export.F * !USE_NATIVE, 'Symbol', {
      // 19.4.2.1 Symbol.for(key)
      'for': function _for(key) {
        return has(SymbolRegistry, key += '') ? SymbolRegistry[key] : SymbolRegistry[key] = $Symbol(key);
      },
      // 19.4.2.5 Symbol.keyFor(sym)
      keyFor: function keyFor(sym) {
        if (!isSymbol(sym)) throw TypeError(sym + ' is not a symbol!');

        for (var key in SymbolRegistry) {
          if (SymbolRegistry[key] === sym) return key;
        }
      },
      useSetter: function useSetter() {
        setter = true;
      },
      useSimple: function useSimple() {
        setter = false;
      }
    });
    $export($export.S + $export.F * !USE_NATIVE, 'Object', {
      // 19.1.2.2 Object.create(O [, Properties])
      create: $create,
      // 19.1.2.4 Object.defineProperty(O, P, Attributes)
      defineProperty: $defineProperty,
      // 19.1.2.3 Object.defineProperties(O, Properties)
      defineProperties: $defineProperties,
      // 19.1.2.6 Object.getOwnPropertyDescriptor(O, P)
      getOwnPropertyDescriptor: $getOwnPropertyDescriptor,
      // 19.1.2.7 Object.getOwnPropertyNames(O)
      getOwnPropertyNames: $getOwnPropertyNames,
      // 19.1.2.8 Object.getOwnPropertySymbols(O)
      getOwnPropertySymbols: $getOwnPropertySymbols
    }); // 24.3.2 JSON.stringify(value [, replacer [, space]])

    $JSON && $export($export.S + $export.F * (!USE_NATIVE || $fails(function () {
      var S = $Symbol(); // MS Edge converts symbol values to JSON as {}
      // WebKit converts symbol values to JSON as null
      // V8 throws on boxed symbols

      return _stringify([S]) != '[null]' || _stringify({
        a: S
      }) != '{}' || _stringify(Object(S)) != '{}';
    })), 'JSON', {
      stringify: function stringify(it) {
        var args = [it];
        var i = 1;
        var replacer, $replacer;

        while (arguments.length > i) {
          args.push(arguments[i++]);
        }

        $replacer = replacer = args[1];
        if (!isObject(replacer) && it === undefined || isSymbol(it)) return; // IE8 returns string on undefined

        if (!isArray(replacer)) replacer = function replacer(key, value) {
          if (typeof $replacer == 'function') value = $replacer.call(this, key, value);
          if (!isSymbol(value)) return value;
        };
        args[1] = replacer;
        return _stringify.apply($JSON, args);
      }
    }); // 19.4.3.4 Symbol.prototype[@@toPrimitive](hint)

    $Symbol[PROTOTYPE][TO_PRIMITIVE] || __webpack_require__("32e9")($Symbol[PROTOTYPE], TO_PRIMITIVE, $Symbol[PROTOTYPE].valueOf); // 19.4.3.5 Symbol.prototype[@@toStringTag]

    setToStringTag($Symbol, 'Symbol'); // 20.2.1.9 Math[@@toStringTag]

    setToStringTag(Math, 'Math', true); // 24.3.3 JSON[@@toStringTag]

    setToStringTag(global.JSON, 'JSON', true);
    /***/
  },

  /***/
  "8bab":
  /***/
  function bab(module, exports, __webpack_require__) {
    // fallback for non-array-like ES3 and non-enumerable old V8 strings
    var cof = __webpack_require__("6e1f"); // eslint-disable-next-line no-prototype-builtins


    module.exports = Object('z').propertyIsEnumerable(0) ? Object : function (it) {
      return cof(it) == 'String' ? it.split('') : Object(it);
    };
    /***/
  },

  /***/
  "8bbf":
  /***/
  function bbf(module, exports) {
    module.exports = __webpack_require__("8bbf");
    /***/
  },

  /***/
  "8ce0":
  /***/
  function ce0(module, exports, __webpack_require__) {
    var dP = __webpack_require__("3adc");

    var createDesc = __webpack_require__("f845");

    module.exports = __webpack_require__("7d95") ? function (object, key, value) {
      return dP.f(object, key, createDesc(1, value));
    } : function (object, key, value) {
      object[key] = value;
      return object;
    };
    /***/
  },

  /***/
  "8ffe":
  /***/
  function ffe(module, exports, __webpack_require__) {// extracted by mini-css-extract-plugin

    /***/
  },

  /***/
  "9093":
  /***/
  function _(module, exports, __webpack_require__) {
    // 19.1.2.7 / 15.2.3.4 Object.getOwnPropertyNames(O)
    var $keys = __webpack_require__("ce10");

    var hiddenKeys = __webpack_require__("e11e").concat('length', 'prototype');

    exports.f = _Object$getOwnPropertyNames || function getOwnPropertyNames(O) {
      return $keys(O, hiddenKeys);
    };
    /***/

  },

  /***/
  "917c":
  /***/
  function c(module, exports, __webpack_require__) {// extracted by mini-css-extract-plugin

    /***/
  },

  /***/
  "93c4":
  /***/
  function c4(module, exports, __webpack_require__) {
    "use strict";

    var $at = __webpack_require__("2a4e")(true); // 21.1.3.27 String.prototype[@@iterator]()


    __webpack_require__("e4a9")(String, 'String', function (iterated) {
      this._t = String(iterated); // target

      this._i = 0; // next index
      // 21.1.5.2.1 %StringIteratorPrototype%.next()
    }, function () {
      var O = this._t;
      var index = this._i;
      var point;
      if (index >= O.length) return {
        value: undefined,
        done: true
      };
      point = $at(O, index);
      this._i += point.length;
      return {
        value: point,
        done: false
      };
    });
    /***/

  },

  /***/
  "96cf":
  /***/
  function cf(module, exports, __webpack_require__) {
    /**
     * Copyright (c) 2014-present, Facebook, Inc.
     *
     * This source code is licensed under the MIT license found in the
     * LICENSE file in the root directory of this source tree.
     */
    var runtime = function (exports) {
      "use strict";

      var Op = Object.prototype;
      var hasOwn = Op.hasOwnProperty;
      var undefined; // More compressible than void 0.

      var $Symbol = typeof _Symbol === "function" ? _Symbol : {};
      var iteratorSymbol = $Symbol.iterator || "@@iterator";
      var asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator";
      var toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag";

      function wrap(innerFn, outerFn, self, tryLocsList) {
        // If outerFn provided and outerFn.prototype is a Generator, then outerFn.prototype instanceof Generator.
        var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator;

        var generator = _Object$create(protoGenerator.prototype);

        var context = new Context(tryLocsList || []); // The ._invoke method unifies the implementations of the .next,
        // .throw, and .return methods.

        generator._invoke = makeInvokeMethod(innerFn, self, context);
        return generator;
      }

      exports.wrap = wrap; // Try/catch helper to minimize deoptimizations. Returns a completion
      // record like context.tryEntries[i].completion. This interface could
      // have been (and was previously) designed to take a closure to be
      // invoked without arguments, but in all the cases we care about we
      // already have an existing method we want to call, so there's no need
      // to create a new function object. We can even get away with assuming
      // the method takes exactly one argument, since that happens to be true
      // in every case, so we don't have to touch the arguments object. The
      // only additional allocation required is the completion record, which
      // has a stable shape and so hopefully should be cheap to allocate.

      function tryCatch(fn, obj, arg) {
        try {
          return {
            type: "normal",
            arg: fn.call(obj, arg)
          };
        } catch (err) {
          return {
            type: "throw",
            arg: err
          };
        }
      }

      var GenStateSuspendedStart = "suspendedStart";
      var GenStateSuspendedYield = "suspendedYield";
      var GenStateExecuting = "executing";
      var GenStateCompleted = "completed"; // Returning this object from the innerFn has the same effect as
      // breaking out of the dispatch switch statement.

      var ContinueSentinel = {}; // Dummy constructor functions that we use as the .constructor and
      // .constructor.prototype properties for functions that return Generator
      // objects. For full spec compliance, you may wish to configure your
      // minifier not to mangle the names of these two functions.

      function Generator() {}

      function GeneratorFunction() {}

      function GeneratorFunctionPrototype() {} // This is a polyfill for %IteratorPrototype% for environments that
      // don't natively support it.


      var IteratorPrototype = {};

      IteratorPrototype[iteratorSymbol] = function () {
        return this;
      };

      var getProto = _Object$getPrototypeOf;
      var NativeIteratorPrototype = getProto && getProto(getProto(values([])));

      if (NativeIteratorPrototype && NativeIteratorPrototype !== Op && hasOwn.call(NativeIteratorPrototype, iteratorSymbol)) {
        // This environment has a native %IteratorPrototype%; use it instead
        // of the polyfill.
        IteratorPrototype = NativeIteratorPrototype;
      }

      var Gp = GeneratorFunctionPrototype.prototype = Generator.prototype = _Object$create(IteratorPrototype);

      GeneratorFunction.prototype = Gp.constructor = GeneratorFunctionPrototype;
      GeneratorFunctionPrototype.constructor = GeneratorFunction;
      GeneratorFunctionPrototype[toStringTagSymbol] = GeneratorFunction.displayName = "GeneratorFunction"; // Helper for defining the .next, .throw, and .return methods of the
      // Iterator interface in terms of a single ._invoke method.

      function defineIteratorMethods(prototype) {
        ["next", "throw", "return"].forEach(function (method) {
          prototype[method] = function (arg) {
            return this._invoke(method, arg);
          };
        });
      }

      exports.isGeneratorFunction = function (genFun) {
        var ctor = typeof genFun === "function" && genFun.constructor;
        return ctor ? ctor === GeneratorFunction || // For the native GeneratorFunction constructor, the best we can
        // do is to check its .name property.
        (ctor.displayName || ctor.name) === "GeneratorFunction" : false;
      };

      exports.mark = function (genFun) {
        if (_Object$setPrototypeOf) {
          _Object$setPrototypeOf(genFun, GeneratorFunctionPrototype);
        } else {
          genFun.__proto__ = GeneratorFunctionPrototype;

          if (!(toStringTagSymbol in genFun)) {
            genFun[toStringTagSymbol] = "GeneratorFunction";
          }
        }

        genFun.prototype = _Object$create(Gp);
        return genFun;
      }; // Within the body of any async function, `await x` is transformed to
      // `yield regeneratorRuntime.awrap(x)`, so that the runtime can test
      // `hasOwn.call(value, "__await")` to determine if the yielded value is
      // meant to be awaited.


      exports.awrap = function (arg) {
        return {
          __await: arg
        };
      };

      function AsyncIterator(generator) {
        function invoke(method, arg, resolve, reject) {
          var record = tryCatch(generator[method], generator, arg);

          if (record.type === "throw") {
            reject(record.arg);
          } else {
            var result = record.arg;
            var value = result.value;

            if (value && _typeof2(value) === "object" && hasOwn.call(value, "__await")) {
              return _Promise.resolve(value.__await).then(function (value) {
                invoke("next", value, resolve, reject);
              }, function (err) {
                invoke("throw", err, resolve, reject);
              });
            }

            return _Promise.resolve(value).then(function (unwrapped) {
              // When a yielded Promise is resolved, its final value becomes
              // the .value of the Promise<{value,done}> result for the
              // current iteration.
              result.value = unwrapped;
              resolve(result);
            }, function (error) {
              // If a rejected Promise was yielded, throw the rejection back
              // into the async generator function so it can be handled there.
              return invoke("throw", error, resolve, reject);
            });
          }
        }

        var previousPromise;

        function enqueue(method, arg) {
          function callInvokeWithMethodAndArg() {
            return new _Promise(function (resolve, reject) {
              invoke(method, arg, resolve, reject);
            });
          }

          return previousPromise = // If enqueue has been called before, then we want to wait until
          // all previous Promises have been resolved before calling invoke,
          // so that results are always delivered in the correct order. If
          // enqueue has not been called before, then it is important to
          // call invoke immediately, without waiting on a callback to fire,
          // so that the async generator function has the opportunity to do
          // any necessary setup in a predictable way. This predictability
          // is why the Promise constructor synchronously invokes its
          // executor callback, and why async functions synchronously
          // execute code before the first await. Since we implement simple
          // async functions in terms of async generators, it is especially
          // important to get this right, even though it requires care.
          previousPromise ? previousPromise.then(callInvokeWithMethodAndArg, // Avoid propagating failures to Promises returned by later
          // invocations of the iterator.
          callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg();
        } // Define the unified helper method that is used to implement .next,
        // .throw, and .return (see defineIteratorMethods).


        this._invoke = enqueue;
      }

      defineIteratorMethods(AsyncIterator.prototype);

      AsyncIterator.prototype[asyncIteratorSymbol] = function () {
        return this;
      };

      exports.AsyncIterator = AsyncIterator; // Note that simple async functions are implemented on top of
      // AsyncIterator objects; they just return a Promise for the value of
      // the final result produced by the iterator.

      exports.async = function (innerFn, outerFn, self, tryLocsList) {
        var iter = new AsyncIterator(wrap(innerFn, outerFn, self, tryLocsList));
        return exports.isGeneratorFunction(outerFn) ? iter // If outerFn is a generator, return the full iterator.
        : iter.next().then(function (result) {
          return result.done ? result.value : iter.next();
        });
      };

      function makeInvokeMethod(innerFn, self, context) {
        var state = GenStateSuspendedStart;
        return function invoke(method, arg) {
          if (state === GenStateExecuting) {
            throw new Error("Generator is already running");
          }

          if (state === GenStateCompleted) {
            if (method === "throw") {
              throw arg;
            } // Be forgiving, per 25.3.3.3.3 of the spec:
            // https://people.mozilla.org/~jorendorff/es6-draft.html#sec-generatorresume


            return doneResult();
          }

          context.method = method;
          context.arg = arg;

          while (true) {
            var delegate = context.delegate;

            if (delegate) {
              var delegateResult = maybeInvokeDelegate(delegate, context);

              if (delegateResult) {
                if (delegateResult === ContinueSentinel) continue;
                return delegateResult;
              }
            }

            if (context.method === "next") {
              // Setting context._sent for legacy support of Babel's
              // function.sent implementation.
              context.sent = context._sent = context.arg;
            } else if (context.method === "throw") {
              if (state === GenStateSuspendedStart) {
                state = GenStateCompleted;
                throw context.arg;
              }

              context.dispatchException(context.arg);
            } else if (context.method === "return") {
              context.abrupt("return", context.arg);
            }

            state = GenStateExecuting;
            var record = tryCatch(innerFn, self, context);

            if (record.type === "normal") {
              // If an exception is thrown from innerFn, we leave state ===
              // GenStateExecuting and loop back for another invocation.
              state = context.done ? GenStateCompleted : GenStateSuspendedYield;

              if (record.arg === ContinueSentinel) {
                continue;
              }

              return {
                value: record.arg,
                done: context.done
              };
            } else if (record.type === "throw") {
              state = GenStateCompleted; // Dispatch the exception by looping back around to the
              // context.dispatchException(context.arg) call above.

              context.method = "throw";
              context.arg = record.arg;
            }
          }
        };
      } // Call delegate.iterator[context.method](context.arg) and handle the
      // result, either by returning a { value, done } result from the
      // delegate iterator, or by modifying context.method and context.arg,
      // setting context.delegate to null, and returning the ContinueSentinel.


      function maybeInvokeDelegate(delegate, context) {
        var method = delegate.iterator[context.method];

        if (method === undefined) {
          // A .throw or .return when the delegate iterator has no .throw
          // method always terminates the yield* loop.
          context.delegate = null;

          if (context.method === "throw") {
            // Note: ["return"] must be used for ES3 parsing compatibility.
            if (delegate.iterator["return"]) {
              // If the delegate iterator has a return method, give it a
              // chance to clean up.
              context.method = "return";
              context.arg = undefined;
              maybeInvokeDelegate(delegate, context);

              if (context.method === "throw") {
                // If maybeInvokeDelegate(context) changed context.method from
                // "return" to "throw", let that override the TypeError below.
                return ContinueSentinel;
              }
            }

            context.method = "throw";
            context.arg = new TypeError("The iterator does not provide a 'throw' method");
          }

          return ContinueSentinel;
        }

        var record = tryCatch(method, delegate.iterator, context.arg);

        if (record.type === "throw") {
          context.method = "throw";
          context.arg = record.arg;
          context.delegate = null;
          return ContinueSentinel;
        }

        var info = record.arg;

        if (!info) {
          context.method = "throw";
          context.arg = new TypeError("iterator result is not an object");
          context.delegate = null;
          return ContinueSentinel;
        }

        if (info.done) {
          // Assign the result of the finished delegate to the temporary
          // variable specified by delegate.resultName (see delegateYield).
          context[delegate.resultName] = info.value; // Resume execution at the desired location (see delegateYield).

          context.next = delegate.nextLoc; // If context.method was "throw" but the delegate handled the
          // exception, let the outer generator proceed normally. If
          // context.method was "next", forget context.arg since it has been
          // "consumed" by the delegate iterator. If context.method was
          // "return", allow the original .return call to continue in the
          // outer generator.

          if (context.method !== "return") {
            context.method = "next";
            context.arg = undefined;
          }
        } else {
          // Re-yield the result returned by the delegate method.
          return info;
        } // The delegate iterator is finished, so forget it and continue with
        // the outer generator.


        context.delegate = null;
        return ContinueSentinel;
      } // Define Generator.prototype.{next,throw,return} in terms of the
      // unified ._invoke helper method.


      defineIteratorMethods(Gp);
      Gp[toStringTagSymbol] = "Generator"; // A Generator should always return itself as the iterator object when the
      // @@iterator function is called on it. Some browsers' implementations of the
      // iterator prototype chain incorrectly implement this, causing the Generator
      // object to not be returned from this call. This ensures that doesn't happen.
      // See https://github.com/facebook/regenerator/issues/274 for more details.

      Gp[iteratorSymbol] = function () {
        return this;
      };

      Gp.toString = function () {
        return "[object Generator]";
      };

      function pushTryEntry(locs) {
        var entry = {
          tryLoc: locs[0]
        };

        if (1 in locs) {
          entry.catchLoc = locs[1];
        }

        if (2 in locs) {
          entry.finallyLoc = locs[2];
          entry.afterLoc = locs[3];
        }

        this.tryEntries.push(entry);
      }

      function resetTryEntry(entry) {
        var record = entry.completion || {};
        record.type = "normal";
        delete record.arg;
        entry.completion = record;
      }

      function Context(tryLocsList) {
        // The root entry object (effectively a try statement without a catch
        // or a finally block) gives us a place to store values thrown from
        // locations where there is no enclosing try statement.
        this.tryEntries = [{
          tryLoc: "root"
        }];
        tryLocsList.forEach(pushTryEntry, this);
        this.reset(true);
      }

      exports.keys = function (object) {
        var keys = [];

        for (var key in object) {
          keys.push(key);
        }

        keys.reverse(); // Rather than returning an object with a next method, we keep
        // things simple and return the next function itself.

        return function next() {
          while (keys.length) {
            var key = keys.pop();

            if (key in object) {
              next.value = key;
              next.done = false;
              return next;
            }
          } // To avoid creating an additional object, we just hang the .value
          // and .done properties off the next function object itself. This
          // also ensures that the minifier will not anonymize the function.


          next.done = true;
          return next;
        };
      };

      function values(iterable) {
        if (iterable) {
          var iteratorMethod = iterable[iteratorSymbol];

          if (iteratorMethod) {
            return iteratorMethod.call(iterable);
          }

          if (typeof iterable.next === "function") {
            return iterable;
          }

          if (!isNaN(iterable.length)) {
            var i = -1,
                next = function next() {
              while (++i < iterable.length) {
                if (hasOwn.call(iterable, i)) {
                  next.value = iterable[i];
                  next.done = false;
                  return next;
                }
              }

              next.value = undefined;
              next.done = true;
              return next;
            };

            return next.next = next;
          }
        } // Return an iterator with no values.


        return {
          next: doneResult
        };
      }

      exports.values = values;

      function doneResult() {
        return {
          value: undefined,
          done: true
        };
      }

      Context.prototype = {
        constructor: Context,
        reset: function reset(skipTempReset) {
          this.prev = 0;
          this.next = 0; // Resetting context._sent for legacy support of Babel's
          // function.sent implementation.

          this.sent = this._sent = undefined;
          this.done = false;
          this.delegate = null;
          this.method = "next";
          this.arg = undefined;
          this.tryEntries.forEach(resetTryEntry);

          if (!skipTempReset) {
            for (var name in this) {
              // Not sure about the optimal order of these conditions:
              if (name.charAt(0) === "t" && hasOwn.call(this, name) && !isNaN(+name.slice(1))) {
                this[name] = undefined;
              }
            }
          }
        },
        stop: function stop() {
          this.done = true;
          var rootEntry = this.tryEntries[0];
          var rootRecord = rootEntry.completion;

          if (rootRecord.type === "throw") {
            throw rootRecord.arg;
          }

          return this.rval;
        },
        dispatchException: function dispatchException(exception) {
          if (this.done) {
            throw exception;
          }

          var context = this;

          function handle(loc, caught) {
            record.type = "throw";
            record.arg = exception;
            context.next = loc;

            if (caught) {
              // If the dispatched exception was caught by a catch block,
              // then let that catch block handle the exception normally.
              context.method = "next";
              context.arg = undefined;
            }

            return !!caught;
          }

          for (var i = this.tryEntries.length - 1; i >= 0; --i) {
            var entry = this.tryEntries[i];
            var record = entry.completion;

            if (entry.tryLoc === "root") {
              // Exception thrown outside of any try block that could handle
              // it, so set the completion value of the entire function to
              // throw the exception.
              return handle("end");
            }

            if (entry.tryLoc <= this.prev) {
              var hasCatch = hasOwn.call(entry, "catchLoc");
              var hasFinally = hasOwn.call(entry, "finallyLoc");

              if (hasCatch && hasFinally) {
                if (this.prev < entry.catchLoc) {
                  return handle(entry.catchLoc, true);
                } else if (this.prev < entry.finallyLoc) {
                  return handle(entry.finallyLoc);
                }
              } else if (hasCatch) {
                if (this.prev < entry.catchLoc) {
                  return handle(entry.catchLoc, true);
                }
              } else if (hasFinally) {
                if (this.prev < entry.finallyLoc) {
                  return handle(entry.finallyLoc);
                }
              } else {
                throw new Error("try statement without catch or finally");
              }
            }
          }
        },
        abrupt: function abrupt(type, arg) {
          for (var i = this.tryEntries.length - 1; i >= 0; --i) {
            var entry = this.tryEntries[i];

            if (entry.tryLoc <= this.prev && hasOwn.call(entry, "finallyLoc") && this.prev < entry.finallyLoc) {
              var finallyEntry = entry;
              break;
            }
          }

          if (finallyEntry && (type === "break" || type === "continue") && finallyEntry.tryLoc <= arg && arg <= finallyEntry.finallyLoc) {
            // Ignore the finally entry if control is not jumping to a
            // location outside the try/catch block.
            finallyEntry = null;
          }

          var record = finallyEntry ? finallyEntry.completion : {};
          record.type = type;
          record.arg = arg;

          if (finallyEntry) {
            this.method = "next";
            this.next = finallyEntry.finallyLoc;
            return ContinueSentinel;
          }

          return this.complete(record);
        },
        complete: function complete(record, afterLoc) {
          if (record.type === "throw") {
            throw record.arg;
          }

          if (record.type === "break" || record.type === "continue") {
            this.next = record.arg;
          } else if (record.type === "return") {
            this.rval = this.arg = record.arg;
            this.method = "return";
            this.next = "end";
          } else if (record.type === "normal" && afterLoc) {
            this.next = afterLoc;
          }

          return ContinueSentinel;
        },
        finish: function finish(finallyLoc) {
          for (var i = this.tryEntries.length - 1; i >= 0; --i) {
            var entry = this.tryEntries[i];

            if (entry.finallyLoc === finallyLoc) {
              this.complete(entry.completion, entry.afterLoc);
              resetTryEntry(entry);
              return ContinueSentinel;
            }
          }
        },
        "catch": function _catch(tryLoc) {
          for (var i = this.tryEntries.length - 1; i >= 0; --i) {
            var entry = this.tryEntries[i];

            if (entry.tryLoc === tryLoc) {
              var record = entry.completion;

              if (record.type === "throw") {
                var thrown = record.arg;
                resetTryEntry(entry);
              }

              return thrown;
            }
          } // The context.catch method must only be called with a location
          // argument that corresponds to a known catch block.


          throw new Error("illegal catch attempt");
        },
        delegateYield: function delegateYield(iterable, resultName, nextLoc) {
          this.delegate = {
            iterator: values(iterable),
            resultName: resultName,
            nextLoc: nextLoc
          };

          if (this.method === "next") {
            // Deliberately forget the last sent value so that we don't
            // accidentally pass it on to the delegate.
            this.arg = undefined;
          }

          return ContinueSentinel;
        }
      }; // Regardless of whether this script is executing as a CommonJS module
      // or not, return the runtime object so that we can declare the variable
      // regeneratorRuntime in the outer scope, which allows this module to be
      // injected easily by `bin/regenerator --include-runtime script.js`.

      return exports;
    }( // If this script is executing as a CommonJS module, use module.exports
    // as the regeneratorRuntime namespace. Otherwise create a new empty
    // object. Either way, the resulting object will be used to initialize
    // the regeneratorRuntime variable at the top of this file.
    true ? module.exports : undefined);

    try {
      regeneratorRuntime = runtime;
    } catch (accidentalStrictMode) {
      // This module should not be running in strict mode, so the above
      // assignment should always work unless something is misconfigured. Just
      // in case runtime.js accidentally runs in strict mode, we can escape
      // strict mode using a global Function call. This could conceivably fail
      // if a Content Security Policy forbids using Function, but in that case
      // the proper solution is to fix the accidental strict mode problem. If
      // you've misconfigured your bundler to force strict mode and applied a
      // CSP to forbid Function, and you're not willing to fix either of those
      // problems, please detail your unique predicament in a GitHub issue.
      Function("r", "regeneratorRuntime = r")(runtime);
    }
    /***/

  },

  /***/
  "9b43":
  /***/
  function b43(module, exports, __webpack_require__) {
    // optional / simple context binding
    var aFunction = __webpack_require__("d8e8");

    module.exports = function (fn, that, length) {
      aFunction(fn);
      if (that === undefined) return fn;

      switch (length) {
        case 1:
          return function (a) {
            return fn.call(that, a);
          };

        case 2:
          return function (a, b) {
            return fn.call(that, a, b);
          };

        case 3:
          return function (a, b, c) {
            return fn.call(that, a, b, c);
          };
      }

      return function ()
      /* ...args */
      {
        return fn.apply(that, arguments);
      };
    };
    /***/

  },

  /***/
  "9c6c":
  /***/
  function c6c(module, exports, __webpack_require__) {
    // 22.1.3.31 Array.prototype[@@unscopables]
    var UNSCOPABLES = __webpack_require__("2b4c")('unscopables');

    var ArrayProto = Array.prototype;
    if (ArrayProto[UNSCOPABLES] == undefined) __webpack_require__("32e9")(ArrayProto, UNSCOPABLES, {});

    module.exports = function (key) {
      ArrayProto[UNSCOPABLES][key] = true;
    };
    /***/

  },

  /***/
  "9c93":
  /***/
  function c93(module, exports, __webpack_require__) {
    // call something on iterator step with safe closing on error
    var anObject = __webpack_require__("0f89");

    module.exports = function (iterator, fn, value, entries) {
      try {
        return entries ? fn(anObject(value)[0], value[1]) : fn(value); // 7.4.6 IteratorClose(iterator, completion)
      } catch (e) {
        var ret = iterator['return'];
        if (ret !== undefined) anObject(ret.call(iterator));
        throw e;
      }
    };
    /***/

  },

  /***/
  "9cb8":
  /***/
  function cb8(module, exports, __webpack_require__) {// extracted by mini-css-extract-plugin

    /***/
  },

  /***/
  "9def":
  /***/
  function def(module, exports, __webpack_require__) {
    // 7.1.15 ToLength
    var toInteger = __webpack_require__("4588");

    var min = Math.min;

    module.exports = function (it) {
      return it > 0 ? min(toInteger(it), 0x1fffffffffffff) : 0; // pow(2, 53) - 1 == 9007199254740991
    };
    /***/

  },

  /***/
  "9e1e":
  /***/
  function e1e(module, exports, __webpack_require__) {
    // Thank's IE8 for his funny defineProperty
    module.exports = !__webpack_require__("79e5")(function () {
      return Object.defineProperty({}, 'a', {
        get: function get() {
          return 7;
        }
      }).a != 7;
    });
    /***/
  },

  /***/
  "9e99":
  /***/
  function e99(module) {
    module.exports = {
      "structs": {
        "Field": {
          "GetCardinal": {
            "In": [],
            "Out": []
          },
          "GetAndMode": {
            "In": [],
            "Out": []
          },
          "SelectValues": {
            "In": [{
              "Name": "qFieldValues",
              "DefaultValue": [{
                "qText": "",
                "qIsNumeric": false,
                "qNumber": 0
              }]
            }, {
              "Name": "qToggleMode",
              "DefaultValue": false,
              "Optional": true
            }, {
              "Name": "qSoftLock",
              "DefaultValue": false,
              "Optional": true
            }],
            "Out": []
          },
          "Select": {
            "In": [{
              "Name": "qMatch",
              "DefaultValue": ""
            }, {
              "Name": "qSoftLock",
              "DefaultValue": false,
              "Optional": true
            }, {
              "Name": "qExcludedValuesMode",
              "DefaultValue": 0,
              "Optional": true
            }],
            "Out": []
          },
          "ToggleSelect": {
            "In": [{
              "Name": "qMatch",
              "DefaultValue": ""
            }, {
              "Name": "qSoftLock",
              "DefaultValue": false,
              "Optional": true
            }, {
              "Name": "qExcludedValuesMode",
              "DefaultValue": 0,
              "Optional": true
            }],
            "Out": []
          },
          "ClearAllButThis": {
            "In": [{
              "Name": "qSoftLock",
              "DefaultValue": false,
              "Optional": true
            }],
            "Out": []
          },
          "SelectPossible": {
            "In": [{
              "Name": "qSoftLock",
              "DefaultValue": false,
              "Optional": true
            }],
            "Out": []
          },
          "SelectExcluded": {
            "In": [{
              "Name": "qSoftLock",
              "DefaultValue": false,
              "Optional": true
            }],
            "Out": []
          },
          "SelectAll": {
            "In": [{
              "Name": "qSoftLock",
              "DefaultValue": false,
              "Optional": true
            }],
            "Out": []
          },
          "Lock": {
            "In": [],
            "Out": []
          },
          "Unlock": {
            "In": [],
            "Out": []
          },
          "GetNxProperties": {
            "In": [],
            "Out": [{
              "Name": "qProperties"
            }]
          },
          "SetNxProperties": {
            "In": [{
              "Name": "qProperties",
              "DefaultValue": {
                "qOneAndOnlyOne": false
              }
            }],
            "Out": []
          },
          "SetAndMode": {
            "In": [{
              "Name": "qAndMode",
              "DefaultValue": false
            }],
            "Out": []
          },
          "SelectAlternative": {
            "In": [{
              "Name": "qSoftLock",
              "DefaultValue": false,
              "Optional": true
            }],
            "Out": []
          },
          "LowLevelSelect": {
            "In": [{
              "Name": "qValues",
              "DefaultValue": [0]
            }, {
              "Name": "qToggleMode",
              "DefaultValue": false
            }, {
              "Name": "qSoftLock",
              "DefaultValue": false,
              "Optional": true
            }],
            "Out": []
          },
          "Clear": {
            "In": [],
            "Out": []
          }
        },
        "Variable": {
          "GetContent": {
            "In": [],
            "Out": [{
              "Name": "qContent"
            }]
          },
          "GetRawContent": {
            "In": [],
            "Out": []
          },
          "SetContent": {
            "In": [{
              "Name": "qContent",
              "DefaultValue": ""
            }, {
              "Name": "qUpdateMRU",
              "DefaultValue": false
            }],
            "Out": []
          },
          "ForceContent": {
            "In": [{
              "Name": "qs",
              "DefaultValue": ""
            }, {
              "Name": "qd",
              "DefaultValue": 0
            }],
            "Out": []
          },
          "GetNxProperties": {
            "In": [],
            "Out": [{
              "Name": "qProperties"
            }]
          },
          "SetNxProperties": {
            "In": [{
              "Name": "qProperties",
              "DefaultValue": {
                "qName": "",
                "qNumberPresentation": {
                  "qType": 0,
                  "qnDec": 0,
                  "qUseThou": 0,
                  "qFmt": "",
                  "qDec": "",
                  "qThou": ""
                },
                "qIncludeInBookmark": false,
                "qUsePredefListedValues": false,
                "qPreDefinedList": [""]
              }
            }],
            "Out": []
          }
        },
        "GenericObject": {
          "GetLayout": {
            "In": [],
            "Out": [{
              "Name": "qLayout"
            }]
          },
          "GetListObjectData": {
            "In": [{
              "Name": "qPath",
              "DefaultValue": ""
            }, {
              "Name": "qPages",
              "DefaultValue": [{
                "qLeft": 0,
                "qTop": 0,
                "qWidth": 0,
                "qHeight": 0
              }]
            }],
            "Out": [{
              "Name": "qDataPages"
            }]
          },
          "GetHyperCubeData": {
            "In": [{
              "Name": "qPath",
              "DefaultValue": ""
            }, {
              "Name": "qPages",
              "DefaultValue": [{
                "qLeft": 0,
                "qTop": 0,
                "qWidth": 0,
                "qHeight": 0
              }]
            }],
            "Out": [{
              "Name": "qDataPages"
            }]
          },
          "GetHyperCubeReducedData": {
            "In": [{
              "Name": "qPath",
              "DefaultValue": ""
            }, {
              "Name": "qPages",
              "DefaultValue": [{
                "qLeft": 0,
                "qTop": 0,
                "qWidth": 0,
                "qHeight": 0
              }]
            }, {
              "Name": "qZoomFactor",
              "DefaultValue": 0
            }, {
              "Name": "qReductionMode",
              "DefaultValue": 0
            }],
            "Out": [{
              "Name": "qDataPages"
            }]
          },
          "GetHyperCubePivotData": {
            "In": [{
              "Name": "qPath",
              "DefaultValue": ""
            }, {
              "Name": "qPages",
              "DefaultValue": [{
                "qLeft": 0,
                "qTop": 0,
                "qWidth": 0,
                "qHeight": 0
              }]
            }],
            "Out": [{
              "Name": "qDataPages"
            }]
          },
          "GetHyperCubeStackData": {
            "In": [{
              "Name": "qPath",
              "DefaultValue": ""
            }, {
              "Name": "qPages",
              "DefaultValue": [{
                "qLeft": 0,
                "qTop": 0,
                "qWidth": 0,
                "qHeight": 0
              }]
            }, {
              "Name": "qMaxNbrCells",
              "DefaultValue": 0,
              "Optional": true
            }],
            "Out": [{
              "Name": "qDataPages"
            }]
          },
          "GetHyperCubeContinuousData": {
            "In": [{
              "Name": "qPath",
              "DefaultValue": ""
            }, {
              "Name": "qOptions",
              "DefaultValue": {
                "qStart": 0,
                "qEnd": 0,
                "qNbrPoints": 0,
                "qMaxNbrTicks": 0,
                "qMaxNumberLines": 0
              }
            }, {
              "Name": "qReverseSort",
              "DefaultValue": false,
              "Optional": true
            }],
            "Out": [{
              "Name": "qDataPages"
            }, {
              "Name": "qAxisData"
            }]
          },
          "GetHyperCubeTreeData": {
            "In": [{
              "Name": "qPath",
              "DefaultValue": ""
            }, {
              "Name": "qNodeOptions",
              "DefaultValue": {
                "qMaxNbrOfNodes": 0,
                "qTreeNodes": [{
                  "qArea": {
                    "qLeft": 0,
                    "qTop": 0,
                    "qWidth": 0,
                    "qHeight": 0
                  },
                  "qAllValues": false
                }],
                "qTreeLevels": {
                  "qLeft": 0,
                  "qDepth": 0
                }
              },
              "Optional": true
            }],
            "Out": [{
              "Name": "qNodes"
            }]
          },
          "GetHyperCubeBinnedData": {
            "In": [{
              "Name": "qPath",
              "DefaultValue": ""
            }, {
              "Name": "qPages",
              "DefaultValue": [{
                "qLeft": 0,
                "qTop": 0,
                "qWidth": 0,
                "qHeight": 0
              }]
            }, {
              "Name": "qViewport",
              "DefaultValue": {
                "qWidth": 0,
                "qHeight": 0,
                "qZoomLevel": 0
              }
            }, {
              "Name": "qDataRanges",
              "DefaultValue": [{
                "qLeft": 0,
                "qTop": 0,
                "qWidth": 0,
                "qHeight": 0
              }]
            }, {
              "Name": "qMaxNbrCells",
              "DefaultValue": 0
            }, {
              "Name": "qQueryLevel",
              "DefaultValue": 0
            }, {
              "Name": "qBinningMethod",
              "DefaultValue": 0
            }],
            "Out": [{
              "Name": "qDataPages"
            }]
          },
          "ApplyPatches": {
            "In": [{
              "Name": "qPatches",
              "DefaultValue": [{
                "qOp": 0,
                "qPath": "",
                "qValue": ""
              }]
            }, {
              "Name": "qSoftPatch",
              "DefaultValue": false,
              "Optional": true
            }],
            "Out": []
          },
          "ClearSoftPatches": {
            "In": [],
            "Out": []
          },
          "SetProperties": {
            "In": [{
              "Name": "qProp",
              "DefaultValue": {
                "qInfo": {
                  "qId": "",
                  "qType": ""
                },
                "qExtendsId": "",
                "qMetaDef": {}
              }
            }],
            "Out": []
          },
          "GetProperties": {
            "In": [],
            "Out": [{
              "Name": "qProp"
            }]
          },
          "GetEffectiveProperties": {
            "In": [],
            "Out": [{
              "Name": "qProp"
            }]
          },
          "SetFullPropertyTree": {
            "In": [{
              "Name": "qPropEntry",
              "DefaultValue": {
                "qProperty": {
                  "qInfo": {
                    "qId": "",
                    "qType": ""
                  },
                  "qExtendsId": "",
                  "qMetaDef": {}
                },
                "qChildren": [],
                "qEmbeddedSnapshotRef": null
              }
            }],
            "Out": []
          },
          "GetFullPropertyTree": {
            "In": [],
            "Out": [{
              "Name": "qPropEntry"
            }]
          },
          "GetInfo": {
            "In": [],
            "Out": [{
              "Name": "qInfo"
            }]
          },
          "ClearSelections": {
            "In": [{
              "Name": "qPath",
              "DefaultValue": ""
            }, {
              "Name": "qColIndices",
              "DefaultValue": [0],
              "Optional": true
            }],
            "Out": []
          },
          "ExportData": {
            "In": [{
              "Name": "qFileType",
              "DefaultValue": 0
            }, {
              "Name": "qPath",
              "DefaultValue": "",
              "Optional": true
            }, {
              "Name": "qFileName",
              "DefaultValue": "",
              "Optional": true
            }, {
              "Name": "qExportState",
              "DefaultValue": 0,
              "Optional": true
            }],
            "Out": [{
              "Name": "qUrl"
            }, {
              "Name": "qWarnings"
            }]
          },
          "SelectListObjectValues": {
            "In": [{
              "Name": "qPath",
              "DefaultValue": ""
            }, {
              "Name": "qValues",
              "DefaultValue": [0]
            }, {
              "Name": "qToggleMode",
              "DefaultValue": false
            }, {
              "Name": "qSoftLock",
              "DefaultValue": false,
              "Optional": true
            }],
            "Out": [{
              "Name": "qSuccess"
            }]
          },
          "SelectListObjectPossible": {
            "In": [{
              "Name": "qPath",
              "DefaultValue": ""
            }, {
              "Name": "qSoftLock",
              "DefaultValue": false,
              "Optional": true
            }],
            "Out": [{
              "Name": "qSuccess"
            }]
          },
          "SelectListObjectExcluded": {
            "In": [{
              "Name": "qPath",
              "DefaultValue": ""
            }, {
              "Name": "qSoftLock",
              "DefaultValue": false,
              "Optional": true
            }],
            "Out": [{
              "Name": "qSuccess"
            }]
          },
          "SelectListObjectAlternative": {
            "In": [{
              "Name": "qPath",
              "DefaultValue": ""
            }, {
              "Name": "qSoftLock",
              "DefaultValue": false,
              "Optional": true
            }],
            "Out": [{
              "Name": "qSuccess"
            }]
          },
          "SelectListObjectAll": {
            "In": [{
              "Name": "qPath",
              "DefaultValue": ""
            }, {
              "Name": "qSoftLock",
              "DefaultValue": false,
              "Optional": true
            }],
            "Out": [{
              "Name": "qSuccess"
            }]
          },
          "SelectListObjectContinuousRange": {
            "In": [{
              "Name": "qPath",
              "DefaultValue": ""
            }, {
              "Name": "qRanges",
              "DefaultValue": [{
                "qMin": 0,
                "qMax": 0,
                "qMinInclEq": false,
                "qMaxInclEq": false
              }]
            }, {
              "Name": "qSoftLock",
              "DefaultValue": false,
              "Optional": true
            }],
            "Out": [{
              "Name": "qSuccess"
            }]
          },
          "SearchListObjectFor": {
            "In": [{
              "Name": "qPath",
              "DefaultValue": ""
            }, {
              "Name": "qMatch",
              "DefaultValue": ""
            }],
            "Out": [{
              "Name": "qSuccess"
            }]
          },
          "AbortListObjectSearch": {
            "In": [{
              "Name": "qPath",
              "DefaultValue": ""
            }],
            "Out": []
          },
          "AcceptListObjectSearch": {
            "In": [{
              "Name": "qPath",
              "DefaultValue": ""
            }, {
              "Name": "qToggleMode",
              "DefaultValue": false
            }, {
              "Name": "qSoftLock",
              "DefaultValue": false,
              "Optional": true
            }],
            "Out": []
          },
          "ExpandLeft": {
            "In": [{
              "Name": "qPath",
              "DefaultValue": ""
            }, {
              "Name": "qRow",
              "DefaultValue": 0
            }, {
              "Name": "qCol",
              "DefaultValue": 0
            }, {
              "Name": "qAll",
              "DefaultValue": false
            }],
            "Out": []
          },
          "ExpandTop": {
            "In": [{
              "Name": "qPath",
              "DefaultValue": ""
            }, {
              "Name": "qRow",
              "DefaultValue": 0
            }, {
              "Name": "qCol",
              "DefaultValue": 0
            }, {
              "Name": "qAll",
              "DefaultValue": false
            }],
            "Out": []
          },
          "CollapseLeft": {
            "In": [{
              "Name": "qPath",
              "DefaultValue": ""
            }, {
              "Name": "qRow",
              "DefaultValue": 0
            }, {
              "Name": "qCol",
              "DefaultValue": 0
            }, {
              "Name": "qAll",
              "DefaultValue": false
            }],
            "Out": []
          },
          "CollapseTop": {
            "In": [{
              "Name": "qPath",
              "DefaultValue": ""
            }, {
              "Name": "qRow",
              "DefaultValue": 0
            }, {
              "Name": "qCol",
              "DefaultValue": 0
            }, {
              "Name": "qAll",
              "DefaultValue": false
            }],
            "Out": []
          },
          "DrillUp": {
            "In": [{
              "Name": "qPath",
              "DefaultValue": ""
            }, {
              "Name": "qDimNo",
              "DefaultValue": 0
            }, {
              "Name": "qNbrSteps",
              "DefaultValue": 0
            }],
            "Out": []
          },
          "Lock": {
            "In": [{
              "Name": "qPath",
              "DefaultValue": ""
            }, {
              "Name": "qColIndices",
              "DefaultValue": [0],
              "Optional": true
            }],
            "Out": []
          },
          "Unlock": {
            "In": [{
              "Name": "qPath",
              "DefaultValue": ""
            }, {
              "Name": "qColIndices",
              "DefaultValue": [0],
              "Optional": true
            }],
            "Out": []
          },
          "SelectHyperCubeValues": {
            "In": [{
              "Name": "qPath",
              "DefaultValue": ""
            }, {
              "Name": "qDimNo",
              "DefaultValue": 0
            }, {
              "Name": "qValues",
              "DefaultValue": [0]
            }, {
              "Name": "qToggleMode",
              "DefaultValue": false
            }],
            "Out": [{
              "Name": "qSuccess"
            }]
          },
          "SelectHyperCubeCells": {
            "In": [{
              "Name": "qPath",
              "DefaultValue": ""
            }, {
              "Name": "qRowIndices",
              "DefaultValue": [0]
            }, {
              "Name": "qColIndices",
              "DefaultValue": [0]
            }, {
              "Name": "qSoftLock",
              "DefaultValue": false,
              "Optional": true
            }, {
              "Name": "qDeselectOnlyOneSelected",
              "DefaultValue": false,
              "Optional": true
            }],
            "Out": [{
              "Name": "qSuccess"
            }]
          },
          "SelectPivotCells": {
            "In": [{
              "Name": "qPath",
              "DefaultValue": ""
            }, {
              "Name": "qSelections",
              "DefaultValue": [{
                "qType": 0,
                "qCol": 0,
                "qRow": 0
              }]
            }, {
              "Name": "qSoftLock",
              "DefaultValue": false,
              "Optional": true
            }, {
              "Name": "qDeselectOnlyOneSelected",
              "DefaultValue": false,
              "Optional": true
            }],
            "Out": [{
              "Name": "qSuccess"
            }]
          },
          "RangeSelectHyperCubeValues": {
            "In": [{
              "Name": "qPath",
              "DefaultValue": ""
            }, {
              "Name": "qRanges",
              "DefaultValue": [{
                "qRange": {
                  "qMin": 0,
                  "qMax": 0,
                  "qMinInclEq": false,
                  "qMaxInclEq": false
                },
                "qMeasureIx": 0
              }]
            }, {
              "Name": "qColumnsToSelect",
              "DefaultValue": [0],
              "Optional": true
            }, {
              "Name": "qOrMode",
              "DefaultValue": false,
              "Optional": true
            }, {
              "Name": "qDeselectOnlyOneSelected",
              "DefaultValue": false,
              "Optional": true
            }],
            "Out": [{
              "Name": "qSuccess"
            }]
          },
          "MultiRangeSelectHyperCubeValues": {
            "In": [{
              "Name": "qPath",
              "DefaultValue": ""
            }, {
              "Name": "qRanges",
              "DefaultValue": [{
                "qRanges": [{
                  "qRange": {
                    "qMin": 0,
                    "qMax": 0,
                    "qMinInclEq": false,
                    "qMaxInclEq": false
                  },
                  "qMeasureIx": 0
                }],
                "qColumnsToSelect": [0]
              }]
            }, {
              "Name": "qOrMode",
              "DefaultValue": false,
              "Optional": true
            }, {
              "Name": "qDeselectOnlyOneSelected",
              "DefaultValue": false,
              "Optional": true
            }],
            "Out": [{
              "Name": "qSuccess"
            }]
          },
          "MultiRangeSelectTreeDataValues": {
            "In": [{
              "Name": "qPath",
              "DefaultValue": ""
            }, {
              "Name": "qRanges",
              "DefaultValue": [{
                "qRanges": [{
                  "qRange": {
                    "qMin": 0,
                    "qMax": 0,
                    "qMinInclEq": false,
                    "qMaxInclEq": false
                  },
                  "qMeasureIx": 0,
                  "qDimensionIx": 0
                }]
              }]
            }, {
              "Name": "qOrMode",
              "DefaultValue": false,
              "Optional": true
            }, {
              "Name": "qDeselectOnlyOneSelected",
              "DefaultValue": false,
              "Optional": true
            }],
            "Out": [{
              "Name": "qSuccess"
            }]
          },
          "SelectHyperCubeContinuousRange": {
            "In": [{
              "Name": "qPath",
              "DefaultValue": ""
            }, {
              "Name": "qRanges",
              "DefaultValue": [{
                "qRange": {
                  "qMin": 0,
                  "qMax": 0,
                  "qMinInclEq": false,
                  "qMaxInclEq": false
                },
                "qDimIx": 0
              }]
            }, {
              "Name": "qSoftLock",
              "DefaultValue": false,
              "Optional": true
            }],
            "Out": [{
              "Name": "qSuccess"
            }]
          },
          "GetChild": {
            "In": [{
              "Name": "qId",
              "DefaultValue": ""
            }],
            "Out": []
          },
          "GetChildInfos": {
            "In": [],
            "Out": [{
              "Name": "qInfos"
            }]
          },
          "CreateChild": {
            "In": [{
              "Name": "qProp",
              "DefaultValue": {
                "qInfo": {
                  "qId": "",
                  "qType": ""
                },
                "qExtendsId": "",
                "qMetaDef": {}
              }
            }, {
              "Name": "qPropForThis",
              "DefaultValue": null,
              "Optional": true
            }],
            "Out": [{
              "Name": "qInfo"
            }]
          },
          "DestroyChild": {
            "In": [{
              "Name": "qId",
              "DefaultValue": ""
            }, {
              "Name": "qPropForThis",
              "DefaultValue": null,
              "Optional": true
            }],
            "Out": [{
              "Name": "qSuccess"
            }]
          },
          "DestroyAllChildren": {
            "In": [{
              "Name": "qPropForThis",
              "DefaultValue": null,
              "Optional": true
            }],
            "Out": []
          },
          "SetChildArrayOrder": {
            "In": [{
              "Name": "qIds",
              "DefaultValue": [""]
            }],
            "Out": []
          },
          "GetLinkedObjects": {
            "In": [],
            "Out": [{
              "Name": "qItems"
            }]
          },
          "CopyFrom": {
            "In": [{
              "Name": "qFromId",
              "DefaultValue": ""
            }],
            "Out": []
          },
          "BeginSelections": {
            "In": [{
              "Name": "qPaths",
              "DefaultValue": [""]
            }],
            "Out": []
          },
          "EndSelections": {
            "In": [{
              "Name": "qAccept",
              "DefaultValue": false
            }],
            "Out": []
          },
          "ResetMadeSelections": {
            "In": [],
            "Out": []
          },
          "EmbedSnapshotObject": {
            "In": [{
              "Name": "qId",
              "DefaultValue": ""
            }],
            "Out": []
          },
          "GetSnapshotObject": {
            "In": [],
            "Out": []
          },
          "Publish": {
            "In": [],
            "Out": []
          },
          "UnPublish": {
            "In": [],
            "Out": []
          },
          "Approve": {
            "In": [],
            "Out": []
          },
          "UnApprove": {
            "In": [],
            "Out": []
          }
        },
        "GenericDimension": {
          "GetLayout": {
            "In": [],
            "Out": [{
              "Name": "qLayout"
            }]
          },
          "ApplyPatches": {
            "In": [{
              "Name": "qPatches",
              "DefaultValue": [{
                "qOp": 0,
                "qPath": "",
                "qValue": ""
              }]
            }],
            "Out": []
          },
          "SetProperties": {
            "In": [{
              "Name": "qProp",
              "DefaultValue": {
                "qInfo": {
                  "qId": "",
                  "qType": ""
                },
                "qDim": {
                  "qGrouping": 0,
                  "qFieldDefs": [""],
                  "qFieldLabels": [""],
                  "qLabelExpression": ""
                },
                "qMetaDef": {}
              }
            }],
            "Out": []
          },
          "GetProperties": {
            "In": [],
            "Out": [{
              "Name": "qProp"
            }]
          },
          "GetInfo": {
            "In": [],
            "Out": [{
              "Name": "qInfo"
            }]
          },
          "GetDimension": {
            "In": [],
            "Out": [{
              "Name": "qDim"
            }]
          },
          "GetLinkedObjects": {
            "In": [],
            "Out": [{
              "Name": "qItems"
            }]
          },
          "Publish": {
            "In": [],
            "Out": []
          },
          "UnPublish": {
            "In": [],
            "Out": []
          },
          "Approve": {
            "In": [],
            "Out": []
          },
          "UnApprove": {
            "In": [],
            "Out": []
          }
        },
        "GenericBookmark": {
          "GetFieldValues": {
            "In": [{
              "Name": "qField",
              "DefaultValue": ""
            }, {
              "Name": "qGetExcludedValues",
              "DefaultValue": false
            }, {
              "Name": "qDataPage",
              "DefaultValue": {
                "qStartIndex": 0,
                "qEndIndex": 0
              }
            }],
            "Out": [{
              "Name": "qFieldValues"
            }]
          },
          "GetLayout": {
            "In": [],
            "Out": [{
              "Name": "qLayout"
            }]
          },
          "ApplyPatches": {
            "In": [{
              "Name": "qPatches",
              "DefaultValue": [{
                "qOp": 0,
                "qPath": "",
                "qValue": ""
              }]
            }],
            "Out": []
          },
          "SetProperties": {
            "In": [{
              "Name": "qProp",
              "DefaultValue": {
                "qInfo": {
                  "qId": "",
                  "qType": ""
                },
                "qMetaDef": {}
              }
            }],
            "Out": []
          },
          "GetProperties": {
            "In": [],
            "Out": [{
              "Name": "qProp"
            }]
          },
          "GetInfo": {
            "In": [],
            "Out": [{
              "Name": "qInfo"
            }]
          },
          "Apply": {
            "In": [],
            "Out": [{
              "Name": "qSuccess"
            }]
          },
          "Publish": {
            "In": [],
            "Out": []
          },
          "UnPublish": {
            "In": [],
            "Out": []
          },
          "Approve": {
            "In": [],
            "Out": []
          },
          "UnApprove": {
            "In": [],
            "Out": []
          }
        },
        "GenericVariable": {
          "GetLayout": {
            "In": [],
            "Out": [{
              "Name": "qLayout"
            }]
          },
          "ApplyPatches": {
            "In": [{
              "Name": "qPatches",
              "DefaultValue": [{
                "qOp": 0,
                "qPath": "",
                "qValue": ""
              }]
            }],
            "Out": []
          },
          "SetProperties": {
            "In": [{
              "Name": "qProp",
              "DefaultValue": {
                "qInfo": {
                  "qId": "",
                  "qType": ""
                },
                "qMetaDef": {},
                "qName": "",
                "qComment": "",
                "qNumberPresentation": {
                  "qType": 0,
                  "qnDec": 0,
                  "qUseThou": 0,
                  "qFmt": "",
                  "qDec": "",
                  "qThou": ""
                },
                "qIncludeInBookmark": false,
                "qDefinition": ""
              }
            }],
            "Out": []
          },
          "GetProperties": {
            "In": [],
            "Out": [{
              "Name": "qProp"
            }]
          },
          "GetInfo": {
            "In": [],
            "Out": [{
              "Name": "qInfo"
            }]
          },
          "SetStringValue": {
            "In": [{
              "Name": "qVal",
              "DefaultValue": ""
            }],
            "Out": []
          },
          "SetNumValue": {
            "In": [{
              "Name": "qVal",
              "DefaultValue": 0
            }],
            "Out": []
          },
          "SetDualValue": {
            "In": [{
              "Name": "qText",
              "DefaultValue": ""
            }, {
              "Name": "qNum",
              "DefaultValue": 0
            }],
            "Out": []
          }
        },
        "GenericMeasure": {
          "GetLayout": {
            "In": [],
            "Out": [{
              "Name": "qLayout"
            }]
          },
          "ApplyPatches": {
            "In": [{
              "Name": "qPatches",
              "DefaultValue": [{
                "qOp": 0,
                "qPath": "",
                "qValue": ""
              }]
            }],
            "Out": []
          },
          "SetProperties": {
            "In": [{
              "Name": "qProp",
              "DefaultValue": {
                "qInfo": {
                  "qId": "",
                  "qType": ""
                },
                "qMeasure": {
                  "qLabel": "",
                  "qDef": "",
                  "qGrouping": 0,
                  "qExpressions": [""],
                  "qActiveExpression": 0,
                  "qLabelExpression": ""
                },
                "qMetaDef": {}
              }
            }],
            "Out": []
          },
          "GetProperties": {
            "In": [],
            "Out": [{
              "Name": "qProp"
            }]
          },
          "GetInfo": {
            "In": [],
            "Out": [{
              "Name": "qInfo"
            }]
          },
          "GetMeasure": {
            "In": [],
            "Out": [{
              "Name": "qMeasure"
            }]
          },
          "GetLinkedObjects": {
            "In": [],
            "Out": [{
              "Name": "qItems"
            }]
          },
          "Publish": {
            "In": [],
            "Out": []
          },
          "UnPublish": {
            "In": [],
            "Out": []
          },
          "Approve": {
            "In": [],
            "Out": []
          },
          "UnApprove": {
            "In": [],
            "Out": []
          }
        },
        "Doc": {
          "GetField": {
            "In": [{
              "Name": "qFieldName",
              "DefaultValue": ""
            }, {
              "Name": "qStateName",
              "DefaultValue": "",
              "Optional": true
            }],
            "Out": []
          },
          "GetFieldDescription": {
            "In": [{
              "Name": "qFieldName",
              "DefaultValue": ""
            }],
            "Out": []
          },
          "GetVariable": {
            "In": [{
              "Name": "qName",
              "DefaultValue": ""
            }],
            "Out": []
          },
          "GetLooselyCoupledVector": {
            "In": [],
            "Out": [{
              "Name": "qv"
            }]
          },
          "SetLooselyCoupledVector": {
            "In": [{
              "Name": "qv",
              "DefaultValue": [0]
            }],
            "Out": []
          },
          "Evaluate": {
            "In": [{
              "Name": "qExpression",
              "DefaultValue": ""
            }],
            "Out": []
          },
          "EvaluateEx": {
            "In": [{
              "Name": "qExpression",
              "DefaultValue": ""
            }],
            "Out": [{
              "Name": "qValue"
            }]
          },
          "ClearAll": {
            "In": [{
              "Name": "qLockedAlso",
              "DefaultValue": false,
              "Optional": true
            }, {
              "Name": "qStateName",
              "DefaultValue": "",
              "Optional": true
            }],
            "Out": []
          },
          "LockAll": {
            "In": [{
              "Name": "qStateName",
              "DefaultValue": "",
              "Optional": true
            }],
            "Out": []
          },
          "UnlockAll": {
            "In": [{
              "Name": "qStateName",
              "DefaultValue": "",
              "Optional": true
            }],
            "Out": []
          },
          "Back": {
            "In": [],
            "Out": []
          },
          "Forward": {
            "In": [],
            "Out": []
          },
          "CreateVariable": {
            "In": [{
              "Name": "qName",
              "DefaultValue": ""
            }],
            "Out": []
          },
          "RemoveVariable": {
            "In": [{
              "Name": "qName",
              "DefaultValue": ""
            }],
            "Out": []
          },
          "GetLocaleInfo": {
            "In": [],
            "Out": []
          },
          "GetTablesAndKeys": {
            "In": [{
              "Name": "qWindowSize",
              "DefaultValue": {
                "qcx": 0,
                "qcy": 0
              }
            }, {
              "Name": "qNullSize",
              "DefaultValue": {
                "qcx": 0,
                "qcy": 0
              }
            }, {
              "Name": "qCellHeight",
              "DefaultValue": 0
            }, {
              "Name": "qSyntheticMode",
              "DefaultValue": false
            }, {
              "Name": "qIncludeSysVars",
              "DefaultValue": false
            }],
            "Out": [{
              "Name": "qtr"
            }, {
              "Name": "qk"
            }]
          },
          "GetViewDlgSaveInfo": {
            "In": [],
            "Out": []
          },
          "SetViewDlgSaveInfo": {
            "In": [{
              "Name": "qInfo",
              "DefaultValue": {
                "qPos": {
                  "qLeft": 0,
                  "qTop": 0,
                  "qWidth": 0,
                  "qHeight": 0
                },
                "qCtlInfo": {
                  "qInternalView": {
                    "qTables": [{
                      "qPos": {
                        "qLeft": 0,
                        "qTop": 0,
                        "qWidth": 0,
                        "qHeight": 0
                      },
                      "qCaption": ""
                    }],
                    "qBroomPoints": [{
                      "qPos": {
                        "qx": 0,
                        "qy": 0
                      },
                      "qTable": "",
                      "qFields": [""]
                    }],
                    "qConnectionPoints": [{
                      "qPos": {
                        "qx": 0,
                        "qy": 0
                      },
                      "qFields": [""]
                    }],
                    "qZoomFactor": 0
                  },
                  "qSourceView": {
                    "qTables": [{
                      "qPos": {
                        "qLeft": 0,
                        "qTop": 0,
                        "qWidth": 0,
                        "qHeight": 0
                      },
                      "qCaption": ""
                    }],
                    "qBroomPoints": [{
                      "qPos": {
                        "qx": 0,
                        "qy": 0
                      },
                      "qTable": "",
                      "qFields": [""]
                    }],
                    "qConnectionPoints": [{
                      "qPos": {
                        "qx": 0,
                        "qy": 0
                      },
                      "qFields": [""]
                    }],
                    "qZoomFactor": 0
                  }
                },
                "qMode": 0
              }
            }],
            "Out": []
          },
          "GetEmptyScript": {
            "In": [{
              "Name": "qLocalizedMainSection",
              "DefaultValue": "",
              "Optional": true
            }],
            "Out": []
          },
          "DoReload": {
            "In": [{
              "Name": "qMode",
              "DefaultValue": 0,
              "Optional": true
            }, {
              "Name": "qPartial",
              "DefaultValue": false,
              "Optional": true
            }, {
              "Name": "qDebug",
              "DefaultValue": false,
              "Optional": true
            }],
            "Out": []
          },
          "GetScriptBreakpoints": {
            "In": [],
            "Out": [{
              "Name": "qBreakpoints"
            }]
          },
          "SetScriptBreakpoints": {
            "In": [{
              "Name": "qBreakpoints",
              "DefaultValue": [{
                "qbufferName": "",
                "qlineIx": 0,
                "qEnabled": false
              }]
            }],
            "Out": []
          },
          "GetScript": {
            "In": [],
            "Out": [{
              "Name": "qScript"
            }]
          },
          "GetTextMacros": {
            "In": [],
            "Out": [{
              "Name": "qMacros"
            }]
          },
          "SetFetchLimit": {
            "In": [{
              "Name": "qLimit",
              "DefaultValue": 0
            }],
            "Out": []
          },
          "DoSave": {
            "In": [{
              "Name": "qFileName",
              "DefaultValue": "",
              "Optional": true
            }],
            "Out": []
          },
          "GetTableData": {
            "In": [{
              "Name": "qOffset",
              "DefaultValue": 0
            }, {
              "Name": "qRows",
              "DefaultValue": 0
            }, {
              "Name": "qSyntheticMode",
              "DefaultValue": false
            }, {
              "Name": "qTableName",
              "DefaultValue": ""
            }],
            "Out": [{
              "Name": "qData"
            }]
          },
          "GetAppLayout": {
            "In": [],
            "Out": [{
              "Name": "qLayout"
            }]
          },
          "SetAppProperties": {
            "In": [{
              "Name": "qProp",
              "DefaultValue": {
                "qTitle": "",
                "qLastReloadTime": "",
                "qMigrationHash": "",
                "qSavedInProductVersion": "",
                "qThumbnail": {
                  "qUrl": ""
                }
              }
            }],
            "Out": []
          },
          "GetAppProperties": {
            "In": [],
            "Out": [{
              "Name": "qProp"
            }]
          },
          "GetLineage": {
            "In": [],
            "Out": [{
              "Name": "qLineage"
            }]
          },
          "CreateSessionObject": {
            "In": [{
              "Name": "qProp",
              "DefaultValue": {
                "qInfo": {
                  "qId": "",
                  "qType": ""
                },
                "qExtendsId": "",
                "qMetaDef": {}
              }
            }],
            "Out": []
          },
          "DestroySessionObject": {
            "In": [{
              "Name": "qId",
              "DefaultValue": ""
            }],
            "Out": [{
              "Name": "qSuccess"
            }]
          },
          "CreateObject": {
            "In": [{
              "Name": "qProp",
              "DefaultValue": {
                "qInfo": {
                  "qId": "",
                  "qType": ""
                },
                "qExtendsId": "",
                "qMetaDef": {}
              }
            }],
            "Out": [{
              "Name": "qInfo"
            }]
          },
          "DestroyObject": {
            "In": [{
              "Name": "qId",
              "DefaultValue": ""
            }],
            "Out": [{
              "Name": "qSuccess"
            }]
          },
          "GetObject": {
            "In": [{
              "Name": "qId",
              "DefaultValue": ""
            }],
            "Out": []
          },
          "GetObjects": {
            "In": [{
              "Name": "qOptions",
              "DefaultValue": {
                "qTypes": [""],
                "qIncludeSessionObjects": false,
                "qData": {}
              }
            }],
            "Out": [{
              "Name": "qList"
            }]
          },
          "GetBookmarks": {
            "In": [{
              "Name": "qOptions",
              "DefaultValue": {
                "qTypes": [""],
                "qData": {}
              }
            }],
            "Out": [{
              "Name": "qList"
            }]
          },
          "CloneObject": {
            "In": [{
              "Name": "qId",
              "DefaultValue": ""
            }],
            "Out": [{
              "Name": "qCloneId"
            }]
          },
          "CreateDraft": {
            "In": [{
              "Name": "qId",
              "DefaultValue": ""
            }],
            "Out": [{
              "Name": "qDraftId"
            }]
          },
          "CommitDraft": {
            "In": [{
              "Name": "qId",
              "DefaultValue": ""
            }],
            "Out": []
          },
          "DestroyDraft": {
            "In": [{
              "Name": "qId",
              "DefaultValue": ""
            }, {
              "Name": "qSourceId",
              "DefaultValue": ""
            }],
            "Out": [{
              "Name": "qSuccess"
            }]
          },
          "Undo": {
            "In": [],
            "Out": [{
              "Name": "qSuccess"
            }]
          },
          "Redo": {
            "In": [],
            "Out": [{
              "Name": "qSuccess"
            }]
          },
          "ClearUndoBuffer": {
            "In": [],
            "Out": []
          },
          "CreateDimension": {
            "In": [{
              "Name": "qProp",
              "DefaultValue": {
                "qInfo": {
                  "qId": "",
                  "qType": ""
                },
                "qDim": {
                  "qGrouping": 0,
                  "qFieldDefs": [""],
                  "qFieldLabels": [""],
                  "qLabelExpression": ""
                },
                "qMetaDef": {}
              }
            }],
            "Out": [{
              "Name": "qInfo"
            }]
          },
          "DestroyDimension": {
            "In": [{
              "Name": "qId",
              "DefaultValue": ""
            }],
            "Out": [{
              "Name": "qSuccess"
            }]
          },
          "GetDimension": {
            "In": [{
              "Name": "qId",
              "DefaultValue": ""
            }],
            "Out": []
          },
          "CloneDimension": {
            "In": [{
              "Name": "qId",
              "DefaultValue": ""
            }],
            "Out": [{
              "Name": "qCloneId"
            }]
          },
          "CreateMeasure": {
            "In": [{
              "Name": "qProp",
              "DefaultValue": {
                "qInfo": {
                  "qId": "",
                  "qType": ""
                },
                "qMeasure": {
                  "qLabel": "",
                  "qDef": "",
                  "qGrouping": 0,
                  "qExpressions": [""],
                  "qActiveExpression": 0,
                  "qLabelExpression": ""
                },
                "qMetaDef": {}
              }
            }],
            "Out": [{
              "Name": "qInfo"
            }]
          },
          "DestroyMeasure": {
            "In": [{
              "Name": "qId",
              "DefaultValue": ""
            }],
            "Out": [{
              "Name": "qSuccess"
            }]
          },
          "GetMeasure": {
            "In": [{
              "Name": "qId",
              "DefaultValue": ""
            }],
            "Out": []
          },
          "CloneMeasure": {
            "In": [{
              "Name": "qId",
              "DefaultValue": ""
            }],
            "Out": [{
              "Name": "qCloneId"
            }]
          },
          "CreateSessionVariable": {
            "In": [{
              "Name": "qProp",
              "DefaultValue": {
                "qInfo": {
                  "qId": "",
                  "qType": ""
                },
                "qMetaDef": {},
                "qName": "",
                "qComment": "",
                "qNumberPresentation": {
                  "qType": 0,
                  "qnDec": 0,
                  "qUseThou": 0,
                  "qFmt": "",
                  "qDec": "",
                  "qThou": ""
                },
                "qIncludeInBookmark": false,
                "qDefinition": ""
              }
            }],
            "Out": []
          },
          "DestroySessionVariable": {
            "In": [{
              "Name": "qId",
              "DefaultValue": ""
            }],
            "Out": [{
              "Name": "qSuccess"
            }]
          },
          "CreateVariableEx": {
            "In": [{
              "Name": "qProp",
              "DefaultValue": {
                "qInfo": {
                  "qId": "",
                  "qType": ""
                },
                "qMetaDef": {},
                "qName": "",
                "qComment": "",
                "qNumberPresentation": {
                  "qType": 0,
                  "qnDec": 0,
                  "qUseThou": 0,
                  "qFmt": "",
                  "qDec": "",
                  "qThou": ""
                },
                "qIncludeInBookmark": false,
                "qDefinition": ""
              }
            }],
            "Out": [{
              "Name": "qInfo"
            }]
          },
          "DestroyVariableById": {
            "In": [{
              "Name": "qId",
              "DefaultValue": ""
            }],
            "Out": [{
              "Name": "qSuccess"
            }]
          },
          "DestroyVariableByName": {
            "In": [{
              "Name": "qName",
              "DefaultValue": ""
            }],
            "Out": [{
              "Name": "qSuccess"
            }]
          },
          "GetVariableById": {
            "In": [{
              "Name": "qId",
              "DefaultValue": ""
            }],
            "Out": []
          },
          "GetVariableByName": {
            "In": [{
              "Name": "qName",
              "DefaultValue": ""
            }],
            "Out": []
          },
          "MigrateVariables": {
            "In": [],
            "Out": []
          },
          "MigrateDerivedFields": {
            "In": [],
            "Out": []
          },
          "CheckExpression": {
            "In": [{
              "Name": "qExpr",
              "DefaultValue": ""
            }, {
              "Name": "qLabels",
              "DefaultValue": [""],
              "Optional": true
            }],
            "Out": [{
              "Name": "qErrorMsg"
            }, {
              "Name": "qBadFieldNames"
            }, {
              "Name": "qDangerousFieldNames"
            }]
          },
          "CheckNumberOrExpression": {
            "In": [{
              "Name": "qExpr",
              "DefaultValue": ""
            }],
            "Out": [{
              "Name": "qErrorMsg"
            }, {
              "Name": "qBadFieldNames"
            }]
          },
          "AddAlternateState": {
            "In": [{
              "Name": "qStateName",
              "DefaultValue": ""
            }],
            "Out": []
          },
          "RemoveAlternateState": {
            "In": [{
              "Name": "qStateName",
              "DefaultValue": ""
            }],
            "Out": []
          },
          "CreateBookmark": {
            "In": [{
              "Name": "qProp",
              "DefaultValue": {
                "qInfo": {
                  "qId": "",
                  "qType": ""
                },
                "qMetaDef": {}
              }
            }],
            "Out": [{
              "Name": "qInfo"
            }]
          },
          "DestroyBookmark": {
            "In": [{
              "Name": "qId",
              "DefaultValue": ""
            }],
            "Out": [{
              "Name": "qSuccess"
            }]
          },
          "GetBookmark": {
            "In": [{
              "Name": "qId",
              "DefaultValue": ""
            }],
            "Out": []
          },
          "ApplyBookmark": {
            "In": [{
              "Name": "qId",
              "DefaultValue": ""
            }],
            "Out": [{
              "Name": "qSuccess"
            }]
          },
          "CloneBookmark": {
            "In": [{
              "Name": "qId",
              "DefaultValue": ""
            }],
            "Out": [{
              "Name": "qCloneId"
            }]
          },
          "AddFieldFromExpression": {
            "In": [{
              "Name": "qName",
              "DefaultValue": ""
            }, {
              "Name": "qExpr",
              "DefaultValue": ""
            }],
            "Out": [{
              "Name": "qSuccess"
            }]
          },
          "GetFieldOnTheFlyByName": {
            "In": [{
              "Name": "qReadableName",
              "DefaultValue": ""
            }],
            "Out": [{
              "Name": "qName"
            }]
          },
          "GetAllInfos": {
            "In": [],
            "Out": [{
              "Name": "qInfos"
            }]
          },
          "Resume": {
            "In": [],
            "Out": []
          },
          "AbortModal": {
            "In": [{
              "Name": "qAccept",
              "DefaultValue": false
            }],
            "Out": []
          },
          "Publish": {
            "In": [{
              "Name": "qStreamId",
              "DefaultValue": ""
            }, {
              "Name": "qName",
              "DefaultValue": "",
              "Optional": true
            }],
            "Out": []
          },
          "UnPublish": {
            "In": [],
            "Out": []
          },
          "GetMatchingFields": {
            "In": [{
              "Name": "qTags",
              "DefaultValue": [""]
            }, {
              "Name": "qMatchingFieldMode",
              "DefaultValue": 0,
              "Optional": true
            }],
            "Out": [{
              "Name": "qFieldNames"
            }]
          },
          "FindMatchingFields": {
            "In": [{
              "Name": "qFieldName",
              "DefaultValue": ""
            }, {
              "Name": "qTags",
              "DefaultValue": [""]
            }],
            "Out": [{
              "Name": "qFieldNames"
            }]
          },
          "Scramble": {
            "In": [{
              "Name": "qFieldName",
              "DefaultValue": ""
            }],
            "Out": []
          },
          "SaveObjects": {
            "In": [],
            "Out": []
          },
          "GetAssociationScores": {
            "In": [{
              "Name": "qTable1",
              "DefaultValue": ""
            }, {
              "Name": "qTable2",
              "DefaultValue": ""
            }],
            "Out": [{
              "Name": "qScore"
            }]
          },
          "GetMediaList": {
            "In": [],
            "Out": [{
              "Name": "qList"
            }]
          },
          "GetContentLibraries": {
            "In": [],
            "Out": [{
              "Name": "qList"
            }]
          },
          "GetLibraryContent": {
            "In": [{
              "Name": "qName",
              "DefaultValue": ""
            }],
            "Out": [{
              "Name": "qList"
            }]
          },
          "DoReloadEx": {
            "In": [{
              "Name": "qParams",
              "DefaultValue": {
                "qMode": 0,
                "qPartial": false,
                "qDebug": false
              },
              "Optional": true
            }],
            "Out": [{
              "Name": "qResult"
            }]
          },
          "BackCount": {
            "In": [],
            "Out": []
          },
          "ForwardCount": {
            "In": [],
            "Out": []
          },
          "ExportReducedData": {
            "In": [{
              "Name": "qOptions",
              "DefaultValue": {
                "qBookmarkId": "",
                "qExpires": 0
              },
              "Optional": true
            }],
            "Out": [{
              "Name": "qDownloadInfo"
            }]
          },
          "SetScript": {
            "In": [{
              "Name": "qScript",
              "DefaultValue": ""
            }],
            "Out": []
          },
          "CheckScriptSyntax": {
            "In": [],
            "Out": [{
              "Name": "qErrors"
            }]
          },
          "GetFavoriteVariables": {
            "In": [],
            "Out": [{
              "Name": "qNames"
            }]
          },
          "SetFavoriteVariables": {
            "In": [{
              "Name": "qNames",
              "DefaultValue": [""]
            }],
            "Out": []
          },
          "GetIncludeFileContent": {
            "In": [{
              "Name": "qPath",
              "DefaultValue": ""
            }],
            "Out": [{
              "Name": "qContent"
            }]
          },
          "CreateConnection": {
            "In": [{
              "Name": "qConnection",
              "DefaultValue": {
                "qId": "",
                "qName": "",
                "qConnectionString": "",
                "qType": "",
                "qUserName": "",
                "qPassword": "",
                "qModifiedDate": "",
                "qMeta": {
                  "qName": ""
                },
                "qLogOn": 0
              }
            }],
            "Out": [{
              "Name": "qConnectionId"
            }]
          },
          "ModifyConnection": {
            "In": [{
              "Name": "qConnectionId",
              "DefaultValue": ""
            }, {
              "Name": "qConnection",
              "DefaultValue": {
                "qId": "",
                "qName": "",
                "qConnectionString": "",
                "qType": "",
                "qUserName": "",
                "qPassword": "",
                "qModifiedDate": "",
                "qMeta": {
                  "qName": ""
                },
                "qLogOn": 0
              }
            }, {
              "Name": "qOverrideCredentials",
              "DefaultValue": false,
              "Optional": true
            }],
            "Out": []
          },
          "DeleteConnection": {
            "In": [{
              "Name": "qConnectionId",
              "DefaultValue": ""
            }],
            "Out": []
          },
          "GetConnection": {
            "In": [{
              "Name": "qConnectionId",
              "DefaultValue": ""
            }],
            "Out": [{
              "Name": "qConnection"
            }]
          },
          "GetConnections": {
            "In": [],
            "Out": [{
              "Name": "qConnections"
            }]
          },
          "GetDatabaseInfo": {
            "In": [{
              "Name": "qConnectionId",
              "DefaultValue": ""
            }],
            "Out": [{
              "Name": "qInfo"
            }]
          },
          "GetDatabases": {
            "In": [{
              "Name": "qConnectionId",
              "DefaultValue": ""
            }],
            "Out": [{
              "Name": "qDatabases"
            }]
          },
          "GetDatabaseOwners": {
            "In": [{
              "Name": "qConnectionId",
              "DefaultValue": ""
            }, {
              "Name": "qDatabase",
              "DefaultValue": "",
              "Optional": true
            }],
            "Out": [{
              "Name": "qOwners"
            }]
          },
          "GetDatabaseTables": {
            "In": [{
              "Name": "qConnectionId",
              "DefaultValue": ""
            }, {
              "Name": "qDatabase",
              "DefaultValue": "",
              "Optional": true
            }, {
              "Name": "qOwner",
              "DefaultValue": "",
              "Optional": true
            }],
            "Out": [{
              "Name": "qTables"
            }]
          },
          "GetDatabaseTableFields": {
            "In": [{
              "Name": "qConnectionId",
              "DefaultValue": ""
            }, {
              "Name": "qDatabase",
              "DefaultValue": "",
              "Optional": true
            }, {
              "Name": "qOwner",
              "DefaultValue": "",
              "Optional": true
            }, {
              "Name": "qTable",
              "DefaultValue": ""
            }],
            "Out": [{
              "Name": "qFields"
            }]
          },
          "GetDatabaseTablePreview": {
            "In": [{
              "Name": "qConnectionId",
              "DefaultValue": ""
            }, {
              "Name": "qDatabase",
              "DefaultValue": "",
              "Optional": true
            }, {
              "Name": "qOwner",
              "DefaultValue": "",
              "Optional": true
            }, {
              "Name": "qTable",
              "DefaultValue": ""
            }, {
              "Name": "qConditions",
              "DefaultValue": {
                "qType": 0,
                "qWherePredicate": ""
              },
              "Optional": true
            }],
            "Out": [{
              "Name": "qPreview"
            }, {
              "Name": "qRowCount"
            }]
          },
          "GetFolderItemsForConnection": {
            "In": [{
              "Name": "qConnectionId",
              "DefaultValue": ""
            }, {
              "Name": "qRelativePath",
              "DefaultValue": "",
              "Optional": true
            }],
            "Out": [{
              "Name": "qFolderItems"
            }]
          },
          "GuessFileType": {
            "In": [{
              "Name": "qConnectionId",
              "DefaultValue": ""
            }, {
              "Name": "qRelativePath",
              "DefaultValue": "",
              "Optional": true
            }],
            "Out": [{
              "Name": "qDataFormat"
            }]
          },
          "GetFileTables": {
            "In": [{
              "Name": "qConnectionId",
              "DefaultValue": ""
            }, {
              "Name": "qRelativePath",
              "DefaultValue": "",
              "Optional": true
            }, {
              "Name": "qDataFormat",
              "DefaultValue": {
                "qType": 0,
                "qLabel": "",
                "qQuote": "",
                "qComment": "",
                "qDelimiter": {
                  "qName": "",
                  "qScriptCode": "",
                  "qNumber": 0,
                  "qIsMultiple": false
                },
                "qCodePage": 0,
                "qHeaderSize": 0,
                "qRecordSize": 0,
                "qTabSize": 0,
                "qIgnoreEOF": false,
                "qFixedWidthDelimiters": ""
              }
            }],
            "Out": [{
              "Name": "qTables"
            }]
          },
          "GetFileTableFields": {
            "In": [{
              "Name": "qConnectionId",
              "DefaultValue": ""
            }, {
              "Name": "qRelativePath",
              "DefaultValue": "",
              "Optional": true
            }, {
              "Name": "qDataFormat",
              "DefaultValue": {
                "qType": 0,
                "qLabel": "",
                "qQuote": "",
                "qComment": "",
                "qDelimiter": {
                  "qName": "",
                  "qScriptCode": "",
                  "qNumber": 0,
                  "qIsMultiple": false
                },
                "qCodePage": 0,
                "qHeaderSize": 0,
                "qRecordSize": 0,
                "qTabSize": 0,
                "qIgnoreEOF": false,
                "qFixedWidthDelimiters": ""
              }
            }, {
              "Name": "qTable",
              "DefaultValue": ""
            }],
            "Out": [{
              "Name": "qFields"
            }, {
              "Name": "qFormatSpec"
            }]
          },
          "GetFileTablePreview": {
            "In": [{
              "Name": "qConnectionId",
              "DefaultValue": ""
            }, {
              "Name": "qRelativePath",
              "DefaultValue": "",
              "Optional": true
            }, {
              "Name": "qDataFormat",
              "DefaultValue": {
                "qType": 0,
                "qLabel": "",
                "qQuote": "",
                "qComment": "",
                "qDelimiter": {
                  "qName": "",
                  "qScriptCode": "",
                  "qNumber": 0,
                  "qIsMultiple": false
                },
                "qCodePage": 0,
                "qHeaderSize": 0,
                "qRecordSize": 0,
                "qTabSize": 0,
                "qIgnoreEOF": false,
                "qFixedWidthDelimiters": ""
              }
            }, {
              "Name": "qTable",
              "DefaultValue": ""
            }],
            "Out": [{
              "Name": "qPreview"
            }, {
              "Name": "qFormatSpec"
            }]
          },
          "GetFileTablesEx": {
            "In": [{
              "Name": "qConnectionId",
              "DefaultValue": ""
            }, {
              "Name": "qRelativePath",
              "DefaultValue": "",
              "Optional": true
            }, {
              "Name": "qDataFormat",
              "DefaultValue": {
                "qType": 0,
                "qLabel": "",
                "qQuote": "",
                "qComment": "",
                "qDelimiter": {
                  "qName": "",
                  "qScriptCode": "",
                  "qNumber": 0,
                  "qIsMultiple": false
                },
                "qCodePage": 0,
                "qHeaderSize": 0,
                "qRecordSize": 0,
                "qTabSize": 0,
                "qIgnoreEOF": false,
                "qFixedWidthDelimiters": ""
              }
            }],
            "Out": [{
              "Name": "qTables"
            }]
          },
          "SendGenericCommandToCustomConnector": {
            "In": [{
              "Name": "qProvider",
              "DefaultValue": ""
            }, {
              "Name": "qCommand",
              "DefaultValue": ""
            }, {
              "Name": "qMethod",
              "DefaultValue": ""
            }, {
              "Name": "qParameters",
              "DefaultValue": [""]
            }, {
              "Name": "qAppendConnection",
              "DefaultValue": ""
            }],
            "Out": [{
              "Name": "qResult"
            }]
          },
          "SearchSuggest": {
            "In": [{
              "Name": "qOptions",
              "DefaultValue": {
                "qSearchFields": [""],
                "qContext": 0,
                "qCharEncoding": 0,
                "qAttributes": [""]
              }
            }, {
              "Name": "qTerms",
              "DefaultValue": [""]
            }],
            "Out": [{
              "Name": "qResult"
            }]
          },
          "SearchAssociations": {
            "In": [{
              "Name": "qOptions",
              "DefaultValue": {
                "qSearchFields": [""],
                "qContext": 0,
                "qCharEncoding": 0,
                "qAttributes": [""]
              }
            }, {
              "Name": "qTerms",
              "DefaultValue": [""]
            }, {
              "Name": "qPage",
              "DefaultValue": {
                "qOffset": 0,
                "qCount": 0,
                "qMaxNbrFieldMatches": 0,
                "qGroupOptions": [{
                  "qGroupType": 0,
                  "qOffset": 0,
                  "qCount": 0
                }],
                "qGroupItemOptions": [{
                  "qGroupItemType": 0,
                  "qOffset": 0,
                  "qCount": 0
                }]
              }
            }],
            "Out": [{
              "Name": "qResults"
            }]
          },
          "SelectAssociations": {
            "In": [{
              "Name": "qOptions",
              "DefaultValue": {
                "qSearchFields": [""],
                "qContext": 0,
                "qCharEncoding": 0,
                "qAttributes": [""]
              }
            }, {
              "Name": "qTerms",
              "DefaultValue": [""]
            }, {
              "Name": "qMatchIx",
              "DefaultValue": 0
            }, {
              "Name": "qSoftLock",
              "DefaultValue": null,
              "Optional": true
            }],
            "Out": []
          },
          "SearchResults": {
            "In": [{
              "Name": "qOptions",
              "DefaultValue": {
                "qSearchFields": [""],
                "qContext": 0,
                "qCharEncoding": 0,
                "qAttributes": [""]
              }
            }, {
              "Name": "qTerms",
              "DefaultValue": [""]
            }, {
              "Name": "qPage",
              "DefaultValue": {
                "qOffset": 0,
                "qCount": 0,
                "qMaxNbrFieldMatches": 0,
                "qGroupOptions": [{
                  "qGroupType": 0,
                  "qOffset": 0,
                  "qCount": 0
                }],
                "qGroupItemOptions": [{
                  "qGroupItemType": 0,
                  "qOffset": 0,
                  "qCount": 0
                }]
              }
            }],
            "Out": [{
              "Name": "qResult"
            }]
          },
          "SearchObjects": {
            "In": [{
              "Name": "qOptions",
              "DefaultValue": {
                "qAttributes": [""],
                "qCharEncoding": 0
              }
            }, {
              "Name": "qTerms",
              "DefaultValue": [""]
            }, {
              "Name": "qPage",
              "DefaultValue": {
                "qOffset": 0,
                "qCount": 0,
                "qMaxNbrFieldMatches": 0,
                "qGroupOptions": [{
                  "qGroupType": 0,
                  "qOffset": 0,
                  "qCount": 0
                }],
                "qGroupItemOptions": [{
                  "qGroupItemType": 0,
                  "qOffset": 0,
                  "qCount": 0
                }]
              }
            }],
            "Out": [{
              "Name": "qResult"
            }]
          },
          "GetScriptEx": {
            "In": [],
            "Out": [{
              "Name": "qScript"
            }]
          }
        },
        "Global": {
          "AbortRequest": {
            "In": [{
              "Name": "qRequestId",
              "DefaultValue": 0
            }],
            "Out": []
          },
          "AbortAll": {
            "In": [],
            "Out": []
          },
          "GetProgress": {
            "In": [{
              "Name": "qRequestId",
              "DefaultValue": 0
            }],
            "Out": [{
              "Name": "qProgressData"
            }]
          },
          "QvVersion": {
            "In": [],
            "Out": []
          },
          "OSVersion": {
            "In": [],
            "Out": []
          },
          "OSName": {
            "In": [],
            "Out": []
          },
          "QTProduct": {
            "In": [],
            "Out": []
          },
          "GetDocList": {
            "In": [],
            "Out": [{
              "Name": "qDocList"
            }]
          },
          "GetInteract": {
            "In": [{
              "Name": "qRequestId",
              "DefaultValue": 0
            }],
            "Out": [{
              "Name": "qDef"
            }]
          },
          "InteractDone": {
            "In": [{
              "Name": "qRequestId",
              "DefaultValue": 0
            }, {
              "Name": "qDef",
              "DefaultValue": {
                "qType": 0,
                "qTitle": "",
                "qMsg": "",
                "qButtons": 0,
                "qLine": "",
                "qOldLineNr": 0,
                "qNewLineNr": 0,
                "qPath": "",
                "qHidden": false,
                "qResult": 0,
                "qInput": ""
              }
            }],
            "Out": []
          },
          "GetAuthenticatedUser": {
            "In": [],
            "Out": []
          },
          "CreateDocEx": {
            "In": [{
              "Name": "qDocName",
              "DefaultValue": ""
            }, {
              "Name": "qUserName",
              "DefaultValue": "",
              "Optional": true
            }, {
              "Name": "qPassword",
              "DefaultValue": "",
              "Optional": true
            }, {
              "Name": "qSerial",
              "DefaultValue": "",
              "Optional": true
            }, {
              "Name": "qLocalizedScriptMainSection",
              "DefaultValue": "",
              "Optional": true
            }],
            "Out": [{
              "Name": "qDocId"
            }]
          },
          "GetActiveDoc": {
            "In": [],
            "Out": []
          },
          "AllowCreateApp": {
            "In": [],
            "Out": []
          },
          "CreateApp": {
            "In": [{
              "Name": "qAppName",
              "DefaultValue": ""
            }, {
              "Name": "qLocalizedScriptMainSection",
              "DefaultValue": "",
              "Optional": true
            }],
            "Out": [{
              "Name": "qSuccess"
            }, {
              "Name": "qAppId"
            }]
          },
          "DeleteApp": {
            "In": [{
              "Name": "qAppId",
              "DefaultValue": ""
            }],
            "Out": [{
              "Name": "qSuccess"
            }]
          },
          "IsDesktopMode": {
            "In": [],
            "Out": []
          },
          "GetConfiguration": {
            "In": [],
            "Out": [{
              "Name": "qConfig"
            }]
          },
          "CancelRequest": {
            "In": [{
              "Name": "qRequestId",
              "DefaultValue": 0
            }],
            "Out": []
          },
          "ShutdownProcess": {
            "In": [],
            "Out": []
          },
          "ReloadExtensionList": {
            "In": [],
            "Out": []
          },
          "ReplaceAppFromID": {
            "In": [{
              "Name": "qTargetAppId",
              "DefaultValue": ""
            }, {
              "Name": "qSrcAppID",
              "DefaultValue": ""
            }, {
              "Name": "qIds",
              "DefaultValue": [""]
            }],
            "Out": [{
              "Name": "qSuccess"
            }]
          },
          "ReplaceAppFromPath": {
            "In": [{
              "Name": "qTargetAppId",
              "DefaultValue": ""
            }, {
              "Name": "qSrcPath",
              "DefaultValue": ""
            }, {
              "Name": "qIds",
              "DefaultValue": [""]
            }, {
              "Name": "qNoData",
              "DefaultValue": false
            }],
            "Out": []
          },
          "CopyApp": {
            "In": [{
              "Name": "qTargetAppId",
              "DefaultValue": ""
            }, {
              "Name": "qSrcAppId",
              "DefaultValue": ""
            }, {
              "Name": "qIds",
              "DefaultValue": [""]
            }],
            "Out": [{
              "Name": "qSuccess"
            }]
          },
          "ImportApp": {
            "In": [{
              "Name": "qAppId",
              "DefaultValue": ""
            }, {
              "Name": "qSrcPath",
              "DefaultValue": ""
            }, {
              "Name": "qIds",
              "DefaultValue": [""]
            }],
            "Out": [{
              "Name": "qSuccess"
            }]
          },
          "ImportAppEx": {
            "In": [{
              "Name": "qAppId",
              "DefaultValue": ""
            }, {
              "Name": "qSrcPath",
              "DefaultValue": ""
            }, {
              "Name": "qIds",
              "DefaultValue": [""]
            }, {
              "Name": "qExcludeConnections",
              "DefaultValue": false
            }],
            "Out": []
          },
          "ExportApp": {
            "In": [{
              "Name": "qTargetPath",
              "DefaultValue": ""
            }, {
              "Name": "qSrcAppId",
              "DefaultValue": ""
            }, {
              "Name": "qIds",
              "DefaultValue": [""]
            }, {
              "Name": "qNoData",
              "DefaultValue": false,
              "Optional": true
            }],
            "Out": [{
              "Name": "qSuccess"
            }]
          },
          "PublishApp": {
            "In": [{
              "Name": "qAppId",
              "DefaultValue": ""
            }, {
              "Name": "qName",
              "DefaultValue": ""
            }, {
              "Name": "qStreamId",
              "DefaultValue": ""
            }],
            "Out": []
          },
          "IsPersonalMode": {
            "In": [],
            "Out": []
          },
          "GetUniqueID": {
            "In": [],
            "Out": [{
              "Name": "qUniqueID"
            }]
          },
          "OpenDoc": {
            "In": [{
              "Name": "qDocName",
              "DefaultValue": ""
            }, {
              "Name": "qUserName",
              "DefaultValue": "",
              "Optional": true
            }, {
              "Name": "qPassword",
              "DefaultValue": "",
              "Optional": true
            }, {
              "Name": "qSerial",
              "DefaultValue": "",
              "Optional": true
            }, {
              "Name": "qNoData",
              "DefaultValue": false,
              "Optional": true
            }],
            "Out": []
          },
          "CreateSessionApp": {
            "In": [],
            "Out": [{
              "Name": "qSessionAppId"
            }]
          },
          "CreateSessionAppFromApp": {
            "In": [{
              "Name": "qSrcAppId",
              "DefaultValue": ""
            }],
            "Out": [{
              "Name": "qSessionAppId"
            }]
          },
          "ProductVersion": {
            "In": [],
            "Out": []
          },
          "GetAppEntry": {
            "In": [{
              "Name": "qAppID",
              "DefaultValue": ""
            }],
            "Out": [{
              "Name": "qEntry"
            }]
          },
          "ConfigureReload": {
            "In": [{
              "Name": "qCancelOnScriptError",
              "DefaultValue": false
            }, {
              "Name": "qUseErrorData",
              "DefaultValue": false
            }, {
              "Name": "qInteractOnError",
              "DefaultValue": false
            }],
            "Out": []
          },
          "CancelReload": {
            "In": [],
            "Out": []
          },
          "GetBNF": {
            "In": [{
              "Name": "qBnfType",
              "DefaultValue": 0
            }],
            "Out": [{
              "Name": "qBnfDefs"
            }]
          },
          "GetFunctions": {
            "In": [{
              "Name": "qGroup",
              "DefaultValue": 0,
              "Optional": true
            }],
            "Out": [{
              "Name": "qFunctions"
            }]
          },
          "GetOdbcDsns": {
            "In": [],
            "Out": [{
              "Name": "qOdbcDsns"
            }]
          },
          "GetOleDbProviders": {
            "In": [],
            "Out": [{
              "Name": "qOleDbProviders"
            }]
          },
          "GetDatabasesFromConnectionString": {
            "In": [{
              "Name": "qConnection",
              "DefaultValue": {
                "qId": "",
                "qName": "",
                "qConnectionString": "",
                "qType": "",
                "qUserName": "",
                "qPassword": "",
                "qModifiedDate": "",
                "qMeta": {
                  "qName": ""
                },
                "qLogOn": 0
              }
            }],
            "Out": [{
              "Name": "qDatabases"
            }]
          },
          "IsValidConnectionString": {
            "In": [{
              "Name": "qConnection",
              "DefaultValue": {
                "qId": "",
                "qName": "",
                "qConnectionString": "",
                "qType": "",
                "qUserName": "",
                "qPassword": "",
                "qModifiedDate": "",
                "qMeta": {
                  "qName": ""
                },
                "qLogOn": 0
              }
            }],
            "Out": []
          },
          "GetDefaultAppFolder": {
            "In": [],
            "Out": [{
              "Name": "qPath"
            }]
          },
          "GetMyDocumentsFolder": {
            "In": [],
            "Out": [{
              "Name": "qFolder"
            }]
          },
          "GetLogicalDriveStrings": {
            "In": [],
            "Out": [{
              "Name": "qDrives"
            }]
          },
          "GetFolderItemsForPath": {
            "In": [{
              "Name": "qPath",
              "DefaultValue": ""
            }],
            "Out": [{
              "Name": "qFolderItems"
            }]
          },
          "GetSupportedCodePages": {
            "In": [],
            "Out": [{
              "Name": "qCodePages"
            }]
          },
          "GetCustomConnectors": {
            "In": [{
              "Name": "qReloadList",
              "DefaultValue": false,
              "Optional": true
            }],
            "Out": [{
              "Name": "qConnectors"
            }]
          },
          "GetStreamList": {
            "In": [],
            "Out": [{
              "Name": "qStreamList"
            }]
          },
          "EngineVersion": {
            "In": [],
            "Out": [{
              "Name": "qVersion"
            }]
          },
          "GetBaseBNF": {
            "In": [{
              "Name": "qBnfType",
              "DefaultValue": 0
            }],
            "Out": [{
              "Name": "qBnfDefs"
            }, {
              "Name": "qBnfHash"
            }]
          },
          "GetBaseBNFHash": {
            "In": [{
              "Name": "qBnfType",
              "DefaultValue": 0
            }],
            "Out": [{
              "Name": "qBnfHash"
            }]
          },
          "GetBaseBNFString": {
            "In": [{
              "Name": "qBnfType",
              "DefaultValue": 0
            }],
            "Out": [{
              "Name": "qBnfStr"
            }, {
              "Name": "qBnfHash"
            }]
          }
        }
      },
      "enums": {
        "LocalizedMessageCode": {
          "LOCMSG_SCRIPTEDITOR_EMPTY_MESSAGE": 0,
          "LOCMSG_SCRIPTEDITOR_PROGRESS_SAVING_STARTED": 1,
          "LOCMSG_SCRIPTEDITOR_PROGRESS_BYTES_LEFT": 2,
          "LOCMSG_SCRIPTEDITOR_PROGRESS_STORING_TABLES": 3,
          "LOCMSG_SCRIPTEDITOR_PROGRESS_QVD_ROWS_SO_FAR": 4,
          "LOCMSG_SCRIPTEDITOR_PROGRESS_CONNECTED": 5,
          "LOCMSG_SCRIPTEDITOR_PROGRESS_CONNECTING_TO": 6,
          "LOCMSG_SCRIPTEDITOR_PROGRESS_CONNECT_FAILED": 7,
          "LOCMSG_SCRIPTEDITOR_PROGRESS_QVD_ROWISH": 8,
          "LOCMSG_SCRIPTEDITOR_PROGRESS_QVD_COLUMNAR": 9,
          "LOCMSG_SCRIPTEDITOR_ERROR": 10,
          "LOCMSG_SCRIPTEDITOR_DONE": 11,
          "LOCMSG_SCRIPTEDITOR_LOAD_EXTERNAL_DATA": 12,
          "LOCMSG_SCRIPTEDITOR_PROGRESS_OLD_QVD_ISLOADING": 13,
          "LOCMSG_SCRIPTEDITOR_PROGRESS_QVC_LOADING": 14,
          "LOCMSG_SCRIPTEDITOR_PROGRESS_QVD_BUFFERED": 15,
          "LOCMSG_SCRIPTEDITOR_PROGRESS_QVC_PREPARING": 16,
          "LOCMSG_SCRIPTEDITOR_PROGRESS_QVC_APPENDING": 17,
          "LOCMSG_SCRIPTEDITOR_REMOVE_SYNTHETIC": 18,
          "LOCMSG_SCRIPTEDITOR_PENDING_LINKEDTABLE_FETCHING": 19,
          "LOCMSG_SCRIPTEDITOR_RELOAD": 20,
          "LOCMSG_SCRIPTEDITOR_LINES_FETCHED": 21,
          "LOCMSG_SCRIPTEDITOR_SEARCHINDEX_START": 22,
          "LOCMSG_SCRIPTEDITOR_SEARCHINDEX_FIELD": 23,
          "LOCMSG_SCRIPTEDITOR_SEARCHINDEX_SUCCESS": 24,
          "LOCMSG_SCRIPTEDITOR_SEARCHINDEX_FAILURE": 25,
          "LOCMSG_SCRIPTEDITOR_SEARCHINDEX_STARTABORT": 26,
          "LOCMSG_SCRIPTEDITOR_SEARCHINDEX_ENDABORT": 27,
          "LOCMSG_SCRIPTEDITOR_SEARCHINDEX_TIMEOUT": 28,
          "LOCMSG_SCRIPTEDITOR_SEARCHINDEX_OUTOFMEMORY": 29
        },
        "LocalizedErrorCode": {
          "LOCERR_INTERNAL_ERROR": -128,
          "LOCERR_GENERIC_UNKNOWN": -1,
          "LOCERR_GENERIC_OK": 0,
          "LOCERR_GENERIC_NOT_SET": 1,
          "LOCERR_GENERIC_NOT_FOUND": 2,
          "LOCERR_GENERIC_ALREADY_EXISTS": 3,
          "LOCERR_GENERIC_INVALID_PATH": 4,
          "LOCERR_GENERIC_ACCESS_DENIED": 5,
          "LOCERR_GENERIC_OUT_OF_MEMORY": 6,
          "LOCERR_GENERIC_NOT_INITIALIZED": 7,
          "LOCERR_GENERIC_INVALID_PARAMETERS": 8,
          "LOCERR_GENERIC_EMPTY_PARAMETERS": 9,
          "LOCERR_GENERIC_INTERNAL_ERROR": 10,
          "LOCERR_GENERIC_CORRUPT_DATA": 11,
          "LOCERR_GENERIC_MEMORY_INCONSISTENCY": 12,
          "LOCERR_GENERIC_INVISIBLE_OWNER_ABORT": 13,
          "LOCERR_GENERIC_PROHIBIT_VALIDATE": 14,
          "LOCERR_GENERIC_ABORTED": 15,
          "LOCERR_GENERIC_CONNECTION_LOST": 16,
          "LOCERR_GENERIC_UNSUPPORTED_IN_PRODUCT_VERSION": 17,
          "LOCERR_GENERIC_REST_CONNECTION_FAILURE": 18,
          "LOCERR_GENERIC_MEMORY_LIMIT_REACHED": 19,
          "LOCERR_HTTP_400": 400,
          "LOCERR_HTTP_401": 401,
          "LOCERR_HTTP_402": 402,
          "LOCERR_HTTP_403": 403,
          "LOCERR_HTTP_404": 404,
          "LOCERR_HTTP_405": 405,
          "LOCERR_HTTP_406": 406,
          "LOCERR_HTTP_407": 407,
          "LOCERR_HTTP_408": 408,
          "LOCERR_HTTP_409": 409,
          "LOCERR_HTTP_410": 410,
          "LOCERR_HTTP_411": 411,
          "LOCERR_HTTP_412": 412,
          "LOCERR_HTTP_413": 413,
          "LOCERR_HTTP_414": 414,
          "LOCERR_HTTP_415": 415,
          "LOCERR_HTTP_416": 416,
          "LOCERR_HTTP_417": 417,
          "LOCERR_HTTP_422": 422,
          "LOCERR_HTTP_429": 429,
          "LOCERR_HTTP_500": 500,
          "LOCERR_HTTP_501": 501,
          "LOCERR_HTTP_502": 502,
          "LOCERR_HTTP_503": 503,
          "LOCERR_HTTP_504": 504,
          "LOCERR_HTTP_505": 505,
          "LOCERR_HTTP_509": 509,
          "LOCERR_HTTP_COULD_NOT_RESOLVE_HOST": 700,
          "LOCERR_APP_ALREADY_EXISTS": 1000,
          "LOCERR_APP_INVALID_NAME": 1001,
          "LOCERR_APP_ALREADY_OPEN": 1002,
          "LOCERR_APP_NOT_FOUND": 1003,
          "LOCERR_APP_IMPORT_FAILED": 1004,
          "LOCERR_APP_SAVE_FAILED": 1005,
          "LOCERR_APP_CREATE_FAILED": 1006,
          "LOCERR_APP_INVALID": 1007,
          "LOCERR_APP_CONNECT_FAILED": 1008,
          "LOCERR_APP_ALREADY_OPEN_IN_DIFFERENT_MODE": 1009,
          "LOCERR_APP_MIGRATION_COULD_NOT_CONTACT_MIGRATION_SERVICE": 1010,
          "LOCERR_APP_MIGRATION_COULD_NOT_START_MIGRATION": 1011,
          "LOCERR_APP_MIGRATION_FAILURE": 1012,
          "LOCERR_APP_SCRIPT_MISSING": 1013,
          "LOCERR_APP_EXPORT_FAILED": 1014,
          "LOCERR_CONNECTION_ALREADY_EXISTS": 2000,
          "LOCERR_CONNECTION_NOT_FOUND": 2001,
          "LOCERR_CONNECTION_FAILED_TO_LOAD": 2002,
          "LOCERR_CONNECTION_FAILED_TO_IMPORT": 2003,
          "LOCERR_CONNECTION_NAME_IS_INVALID": 2004,
          "LOCERR_CONNECTOR_NO_FILE_STREAMING_SUPPORT": 2300,
          "LOCERR_CONNECTOR_FILESIZE_EXCEEDED_BUFFER_SIZE": 2301,
          "LOCERR_FILE_ACCESS_DENIED": 3000,
          "LOCERR_FILE_NAME_INVALID": 3001,
          "LOCERR_FILE_CORRUPT": 3002,
          "LOCERR_FILE_NOT_FOUND": 3003,
          "LOCERR_FILE_FORMAT_UNSUPPORTED": 3004,
          "LOCERR_FILE_OPENED_IN_UNSUPPORTED_MODE": 3005,
          "LOCERR_FILE_TABLE_NOT_FOUND": 3006,
          "LOCERR_USER_ACCESS_DENIED": 4000,
          "LOCERR_USER_IMPERSONATION_FAILED": 4001,
          "LOCERR_SERVER_OUT_OF_SESSION_AND_USER_CALS": 5000,
          "LOCERR_SERVER_OUT_OF_SESSION_CALS": 5001,
          "LOCERR_SERVER_OUT_OF_USAGE_CALS": 5002,
          "LOCERR_SERVER_OUT_OF_CALS": 5003,
          "LOCERR_SERVER_OUT_OF_NAMED_CALS": 5004,
          "LOCERR_SERVER_OFF_DUTY": 5005,
          "LOCERR_SERVER_BUSY": 5006,
          "LOCERR_SERVER_LICENSE_EXPIRED": 5007,
          "LOCERR_SERVER_AJAX_DISABLED": 5008,
          "LOCERR_HC_INVALID_OBJECT": 6000,
          "LOCERR_HC_RESULT_TOO_LARGE": 6001,
          "LOCERR_HC_INVALID_OBJECT_STATE": 6002,
          "LOCERR_HC_MODAL_OBJECT_ERROR": 6003,
          "LOCERR_CALC_INVALID_DEF": 7000,
          "LOCERR_CALC_NOT_IN_LIB": 7001,
          "LOCERR_CALC_HEAP_ERROR": 7002,
          "LOCERR_CALC_TOO_LARGE": 7003,
          "LOCERR_CALC_TIMEOUT": 7004,
          "LOCERR_CALC_EVAL_CONDITION_FAILED": 7005,
          "LOCERR_CALC_MIXED_LINKED_AGGREGATION": 7006,
          "LOCERR_CALC_MISSING_LINKED": 7007,
          "LOCERR_CALC_INVALID_COL_SORT": 7008,
          "LOCERR_CALC_PAGES_TOO_LARGE": 7009,
          "LOCERR_CALC_SEMANTIC_FIELD_NOT_ALLOWED": 7010,
          "LOCERR_CALC_VALIDATION_STATE_INVALID": 7011,
          "LOCERR_CALC_PIVOT_DIMENSIONS_ALREADY_EXISTS": 7012,
          "LOCERR_CALC_MISSING_LINKED_FIELD": 7013,
          "LOCERR_CALC_NOT_CALCULATED": 7014,
          "LOCERR_LAYOUT_EXTENDS_INVALID_ID": 8000,
          "LOCERR_LAYOUT_LINKED_OBJECT_NOT_FOUND": 8001,
          "LOCERR_LAYOUT_LINKED_OBJECT_INVALID": 8002,
          "LOCERR_PERSISTENCE_WRITE_FAILED": 9000,
          "LOCERR_PERSISTENCE_READ_FAILED": 9001,
          "LOCERR_PERSISTENCE_DELETE_FAILED": 9002,
          "LOCERR_PERSISTENCE_NOT_FOUND": 9003,
          "LOCERR_PERSISTENCE_UNSUPPORTED_VERSION": 9004,
          "LOCERR_PERSISTENCE_MIGRATION_FAILED_READ_ONLY": 9005,
          "LOCERR_PERSISTENCE_MIGRATION_CANCELLED": 9006,
          "LOCERR_PERSISTENCE_MIGRATION_BACKUP_FAILED": 9007,
          "LOCERR_PERSISTENCE_DISK_FULL": 9008,
          "LOCERR_PERSISTENCE_NOT_SUPPORTED_FOR_SESSION_APP": 9009,
          "LOCERR_PERSISTENCE_SYNC_SET_CHUNK_INVALID_PARAMETERS": 9510,
          "LOCERR_PERSISTENCE_SYNC_GET_CHUNK_INVALID_PARAMETERS": 9511,
          "LOCERR_SCRIPT_DATASOURCE_ACCESS_DENIED": 10000,
          "LOCERR_RELOAD_IN_PROGRESS": 11000,
          "LOCERR_RELOAD_TABLE_X_NOT_FOUND": 11001,
          "LOCERR_RELOAD_UNKNOWN_STATEMENT": 11002,
          "LOCERR_RELOAD_EXPECTED_SOMETHING_FOUND_UNKNOWN": 11003,
          "LOCERR_RELOAD_EXPECTED_NOTHING_FOUND_UNKNOWN": 11004,
          "LOCERR_RELOAD_EXPECTED_ONE_OF_1_TOKENS_FOUND_UNKNOWN": 11005,
          "LOCERR_RELOAD_EXPECTED_ONE_OF_2_TOKENS_FOUND_UNKNOWN": 11006,
          "LOCERR_RELOAD_EXPECTED_ONE_OF_3_TOKENS_FOUND_UNKNOWN": 11007,
          "LOCERR_RELOAD_EXPECTED_ONE_OF_4_TOKENS_FOUND_UNKNOWN": 11008,
          "LOCERR_RELOAD_EXPECTED_ONE_OF_5_TOKENS_FOUND_UNKNOWN": 11009,
          "LOCERR_RELOAD_EXPECTED_ONE_OF_6_TOKENS_FOUND_UNKNOWN": 11010,
          "LOCERR_RELOAD_EXPECTED_ONE_OF_7_TOKENS_FOUND_UNKNOWN": 11011,
          "LOCERR_RELOAD_EXPECTED_ONE_OF_8_OR_MORE_TOKENS_FOUND_UNKNOWN": 11012,
          "LOCERR_RELOAD_FIELD_X_NOT_FOUND": 11013,
          "LOCERR_RELOAD_MAPPING_TABLE_X_NOT_FOUND": 11014,
          "LOCERR_RELOAD_LIB_CONNECTION_X_NOT_FOUND": 11015,
          "LOCERR_RELOAD_NAME_ALREADY_TAKEN": 11016,
          "LOCERR_RELOAD_WRONG_FILE_FORMAT_DIF": 11017,
          "LOCERR_RELOAD_WRONG_FILE_FORMAT_BIFF": 11018,
          "LOCERR_RELOAD_WRONG_FILE_FORMAT_ENCRYPTED": 11019,
          "LOCERR_RELOAD_OPEN_FILE_ERROR": 11020,
          "LOCERR_RELOAD_AUTO_GENERATE_COUNT": 11021,
          "LOCERR_RELOAD_PE_ILLEGAL_PREFIX_COMB": 11022,
          "LOCERR_RELOAD_MATCHING_CONTROL_STATEMENT_ERROR": 11023,
          "LOCERR_RELOAD_MATCHING_LIBPATH_X_NOT_FOUND": 11024,
          "LOCERR_RELOAD_MATCHING_LIBPATH_X_INVALID": 11025,
          "LOCERR_RELOAD_MATCHING_LIBPATH_X_OUTSIDE": 11026,
          "LOCERR_RELOAD_NO_QUALIFIED_PATH_FOR_FILE": 11027,
          "LOCERR_RELOAD_MODE_STATEMENT_ONLY_FOR_LIB_PATHS": 11028,
          "LOCERR_RELOAD_INCONSISTENT_USE_OF_SEMANTIC_FIELDS": 11029,
          "LOCERR_RELOAD_NO_OPEN_DATABASE": 11030,
          "LOCERR_RELOAD_AGGREGATION_REQUIRED_BY_GROUP_BY": 11031,
          "LOCERR_RELOAD_CONNECT_MUST_USE_LIB_PREFIX_IN_THIS_MODE": 11032,
          "LOCERR_RELOAD_ODBC_CONNECT_FAILED": 11033,
          "LOCERR_RELOAD_OLEDB_CONNECT_FAILED": 11034,
          "LOCERR_RELOAD_CUSTOM_CONNECT_FAILED": 11035,
          "LOCERR_RELOAD_ODBC_READ_FAILED": 11036,
          "LOCERR_RELOAD_OLEDB_READ_FAILED": 11037,
          "LOCERR_RELOAD_CUSTOM_READ_FAILED": 11038,
          "LOCERR_RELOAD_BINARY_LOAD_PROHIBITED": 11039,
          "LOCERR_RELOAD_CONNECTOR_START_FAILED": 11040,
          "LOCERR_RELOAD_CONNECTOR_NOT_RESPONDING": 11041,
          "LOCERR_RELOAD_CONNECTOR_REPLY_ERROR": 11042,
          "LOCERR_RELOAD_CONNECTOR_CONNECT_ERROR": 11043,
          "LOCERR_RELOAD_CONNECTOR_NOT_FOUND_ERROR": 11044,
          "LOCERR_RELOAD_INPUT_FIELD_WITH_DUPLICATE_KEYS": 11045,
          "LOCERR_RELOAD_CONCATENATE_LOAD_NO_PREVIOUS_TABLE": 11046,
          "LOCERR_PERSONAL_NEW_VERSION_AVAILABLE": 12000,
          "LOCERR_PERSONAL_VERSION_EXPIRED": 12001,
          "LOCERR_PERSONAL_SECTION_ACCESS_DETECTED": 12002,
          "LOCERR_PERSONAL_APP_DELETION_FAILED": 12003,
          "LOCERR_USER_AUTHENTICATION_FAILURE": 12004,
          "LOCERR_EXPORT_OUT_OF_MEMORY": 13000,
          "LOCERR_EXPORT_NO_DATA": 13001,
          "LOCERR_SYNC_INVALID_OFFSET": 14000,
          "LOCERR_SEARCH_TIMEOUT": 15000,
          "LOCERR_DIRECT_DISCOVERY_LINKED_EXPRESSION_FAIL": 16000,
          "LOCERR_DIRECT_DISCOVERY_ROWCOUNT_OVERFLOW": 16001,
          "LOCERR_DIRECT_DISCOVERY_EMPTY_RESULT": 16002,
          "LOCERR_DIRECT_DISCOVERY_DB_CONNECTION_FAILED": 16003,
          "LOCERR_DIRECT_DISCOVERY_MEASURE_NOT_ALLOWED": 16004,
          "LOCERR_DIRECT_DISCOVERY_DETAIL_NOT_ALLOWED": 16005,
          "LOCERR_DIRECT_DISCOVERY_NOT_SYNTH_CIRCULAR_ALLOWED": 16006,
          "LOCERR_DIRECT_DISCOVERY_ONLY_ONE_DD_TABLE_ALLOWED": 16007,
          "LOCERR_DIRECT_DISCOVERY_DB_AUTHORIZATION_FAILED": 16008,
          "LOCERR_SMART_LOAD_TABLE_NOT_FOUND": 17000,
          "LOCERR_SMART_LOAD_TABLE_DUPLICATED": 17001,
          "LOCERR_VARIABLE_NO_NAME": 18000,
          "LOCERR_VARIABLE_DUPLICATE_NAME": 18001,
          "LOCERR_VARIABLE_INCONSISTENCY": 18002,
          "LOCERR_MEDIA_LIBRARY_LIST_FAILED": 19000,
          "LOCERR_MEDIA_LIBRARY_CONTENT_FAILED": 19001,
          "LOCERR_MEDIA_BUNDLING_FAILED": 19002,
          "LOCERR_MEDIA_UNBUNDLING_FAILED": 19003,
          "LOCERR_MEDIA_LIBRARY_NOT_FOUND": 19004,
          "LOCERR_FEATURE_DISABLED": 20000,
          "LOCERR_JSON_RPC_INVALID_REQUEST": -32600,
          "LOCERR_JSON_RPC_METHOD_NOT_FOUND": -32601,
          "LOCERR_JSON_RPC_INVALID_PARAMETERS": -32602,
          "LOCERR_JSON_RPC_INTERNAL_ERROR": -32603,
          "LOCERR_JSON_RPC_PARSE_ERROR": -32700,
          "LOCERR_MQ_SOCKET_CONNECT_FAILURE": 33000,
          "LOCERR_MQ_SOCKET_OPEN_FAILURE": 33001,
          "LOCERR_MQ_PROTOCOL_NO_RESPONE": 33002,
          "LOCERR_MQ_PROTOCOL_LIBRARY_EXCEPTION": 33003,
          "LOCERR_MQ_PROTOCOL_CONNECTION_CLOSED": 33004,
          "LOCERR_MQ_PROTOCOL_CHANNEL_CLOSED": 33005,
          "LOCERR_MQ_PROTOCOL_UNKNOWN_ERROR": 33006,
          "LOCERR_MQ_PROTOCOL_INVALID_STATUS": 33007,
          "LOCERR_EXTENGINE_GRPC_STATUS_OK": 22000,
          "LOCERR_EXTENGINE_GRPC_STATUS_CANCELLED": 22001,
          "LOCERR_EXTENGINE_GRPC_STATUS_UNKNOWN": 22002,
          "LOCERR_EXTENGINE_GRPC_STATUS_INVALID_ARGUMENT": 22003,
          "LOCERR_EXTENGINE_GRPC_STATUS_DEADLINE_EXCEEDED": 22004,
          "LOCERR_EXTENGINE_GRPC_STATUS_NOT_FOUND": 22005,
          "LOCERR_EXTENGINE_GRPC_STATUS_ALREADY_EXISTS": 22006,
          "LOCERR_EXTENGINE_GRPC_STATUS_PERMISSION_DENIED": 22007,
          "LOCERR_EXTENGINE_GRPC_STATUS_RESOURCE_EXHAUSTED": 22008,
          "LOCERR_EXTENGINE_GRPC_STATUS_FAILED_PRECONDITION": 22009,
          "LOCERR_EXTENGINE_GRPC_STATUS_ABORTED": 22010,
          "LOCERR_EXTENGINE_GRPC_STATUS_OUT_OF_RANGE": 22011,
          "LOCERR_EXTENGINE_GRPC_STATUS_UNIMPLEMENTED": 22012,
          "LOCERR_EXTENGINE_GRPC_STATUS_INTERNAL": 22013,
          "LOCERR_EXTENGINE_GRPC_STATUS_UNAVAILABLE": 22014,
          "LOCERR_EXTENGINE_GRPC_STATUS_DATA_LOSS": 22015,
          "LOCERR_EXTENGINE_GRPC_STATUS_UNAUTHENTICATED": 22016,
          "LOCERR_LXW_INVALID_OBJ": 23001,
          "LOCERR_LXW_INVALID_FILE": 23002,
          "LOCERR_LXW_INVALID_SHEET": 23003,
          "LOCERR_LXW_INVALID_EXPORT_RANGE": 23004,
          "LOCERR_LXW_ERROR": 23005,
          "LOCERR_LXW_ERROR_MEMORY_MALLOC_FAILED": 23006,
          "LOCERR_LXW_ERROR_CREATING_XLSX_FILE": 23007,
          "LOCERR_LXW_ERROR_CREATING_TMPFILE": 23008,
          "LOCERR_LXW_ERROR_ZIP_FILE_OPERATION": 23009,
          "LOCERR_LXW_ERROR_ZIP_FILE_ADD": 23010,
          "LOCERR_LXW_ERROR_ZIP_CLOSE": 23011,
          "LOCERR_LXW_ERROR_NULL_PARAMETER_IGNORED": 23012,
          "LOCERR_LXW_ERROR_MAX_STRING_LENGTH_EXCEEDED": 23013,
          "LOCERR_LXW_ERROR_255_STRING_LENGTH_EXCEEDED": 23014,
          "LOCERR_LXW_ERROR_SHARED_STRING_INDEX_NOT_FOUND": 23015,
          "LOCERR_LXW_ERROR_WORKSHEET_INDEX_OUT_OF_RANGE": 23016,
          "LOCERR_LXW_ERROR_WORKSHEET_MAX_NUMBER_URLS_EXCEEDED": 23017,
          "LOCERR_CURL_UNSUPPORTED_PROTOCOL": 30000,
          "LOCERR_CURL_COULDNT_RESOLVE_PROXY": 30001,
          "LOCERR_CURL_COULDNT_CONNECT": 30002,
          "LOCERR_CURL_REMOTE_ACCESS_DENIED": 30003,
          "LOCERR_CURL_FTP_ACCEPT_FAILED": 30004,
          "LOCERR_CURL_FTP_ACCEPT_TIMEOUT": 30005,
          "LOCERR_CURL_FTP_CANT_GET_HOST": 30006,
          "LOCERR_CURL_PARTIAL_FILE": 30007,
          "LOCERR_CURL_QUOTE_ERROR": 30008,
          "LOCERR_CURL_WRITE_ERROR": 30009,
          "LOCERR_CURL_UPLOAD_FAILED": 30010,
          "LOCERR_CURL_OUT_OF_MEMORY": 30011,
          "LOCERR_CURL_OPERATION_TIMEDOUT": 30012,
          "LOCERR_CURL_FTP_COULDNT_USE_REST": 30013,
          "LOCERR_CURL_HTTP_POST_ERROR": 30014,
          "LOCERR_CURL_SSL_CONNECT_ERROR": 30015,
          "LOCERR_CURL_FILE_COULDNT_READ_FILE": 30016,
          "LOCERR_CURL_LDAP_CANNOT_BIND": 30017,
          "LOCERR_CURL_LDAP_SEARCH_FAILED": 30018,
          "LOCERR_CURL_TOO_MANY_REDIRECTS": 30019,
          "LOCERR_CURL_PEER_FAILED_VERIFICATION": 30020,
          "LOCERR_CURL_GOT_NOTHING": 30021,
          "LOCERR_CURL_SSL_ENGINE_NOTFOUND": 30022,
          "LOCERR_CURL_SSL_ENGINE_SETFAILED": 30023,
          "LOCERR_CURL_SSL_CERTPROBLEM": 30024,
          "LOCERR_CURL_SSL_CIPHER": 30025,
          "LOCERR_CURL_SSL_CACERT": 30026,
          "LOCERR_CURL_BAD_CONTENT_ENCODING": 30027,
          "LOCERR_CURL_LDAP_INVALID_URL": 30028,
          "LOCERR_CURL_USE_SSL_FAILED": 30029,
          "LOCERR_CURL_SSL_ENGINE_INITFAILED": 30030,
          "LOCERR_CURL_LOGIN_DENIED": 30031,
          "LOCERR_CURL_TFTP_NOTFOUND": 30032,
          "LOCERR_CURL_TFTP_ILLEGAL": 30033,
          "LOCERR_CURL_SSH": 30034
        },
        "LocalizedWarningCode": {
          "LOCWARN_PERSONAL_RELOAD_REQUIRED": 0,
          "LOCWARN_PERSONAL_VERSION_EXPIRES_SOON": 1,
          "LOCWARN_EXPORT_DATA_TRUNCATED": 1000,
          "LOCWARN_COULD_NOT_OPEN_ALL_OBJECTS": 2000
        },
        "GrpType": {
          "GRP_NX_NONE": 0,
          "GRP_NX_HIEARCHY": 1,
          "GRP_NX_COLLECTION": 2
        },
        "ExportFileType": {
          "EXPORT_CSV_C": 0,
          "EXPORT_CSV_T": 1,
          "EXPORT_OOXML": 2
        },
        "ExportState": {
          "EXPORT_POSSIBLE": 0,
          "EXPORT_ALL": 1
        },
        "DimCellType": {
          "NX_DIM_CELL_VALUE": 0,
          "NX_DIM_CELL_EMPTY": 1,
          "NX_DIM_CELL_NORMAL": 2,
          "NX_DIM_CELL_TOTAL": 3,
          "NX_DIM_CELL_OTHER": 4,
          "NX_DIM_CELL_AGGR": 5,
          "NX_DIM_CELL_PSEUDO": 6,
          "NX_DIM_CELL_ROOT": 7,
          "NX_DIM_CELL_NULL": 8,
          "NX_DIM_CELL_GENERATED": 9
        },
        "StackElemType": {
          "NX_STACK_CELL_NORMAL": 0,
          "NX_STACK_CELL_TOTAL": 1,
          "NX_STACK_CELL_OTHER": 2,
          "NX_STACK_CELL_SUM": 3,
          "NX_STACK_CELL_VALUE": 4,
          "NX_STACK_CELL_PSEUDO": 5
        },
        "SortIndicatorType": {
          "NX_SORT_INDICATE_NONE": 0,
          "NX_SORT_INDICATE_ASC": 1,
          "NX_SORT_INDICATE_DESC": 2
        },
        "DimensionType": {
          "NX_DIMENSION_TYPE_DISCRETE": 0,
          "NX_DIMENSION_TYPE_NUMERIC": 1,
          "NX_DIMENSION_TYPE_TIME": 2
        },
        "FieldSelectionMode": {
          "SELECTION_MODE_NORMAL": 0,
          "SELECTION_MODE_AND": 1,
          "SELECTION_MODE_NOT": 2
        },
        "FrequencyMode": {
          "NX_FREQUENCY_NONE": 0,
          "NX_FREQUENCY_VALUE": 1,
          "NX_FREQUENCY_PERCENT": 2,
          "NX_FREQUENCY_RELATIVE": 3
        },
        "DataReductionMode": {
          "DATA_REDUCTION_NONE": 0,
          "DATA_REDUCTION_ONEDIM": 1,
          "DATA_REDUCTION_SCATTERED": 2,
          "DATA_REDUCTION_CLUSTERED": 3,
          "DATA_REDUCTION_STACKED": 4
        },
        "HypercubeMode": {
          "DATA_MODE_STRAIGHT": 0,
          "DATA_MODE_PIVOT": 1,
          "DATA_MODE_PIVOT_STACK": 2,
          "DATA_MODE_TREE": 3
        },
        "PatchOperationType": {
          "Add": 0,
          "Remove": 1,
          "Replace": 2
        },
        "SelectionCellType": {
          "NX_CELL_DATA": 0,
          "NX_CELL_TOP": 1,
          "NX_CELL_LEFT": 2
        },
        "MatchingFieldMode": {
          "MATCHINGFIELDMODE_MATCH_ALL": 0,
          "MATCHINGFIELDMODE_MATCH_ONE": 1
        },
        "SessionState": {
          "SESSION_CREATED": 0,
          "SESSION_ATTACHED": 1,
          "SESSION_ERROR_NO_LICENSE": 2,
          "SESSION_ERROR_LICENSE_RENEW": 3
        },
        "ReloadState": {
          "RELOAD_PAUSED": 0,
          "RELOAD_STARTED": 1,
          "RELOAD_ABORTED": 2
        },
        "QrsChangeType": {
          "QRS_CHANGE_UNDEFINED": 0,
          "QRS_CHANGE_ADD": 1,
          "QRS_CHANGE_UPDATE": 2,
          "QRS_CHANGE_DELETE": 3
        },
        "ExtEngineDataType": {
          "NX_EXT_DATATYPE_STRING": 0,
          "NX_EXT_DATATYPE_DOUBLE": 1,
          "NX_EXT_DATATYPE_BOTH": 2
        },
        "ExtEngineFunctionType": {
          "NX_EXT_FUNCTIONTYPE_SCALAR": 0,
          "NX_EXT_FUNCTIONTYPE_AGGR": 1,
          "NX_EXT_FUNCTIONTYPE_TENSOR": 2
        },
        "ExtEngineMsgType": {
          "NX_EXT_MSGTYPE_FUNCTION_CALL": 1,
          "NX_EXT_MSGTYPE_SCRIPT_CALL": 2,
          "NX_EXT_MSGTYPE_RETURN_VALUE": 3,
          "NX_EXT_MSGTYPE_RETURN_MULTIPLE": 4,
          "NX_EXT_MSGTYPE_RETURN_ERROR": 5
        }
      },
      "version": "12.170.2"
    };
    /***/
  },

  /***/
  "a47f":
  /***/
  function a47f(module, exports, __webpack_require__) {
    module.exports = !__webpack_require__("7d95") && !__webpack_require__("d782")(function () {
      return Object.defineProperty(__webpack_require__("12fd")('div'), 'a', {
        get: function get() {
          return 7;
        }
      }).a != 7;
    });
    /***/
  },

  /***/
  "a5ab":
  /***/
  function a5ab(module, exports, __webpack_require__) {
    // 7.1.15 ToLength
    var toInteger = __webpack_require__("a812");

    var min = Math.min;

    module.exports = function (it) {
      return it > 0 ? min(toInteger(it), 0x1fffffffffffff) : 0; // pow(2, 53) - 1 == 9007199254740991
    };
    /***/

  },

  /***/
  "a7d3":
  /***/
  function a7d3(module, exports) {
    var core = module.exports = {
      version: '2.6.8'
    };
    if (typeof __e == 'number') __e = core; // eslint-disable-line no-undef

    /***/
  },

  /***/
  "a812":
  /***/
  function a812(module, exports) {
    // 7.1.4 ToInteger
    var ceil = Math.ceil;
    var floor = Math.floor;

    module.exports = function (it) {
      return isNaN(it = +it) ? 0 : (it > 0 ? floor : ceil)(it);
    };
    /***/

  },

  /***/
  "ac4d":
  /***/
  function ac4d(module, exports, __webpack_require__) {
    __webpack_require__("3a72")('asyncIterator');
    /***/

  },

  /***/
  "ac6a":
  /***/
  function ac6a(module, exports, __webpack_require__) {
    var $iterators = __webpack_require__("cadf");

    var getKeys = __webpack_require__("0d58");

    var redefine = __webpack_require__("2aba");

    var global = __webpack_require__("7726");

    var hide = __webpack_require__("32e9");

    var Iterators = __webpack_require__("84f2");

    var wks = __webpack_require__("2b4c");

    var ITERATOR = wks('iterator');
    var TO_STRING_TAG = wks('toStringTag');
    var ArrayValues = Iterators.Array;
    var DOMIterables = {
      CSSRuleList: true,
      // TODO: Not spec compliant, should be false.
      CSSStyleDeclaration: false,
      CSSValueList: false,
      ClientRectList: false,
      DOMRectList: false,
      DOMStringList: false,
      DOMTokenList: true,
      DataTransferItemList: false,
      FileList: false,
      HTMLAllCollection: false,
      HTMLCollection: false,
      HTMLFormElement: false,
      HTMLSelectElement: false,
      MediaList: true,
      // TODO: Not spec compliant, should be false.
      MimeTypeArray: false,
      NamedNodeMap: false,
      NodeList: true,
      PaintRequestList: false,
      Plugin: false,
      PluginArray: false,
      SVGLengthList: false,
      SVGNumberList: false,
      SVGPathSegList: false,
      SVGPointList: false,
      SVGStringList: false,
      SVGTransformList: false,
      SourceBufferList: false,
      StyleSheetList: true,
      // TODO: Not spec compliant, should be false.
      TextTrackCueList: false,
      TextTrackList: false,
      TouchList: false
    };

    for (var collections = getKeys(DOMIterables), i = 0; i < collections.length; i++) {
      var NAME = collections[i];
      var explicit = DOMIterables[NAME];
      var Collection = global[NAME];
      var proto = Collection && Collection.prototype;
      var key;

      if (proto) {
        if (!proto[ITERATOR]) hide(proto, ITERATOR, ArrayValues);
        if (!proto[TO_STRING_TAG]) hide(proto, TO_STRING_TAG, NAME);
        Iterators[NAME] = ArrayValues;
        if (explicit) for (key in $iterators) {
          if (!proto[key]) redefine(proto, key, $iterators[key], true);
        }
      }
    }
    /***/

  },

  /***/
  "b0bc":
  /***/
  function b0bc(module, exports) {
    module.exports = function (it, Constructor, name, forbiddenField) {
      if (!(it instanceof Constructor) || forbiddenField !== undefined && forbiddenField in it) {
        throw TypeError(name + ': incorrect invocation!');
      }

      return it;
    };
    /***/

  },

  /***/
  "b22a":
  /***/
  function b22a(module, exports) {
    module.exports = {};
    /***/
  },

  /***/
  "b3e7":
  /***/
  function b3e7(module, exports) {
    module.exports = function () {
      /* empty */
    };
    /***/

  },

  /***/
  "b42c":
  /***/
  function b42c(module, exports, __webpack_require__) {
    __webpack_require__("fa54");

    var global = __webpack_require__("da3c");

    var hide = __webpack_require__("8ce0");

    var Iterators = __webpack_require__("b22a");

    var TO_STRING_TAG = __webpack_require__("1b55")('toStringTag');

    var DOMIterables = ('CSSRuleList,CSSStyleDeclaration,CSSValueList,ClientRectList,DOMRectList,DOMStringList,' + 'DOMTokenList,DataTransferItemList,FileList,HTMLAllCollection,HTMLCollection,HTMLFormElement,HTMLSelectElement,' + 'MediaList,MimeTypeArray,NamedNodeMap,NodeList,PaintRequestList,Plugin,PluginArray,SVGLengthList,SVGNumberList,' + 'SVGPathSegList,SVGPointList,SVGStringList,SVGTransformList,SourceBufferList,StyleSheetList,TextTrackCueList,' + 'TextTrackList,TouchList').split(',');

    for (var i = 0; i < DOMIterables.length; i++) {
      var NAME = DOMIterables[i];
      var Collection = global[NAME];
      var proto = Collection && Collection.prototype;
      if (proto && !proto[TO_STRING_TAG]) hide(proto, TO_STRING_TAG, NAME);
      Iterators[NAME] = Iterators.Array;
    }
    /***/

  },

  /***/
  "b457":
  /***/
  function b457(module, exports) {
    module.exports = true;
    /***/
  },

  /***/
  "b604":
  /***/
  function b604(module, exports, __webpack_require__) {
    "use strict"; // https://github.com/tc39/proposal-promise-finally

    var $export = __webpack_require__("d13f");

    var core = __webpack_require__("a7d3");

    var global = __webpack_require__("da3c");

    var speciesConstructor = __webpack_require__("302f");

    var promiseResolve = __webpack_require__("decf");

    $export($export.P + $export.R, 'Promise', {
      'finally': function _finally(onFinally) {
        var C = speciesConstructor(this, core.Promise || global.Promise);
        var isFunction = typeof onFinally == 'function';
        return this.then(isFunction ? function (x) {
          return promiseResolve(C, onFinally()).then(function () {
            return x;
          });
        } : onFinally, isFunction ? function (e) {
          return promiseResolve(C, onFinally()).then(function () {
            throw e;
          });
        } : onFinally);
      }
    });
    /***/
  },

  /***/
  "bc25":
  /***/
  function bc25(module, exports, __webpack_require__) {
    // optional / simple context binding
    var aFunction = __webpack_require__("f2fe");

    module.exports = function (fn, that, length) {
      aFunction(fn);
      if (that === undefined) return fn;

      switch (length) {
        case 1:
          return function (a) {
            return fn.call(that, a);
          };

        case 2:
          return function (a, b) {
            return fn.call(that, a, b);
          };

        case 3:
          return function (a, b, c) {
            return fn.call(that, a, b, c);
          };
      }

      return function ()
      /* ...args */
      {
        return fn.apply(that, arguments);
      };
    };
    /***/

  },

  /***/
  "be13":
  /***/
  function be13(module, exports) {
    // 7.2.1 RequireObjectCoercible(argument)
    module.exports = function (it) {
      if (it == undefined) throw TypeError("Can't call method on  " + it);
      return it;
    };
    /***/

  },

  /***/
  "c0d8":
  /***/
  function c0d8(module, exports, __webpack_require__) {
    var def = __webpack_require__("3adc").f;

    var has = __webpack_require__("43c8");

    var TAG = __webpack_require__("1b55")('toStringTag');

    module.exports = function (it, tag, stat) {
      if (it && !has(it = stat ? it : it.prototype, TAG)) def(it, TAG, {
        configurable: true,
        value: tag
      });
    };
    /***/

  },

  /***/
  "c227":
  /***/
  function c227(module, exports, __webpack_require__) {
    // check on default Array iterator
    var Iterators = __webpack_require__("b22a");

    var ITERATOR = __webpack_require__("1b55")('iterator');

    var ArrayProto = Array.prototype;

    module.exports = function (it) {
      return it !== undefined && (Iterators.Array === it || ArrayProto[ITERATOR] === it);
    };
    /***/

  },

  /***/
  "c366":
  /***/
  function c366(module, exports, __webpack_require__) {
    // false -> Array#indexOf
    // true  -> Array#includes
    var toIObject = __webpack_require__("6821");

    var toLength = __webpack_require__("9def");

    var toAbsoluteIndex = __webpack_require__("77f1");

    module.exports = function (IS_INCLUDES) {
      return function ($this, el, fromIndex) {
        var O = toIObject($this);
        var length = toLength(O.length);
        var index = toAbsoluteIndex(fromIndex, length);
        var value; // Array#includes uses SameValueZero equality algorithm
        // eslint-disable-next-line no-self-compare

        if (IS_INCLUDES && el != el) while (length > index) {
          value = O[index++]; // eslint-disable-next-line no-self-compare

          if (value != value) return true; // Array#indexOf ignores holes, Array#includes - not
        } else for (; length > index; index++) {
          if (IS_INCLUDES || index in O) {
            if (O[index] === el) return IS_INCLUDES || index || 0;
          }
        }
        return !IS_INCLUDES && -1;
      };
    };
    /***/

  },

  /***/
  "c609":
  /***/
  function c609(module, exports, __webpack_require__) {
    "use strict"; // https://github.com/tc39/proposal-promise-try

    var $export = __webpack_require__("d13f");

    var newPromiseCapability = __webpack_require__("03ca");

    var perform = __webpack_require__("75c9");

    $export($export.S, 'Promise', {
      'try': function _try(callbackfn) {
        var promiseCapability = newPromiseCapability.f(this);
        var result = perform(callbackfn);
        (result.e ? promiseCapability.reject : promiseCapability.resolve)(result.v);
        return promiseCapability.promise;
      }
    });
    /***/
  },

  /***/
  "c69a":
  /***/
  function c69a(module, exports, __webpack_require__) {
    module.exports = !__webpack_require__("9e1e") && !__webpack_require__("79e5")(function () {
      return Object.defineProperty(__webpack_require__("230e")('div'), 'a', {
        get: function get() {
          return 7;
        }
      }).a != 7;
    });
    /***/
  },

  /***/
  "c7db":
  /***/
  function c7db(module, exports, __webpack_require__) {
    "use strict";

    var Vue = __webpack_require__("8bbf");

    Vue = 'default' in Vue ? Vue['default'] : Vue;
    var version = '2.2.2';
    var compatible = /^2\./.test(Vue.version);

    if (!compatible) {
      Vue.util.warn('VueClickaway ' + version + ' only supports Vue 2.x, and does not support Vue ' + Vue.version);
    } // @SECTION: implementation


    var HANDLER = '_vue_clickaway_handler';

    function bind(el, binding, vnode) {
      unbind(el);
      var vm = vnode.context;
      var callback = binding.value;

      if (typeof callback !== 'function') {
        if (false) {}

        return;
      } // @NOTE: Vue binds directives in microtasks, while UI events are dispatched
      //        in macrotasks. This causes the listener to be set up before
      //        the "origin" click event (the event that lead to the binding of
      //        the directive) arrives at the document root. To work around that,
      //        we ignore events until the end of the "initial" macrotask.
      // @REFERENCE: https://jakearchibald.com/2015/tasks-microtasks-queues-and-schedules/
      // @REFERENCE: https://github.com/simplesmiler/vue-clickaway/issues/8


      var initialMacrotaskEnded = false;
      setTimeout(function () {
        initialMacrotaskEnded = true;
      }, 0);

      el[HANDLER] = function (ev) {
        // @NOTE: this test used to be just `el.containts`, but working with path is better,
        //        because it tests whether the element was there at the time of
        //        the click, not whether it is there now, that the event has arrived
        //        to the top.
        // @NOTE: `.path` is non-standard, the standard way is `.composedPath()`
        var path = ev.path || (ev.composedPath ? ev.composedPath() : undefined);

        if (initialMacrotaskEnded && (path ? path.indexOf(el) < 0 : !el.contains(ev.target))) {
          return callback.call(vm, ev);
        }
      };

      document.documentElement.addEventListener('click', el[HANDLER], false);
    }

    function unbind(el) {
      document.documentElement.removeEventListener('click', el[HANDLER], false);
      delete el[HANDLER];
    }

    var directive = {
      bind: bind,
      update: function update(el, binding) {
        if (binding.value === binding.oldValue) return;
        bind(el, binding);
      },
      unbind: unbind
    };
    var mixin = {
      directives: {
        onClickaway: directive
      }
    };
    exports.version = version;
    exports.directive = directive;
    exports.mixin = mixin;
    /***/
  },

  /***/
  "c8ba":
  /***/
  function c8ba(module, exports) {
    var g; // This works in non-strict mode

    g = function () {
      return this;
    }();

    try {
      // This works if eval is allowed (see CSP)
      g = g || new Function("return this")();
    } catch (e) {
      // This works if the window reference is available
      if ((typeof window === "undefined" ? "undefined" : _typeof2(window)) === "object") g = window;
    } // g can still be undefined, but nothing to do about it...
    // We return undefined, instead of nothing here, so it's
    // easier to handle this case. if(!global) { ...}


    module.exports = g;
    /***/
  },

  /***/
  "ca5a":
  /***/
  function ca5a(module, exports) {
    var id = 0;
    var px = Math.random();

    module.exports = function (key) {
      return 'Symbol('.concat(key === undefined ? '' : key, ')_', (++id + px).toString(36));
    };
    /***/

  },

  /***/
  "cadf":
  /***/
  function cadf(module, exports, __webpack_require__) {
    "use strict";

    var addToUnscopables = __webpack_require__("9c6c");

    var step = __webpack_require__("d53b");

    var Iterators = __webpack_require__("84f2");

    var toIObject = __webpack_require__("6821"); // 22.1.3.4 Array.prototype.entries()
    // 22.1.3.13 Array.prototype.keys()
    // 22.1.3.29 Array.prototype.values()
    // 22.1.3.30 Array.prototype[@@iterator]()


    module.exports = __webpack_require__("01f9")(Array, 'Array', function (iterated, kind) {
      this._t = toIObject(iterated); // target

      this._i = 0; // next index

      this._k = kind; // kind
      // 22.1.5.2.1 %ArrayIteratorPrototype%.next()
    }, function () {
      var O = this._t;
      var kind = this._k;
      var index = this._i++;

      if (!O || index >= O.length) {
        this._t = undefined;
        return step(1);
      }

      if (kind == 'keys') return step(0, index);
      if (kind == 'values') return step(0, O[index]);
      return step(0, [index, O[index]]);
    }, 'values'); // argumentsList[@@iterator] is %ArrayProto_values% (9.4.4.6, 9.4.4.7)

    Iterators.Arguments = Iterators.Array;
    addToUnscopables('keys');
    addToUnscopables('values');
    addToUnscopables('entries');
    /***/
  },

  /***/
  "cb7c":
  /***/
  function cb7c(module, exports, __webpack_require__) {
    var isObject = __webpack_require__("d3f4");

    module.exports = function (it) {
      if (!isObject(it)) throw TypeError(it + ' is not an object!');
      return it;
    };
    /***/

  },

  /***/
  "ce10":
  /***/
  function ce10(module, exports, __webpack_require__) {
    var has = __webpack_require__("69a8");

    var toIObject = __webpack_require__("6821");

    var arrayIndexOf = __webpack_require__("c366")(false);

    var IE_PROTO = __webpack_require__("613b")('IE_PROTO');

    module.exports = function (object, names) {
      var O = toIObject(object);
      var i = 0;
      var result = [];
      var key;

      for (key in O) {
        if (key != IE_PROTO) has(O, key) && result.push(key);
      } // Don't enum bug & hidden keys


      while (names.length > i) {
        if (has(O, key = names[i++])) {
          ~arrayIndexOf(result, key) || result.push(key);
        }
      }

      return result;
    };
    /***/

  },

  /***/
  "cfb6":
  /***/
  function cfb6(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    /* harmony import */

    var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SearchBox_vue_vue_type_style_index_0_id_73e01aa7_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("da5b");
    /* harmony import */


    var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SearchBox_vue_vue_type_style_index_0_id_73e01aa7_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default =
    /*#__PURE__*/
    __webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SearchBox_vue_vue_type_style_index_0_id_73e01aa7_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
    /* unused harmony reexport * */

    /* unused harmony default export */


    var _unused_webpack_default_export = _node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SearchBox_vue_vue_type_style_index_0_id_73e01aa7_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a;
    /***/
  },

  /***/
  "d13f":
  /***/
  function d13f(module, exports, __webpack_require__) {
    var global = __webpack_require__("da3c");

    var core = __webpack_require__("a7d3");

    var ctx = __webpack_require__("bc25");

    var hide = __webpack_require__("8ce0");

    var has = __webpack_require__("43c8");

    var PROTOTYPE = 'prototype';

    var $export = function $export(type, name, source) {
      var IS_FORCED = type & $export.F;
      var IS_GLOBAL = type & $export.G;
      var IS_STATIC = type & $export.S;
      var IS_PROTO = type & $export.P;
      var IS_BIND = type & $export.B;
      var IS_WRAP = type & $export.W;
      var exports = IS_GLOBAL ? core : core[name] || (core[name] = {});
      var expProto = exports[PROTOTYPE];
      var target = IS_GLOBAL ? global : IS_STATIC ? global[name] : (global[name] || {})[PROTOTYPE];
      var key, own, out;
      if (IS_GLOBAL) source = name;

      for (key in source) {
        // contains in native
        own = !IS_FORCED && target && target[key] !== undefined;
        if (own && has(exports, key)) continue; // export native or passed

        out = own ? target[key] : source[key]; // prevent global pollution for namespaces

        exports[key] = IS_GLOBAL && typeof target[key] != 'function' ? source[key] // bind timers to global for call from export context
        : IS_BIND && own ? ctx(out, global) // wrap global constructors for prevent change them in library
        : IS_WRAP && target[key] == out ? function (C) {
          var F = function F(a, b, c) {
            if (this instanceof C) {
              switch (arguments.length) {
                case 0:
                  return new C();

                case 1:
                  return new C(a);

                case 2:
                  return new C(a, b);
              }

              return new C(a, b, c);
            }

            return C.apply(this, arguments);
          };

          F[PROTOTYPE] = C[PROTOTYPE];
          return F; // make static versions for prototype methods
        }(out) : IS_PROTO && typeof out == 'function' ? ctx(Function.call, out) : out; // export proto methods to core.%CONSTRUCTOR%.methods.%NAME%

        if (IS_PROTO) {
          (exports.virtual || (exports.virtual = {}))[key] = out; // export proto methods to core.%CONSTRUCTOR%.prototype.%NAME%

          if (type & $export.R && expProto && !expProto[key]) hide(expProto, key, out);
        }
      }
    }; // type bitmap


    $export.F = 1; // forced

    $export.G = 2; // global

    $export.S = 4; // static

    $export.P = 8; // proto

    $export.B = 16; // bind

    $export.W = 32; // wrap

    $export.U = 64; // safe

    $export.R = 128; // real proto method for `library`

    module.exports = $export;
    /***/
  },

  /***/
  "d3f4":
  /***/
  function d3f4(module, exports) {
    module.exports = function (it) {
      return _typeof2(it) === 'object' ? it !== null : typeof it === 'function';
    };
    /***/

  },

  /***/
  "d4c0":
  /***/
  function d4c0(module, exports, __webpack_require__) {
    // all enumerable object keys, includes symbols
    var getKeys = __webpack_require__("0d58");

    var gOPS = __webpack_require__("2621");

    var pIE = __webpack_require__("52a7");

    module.exports = function (it) {
      var result = getKeys(it);
      var getSymbols = gOPS.f;

      if (getSymbols) {
        var symbols = getSymbols(it);
        var isEnum = pIE.f;
        var i = 0;
        var key;

        while (symbols.length > i) {
          if (isEnum.call(it, key = symbols[i++])) result.push(key);
        }
      }

      return result;
    };
    /***/

  },

  /***/
  "d53b":
  /***/
  function d53b(module, exports) {
    module.exports = function (done, value) {
      return {
        value: value,
        done: !!done
      };
    };
    /***/

  },

  /***/
  "d782":
  /***/
  function d782(module, exports) {
    module.exports = function (exec) {
      try {
        return !!exec();
      } catch (e) {
        return true;
      }
    };
    /***/

  },

  /***/
  "d8e8":
  /***/
  function d8e8(module, exports) {
    module.exports = function (it) {
      if (typeof it != 'function') throw TypeError(it + ' is not a function!');
      return it;
    };
    /***/

  },

  /***/
  "da3c":
  /***/
  function da3c(module, exports) {
    // https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
    var global = module.exports = typeof window != 'undefined' && window.Math == Math ? window : typeof self != 'undefined' && self.Math == Math ? self // eslint-disable-next-line no-new-func
    : Function('return this')();
    if (typeof __g == 'number') __g = global; // eslint-disable-line no-undef

    /***/
  },

  /***/
  "da5b":
  /***/
  function da5b(module, exports, __webpack_require__) {// extracted by mini-css-extract-plugin

    /***/
  },

  /***/
  "dd04":
  /***/
  function dd04(module, exports, __webpack_require__) {
    __webpack_require__("12fd9");

    __webpack_require__("93c4");

    __webpack_require__("b42c");

    __webpack_require__("5b5f");

    __webpack_require__("b604");

    __webpack_require__("c609");

    module.exports = __webpack_require__("a7d3").Promise;
    /***/
  },

  /***/
  "de55":
  /***/
  function de55(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    /* harmony import */

    var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_FieldContent_vue_vue_type_style_index_0_id_05f06339_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("8496");
    /* harmony import */


    var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_FieldContent_vue_vue_type_style_index_0_id_05f06339_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default =
    /*#__PURE__*/
    __webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_FieldContent_vue_vue_type_style_index_0_id_05f06339_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
    /* unused harmony reexport * */

    /* unused harmony default export */


    var _unused_webpack_default_export = _node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_FieldContent_vue_vue_type_style_index_0_id_05f06339_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a;
    /***/
  },

  /***/
  "decf":
  /***/
  function decf(module, exports, __webpack_require__) {
    var anObject = __webpack_require__("0f89");

    var isObject = __webpack_require__("6f8a");

    var newPromiseCapability = __webpack_require__("03ca");

    module.exports = function (C, x) {
      anObject(C);
      if (isObject(x) && x.constructor === C) return x;
      var promiseCapability = newPromiseCapability.f(C);
      var resolve = promiseCapability.resolve;
      resolve(x);
      return promiseCapability.promise;
    };
    /***/

  },

  /***/
  "df0a":
  /***/
  function df0a(module, exports, __webpack_require__) {
    var ctx = __webpack_require__("bc25");

    var invoke = __webpack_require__("196c");

    var html = __webpack_require__("103a");

    var cel = __webpack_require__("12fd");

    var global = __webpack_require__("da3c");

    var process = global.process;
    var setTask = global.setImmediate;
    var clearTask = global.clearImmediate;
    var MessageChannel = global.MessageChannel;
    var Dispatch = global.Dispatch;
    var counter = 0;
    var queue = {};
    var ONREADYSTATECHANGE = 'onreadystatechange';
    var defer, channel, port;

    var run = function run() {
      var id = +this; // eslint-disable-next-line no-prototype-builtins

      if (queue.hasOwnProperty(id)) {
        var fn = queue[id];
        delete queue[id];
        fn();
      }
    };

    var listener = function listener(event) {
      run.call(event.data);
    }; // Node.js 0.9+ & IE10+ has setImmediate, otherwise:


    if (!setTask || !clearTask) {
      setTask = function setImmediate(fn) {
        var args = [];
        var i = 1;

        while (arguments.length > i) {
          args.push(arguments[i++]);
        }

        queue[++counter] = function () {
          // eslint-disable-next-line no-new-func
          invoke(typeof fn == 'function' ? fn : Function(fn), args);
        };

        defer(counter);
        return counter;
      };

      clearTask = function clearImmediate(id) {
        delete queue[id];
      }; // Node.js 0.8-


      if (__webpack_require__("6e1f")(process) == 'process') {
        defer = function defer(id) {
          process.nextTick(ctx(run, id, 1));
        }; // Sphere (JS game engine) Dispatch API

      } else if (Dispatch && Dispatch.now) {
        defer = function defer(id) {
          Dispatch.now(ctx(run, id, 1));
        }; // Browsers with MessageChannel, includes WebWorkers

      } else if (MessageChannel) {
        channel = new MessageChannel();
        port = channel.port2;
        channel.port1.onmessage = listener;
        defer = ctx(port.postMessage, port, 1); // Browsers with postMessage, skip WebWorkers
        // IE8 has postMessage, but it's sync & typeof its postMessage is 'object'
      } else if (global.addEventListener && typeof postMessage == 'function' && !global.importScripts) {
        defer = function defer(id) {
          global.postMessage(id + '', '*');
        };

        global.addEventListener('message', listener, false); // IE8-
      } else if (ONREADYSTATECHANGE in cel('script')) {
        defer = function defer(id) {
          html.appendChild(cel('script'))[ONREADYSTATECHANGE] = function () {
            html.removeChild(this);
            run.call(id);
          };
        }; // Rest old browsers

      } else {
        defer = function defer(id) {
          setTimeout(ctx(run, id, 1), 0);
        };
      }
    }

    module.exports = {
      set: setTask,
      clear: clearTask
    };
    /***/
  },

  /***/
  "e11e":
  /***/
  function e11e(module, exports) {
    // IE 8- don't enum bug keys
    module.exports = 'constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf'.split(',');
    /***/
  },

  /***/
  "e4a9":
  /***/
  function e4a9(module, exports, __webpack_require__) {
    "use strict";

    var LIBRARY = __webpack_require__("b457");

    var $export = __webpack_require__("d13f");

    var redefine = __webpack_require__("2312");

    var hide = __webpack_require__("8ce0");

    var Iterators = __webpack_require__("b22a");

    var $iterCreate = __webpack_require__("5ce7");

    var setToStringTag = __webpack_require__("c0d8");

    var getPrototypeOf = __webpack_require__("ff0c");

    var ITERATOR = __webpack_require__("1b55")('iterator');

    var BUGGY = !([].keys && 'next' in [].keys()); // Safari has buggy iterators w/o `next`

    var FF_ITERATOR = '@@iterator';
    var KEYS = 'keys';
    var VALUES = 'values';

    var returnThis = function returnThis() {
      return this;
    };

    module.exports = function (Base, NAME, Constructor, next, DEFAULT, IS_SET, FORCED) {
      $iterCreate(Constructor, NAME, next);

      var getMethod = function getMethod(kind) {
        if (!BUGGY && kind in proto) return proto[kind];

        switch (kind) {
          case KEYS:
            return function keys() {
              return new Constructor(this, kind);
            };

          case VALUES:
            return function values() {
              return new Constructor(this, kind);
            };
        }

        return function entries() {
          return new Constructor(this, kind);
        };
      };

      var TAG = NAME + ' Iterator';
      var DEF_VALUES = DEFAULT == VALUES;
      var VALUES_BUG = false;
      var proto = Base.prototype;
      var $native = proto[ITERATOR] || proto[FF_ITERATOR] || DEFAULT && proto[DEFAULT];
      var $default = $native || getMethod(DEFAULT);
      var $entries = DEFAULT ? !DEF_VALUES ? $default : getMethod('entries') : undefined;
      var $anyNative = NAME == 'Array' ? proto.entries || $native : $native;
      var methods, key, IteratorPrototype; // Fix native

      if ($anyNative) {
        IteratorPrototype = getPrototypeOf($anyNative.call(new Base()));

        if (IteratorPrototype !== Object.prototype && IteratorPrototype.next) {
          // Set @@toStringTag to native iterators
          setToStringTag(IteratorPrototype, TAG, true); // fix for some old engines

          if (!LIBRARY && typeof IteratorPrototype[ITERATOR] != 'function') hide(IteratorPrototype, ITERATOR, returnThis);
        }
      } // fix Array#{values, @@iterator}.name in V8 / FF


      if (DEF_VALUES && $native && $native.name !== VALUES) {
        VALUES_BUG = true;

        $default = function values() {
          return $native.call(this);
        };
      } // Define iterator


      if ((!LIBRARY || FORCED) && (BUGGY || VALUES_BUG || !proto[ITERATOR])) {
        hide(proto, ITERATOR, $default);
      } // Plug for library


      Iterators[NAME] = $default;
      Iterators[TAG] = returnThis;

      if (DEFAULT) {
        methods = {
          values: DEF_VALUES ? $default : getMethod(VALUES),
          keys: IS_SET ? $default : getMethod(KEYS),
          entries: $entries
        };
        if (FORCED) for (key in methods) {
          if (!(key in proto)) redefine(proto, key, methods[key]);
        } else $export($export.P + $export.F * (BUGGY || VALUES_BUG), NAME, methods);
      }

      return methods;
    };
    /***/

  },

  /***/
  "e5fa":
  /***/
  function e5fa(module, exports) {
    // 7.2.1 RequireObjectCoercible(argument)
    module.exports = function (it) {
      if (it == undefined) throw TypeError("Can't call method on  " + it);
      return it;
    };
    /***/

  },

  /***/
  "f159":
  /***/
  function f159(module, exports, __webpack_require__) {
    var classof = __webpack_require__("7d8a");

    var ITERATOR = __webpack_require__("1b55")('iterator');

    var Iterators = __webpack_require__("b22a");

    module.exports = __webpack_require__("a7d3").getIteratorMethod = function (it) {
      if (it != undefined) return it[ITERATOR] || it['@@iterator'] || Iterators[classof(it)];
    };
    /***/

  },

  /***/
  "f2fe":
  /***/
  function f2fe(module, exports) {
    module.exports = function (it) {
      if (typeof it != 'function') throw TypeError(it + ' is not a function!');
      return it;
    };
    /***/

  },

  /***/
  "f517":
  /***/
  function f517(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    /* harmony import */

    var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Loading_vue_vue_type_style_index_0_id_cd76fb52_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("9cb8");
    /* harmony import */


    var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Loading_vue_vue_type_style_index_0_id_cd76fb52_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default =
    /*#__PURE__*/
    __webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Loading_vue_vue_type_style_index_0_id_cd76fb52_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
    /* unused harmony reexport * */

    /* unused harmony default export */


    var _unused_webpack_default_export = _node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Loading_vue_vue_type_style_index_0_id_cd76fb52_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a;
    /***/
  },

  /***/
  "f568":
  /***/
  function f568(module, exports, __webpack_require__) {
    var dP = __webpack_require__("3adc");

    var anObject = __webpack_require__("0f89");

    var getKeys = __webpack_require__("7633");

    module.exports = __webpack_require__("7d95") ? _Object$defineProperties : function defineProperties(O, Properties) {
      anObject(O);
      var keys = getKeys(Properties);
      var length = keys.length;
      var i = 0;
      var P;

      while (length > i) {
        dP.f(O, P = keys[i++], Properties[P]);
      }

      return O;
    };
    /***/
  },

  /***/
  "f6fd":
  /***/
  function f6fd(module, exports) {
    // document.currentScript polyfill by Adam Miller
    // MIT license
    (function (document) {
      var currentScript = "currentScript",
          scripts = document.getElementsByTagName('script'); // Live NodeList collection
      // If browser needs currentScript polyfill, add get currentScript() to the document object

      if (!(currentScript in document)) {
        _Object$defineProperty(document, currentScript, {
          get: function get() {
            // IE 6-10 supports script readyState
            // IE 10+ support stack trace
            try {
              throw new Error();
            } catch (err) {
              // Find the second match for the "at" string to get file src url from stack.
              // Specifically works with the format of stack traces in IE.
              var i,
                  res = (/.*at [^\(]*\((.*):.+:.+\)$/ig.exec(err.stack) || [false])[1]; // For all scripts on the page, if src matches or if ready state is interactive, return the script tag

              for (i in scripts) {
                if (scripts[i].src == res || scripts[i].readyState == "interactive") {
                  return scripts[i];
                }
              } // If no match, return null


              return null;
            }
          }
        });
      }
    })(document);
    /***/

  },

  /***/
  "f845":
  /***/
  function f845(module, exports) {
    module.exports = function (bitmap, value) {
      return {
        enumerable: !(bitmap & 1),
        configurable: !(bitmap & 2),
        writable: !(bitmap & 4),
        value: value
      };
    };
    /***/

  },

  /***/
  "fa54":
  /***/
  function fa54(module, exports, __webpack_require__) {
    "use strict";

    var addToUnscopables = __webpack_require__("b3e7");

    var step = __webpack_require__("245b");

    var Iterators = __webpack_require__("b22a");

    var toIObject = __webpack_require__("6a9b"); // 22.1.3.4 Array.prototype.entries()
    // 22.1.3.13 Array.prototype.keys()
    // 22.1.3.29 Array.prototype.values()
    // 22.1.3.30 Array.prototype[@@iterator]()


    module.exports = __webpack_require__("e4a9")(Array, 'Array', function (iterated, kind) {
      this._t = toIObject(iterated); // target

      this._i = 0; // next index

      this._k = kind; // kind
      // 22.1.5.2.1 %ArrayIteratorPrototype%.next()
    }, function () {
      var O = this._t;
      var kind = this._k;
      var index = this._i++;

      if (!O || index >= O.length) {
        this._t = undefined;
        return step(1);
      }

      if (kind == 'keys') return step(0, index);
      if (kind == 'values') return step(0, O[index]);
      return step(0, [index, O[index]]);
    }, 'values'); // argumentsList[@@iterator] is %ArrayProto_values% (9.4.4.6, 9.4.4.7)

    Iterators.Arguments = Iterators.Array;
    addToUnscopables('keys');
    addToUnscopables('values');
    addToUnscopables('entries');
    /***/
  },

  /***/
  "fab2":
  /***/
  function fab2(module, exports, __webpack_require__) {
    var document = __webpack_require__("7726").document;

    module.exports = document && document.documentElement;
    /***/
  },

  /***/
  "fb15":
  /***/
  function fb15(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__); // CONCATENATED MODULE: ./node_modules/@vue/cli-service/lib/commands/build/setPublicPath.js
    // This file is imported into lib/wc client bundles.


    if (typeof window !== 'undefined') {
      if (true) {
        __webpack_require__("f6fd");
      }

      var i;

      if ((i = window.document.currentScript) && (i = i.src.match(/(.+\/)[^/]+\.js(\?.*)?$/))) {
        __webpack_require__.p = i[1]; // eslint-disable-line
      }
    } // Indicate to webpack that this file can be concatenated

    /* harmony default export */


    var setPublicPath = null; // CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"42ec1f80-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/QlikField.vue?vue&type=template&id=3cb9e7fe&

    var render = function render() {
      var _vm = this;

      var _h = _vm.$createElement;

      var _c = _vm._self._c || _h;

      return _c('div', {
        staticClass: "dropdown",
        style: {
          display: _vm.showQlikField
        },
        attrs: {
          "id": "qlik-field"
        }
      }, [_vm.showArrow ? _c('div', {
        staticClass: "arrow",
        style: {
          'border-color': "transparent transparent " + _vm.headerColors.background
        }
      }) : _vm._e(), _c('intersect', {
        on: {
          "enter": function enter($event) {
            return _vm.visibilityChanged(true, true);
          },
          "leave": function leave($event) {
            return _vm.visibilityChanged(false, true);
          }
        }
      }, [_c('div', {
        staticClass: "field-content",
        style: {
          'border-color': "" + _vm.headerColors.background
        }
      }, [_vm.showHeader ? _c('field-header', {
        staticClass: "header",
        attrs: {
          "qData": _vm.qData,
          "headerColors": _vm.headerColors,
          "headerIcons": _vm.headerIcons,
          "fieldInfo": _vm.fieldInfo
        },
        on: {
          "selectAll": _vm.selectAll,
          "selectAlternative": _vm.selectAlternative,
          "selectExcluded": _vm.selectExcluded,
          "lock": _vm.lock,
          "unlock": _vm.unlock,
          "clearAll": _vm.clearAll
        }
      }) : _vm._e(), _vm.isError ? _c('div', {
        staticClass: "error"
      }, [_vm._v(_vm._s(_vm.errorMessage))]) : _vm._e(), _c('field-content', {
        directives: [{
          name: "show",
          rawName: "v-show",
          value: !_vm.isLoading && !_vm.isError,
          expression: "!isLoading && !isError"
        }],
        attrs: {
          "qData": _vm.qData,
          "showSearch": _vm.showSearch,
          "fieldName": _vm.fieldName,
          "bgColors": _vm.bgColors,
          "fontColors": _vm.fontColors
        },
        on: {
          "clicked": _vm.clicked,
          "search": _vm.search,
          "scrolled": _vm.scrolled
        }
      }), _c('loading-animation', {
        directives: [{
          name: "show",
          rawName: "v-show",
          value: _vm.isLoading && !_vm.isError,
          expression: "isLoading && !isError"
        }],
        staticClass: "loading"
      })], 1)])], 1);
    };

    var staticRenderFns = []; // CONCATENATED MODULE: ./src/QlikField.vue?vue&type=template&id=3cb9e7fe&
    // EXTERNAL MODULE: ./node_modules/regenerator-runtime/runtime.js

    var runtime = __webpack_require__("96cf"); // EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/promise.js


    var promise = __webpack_require__("795b");

    var promise_default =
    /*#__PURE__*/
    __webpack_require__.n(promise); // CONCATENATED MODULE: ./node_modules/@babel/runtime-corejs2/helpers/esm/asyncToGenerator.js


    function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) {
      try {
        var info = gen[key](arg);
        var value = info.value;
      } catch (error) {
        reject(error);
        return;
      }

      if (info.done) {
        resolve(value);
      } else {
        promise_default.a.resolve(value).then(_next, _throw);
      }
    }

    function _asyncToGenerator(fn) {
      return function () {
        var self = this,
            args = arguments;
        return new promise_default.a(function (resolve, reject) {
          var gen = fn.apply(self, args);

          function _next(value) {
            asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value);
          }

          function _throw(err) {
            asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err);
          }

          _next(undefined);
        });
      };
    } // EXTERNAL MODULE: ./node_modules/vue-clickaway/dist/vue-clickaway.common.js


    var vue_clickaway_common = __webpack_require__("c7db"); // EXTERNAL MODULE: ./node_modules/vue-observe-visibility/dist/vue-observe-visibility.esm.js


    var vue_observe_visibility_esm = __webpack_require__("85fe"); // CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"42ec1f80-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/FieldHeader.vue?vue&type=template&id=341e5fe8&scoped=true&


    var FieldHeadervue_type_template_id_341e5fe8_scoped_true_render = function FieldHeadervue_type_template_id_341e5fe8_scoped_true_render() {
      var _vm = this;

      var _h = _vm.$createElement;

      var _c = _vm._self._c || _h;

      return _c('div', {
        staticClass: "header",
        style: {
          'background-color': _vm.headerColors.background
        }
      }, [_c('span', {
        staticClass: "lui-icon lui-icon--large lui-icon--select-all pointer lui-disabled",
        style: {
          'color': _vm.headerColors.icons,
          'visibility': _vm.visibility(_vm.headerIcons.selectAll.hidden),
          'opacity': _vm.opacity(_vm.headerIcons.selectAll.enabled)
        },
        attrs: {
          "aria-hidden": "true",
          "title": "Select all"
        },
        on: {
          "click": _vm.selectAll
        }
      }), _c('span', {
        staticClass: "lui-icon lui-icon--large lui-icon--select-alternative pointer",
        style: {
          'color': _vm.headerColors.icons,
          'visibility': _vm.visibility(_vm.headerIcons.selectAlternative.hidden),
          'opacity': _vm.opacity(_vm.headerIcons.selectAlternative.enabled)
        },
        attrs: {
          "aria-hidden": "true",
          "title": "Select alternative"
        },
        on: {
          "click": _vm.selectAlternative
        }
      }), _c('span', {
        staticClass: "lui-icon lui-icon--large lui-icon--select-excluded pointer",
        style: {
          'color': _vm.headerColors.icons,
          'visibility': _vm.visibility(_vm.headerIcons.selectExcluded.hidden),
          'opacity': _vm.opacity(_vm.headerIcons.selectExcluded.enabled)
        },
        attrs: {
          "aria-hidden": "true",
          "title": "Select excluded"
        },
        on: {
          "click": _vm.selectExcluded
        }
      }), _c('div'), _c('span', {
        class: ['lui-icon', 'lui-icon--large', _vm.hasLocked, 'pointer'],
        style: {
          'color': _vm.headerColors.icons,
          'visibility': _vm.visibility(_vm.headerIcons.lock.hidden),
          'opacity': _vm.opacity(_vm.headerIcons.lock.enabled)
        },
        attrs: {
          "aria-hidden": "true",
          "title": "Lock"
        },
        on: {
          "click": _vm.lock
        }
      }), _c('span', {
        staticClass: "lui-icon lui-icon--large lui-icon--clear-selections pointer",
        style: {
          'color': _vm.headerColors.icons,
          'visibility': _vm.visibility(_vm.headerIcons.clearAll.hidden),
          'opacity': _vm.opacity(_vm.headerIcons.clearAll.enabled)
        },
        attrs: {
          "aria-hidden": "true",
          "title": "Clear all"
        },
        on: {
          "click": _vm.clearAll
        }
      })]);
    };

    var FieldHeadervue_type_template_id_341e5fe8_scoped_true_staticRenderFns = []; // CONCATENATED MODULE: ./src/components/FieldHeader.vue?vue&type=template&id=341e5fe8&scoped=true&
    // EXTERNAL MODULE: ./node_modules/core-js/modules/es7.symbol.async-iterator.js

    var es7_symbol_async_iterator = __webpack_require__("ac4d"); // EXTERNAL MODULE: ./node_modules/core-js/modules/es6.symbol.js


    var es6_symbol = __webpack_require__("8a81"); // EXTERNAL MODULE: ./node_modules/core-js/modules/web.dom.iterable.js


    var web_dom_iterable = __webpack_require__("ac6a"); // CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/FieldHeader.vue?vue&type=script&lang=js&
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //

    /* harmony default export */


    var FieldHeadervue_type_script_lang_js_ = {
      name: "QlikFieldHeader",
      props: ["qData", "headerColors", "headerIcons", "fieldInfo"],
      data: function data() {
        return {
          hasLocked: ""
        };
      },
      methods: {
        selectAll: function selectAll() {
          var _this = this;

          if (_this.headerIcons.selectAll.enabled == true) {
            _this.$emit("selectAll");
          }
        },
        selectAlternative: function selectAlternative() {
          var _this = this;

          if (_this.headerIcons.selectAlternative.enabled) {
            _this.$emit("selectAlternative");
          }
        },
        selectExcluded: function selectExcluded() {
          var _this = this;

          if (_this.headerIcons.selectExcluded.enabled) {
            _this.$emit("selectExcluded");
          }
        },
        clearAll: function clearAll() {
          var _this = this;

          if (_this.headerIcons.clearAll.enabled) {
            _this.$emit("clearAll");
          }
        },
        lock: function lock() {
          var _this = this;

          if (_this.headerIcons.lock.enabled) {
            if (_this.hasLocked == "lui-icon--unlock") {
              _this.$emit("unlock");
            } else {
              _this.$emit("lock");
            }
          }
        },
        visibility: function visibility(hidden) {
          if (hidden == true) {
            return "hidden";
          } else {
            return "visible";
          }
        },
        opacity: function opacity(enabled) {
          if (enabled == true) {
            return 1;
          } else {
            return 0.2;
          }
        }
      },
      watch: {
        qData: function qData(newVal, oldVal) {
          this.hasLocked = checkForLocked(newVal);
        }
      },
      mounted: function mounted() {
        this.hasLocked = checkForLocked(this.qData);
      }
    };

    function checkForLocked(qData) {
      var hasLocked = false;
      var _iteratorNormalCompletion = true;
      var _didIteratorError = false;
      var _iteratorError = undefined;

      try {
        for (var _iterator = _getIterator(qData), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
          var value = _step.value;

          if (value[0].qState == "L") {
            hasLocked = true;
            break;
          }
        }
      } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion && _iterator.return != null) {
            _iterator.return();
          }
        } finally {
          if (_didIteratorError) {
            throw _iteratorError;
          }
        }
      }

      var icon = "lock";

      if (hasLocked == true) {
        icon = "unlock";
      }

      return "lui-icon--".concat(icon);
    } // CONCATENATED MODULE: ./src/components/FieldHeader.vue?vue&type=script&lang=js&

    /* harmony default export */


    var components_FieldHeadervue_type_script_lang_js_ = FieldHeadervue_type_script_lang_js_; // EXTERNAL MODULE: ./src/components/FieldHeader.vue?vue&type=style&index=0&id=341e5fe8&scoped=true&lang=css&

    var FieldHeadervue_type_style_index_0_id_341e5fe8_scoped_true_lang_css_ = __webpack_require__("6be5"); // CONCATENATED MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js

    /* globals __VUE_SSR_CONTEXT__ */
    // IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
    // This module is a runtime utility for cleaner component module output and will
    // be included in the final webpack user bundle.


    function normalizeComponent(scriptExports, render, staticRenderFns, functionalTemplate, injectStyles, scopeId, moduleIdentifier,
    /* server only */
    shadowMode
    /* vue-cli only */
    ) {
      // Vue.extend constructor export interop
      var options = typeof scriptExports === 'function' ? scriptExports.options : scriptExports; // render functions

      if (render) {
        options.render = render;
        options.staticRenderFns = staticRenderFns;
        options._compiled = true;
      } // functional template


      if (functionalTemplate) {
        options.functional = true;
      } // scopedId


      if (scopeId) {
        options._scopeId = 'data-v-' + scopeId;
      }

      var hook;

      if (moduleIdentifier) {
        // server build
        hook = function hook(context) {
          // 2.3 injection
          context = context || // cached call
          this.$vnode && this.$vnode.ssrContext || // stateful
          this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext; // functional
          // 2.2 with runInNewContext: true

          if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
            context = __VUE_SSR_CONTEXT__;
          } // inject component styles


          if (injectStyles) {
            injectStyles.call(this, context);
          } // register component module identifier for async chunk inferrence


          if (context && context._registeredComponents) {
            context._registeredComponents.add(moduleIdentifier);
          }
        }; // used by ssr in case component is cached and beforeCreate
        // never gets called


        options._ssrRegister = hook;
      } else if (injectStyles) {
        hook = shadowMode ? function () {
          injectStyles.call(this, this.$root.$options.shadowRoot);
        } : injectStyles;
      }

      if (hook) {
        if (options.functional) {
          // for template-only hot-reload because in that case the render fn doesn't
          // go through the normalizer
          options._injectStyles = hook; // register for functioal component in vue file

          var originalRender = options.render;

          options.render = function renderWithStyleInjection(h, context) {
            hook.call(context);
            return originalRender(h, context);
          };
        } else {
          // inject component registration as beforeCreate hook
          var existing = options.beforeCreate;
          options.beforeCreate = existing ? [].concat(existing, hook) : [hook];
        }
      }

      return {
        exports: scriptExports,
        options: options
      };
    } // CONCATENATED MODULE: ./src/components/FieldHeader.vue

    /* normalize component */


    var component = normalizeComponent(components_FieldHeadervue_type_script_lang_js_, FieldHeadervue_type_template_id_341e5fe8_scoped_true_render, FieldHeadervue_type_template_id_341e5fe8_scoped_true_staticRenderFns, false, null, "341e5fe8", null);
    /* harmony default export */

    var FieldHeader = component.exports; // CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"42ec1f80-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/FieldContent.vue?vue&type=template&id=05f06339&scoped=true&

    var FieldContentvue_type_template_id_05f06339_scoped_true_render = function FieldContentvue_type_template_id_05f06339_scoped_true_render() {
      var _vm = this;

      var _h = _vm.$createElement;

      var _c = _vm._self._c || _h;

      return _c('div', {
        staticClass: "field-content1"
      }, [_vm.showSearch ? _c('search-box', {
        attrs: {
          "fieldName": _vm.fieldName
        }
      }) : _vm._e(), _c('ul', {
        staticClass: "lui-list values",
        on: {
          "scroll": _vm.scrollFunction
        }
      }, _vm._l(_vm.qData, function (value) {
        return _c('field-value', {
          key: _vm.fieldName + value[0].qElemNumber,
          attrs: {
            "qValue": value,
            "bgColors": _vm.bgColors,
            "fontColors": _vm.fontColors
          }
        });
      }), 1)], 1);
    };

    var FieldContentvue_type_template_id_05f06339_scoped_true_staticRenderFns = []; // CONCATENATED MODULE: ./src/components/FieldContent.vue?vue&type=template&id=05f06339&scoped=true&
    // CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"42ec1f80-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/SearchBox.vue?vue&type=template&id=73e01aa7&scoped=true&

    var SearchBoxvue_type_template_id_73e01aa7_scoped_true_render = function SearchBoxvue_type_template_id_73e01aa7_scoped_true_render() {
      var _vm = this;

      var _h = _vm.$createElement;

      var _c = _vm._self._c || _h;

      return _c('div', {
        staticClass: "lui-search search-border"
      }, [_c('span', {
        staticClass: "lui-icon lui-icon--small lui-icon--search lui-search__search-icon"
      }), _c('input', {
        directives: [{
          name: "model",
          rawName: "v-model",
          value: _vm.filter,
          expression: "filter"
        }],
        staticClass: "lui-search__input",
        attrs: {
          "maxlength": "255",
          "spellcheck": "false",
          "type": "text",
          "placeholder": _vm.fieldName
        },
        domProps: {
          "value": _vm.filter
        },
        on: {
          "input": function input($event) {
            if ($event.target.composing) {
              return;
            }

            _vm.filter = $event.target.value;
          }
        }
      }), _c('button', {
        staticClass: "lui-search__clear-button"
      }, [_c('span', {
        staticClass: "lui-icon lui-icon--small lui-icon--close",
        on: {
          "click": function click($event) {
            _vm.filter = '';
          }
        }
      })])]);
    };

    var SearchBoxvue_type_template_id_73e01aa7_scoped_true_staticRenderFns = []; // CONCATENATED MODULE: ./src/components/SearchBox.vue?vue&type=template&id=73e01aa7&scoped=true&
    // CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/SearchBox.vue?vue&type=script&lang=js&
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //

    /* harmony default export */

    var SearchBoxvue_type_script_lang_js_ = {
      name: "SearchBox",
      props: ["fieldName"],
      data: function data() {
        return {
          filter: ""
        };
      },
      watch: {
        filter: function filter(newVal, oldVal) {
          this.$parent.$emit("search", newVal);
        }
      }
    }; // CONCATENATED MODULE: ./src/components/SearchBox.vue?vue&type=script&lang=js&

    /* harmony default export */

    var components_SearchBoxvue_type_script_lang_js_ = SearchBoxvue_type_script_lang_js_; // EXTERNAL MODULE: ./src/components/SearchBox.vue?vue&type=style&index=0&id=73e01aa7&scoped=true&lang=css&

    var SearchBoxvue_type_style_index_0_id_73e01aa7_scoped_true_lang_css_ = __webpack_require__("cfb6"); // CONCATENATED MODULE: ./src/components/SearchBox.vue

    /* normalize component */


    var SearchBox_component = normalizeComponent(components_SearchBoxvue_type_script_lang_js_, SearchBoxvue_type_template_id_73e01aa7_scoped_true_render, SearchBoxvue_type_template_id_73e01aa7_scoped_true_staticRenderFns, false, null, "73e01aa7", null);
    /* harmony default export */

    var SearchBox = SearchBox_component.exports; // CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"42ec1f80-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/FieldValue.vue?vue&type=template&id=bfa4efde&scoped=true&

    var FieldValuevue_type_template_id_bfa4efde_scoped_true_render = function FieldValuevue_type_template_id_bfa4efde_scoped_true_render() {
      var _vm = this;

      var _h = _vm.$createElement;

      var _c = _vm._self._c || _h;

      return _c('li', {
        class: [_vm.valueTypeClass(_vm.qValue[0].qNum), 'pointer', 'lui-list__item', 'item'],
        style: [{
          'background-color': _vm.cssProps(_vm.qValue[0].qState, 'bgColors')
        }, {
          'color': _vm.cssProps(_vm.qValue[0].qState, 'fontColors')
        }],
        on: {
          "click": function click($event) {
            return _vm.selectValue(_vm.value);
          }
        }
      }, [_c('span', {
        staticClass: "lui-list__text text",
        attrs: {
          "title": _vm.qValue[0].qText
        }
      }, [_vm.qValue[0].qState == 'L' ? _c('span', {
        staticClass: "lui-icon lui-icon--small lui-icon--lock",
        attrs: {
          "aria-hidden": "true"
        }
      }) : _vm._e(), _vm._v("      \n    " + _vm._s(_vm.qValue[0].qText) + "\n  ")])]);
    };

    var FieldValuevue_type_template_id_bfa4efde_scoped_true_staticRenderFns = []; // CONCATENATED MODULE: ./src/components/FieldValue.vue?vue&type=template&id=bfa4efde&scoped=true&
    // CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/FieldValue.vue?vue&type=script&lang=js&
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //

    /* harmony default export */

    var FieldValuevue_type_script_lang_js_ = {
      name: "FieldValue",
      props: ["qValue", "bgColors", "fontColors"],
      data: function data() {
        return {
          value: [{
            qText: 1
          }],
          styleObject: {
            color: "red",
            fontSize: "13px"
          }
        };
      },
      methods: {
        selectValue: function selectValue(value) {
          this.$parent.$emit("clicked", this.qValue[0]); // this.qValue[0].qState = "O";
        },
        cssProps: function cssProps(state, colType) {
          var _this = this;

          var classState = "";

          switch (state) {
            case "S":
              classState = _this[colType].selected;
              break;

            case "O":
              classState = _this[colType].optional;
              break;

            case "A":
              classState = _this[colType].alternative;
              break;

            case "D":
              classState = _this[colType].deselected;
              break;

            case "X":
              classState = _this[colType].excluded;
              break;

            case "L":
              classState = _this[colType].locked;
              break;

            case "XS":
              classState = _this[colType].excluded;
              break;

            case "XL":
              classState = _this[colType].excluded;
              break;
          }

          return classState;
        },
        valueTypeClass: function valueTypeClass(qNum) {
          var className = "";

          if (qNum == "NaN") {
            className = "left";
          } else {
            className = "right";
          }

          return className;
        }
      },
      computed: {}
    }; // CONCATENATED MODULE: ./src/components/FieldValue.vue?vue&type=script&lang=js&

    /* harmony default export */

    var components_FieldValuevue_type_script_lang_js_ = FieldValuevue_type_script_lang_js_; // EXTERNAL MODULE: ./src/components/FieldValue.vue?vue&type=style&index=0&id=bfa4efde&scoped=true&lang=css&

    var FieldValuevue_type_style_index_0_id_bfa4efde_scoped_true_lang_css_ = __webpack_require__("38fc"); // CONCATENATED MODULE: ./src/components/FieldValue.vue

    /* normalize component */


    var FieldValue_component = normalizeComponent(components_FieldValuevue_type_script_lang_js_, FieldValuevue_type_template_id_bfa4efde_scoped_true_render, FieldValuevue_type_template_id_bfa4efde_scoped_true_staticRenderFns, false, null, "bfa4efde", null);
    /* harmony default export */

    var FieldValue = FieldValue_component.exports; // CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/FieldContent.vue?vue&type=script&lang=js&
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //

    /* harmony default export */

    var FieldContentvue_type_script_lang_js_ = {
      name: "QlikFieldContent",
      components: {
        SearchBox: SearchBox,
        FieldValue: FieldValue
      },
      props: ["qData", "showSearch", "bgColors", "fontColors", "fieldName"],
      data: function data() {
        return {
          filter: ""
        };
      },
      methods: {
        scrollFunction: function scrollFunction(_ref) {
          var _ref$target = _ref.target,
              scrollTop = _ref$target.scrollTop,
              clientHeight = _ref$target.clientHeight,
              scrollHeight = _ref$target.scrollHeight;

          var _this = this;

          if (scrollTop + clientHeight >= scrollHeight) {
            _this.$emit("scrolled");
          }
        }
      }
    }; // CONCATENATED MODULE: ./src/components/FieldContent.vue?vue&type=script&lang=js&

    /* harmony default export */

    var components_FieldContentvue_type_script_lang_js_ = FieldContentvue_type_script_lang_js_; // EXTERNAL MODULE: ./src/components/FieldContent.vue?vue&type=style&index=0&id=05f06339&scoped=true&lang=css&

    var FieldContentvue_type_style_index_0_id_05f06339_scoped_true_lang_css_ = __webpack_require__("de55"); // CONCATENATED MODULE: ./src/components/FieldContent.vue

    /* normalize component */


    var FieldContent_component = normalizeComponent(components_FieldContentvue_type_script_lang_js_, FieldContentvue_type_template_id_05f06339_scoped_true_render, FieldContentvue_type_template_id_05f06339_scoped_true_staticRenderFns, false, null, "05f06339", null);
    /* harmony default export */

    var FieldContent = FieldContent_component.exports; // CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"42ec1f80-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Loading.vue?vue&type=template&id=cd76fb52&scoped=true&

    var Loadingvue_type_template_id_cd76fb52_scoped_true_render = function Loadingvue_type_template_id_cd76fb52_scoped_true_render() {
      var _vm = this;

      var _h = _vm.$createElement;

      var _c = _vm._self._c || _h;

      return _vm._m(0);
    };

    var Loadingvue_type_template_id_cd76fb52_scoped_true_staticRenderFns = [function () {
      var _vm = this;

      var _h = _vm.$createElement;

      var _c = _vm._self._c || _h;

      return _c('div', [_c('div', {
        staticClass: "lds-grid"
      }, [_c('div'), _c('div'), _c('div'), _c('div'), _c('div'), _c('div'), _c('div'), _c('div'), _c('div')])]);
    }]; // CONCATENATED MODULE: ./src/components/Loading.vue?vue&type=template&id=cd76fb52&scoped=true&
    // CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Loading.vue?vue&type=script&lang=js&
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //

    /* harmony default export */

    var Loadingvue_type_script_lang_js_ = {
      name: "Loader"
    }; // CONCATENATED MODULE: ./src/components/Loading.vue?vue&type=script&lang=js&

    /* harmony default export */

    var components_Loadingvue_type_script_lang_js_ = Loadingvue_type_script_lang_js_; // EXTERNAL MODULE: ./src/components/Loading.vue?vue&type=style&index=0&id=cd76fb52&scoped=true&lang=css&

    var Loadingvue_type_style_index_0_id_cd76fb52_scoped_true_lang_css_ = __webpack_require__("f517"); // CONCATENATED MODULE: ./src/components/Loading.vue

    /* normalize component */


    var Loading_component = normalizeComponent(components_Loadingvue_type_script_lang_js_, Loadingvue_type_template_id_cd76fb52_scoped_true_render, Loadingvue_type_template_id_cd76fb52_scoped_true_staticRenderFns, false, null, "cd76fb52", null);
    /* harmony default export */

    var Loading = Loading_component.exports; // EXTERNAL MODULE: ./node_modules/enigma.js/enigma.js

    var enigma = __webpack_require__("5987");

    var enigma_default =
    /*#__PURE__*/
    __webpack_require__.n(enigma); // EXTERNAL MODULE: ./node_modules/enigma.js/schemas/12.170.2.json


    var _12_170_2 = __webpack_require__("9e99"); // EXTERNAL MODULE: external {"commonjs":"vue","commonjs2":"vue","root":"Vue"}


    var external_commonjs_vue_commonjs2_vue_root_Vue_ = __webpack_require__("8bbf");

    var external_commonjs_vue_commonjs2_vue_root_Vue_default =
    /*#__PURE__*/
    __webpack_require__.n(external_commonjs_vue_commonjs2_vue_root_Vue_); // CONCATENATED MODULE: ./node_modules/vue-intersect/dist/index.js


    var dist_warn = function warn(msg) {
      if (!external_commonjs_vue_commonjs2_vue_root_Vue_default.a.config.silent) {
        console.warn(msg);
      }
    };
    /* harmony default export */


    var dist = {
      name: 'intersect',
      abstract: true,
      props: {
        threshold: {
          type: Array,
          required: false,
          default: function _default() {
            return [0.2];
          }
        },
        root: {
          type: HTMLElement,
          required: false,
          default: function _default() {
            return null;
          }
        },
        rootMargin: {
          type: String,
          required: false,
          default: function _default() {
            return '0px 0px 0px 0px';
          }
        }
      },
      created: function created() {
        var _this = this;

        this.observer = new IntersectionObserver(function (entries) {
          if (!entries[0].isIntersecting) {
            _this.$emit('leave', [entries[0]]);
          } else {
            _this.$emit('enter', [entries[0]]);
          }

          _this.$emit('change', [entries[0]]);
        }, {
          threshold: this.threshold,
          root: this.root,
          rootMargin: this.rootMargin
        });
      },
      mounted: function mounted() {
        var _this2 = this;

        this.$nextTick(function () {
          if (_this2.$slots.default && _this2.$slots.default.length > 1) {
            dist_warn('[VueIntersect] You may only wrap one element in a <intersect> component.');
          } else if (!_this2.$slots.default || _this2.$slots.default.length < 1) {
            dist_warn('[VueIntersect] You must have one child inside a <intersect> component.');
            return;
          }

          _this2.observer.observe(_this2.$slots.default[0].elm);
        });
      },
      destroyed: function destroyed() {
        this.observer.disconnect();
      },
      render: function render() {
        return this.$slots.default ? this.$slots.default[0] : null;
      }
    }; // CONCATENATED MODULE: ./src/functions/helpers.js

    var initialise =
    /*#__PURE__*/
    function () {
      var _ref = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee() {
        var session, global, qDoc, qsDocPath;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                session = {};
                global = {};
                qDoc = {};
                qsDocPath = "/data/Consumer Sales.qvf";
                _context.prev = 4;
                session = enigma_default.a.create({
                  schema: _12_170_2,
                  url: "ws://localhost:19076/app/engineData",
                  createSocket: function createSocket(url) {
                    return new WebSocket(url);
                  }
                });
                _context.next = 8;
                return session.open();

              case 8:
                global = _context.sent;
                _context.next = 14;
                break;

              case 11:
                _context.prev = 11;
                _context.t0 = _context["catch"](4);
                throw "Cannot connect to the Engine";

              case 14:
                _context.prev = 14;
                _context.next = 17;
                return global.openDoc(qsDocPath);

              case 17:
                qDoc = _context.sent;
                return _context.abrupt("return", qDoc);

              case 21:
                _context.prev = 21;
                _context.t1 = _context["catch"](14);
                throw 'Cannot open the app';

              case 24:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[4, 11], [14, 21]]);
      }));

      return function initialise() {
        return _ref.apply(this, arguments);
      };
    }();

    var createSessionObject =
    /*#__PURE__*/
    function () {
      var _ref2 = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee2(fieldName, qDoc, qHeight, qType) {
        var listObjectProperties, sessionObj;
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                listObjectProperties = {
                  qInfo: {
                    qType: qType
                  },
                  qListObjectDef: {
                    qDef: {
                      qFieldDefs: [fieldName],
                      qSortCriterias: [{
                        qSortByState: 1,
                        qExpression: {}
                      }]
                    },
                    qShowAlternatives: true,
                    qInitialDataFetch: [{
                      qTop: 0,
                      qHeight: qHeight,
                      qLeft: 0,
                      qWidth: 1
                    }]
                  }
                };
                _context2.prev = 1;
                _context2.next = 4;
                return qDoc.createSessionObject(listObjectProperties);

              case 4:
                sessionObj = _context2.sent;
                return _context2.abrupt("return", sessionObj);

              case 8:
                _context2.prev = 8;
                _context2.t0 = _context2["catch"](1);
                console.log(_context2.t0);
                throw "Cannot create the session object";

              case 12:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, null, [[1, 8]]);
      }));

      return function createSessionObject(_x, _x2, _x3, _x4) {
        return _ref2.apply(this, arguments);
      };
    }();

    var fieldInfo = function fieldInfo(layout) {
      var info = "Total distinct values: ".concat(layout.qListObject.qDimensionInfo.qCardinal, ";\nField types: ").concat(layout.qListObject.qDimensionInfo.qTags.join(","));
      return info;
    };
    /* harmony default export */


    var helpers = {
      initialise: initialise,
      createSessionObject: createSessionObject,
      fieldInfo: fieldInfo
    }; // CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/QlikField.vue?vue&type=script&lang=js&
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //

    function test() {
      return _test.apply(this, arguments);
    } // import { async } from "q";
    // import { truncate } from "fs";


    function _test() {
      _test = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee14() {
        return regeneratorRuntime.wrap(function _callee14$(_context14) {
          while (1) {
            switch (_context14.prev = _context14.next) {
              case 0:
                return _context14.abrupt("return", {
                  test: 1
                });

              case 1:
              case "end":
                return _context14.stop();
            }
          }
        }, _callee14);
      }));
      return _test.apply(this, arguments);
    }
    /* harmony default export */


    var QlikFieldvue_type_script_lang_js_ = {
      name: "QlikField",
      components: {
        FieldHeader: FieldHeader,
        FieldContent: FieldContent,
        LoadingAnimation: Loading,
        Intersect: dist
      },
      props: {
        clickAway: {
          type: Boolean,
          required: false,
          default: true
        },
        showArrow: {
          type: Boolean,
          required: false,
          default: true
        },
        showSearch: {
          type: Boolean,
          required: false,
          default: true
        },
        showHeader: {
          type: Boolean,
          required: false,
          default: true
        },
        debugMode: {
          type: Boolean,
          required: false,
          default: true
        },
        bgColors: {
          type: Object,
          required: false,
          default: function _default() {
            return {
              locked: "#3194f7",
              selected: "#52cc52",
              optional: "#ffffff",
              deselected: "#f9f9f9",
              alternative: "#dddddd",
              excluded: "#a9a9a9",
              excludedSelected: "#a9a9a9",
              excludedLocked: "#a9a9a9"
            };
          }
        },
        fontColors: {
          type: Object,
          required: false,
          default: function _default() {
            return {
              locked: "#ffffff",
              selected: "#ffffff",
              optional: "",
              deselected: "",
              alternative: "",
              excluded: "",
              excludedSelected: "",
              excludedLocked: ""
            };
          }
        },
        headerColors: {
          type: Object,
          required: false,
          default: function _default() {
            return {
              background: "#e6e6e6",
              icons: "#000"
            };
          }
        },
        headerIcons: {
          type: Object,
          required: false,
          default: function _default() {
            return {
              selectAll: {
                enabled: true,
                hidden: false
              },
              selectAlternative: {
                enabled: true,
                hidden: false
              },
              selectExcluded: {
                enabled: true,
                hidden: false
              },
              lock: {
                enabled: true,
                hidden: false
              },
              clearAll: {
                enabled: true,
                hidden: false
              }
            };
          }
        },
        fieldName: {
          type: String,
          required: false,
          default: "Actual Amount"
        },
        qType: {
          type: String,
          required: false,
          default: "my-list-object"
        },
        qDoc: {
          type: Object,
          required: false
        }
      },
      directives: {
        ObserveVisibility: vue_observe_visibility_esm["a"
        /* ObserveVisibility */
        ]
      },
      mixins: [vue_clickaway_common["mixin"]],
      data: function data() {
        return {
          showQlikField: "block",
          filter: "",
          isVisible: false,
          isPendingChange: false,
          isInitialised: false,
          isCalculated: false,
          isLoading: false,
          isError: false,
          errorMessage: "",
          qData: [],
          sessionObj: {},
          maxRows: 0,
          currentMax: 0,
          currentSelections: {},
          currentSelectionsObj: {},
          field: {},
          fieldInfo: "",
          qDocData: {},
          qHeight: 50
        };
      },
      methods: {
        visibilityChanged: function () {
          var _visibilityChanged = _asyncToGenerator(
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee2(isVisible, entry) {
            var _this, layout, a;

            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    _this = this;
                    _this.isVisible = isVisible; // console.log(isVisible);

                    if (!(_this.isInitialised == false && _this.isVisible == true)) {
                      _context2.next = 35;
                      break;
                    }

                    _context2.prev = 3;
                    _this.isLoading = true;
                    _this.isCalculated = false;

                    if (!(_this.debugMode == true)) {
                      _context2.next = 11;
                      break;
                    }

                    _context2.next = 9;
                    return helpers.initialise();

                  case 9:
                    _this.qDocData = _context2.sent;
                    _this.isInitialised = true;

                  case 11:
                    _this.currentMax = _this.qHeight;
                    _context2.next = 14;
                    return helpers.createSessionObject(_this.fieldName, _this.qDocData, _this.qHeight, _this.qType);

                  case 14:
                    _this.sessionObj = _context2.sent;
                    _context2.next = 17;
                    return _this.sessionObj.getLayout();

                  case 17:
                    layout = _context2.sent;
                    _this.maxRows = layout.qListObject.qDimensionInfo.qCardinal;
                    _this.qData = layout.qListObject.qDataPages[0].qMatrix;
                    _this.fieldInfo = helpers.fieldInfo(layout);
                    _context2.next = 23;
                    return _this.qDocData.getField(_this.fieldName);

                  case 23:
                    _this.field = _context2.sent;
                    _this.isInitialised = true;
                    _this.isCalculated = true;
                    _context2.next = 34;
                    break;

                  case 28:
                    _context2.prev = 28;
                    _context2.t0 = _context2["catch"](3);
                    console.log(_context2.t0);
                    _this.errorMessage = _context2.t0;
                    _this.isLoading = false;
                    _this.isError = true;

                  case 34:
                    try {
                      _this.sessionObj.on("changed",
                      /*#__PURE__*/
                      function () {
                        var _ref = _asyncToGenerator(
                        /*#__PURE__*/
                        regeneratorRuntime.mark(function _callee(d) {
                          var a, i, t;
                          return regeneratorRuntime.wrap(function _callee$(_context) {
                            while (1) {
                              switch (_context.prev = _context.next) {
                                case 0:
                                  if (!_this.isVisible) {
                                    _context.next = 24;
                                    break;
                                  }

                                  _this.isCalculated = false;
                                  _this.qData = [];
                                  a = _this.currentMax;
                                  _context.prev = 4;
                                  i = 0;

                                case 6:
                                  if (!(i < a)) {
                                    _context.next = 14;
                                    break;
                                  }

                                  _context.next = 9;
                                  return _this.sessionObj.getListObjectData("/qListObjectDef", [{
                                    qTop: i,
                                    qLeft: 0,
                                    qHeight: _this.qHeight,
                                    qWidth: 1
                                  }]);

                                case 9:
                                  t = _context.sent;
                                  _this.qData = _this.qData.concat(t[0].qMatrix);

                                case 11:
                                  i += _this.qHeight;
                                  _context.next = 6;
                                  break;

                                case 14:
                                  _context.next = 20;
                                  break;

                                case 16:
                                  _context.prev = 16;
                                  _context.t0 = _context["catch"](4);
                                  _this.errorMessage = "Error while fetching paging data";
                                  _this.isError = true;

                                case 20:
                                  _this.isPendingChange = false;
                                  _this.isCalculated = true;
                                  _context.next = 26;
                                  break;

                                case 24:
                                  _this.isPendingChange = true;
                                  _this.isCalculated = false;

                                case 26:
                                case "end":
                                  return _context.stop();
                              }
                            }
                          }, _callee, null, [[4, 16]]);
                        }));

                        return function (_x3) {
                          return _ref.apply(this, arguments);
                        };
                      }());
                    } catch (e) {
                      console.log(e);
                      _this.errorMessage = "Cannot create the session object";
                      _this.isError = true;
                    }

                  case 35:
                    if (!(_this.isVisible && _this.isPendingChange)) {
                      _context2.next = 42;
                      break;
                    }

                    _context2.next = 38;
                    return _this.sessionObj.getLayout();

                  case 38:
                    a = _context2.sent;
                    _this.qData = a.qListObject.qDataPages[0].qMatrix;
                    _this.isPendingChange = false;
                    _this.isCalculated = true;

                  case 42:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this, [[3, 28]]);
          }));

          function visibilityChanged(_x, _x2) {
            return _visibilityChanged.apply(this, arguments);
          }

          return visibilityChanged;
        }(),
        scrolled: function () {
          var _scrolled = _asyncToGenerator(
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee3() {
            var _this, t;

            return regeneratorRuntime.wrap(function _callee3$(_context3) {
              while (1) {
                switch (_context3.prev = _context3.next) {
                  case 0:
                    _this = this;
                    _this.isCalculated = false;

                    if (!(_this.currentMax < _this.maxRows)) {
                      _context3.next = 20;
                      break;
                    }

                    _context3.prev = 3;
                    _context3.next = 6;
                    return _this.sessionObj.getListObjectData("/qListObjectDef", [{
                      qTop: _this.currentMax,
                      qLeft: 0,
                      qHeight: _this.qHeight,
                      qWidth: 1
                    }]);

                  case 6:
                    t = _context3.sent;
                    _this.currentMax += _this.qHeight;
                    _this.qData = _this.qData.concat(t[0].qMatrix);
                    _this.isCalculated = true;
                    _context3.next = 18;
                    break;

                  case 12:
                    _context3.prev = 12;
                    _context3.t0 = _context3["catch"](3);
                    console.log(_context3.t0);
                    _this.errorMessage = "Error while fetching paging data";
                    _this.isError = true;
                    _this.isLoading = false;

                  case 18:
                    _context3.next = 21;
                    break;

                  case 20:
                    _this.isCalculated = true;

                  case 21:
                  case "end":
                    return _context3.stop();
                }
              }
            }, _callee3, this, [[3, 12]]);
          }));

          function scrolled() {
            return _scrolled.apply(this, arguments);
          }

          return scrolled;
        }(),
        away: function away() {
          var _this = this;

          if (_this.clickAway == true) {
            if (_this.showQlikField == "block") {
              _this.showQlikField = "none";
            } else {
              _this.showQlikField = "block";
            }
          }
        },
        clicked: function () {
          var _clicked = _asyncToGenerator(
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee4(qValue) {
            return regeneratorRuntime.wrap(function _callee4$(_context4) {
              while (1) {
                switch (_context4.prev = _context4.next) {
                  case 0:
                    _context4.next = 2;
                    return this.sessionObj.selectListObjectValues("/qListObjectDef", [qValue.qElemNumber], true);

                  case 2:
                  case "end":
                    return _context4.stop();
                }
              }
            }, _callee4, this);
          }));

          function clicked(_x4) {
            return _clicked.apply(this, arguments);
          }

          return clicked;
        }(),
        selectAll: function () {
          var _selectAll = _asyncToGenerator(
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee5() {
            return regeneratorRuntime.wrap(function _callee5$(_context5) {
              while (1) {
                switch (_context5.prev = _context5.next) {
                  case 0:
                    _context5.next = 2;
                    return this.field.selectAll();

                  case 2:
                  case "end":
                    return _context5.stop();
                }
              }
            }, _callee5, this);
          }));

          function selectAll() {
            return _selectAll.apply(this, arguments);
          }

          return selectAll;
        }(),
        selectAlternative: function () {
          var _selectAlternative = _asyncToGenerator(
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee6() {
            return regeneratorRuntime.wrap(function _callee6$(_context6) {
              while (1) {
                switch (_context6.prev = _context6.next) {
                  case 0:
                    _context6.next = 2;
                    return this.field.selectAlternative();

                  case 2:
                  case "end":
                    return _context6.stop();
                }
              }
            }, _callee6, this);
          }));

          function selectAlternative() {
            return _selectAlternative.apply(this, arguments);
          }

          return selectAlternative;
        }(),
        selectExcluded: function () {
          var _selectExcluded = _asyncToGenerator(
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee7() {
            return regeneratorRuntime.wrap(function _callee7$(_context7) {
              while (1) {
                switch (_context7.prev = _context7.next) {
                  case 0:
                    _context7.next = 2;
                    return this.field.selectExcluded();

                  case 2:
                  case "end":
                    return _context7.stop();
                }
              }
            }, _callee7, this);
          }));

          function selectExcluded() {
            return _selectExcluded.apply(this, arguments);
          }

          return selectExcluded;
        }(),
        lock: function () {
          var _lock = _asyncToGenerator(
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee8() {
            return regeneratorRuntime.wrap(function _callee8$(_context8) {
              while (1) {
                switch (_context8.prev = _context8.next) {
                  case 0:
                    _context8.next = 2;
                    return this.field.lock();

                  case 2:
                  case "end":
                    return _context8.stop();
                }
              }
            }, _callee8, this);
          }));

          function lock() {
            return _lock.apply(this, arguments);
          }

          return lock;
        }(),
        unlock: function () {
          var _unlock = _asyncToGenerator(
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee9() {
            return regeneratorRuntime.wrap(function _callee9$(_context9) {
              while (1) {
                switch (_context9.prev = _context9.next) {
                  case 0:
                    _context9.next = 2;
                    return this.field.unlock();

                  case 2:
                  case "end":
                    return _context9.stop();
                }
              }
            }, _callee9, this);
          }));

          function unlock() {
            return _unlock.apply(this, arguments);
          }

          return unlock;
        }(),
        clearAll: function () {
          var _clearAll = _asyncToGenerator(
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee10() {
            return regeneratorRuntime.wrap(function _callee10$(_context10) {
              while (1) {
                switch (_context10.prev = _context10.next) {
                  case 0:
                    _context10.next = 2;
                    return this.field.clear();

                  case 2:
                  case "end":
                    return _context10.stop();
                }
              }
            }, _callee10, this);
          }));

          function clearAll() {
            return _clearAll.apply(this, arguments);
          }

          return clearAll;
        }(),
        search: function () {
          var _search = _asyncToGenerator(
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee11(searchValue) {
            var searchResult;
            return regeneratorRuntime.wrap(function _callee11$(_context11) {
              while (1) {
                switch (_context11.prev = _context11.next) {
                  case 0:
                    _context11.next = 2;
                    return this.sessionObj.searchListObjectFor("/qListObjectDef", searchValue);

                  case 2:
                    searchResult = _context11.sent;

                  case 3:
                  case "end":
                    return _context11.stop();
                }
              }
            }, _callee11, this);
          }));

          function search(_x5) {
            return _search.apply(this, arguments);
          }

          return search;
        }(),
        throwEror: function throwEror(error, message) {}
      },
      mounted: function () {
        var _mounted = _asyncToGenerator(
        /*#__PURE__*/
        regeneratorRuntime.mark(function _callee12() {
          return regeneratorRuntime.wrap(function _callee12$(_context12) {
            while (1) {
              switch (_context12.prev = _context12.next) {
                case 0:
                  this.qDocData = this.qDoc;

                case 1:
                case "end":
                  return _context12.stop();
              }
            }
          }, _callee12, this);
        }));

        function mounted() {
          return _mounted.apply(this, arguments);
        }

        return mounted;
      }(),
      created: function created() {},
      beforeDestroy: function () {
        var _beforeDestroy = _asyncToGenerator(
        /*#__PURE__*/
        regeneratorRuntime.mark(function _callee13() {
          var self;
          return regeneratorRuntime.wrap(function _callee13$(_context13) {
            while (1) {
              switch (_context13.prev = _context13.next) {
                case 0:
                  self = this;

                  try {
                    self.qDocData.destroySessionObject(self.sessionObj.id);
                  } catch (e) {}

                  if (self.debugMode) {
                    try {
                      self.ses;
                    } catch (e) {}
                  }

                case 3:
                case "end":
                  return _context13.stop();
              }
            }
          }, _callee13, this);
        }));

        function beforeDestroy() {
          return _beforeDestroy.apply(this, arguments);
        }

        return beforeDestroy;
      }(),
      watch: {
        isCalculated: function isCalculated(value) {
          var _this = this;

          if (value == false) {
            setTimeout(function () {
              _this.isLoading = true;

              if (_this.isCalculated == true) {
                _this.isLoading = false;
              }
            }, 200);
          } else {
            _this.isLoading = false;
          }
        }
      }
    }; // CONCATENATED MODULE: ./src/QlikField.vue?vue&type=script&lang=js&

    /* harmony default export */

    var src_QlikFieldvue_type_script_lang_js_ = QlikFieldvue_type_script_lang_js_; // EXTERNAL MODULE: ./src/QlikField.vue?vue&type=style&index=0&lang=css&

    var QlikFieldvue_type_style_index_0_lang_css_ = __webpack_require__("2b5f"); // CONCATENATED MODULE: ./src/QlikField.vue

    /* normalize component */


    var QlikField_component = normalizeComponent(src_QlikFieldvue_type_script_lang_js_, render, staticRenderFns, false, null, null, null);
    /* harmony default export */

    var QlikField = QlikField_component.exports; // CONCATENATED MODULE: ./node_modules/@vue/cli-service/lib/commands/build/entry-lib.js

    /* harmony default export */

    var entry_lib = __webpack_exports__["default"] = QlikField;
    /***/
  },

  /***/
  "ff0c":
  /***/
  function ff0c(module, exports, __webpack_require__) {
    // 19.1.2.9 / 15.2.3.2 Object.getPrototypeOf(O)
    var has = __webpack_require__("43c8");

    var toObject = __webpack_require__("0185");

    var IE_PROTO = __webpack_require__("5d8f")('IE_PROTO');

    var ObjectProto = Object.prototype;

    module.exports = _Object$getPrototypeOf || function (O) {
      O = toObject(O);
      if (has(O, IE_PROTO)) return O[IE_PROTO];

      if (typeof O.constructor == 'function' && O instanceof O.constructor) {
        return O.constructor.prototype;
      }

      return O instanceof Object ? ObjectProto : null;
    };
    /***/

  }
  /******/

})["default"];

/***/ }),

/***/ "241e":
/***/ (function(module, exports, __webpack_require__) {

// 7.1.13 ToObject(argument)
var defined = __webpack_require__("25eb");
module.exports = function (it) {
  return Object(defined(it));
};


/***/ }),

/***/ "24c5":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var LIBRARY = __webpack_require__("b8e3");
var global = __webpack_require__("e53d");
var ctx = __webpack_require__("d864");
var classof = __webpack_require__("40c3");
var $export = __webpack_require__("63b6");
var isObject = __webpack_require__("f772");
var aFunction = __webpack_require__("79aa");
var anInstance = __webpack_require__("1173");
var forOf = __webpack_require__("a22a");
var speciesConstructor = __webpack_require__("f201");
var task = __webpack_require__("4178").set;
var microtask = __webpack_require__("aba2")();
var newPromiseCapabilityModule = __webpack_require__("656e");
var perform = __webpack_require__("4439");
var userAgent = __webpack_require__("bc13");
var promiseResolve = __webpack_require__("cd78");
var PROMISE = 'Promise';
var TypeError = global.TypeError;
var process = global.process;
var versions = process && process.versions;
var v8 = versions && versions.v8 || '';
var $Promise = global[PROMISE];
var isNode = classof(process) == 'process';
var empty = function () { /* empty */ };
var Internal, newGenericPromiseCapability, OwnPromiseCapability, Wrapper;
var newPromiseCapability = newGenericPromiseCapability = newPromiseCapabilityModule.f;

var USE_NATIVE = !!function () {
  try {
    // correct subclassing with @@species support
    var promise = $Promise.resolve(1);
    var FakePromise = (promise.constructor = {})[__webpack_require__("5168")('species')] = function (exec) {
      exec(empty, empty);
    };
    // unhandled rejections tracking support, NodeJS Promise without it fails @@species test
    return (isNode || typeof PromiseRejectionEvent == 'function')
      && promise.then(empty) instanceof FakePromise
      // v8 6.6 (Node 10 and Chrome 66) have a bug with resolving custom thenables
      // https://bugs.chromium.org/p/chromium/issues/detail?id=830565
      // we can't detect it synchronously, so just check versions
      && v8.indexOf('6.6') !== 0
      && userAgent.indexOf('Chrome/66') === -1;
  } catch (e) { /* empty */ }
}();

// helpers
var isThenable = function (it) {
  var then;
  return isObject(it) && typeof (then = it.then) == 'function' ? then : false;
};
var notify = function (promise, isReject) {
  if (promise._n) return;
  promise._n = true;
  var chain = promise._c;
  microtask(function () {
    var value = promise._v;
    var ok = promise._s == 1;
    var i = 0;
    var run = function (reaction) {
      var handler = ok ? reaction.ok : reaction.fail;
      var resolve = reaction.resolve;
      var reject = reaction.reject;
      var domain = reaction.domain;
      var result, then, exited;
      try {
        if (handler) {
          if (!ok) {
            if (promise._h == 2) onHandleUnhandled(promise);
            promise._h = 1;
          }
          if (handler === true) result = value;
          else {
            if (domain) domain.enter();
            result = handler(value); // may throw
            if (domain) {
              domain.exit();
              exited = true;
            }
          }
          if (result === reaction.promise) {
            reject(TypeError('Promise-chain cycle'));
          } else if (then = isThenable(result)) {
            then.call(result, resolve, reject);
          } else resolve(result);
        } else reject(value);
      } catch (e) {
        if (domain && !exited) domain.exit();
        reject(e);
      }
    };
    while (chain.length > i) run(chain[i++]); // variable length - can't use forEach
    promise._c = [];
    promise._n = false;
    if (isReject && !promise._h) onUnhandled(promise);
  });
};
var onUnhandled = function (promise) {
  task.call(global, function () {
    var value = promise._v;
    var unhandled = isUnhandled(promise);
    var result, handler, console;
    if (unhandled) {
      result = perform(function () {
        if (isNode) {
          process.emit('unhandledRejection', value, promise);
        } else if (handler = global.onunhandledrejection) {
          handler({ promise: promise, reason: value });
        } else if ((console = global.console) && console.error) {
          console.error('Unhandled promise rejection', value);
        }
      });
      // Browsers should not trigger `rejectionHandled` event if it was handled here, NodeJS - should
      promise._h = isNode || isUnhandled(promise) ? 2 : 1;
    } promise._a = undefined;
    if (unhandled && result.e) throw result.v;
  });
};
var isUnhandled = function (promise) {
  return promise._h !== 1 && (promise._a || promise._c).length === 0;
};
var onHandleUnhandled = function (promise) {
  task.call(global, function () {
    var handler;
    if (isNode) {
      process.emit('rejectionHandled', promise);
    } else if (handler = global.onrejectionhandled) {
      handler({ promise: promise, reason: promise._v });
    }
  });
};
var $reject = function (value) {
  var promise = this;
  if (promise._d) return;
  promise._d = true;
  promise = promise._w || promise; // unwrap
  promise._v = value;
  promise._s = 2;
  if (!promise._a) promise._a = promise._c.slice();
  notify(promise, true);
};
var $resolve = function (value) {
  var promise = this;
  var then;
  if (promise._d) return;
  promise._d = true;
  promise = promise._w || promise; // unwrap
  try {
    if (promise === value) throw TypeError("Promise can't be resolved itself");
    if (then = isThenable(value)) {
      microtask(function () {
        var wrapper = { _w: promise, _d: false }; // wrap
        try {
          then.call(value, ctx($resolve, wrapper, 1), ctx($reject, wrapper, 1));
        } catch (e) {
          $reject.call(wrapper, e);
        }
      });
    } else {
      promise._v = value;
      promise._s = 1;
      notify(promise, false);
    }
  } catch (e) {
    $reject.call({ _w: promise, _d: false }, e); // wrap
  }
};

// constructor polyfill
if (!USE_NATIVE) {
  // 25.4.3.1 Promise(executor)
  $Promise = function Promise(executor) {
    anInstance(this, $Promise, PROMISE, '_h');
    aFunction(executor);
    Internal.call(this);
    try {
      executor(ctx($resolve, this, 1), ctx($reject, this, 1));
    } catch (err) {
      $reject.call(this, err);
    }
  };
  // eslint-disable-next-line no-unused-vars
  Internal = function Promise(executor) {
    this._c = [];             // <- awaiting reactions
    this._a = undefined;      // <- checked in isUnhandled reactions
    this._s = 0;              // <- state
    this._d = false;          // <- done
    this._v = undefined;      // <- value
    this._h = 0;              // <- rejection state, 0 - default, 1 - handled, 2 - unhandled
    this._n = false;          // <- notify
  };
  Internal.prototype = __webpack_require__("5c95")($Promise.prototype, {
    // 25.4.5.3 Promise.prototype.then(onFulfilled, onRejected)
    then: function then(onFulfilled, onRejected) {
      var reaction = newPromiseCapability(speciesConstructor(this, $Promise));
      reaction.ok = typeof onFulfilled == 'function' ? onFulfilled : true;
      reaction.fail = typeof onRejected == 'function' && onRejected;
      reaction.domain = isNode ? process.domain : undefined;
      this._c.push(reaction);
      if (this._a) this._a.push(reaction);
      if (this._s) notify(this, false);
      return reaction.promise;
    },
    // 25.4.5.1 Promise.prototype.catch(onRejected)
    'catch': function (onRejected) {
      return this.then(undefined, onRejected);
    }
  });
  OwnPromiseCapability = function () {
    var promise = new Internal();
    this.promise = promise;
    this.resolve = ctx($resolve, promise, 1);
    this.reject = ctx($reject, promise, 1);
  };
  newPromiseCapabilityModule.f = newPromiseCapability = function (C) {
    return C === $Promise || C === Wrapper
      ? new OwnPromiseCapability(C)
      : newGenericPromiseCapability(C);
  };
}

$export($export.G + $export.W + $export.F * !USE_NATIVE, { Promise: $Promise });
__webpack_require__("45f2")($Promise, PROMISE);
__webpack_require__("4c95")(PROMISE);
Wrapper = __webpack_require__("584a")[PROMISE];

// statics
$export($export.S + $export.F * !USE_NATIVE, PROMISE, {
  // 25.4.4.5 Promise.reject(r)
  reject: function reject(r) {
    var capability = newPromiseCapability(this);
    var $$reject = capability.reject;
    $$reject(r);
    return capability.promise;
  }
});
$export($export.S + $export.F * (LIBRARY || !USE_NATIVE), PROMISE, {
  // 25.4.4.6 Promise.resolve(x)
  resolve: function resolve(x) {
    return promiseResolve(LIBRARY && this === Wrapper ? $Promise : this, x);
  }
});
$export($export.S + $export.F * !(USE_NATIVE && __webpack_require__("4ee1")(function (iter) {
  $Promise.all(iter)['catch'](empty);
})), PROMISE, {
  // 25.4.4.1 Promise.all(iterable)
  all: function all(iterable) {
    var C = this;
    var capability = newPromiseCapability(C);
    var resolve = capability.resolve;
    var reject = capability.reject;
    var result = perform(function () {
      var values = [];
      var index = 0;
      var remaining = 1;
      forOf(iterable, false, function (promise) {
        var $index = index++;
        var alreadyCalled = false;
        values.push(undefined);
        remaining++;
        C.resolve(promise).then(function (value) {
          if (alreadyCalled) return;
          alreadyCalled = true;
          values[$index] = value;
          --remaining || resolve(values);
        }, reject);
      });
      --remaining || resolve(values);
    });
    if (result.e) reject(result.v);
    return capability.promise;
  },
  // 25.4.4.4 Promise.race(iterable)
  race: function race(iterable) {
    var C = this;
    var capability = newPromiseCapability(C);
    var reject = capability.reject;
    var result = perform(function () {
      forOf(iterable, false, function (promise) {
        C.resolve(promise).then(capability.resolve, reject);
      });
    });
    if (result.e) reject(result.v);
    return capability.promise;
  }
});


/***/ }),

/***/ "25b0":
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("1df8");
module.exports = __webpack_require__("584a").Object.setPrototypeOf;


/***/ }),

/***/ "25eb":
/***/ (function(module, exports) {

// 7.2.1 RequireObjectCoercible(argument)
module.exports = function (it) {
  if (it == undefined) throw TypeError("Can't call method on  " + it);
  return it;
};


/***/ }),

/***/ "268f":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("fde4");

/***/ }),

/***/ "28ef":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var anObject = __webpack_require__("b597");
var sameValue = __webpack_require__("c575");
var regExpExec = __webpack_require__("d25d");

// @@search logic
__webpack_require__("1fbc")('search', 1, function (defined, SEARCH, $search, maybeCallNative) {
  return [
    // `String.prototype.search` method
    // https://tc39.github.io/ecma262/#sec-string.prototype.search
    function search(regexp) {
      var O = defined(this);
      var fn = regexp == undefined ? undefined : regexp[SEARCH];
      return fn !== undefined ? fn.call(regexp, O) : new RegExp(regexp)[SEARCH](String(O));
    },
    // `RegExp.prototype[@@search]` method
    // https://tc39.github.io/ecma262/#sec-regexp.prototype-@@search
    function (regexp) {
      var res = maybeCallNative($search, regexp, this);
      if (res.done) return res.value;
      var rx = anObject(regexp);
      var S = String(this);
      var previousLastIndex = rx.lastIndex;
      if (!sameValue(previousLastIndex, 0)) rx.lastIndex = 0;
      var result = regExpExec(rx, S);
      if (!sameValue(rx.lastIndex, previousLastIndex)) rx.lastIndex = previousLastIndex;
      return result === null ? -1 : result.index;
    }
  ];
});


/***/ }),

/***/ "294c":
/***/ (function(module, exports) {

module.exports = function (exec) {
  try {
    return !!exec();
  } catch (e) {
    return true;
  }
};


/***/ }),

/***/ "2aba":
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__("7726");
var hide = __webpack_require__("32e9");
var has = __webpack_require__("69a8");
var SRC = __webpack_require__("ca5a")('src');
var TO_STRING = 'toString';
var $toString = Function[TO_STRING];
var TPL = ('' + $toString).split(TO_STRING);

__webpack_require__("8378").inspectSource = function (it) {
  return $toString.call(it);
};

(module.exports = function (O, key, val, safe) {
  var isFunction = typeof val == 'function';
  if (isFunction) has(val, 'name') || hide(val, 'name', key);
  if (O[key] === val) return;
  if (isFunction) has(val, SRC) || hide(val, SRC, O[key] ? '' + O[key] : TPL.join(String(key)));
  if (O === global) {
    O[key] = val;
  } else if (!safe) {
    delete O[key];
    hide(O, key, val);
  } else if (O[key]) {
    O[key] = val;
  } else {
    hide(O, key, val);
  }
// add fake Function#toString for correct work wrapped methods / constructors with methods like LoDash isNative
})(Function.prototype, TO_STRING, function toString() {
  return typeof this == 'function' && this[SRC] || $toString.call(this);
});


/***/ }),

/***/ "2f21":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var fails = __webpack_require__("79e5");

module.exports = function (method, arg) {
  return !!method && fails(function () {
    // eslint-disable-next-line no-useless-call
    arg ? method.call(null, function () { /* empty */ }, 1) : method.call(null);
  });
};


/***/ }),

/***/ "3024":
/***/ (function(module, exports) {

// fast apply, http://jsperf.lnkit.com/fast-apply/5
module.exports = function (fn, args, that) {
  var un = that === undefined;
  switch (args.length) {
    case 0: return un ? fn()
                      : fn.call(that);
    case 1: return un ? fn(args[0])
                      : fn.call(that, args[0]);
    case 2: return un ? fn(args[0], args[1])
                      : fn.call(that, args[0], args[1]);
    case 3: return un ? fn(args[0], args[1], args[2])
                      : fn.call(that, args[0], args[1], args[2]);
    case 4: return un ? fn(args[0], args[1], args[2], args[3])
                      : fn.call(that, args[0], args[1], args[2], args[3]);
  } return fn.apply(that, args);
};


/***/ }),

/***/ "30f1":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var LIBRARY = __webpack_require__("b8e3");
var $export = __webpack_require__("63b6");
var redefine = __webpack_require__("9138");
var hide = __webpack_require__("35e8");
var Iterators = __webpack_require__("481b");
var $iterCreate = __webpack_require__("8f60");
var setToStringTag = __webpack_require__("45f2");
var getPrototypeOf = __webpack_require__("53e2");
var ITERATOR = __webpack_require__("5168")('iterator');
var BUGGY = !([].keys && 'next' in [].keys()); // Safari has buggy iterators w/o `next`
var FF_ITERATOR = '@@iterator';
var KEYS = 'keys';
var VALUES = 'values';

var returnThis = function () { return this; };

module.exports = function (Base, NAME, Constructor, next, DEFAULT, IS_SET, FORCED) {
  $iterCreate(Constructor, NAME, next);
  var getMethod = function (kind) {
    if (!BUGGY && kind in proto) return proto[kind];
    switch (kind) {
      case KEYS: return function keys() { return new Constructor(this, kind); };
      case VALUES: return function values() { return new Constructor(this, kind); };
    } return function entries() { return new Constructor(this, kind); };
  };
  var TAG = NAME + ' Iterator';
  var DEF_VALUES = DEFAULT == VALUES;
  var VALUES_BUG = false;
  var proto = Base.prototype;
  var $native = proto[ITERATOR] || proto[FF_ITERATOR] || DEFAULT && proto[DEFAULT];
  var $default = $native || getMethod(DEFAULT);
  var $entries = DEFAULT ? !DEF_VALUES ? $default : getMethod('entries') : undefined;
  var $anyNative = NAME == 'Array' ? proto.entries || $native : $native;
  var methods, key, IteratorPrototype;
  // Fix native
  if ($anyNative) {
    IteratorPrototype = getPrototypeOf($anyNative.call(new Base()));
    if (IteratorPrototype !== Object.prototype && IteratorPrototype.next) {
      // Set @@toStringTag to native iterators
      setToStringTag(IteratorPrototype, TAG, true);
      // fix for some old engines
      if (!LIBRARY && typeof IteratorPrototype[ITERATOR] != 'function') hide(IteratorPrototype, ITERATOR, returnThis);
    }
  }
  // fix Array#{values, @@iterator}.name in V8 / FF
  if (DEF_VALUES && $native && $native.name !== VALUES) {
    VALUES_BUG = true;
    $default = function values() { return $native.call(this); };
  }
  // Define iterator
  if ((!LIBRARY || FORCED) && (BUGGY || VALUES_BUG || !proto[ITERATOR])) {
    hide(proto, ITERATOR, $default);
  }
  // Plug for library
  Iterators[NAME] = $default;
  Iterators[TAG] = returnThis;
  if (DEFAULT) {
    methods = {
      values: DEF_VALUES ? $default : getMethod(VALUES),
      keys: IS_SET ? $default : getMethod(KEYS),
      entries: $entries
    };
    if (FORCED) for (key in methods) {
      if (!(key in proto)) redefine(proto, key, methods[key]);
    } else $export($export.P + $export.F * (BUGGY || VALUES_BUG), NAME, methods);
  }
  return methods;
};


/***/ }),

/***/ "3143":
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__("635a");
var hide = __webpack_require__("59c4");
var has = __webpack_require__("4276");
var SRC = __webpack_require__("11f1")('src');
var TO_STRING = 'toString';
var $toString = Function[TO_STRING];
var TPL = ('' + $toString).split(TO_STRING);

__webpack_require__("7a1e").inspectSource = function (it) {
  return $toString.call(it);
};

(module.exports = function (O, key, val, safe) {
  var isFunction = typeof val == 'function';
  if (isFunction) has(val, 'name') || hide(val, 'name', key);
  if (O[key] === val) return;
  if (isFunction) has(val, SRC) || hide(val, SRC, O[key] ? '' + O[key] : TPL.join(String(key)));
  if (O === global) {
    O[key] = val;
  } else if (!safe) {
    delete O[key];
    hide(O, key, val);
  } else if (O[key]) {
    O[key] = val;
  } else {
    hide(O, key, val);
  }
// add fake Function#toString for correct work wrapped methods / constructors with methods like LoDash isNative
})(Function.prototype, TO_STRING, function toString() {
  return typeof this == 'function' && this[SRC] || $toString.call(this);
});


/***/ }),

/***/ "32a6":
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.14 Object.keys(O)
var toObject = __webpack_require__("241e");
var $keys = __webpack_require__("c3a1");

__webpack_require__("ce7e")('keys', function () {
  return function keys(it) {
    return $keys(toObject(it));
  };
});


/***/ }),

/***/ "32e9":
/***/ (function(module, exports, __webpack_require__) {

var dP = __webpack_require__("86cc");
var createDesc = __webpack_require__("4630");
module.exports = __webpack_require__("9e1e") ? function (object, key, value) {
  return dP.f(object, key, createDesc(1, value));
} : function (object, key, value) {
  object[key] = value;
  return object;
};


/***/ }),

/***/ "32fc":
/***/ (function(module, exports, __webpack_require__) {

var document = __webpack_require__("e53d").document;
module.exports = document && document.documentElement;


/***/ }),

/***/ "335c":
/***/ (function(module, exports, __webpack_require__) {

// fallback for non-array-like ES3 and non-enumerable old V8 strings
var cof = __webpack_require__("6b4c");
// eslint-disable-next-line no-prototype-builtins
module.exports = Object('z').propertyIsEnumerable(0) ? Object : function (it) {
  return cof(it) == 'String' ? it.split('') : Object(it);
};


/***/ }),

/***/ "355d":
/***/ (function(module, exports) {

exports.f = {}.propertyIsEnumerable;


/***/ }),

/***/ "35e8":
/***/ (function(module, exports, __webpack_require__) {

var dP = __webpack_require__("d9f6");
var createDesc = __webpack_require__("aebd");
module.exports = __webpack_require__("8e60") ? function (object, key, value) {
  return dP.f(object, key, createDesc(1, value));
} : function (object, key, value) {
  object[key] = value;
  return object;
};


/***/ }),

/***/ "36c3":
/***/ (function(module, exports, __webpack_require__) {

// to indexed object, toObject with fallback for non-array-like ES3 strings
var IObject = __webpack_require__("335c");
var defined = __webpack_require__("25eb");
module.exports = function (it) {
  return IObject(defined(it));
};


/***/ }),

/***/ "3702":
/***/ (function(module, exports, __webpack_require__) {

// check on default Array iterator
var Iterators = __webpack_require__("481b");
var ITERATOR = __webpack_require__("5168")('iterator');
var ArrayProto = Array.prototype;

module.exports = function (it) {
  return it !== undefined && (Iterators.Array === it || ArrayProto[ITERATOR] === it);
};


/***/ }),

/***/ "3a38":
/***/ (function(module, exports) {

// 7.1.4 ToInteger
var ceil = Math.ceil;
var floor = Math.floor;
module.exports = function (it) {
  return isNaN(it = +it) ? 0 : (it > 0 ? floor : ceil)(it);
};


/***/ }),

/***/ "3a9d":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Search_vue_vue_type_style_index_0_id_d0c1e11e_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("9d93");
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Search_vue_vue_type_style_index_0_id_d0c1e11e_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Search_vue_vue_type_style_index_0_id_d0c1e11e_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */
 /* unused harmony default export */ var _unused_webpack_default_export = (_node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Search_vue_vue_type_style_index_0_id_d0c1e11e_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "3c11":
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// https://github.com/tc39/proposal-promise-finally

var $export = __webpack_require__("63b6");
var core = __webpack_require__("584a");
var global = __webpack_require__("e53d");
var speciesConstructor = __webpack_require__("f201");
var promiseResolve = __webpack_require__("cd78");

$export($export.P + $export.R, 'Promise', { 'finally': function (onFinally) {
  var C = speciesConstructor(this, core.Promise || global.Promise);
  var isFunction = typeof onFinally == 'function';
  return this.then(
    isFunction ? function (x) {
      return promiseResolve(C, onFinally()).then(function () { return x; });
    } : onFinally,
    isFunction ? function (e) {
      return promiseResolve(C, onFinally()).then(function () { throw e; });
    } : onFinally
  );
} });


/***/ }),

/***/ "3e0f":
/***/ (function(module, exports, __webpack_require__) {

var dP = __webpack_require__("120e");
var anObject = __webpack_require__("b597");
var getKeys = __webpack_require__("d0af");

module.exports = __webpack_require__("7e81") ? Object.defineProperties : function defineProperties(O, Properties) {
  anObject(O);
  var keys = getKeys(Properties);
  var length = keys.length;
  var i = 0;
  var P;
  while (length > i) dP.f(O, P = keys[i++], Properties[P]);
  return O;
};


/***/ }),

/***/ "40c3":
/***/ (function(module, exports, __webpack_require__) {

// getting tag from 19.1.3.6 Object.prototype.toString()
var cof = __webpack_require__("6b4c");
var TAG = __webpack_require__("5168")('toStringTag');
// ES3 wrong here
var ARG = cof(function () { return arguments; }()) == 'Arguments';

// fallback for IE11 Script Access Denied error
var tryGet = function (it, key) {
  try {
    return it[key];
  } catch (e) { /* empty */ }
};

module.exports = function (it) {
  var O, T, B;
  return it === undefined ? 'Undefined' : it === null ? 'Null'
    // @@toStringTag case
    : typeof (T = tryGet(O = Object(it), TAG)) == 'string' ? T
    // builtinTag case
    : ARG ? cof(O)
    // ES3 arguments fallback
    : (B = cof(O)) == 'Object' && typeof O.callee == 'function' ? 'Arguments' : B;
};


/***/ }),

/***/ "4178":
/***/ (function(module, exports, __webpack_require__) {

var ctx = __webpack_require__("d864");
var invoke = __webpack_require__("3024");
var html = __webpack_require__("32fc");
var cel = __webpack_require__("1ec9");
var global = __webpack_require__("e53d");
var process = global.process;
var setTask = global.setImmediate;
var clearTask = global.clearImmediate;
var MessageChannel = global.MessageChannel;
var Dispatch = global.Dispatch;
var counter = 0;
var queue = {};
var ONREADYSTATECHANGE = 'onreadystatechange';
var defer, channel, port;
var run = function () {
  var id = +this;
  // eslint-disable-next-line no-prototype-builtins
  if (queue.hasOwnProperty(id)) {
    var fn = queue[id];
    delete queue[id];
    fn();
  }
};
var listener = function (event) {
  run.call(event.data);
};
// Node.js 0.9+ & IE10+ has setImmediate, otherwise:
if (!setTask || !clearTask) {
  setTask = function setImmediate(fn) {
    var args = [];
    var i = 1;
    while (arguments.length > i) args.push(arguments[i++]);
    queue[++counter] = function () {
      // eslint-disable-next-line no-new-func
      invoke(typeof fn == 'function' ? fn : Function(fn), args);
    };
    defer(counter);
    return counter;
  };
  clearTask = function clearImmediate(id) {
    delete queue[id];
  };
  // Node.js 0.8-
  if (__webpack_require__("6b4c")(process) == 'process') {
    defer = function (id) {
      process.nextTick(ctx(run, id, 1));
    };
  // Sphere (JS game engine) Dispatch API
  } else if (Dispatch && Dispatch.now) {
    defer = function (id) {
      Dispatch.now(ctx(run, id, 1));
    };
  // Browsers with MessageChannel, includes WebWorkers
  } else if (MessageChannel) {
    channel = new MessageChannel();
    port = channel.port2;
    channel.port1.onmessage = listener;
    defer = ctx(port.postMessage, port, 1);
  // Browsers with postMessage, skip WebWorkers
  // IE8 has postMessage, but it's sync & typeof its postMessage is 'object'
  } else if (global.addEventListener && typeof postMessage == 'function' && !global.importScripts) {
    defer = function (id) {
      global.postMessage(id + '', '*');
    };
    global.addEventListener('message', listener, false);
  // IE8-
  } else if (ONREADYSTATECHANGE in cel('script')) {
    defer = function (id) {
      html.appendChild(cel('script'))[ONREADYSTATECHANGE] = function () {
        html.removeChild(this);
        run.call(id);
      };
    };
  // Rest old browsers
  } else {
    defer = function (id) {
      setTimeout(ctx(run, id, 1), 0);
    };
  }
}
module.exports = {
  set: setTask,
  clear: clearTask
};


/***/ }),

/***/ "4276":
/***/ (function(module, exports) {

var hasOwnProperty = {}.hasOwnProperty;
module.exports = function (it, key) {
  return hasOwnProperty.call(it, key);
};


/***/ }),

/***/ "43f8":
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.11 Object.isExtensible(O)
var isObject = __webpack_require__("f772");

__webpack_require__("ce7e")('isExtensible', function ($isExtensible) {
  return function isExtensible(it) {
    return isObject(it) ? $isExtensible ? $isExtensible(it) : true : false;
  };
});


/***/ }),

/***/ "43fc":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// https://github.com/tc39/proposal-promise-try
var $export = __webpack_require__("63b6");
var newPromiseCapability = __webpack_require__("656e");
var perform = __webpack_require__("4439");

$export($export.S, 'Promise', { 'try': function (callbackfn) {
  var promiseCapability = newPromiseCapability.f(this);
  var result = perform(callbackfn);
  (result.e ? promiseCapability.reject : promiseCapability.resolve)(result.v);
  return promiseCapability.promise;
} });


/***/ }),

/***/ "4439":
/***/ (function(module, exports) {

module.exports = function (exec) {
  try {
    return { e: false, v: exec() };
  } catch (e) {
    return { e: true, v: e };
  }
};


/***/ }),

/***/ "454f":
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("46a7");
var $Object = __webpack_require__("584a").Object;
module.exports = function defineProperty(it, key, desc) {
  return $Object.defineProperty(it, key, desc);
};


/***/ }),

/***/ "45f2":
/***/ (function(module, exports, __webpack_require__) {

var def = __webpack_require__("d9f6").f;
var has = __webpack_require__("07e3");
var TAG = __webpack_require__("5168")('toStringTag');

module.exports = function (it, tag, stat) {
  if (it && !has(it = stat ? it : it.prototype, TAG)) def(it, TAG, { configurable: true, value: tag });
};


/***/ }),

/***/ "4630":
/***/ (function(module, exports) {

module.exports = function (bitmap, value) {
  return {
    enumerable: !(bitmap & 1),
    configurable: !(bitmap & 2),
    writable: !(bitmap & 4),
    value: value
  };
};


/***/ }),

/***/ "469f":
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("6c1c");
__webpack_require__("1654");
module.exports = __webpack_require__("7d7b");


/***/ }),

/***/ "46a7":
/***/ (function(module, exports, __webpack_require__) {

var $export = __webpack_require__("63b6");
// 19.1.2.4 / 15.2.3.6 Object.defineProperty(O, P, Attributes)
$export($export.S + $export.F * !__webpack_require__("8e60"), 'Object', { defineProperty: __webpack_require__("d9f6").f });


/***/ }),

/***/ "47ee":
/***/ (function(module, exports, __webpack_require__) {

// all enumerable object keys, includes symbols
var getKeys = __webpack_require__("c3a1");
var gOPS = __webpack_require__("9aa9");
var pIE = __webpack_require__("355d");
module.exports = function (it) {
  var result = getKeys(it);
  var getSymbols = gOPS.f;
  if (getSymbols) {
    var symbols = getSymbols(it);
    var isEnum = pIE.f;
    var i = 0;
    var key;
    while (symbols.length > i) if (isEnum.call(it, key = symbols[i++])) result.push(key);
  } return result;
};


/***/ }),

/***/ "481b":
/***/ (function(module, exports) {

module.exports = {};


/***/ }),

/***/ "4910":
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("1cb7");
module.exports = __webpack_require__("584a").Reflect.get;


/***/ }),

/***/ "4aa6":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("dc62");

/***/ }),

/***/ "4bbc":
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("c207");
module.exports = __webpack_require__("ccb9").f('toStringTag');


/***/ }),

/***/ "4bf8":
/***/ (function(module, exports, __webpack_require__) {

// 7.1.13 ToObject(argument)
var defined = __webpack_require__("be13");
module.exports = function (it) {
  return Object(defined(it));
};


/***/ }),

/***/ "4c28":
/***/ (function(module, exports, __webpack_require__) {

// fallback for non-array-like ES3 and non-enumerable old V8 strings
var cof = __webpack_require__("dc45");
// eslint-disable-next-line no-prototype-builtins
module.exports = Object('z').propertyIsEnumerable(0) ? Object : function (it) {
  return cof(it) == 'String' ? it.split('') : Object(it);
};


/***/ }),

/***/ "4c95":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var global = __webpack_require__("e53d");
var core = __webpack_require__("584a");
var dP = __webpack_require__("d9f6");
var DESCRIPTORS = __webpack_require__("8e60");
var SPECIES = __webpack_require__("5168")('species');

module.exports = function (KEY) {
  var C = typeof core[KEY] == 'function' ? core[KEY] : global[KEY];
  if (DESCRIPTORS && C && !C[SPECIES]) dP.f(C, SPECIES, {
    configurable: true,
    get: function () { return this; }
  });
};


/***/ }),

/***/ "4d16":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("25b0");

/***/ }),

/***/ "4dff":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("eada");

/***/ }),

/***/ "4ee1":
/***/ (function(module, exports, __webpack_require__) {

var ITERATOR = __webpack_require__("5168")('iterator');
var SAFE_CLOSING = false;

try {
  var riter = [7][ITERATOR]();
  riter['return'] = function () { SAFE_CLOSING = true; };
  // eslint-disable-next-line no-throw-literal
  Array.from(riter, function () { throw 2; });
} catch (e) { /* empty */ }

module.exports = function (exec, skipClosing) {
  if (!skipClosing && !SAFE_CLOSING) return false;
  var safe = false;
  try {
    var arr = [7];
    var iter = arr[ITERATOR]();
    iter.next = function () { return { done: safe = true }; };
    arr[ITERATOR] = function () { return iter; };
    exec(arr);
  } catch (e) { /* empty */ }
  return safe;
};


/***/ }),

/***/ "50ed":
/***/ (function(module, exports) {

module.exports = function (done, value) {
  return { value: value, done: !!done };
};


/***/ }),

/***/ "5168":
/***/ (function(module, exports, __webpack_require__) {

var store = __webpack_require__("dbdb")('wks');
var uid = __webpack_require__("62a0");
var Symbol = __webpack_require__("e53d").Symbol;
var USE_SYMBOL = typeof Symbol == 'function';

var $exports = module.exports = function (name) {
  return store[name] || (store[name] =
    USE_SYMBOL && Symbol[name] || (USE_SYMBOL ? Symbol : uid)('Symbol.' + name));
};

$exports.store = store;


/***/ }),

/***/ "5176":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("51b6");

/***/ }),

/***/ "51b6":
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("a3c3");
module.exports = __webpack_require__("584a").Object.assign;


/***/ }),

/***/ "531a":
/***/ (function(module, exports, __webpack_require__) {

// to indexed object, toObject with fallback for non-array-like ES3 strings
var IObject = __webpack_require__("4c28");
var defined = __webpack_require__("0f31");
module.exports = function (it) {
  return IObject(defined(it));
};


/***/ }),

/***/ "53e2":
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.9 / 15.2.3.2 Object.getPrototypeOf(O)
var has = __webpack_require__("07e3");
var toObject = __webpack_require__("241e");
var IE_PROTO = __webpack_require__("5559")('IE_PROTO');
var ObjectProto = Object.prototype;

module.exports = Object.getPrototypeOf || function (O) {
  O = toObject(O);
  if (has(O, IE_PROTO)) return O[IE_PROTO];
  if (typeof O.constructor == 'function' && O instanceof O.constructor) {
    return O.constructor.prototype;
  } return O instanceof Object ? ObjectProto : null;
};


/***/ }),

/***/ "5464":
/***/ (function(module, exports, __webpack_require__) {

// false -> Array#indexOf
// true  -> Array#includes
var toIObject = __webpack_require__("531a");
var toLength = __webpack_require__("b4df");
var toAbsoluteIndex = __webpack_require__("172c");
module.exports = function (IS_INCLUDES) {
  return function ($this, el, fromIndex) {
    var O = toIObject($this);
    var length = toLength(O.length);
    var index = toAbsoluteIndex(fromIndex, length);
    var value;
    // Array#includes uses SameValueZero equality algorithm
    // eslint-disable-next-line no-self-compare
    if (IS_INCLUDES && el != el) while (length > index) {
      value = O[index++];
      // eslint-disable-next-line no-self-compare
      if (value != value) return true;
    // Array#indexOf ignores holes, Array#includes - not
    } else for (;length > index; index++) if (IS_INCLUDES || index in O) {
      if (O[index] === el) return IS_INCLUDES || index || 0;
    } return !IS_INCLUDES && -1;
  };
};


/***/ }),

/***/ "549b":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var ctx = __webpack_require__("d864");
var $export = __webpack_require__("63b6");
var toObject = __webpack_require__("241e");
var call = __webpack_require__("b0dc");
var isArrayIter = __webpack_require__("3702");
var toLength = __webpack_require__("b447");
var createProperty = __webpack_require__("20fd");
var getIterFn = __webpack_require__("7cd6");

$export($export.S + $export.F * !__webpack_require__("4ee1")(function (iter) { Array.from(iter); }), 'Array', {
  // 22.1.2.1 Array.from(arrayLike, mapfn = undefined, thisArg = undefined)
  from: function from(arrayLike /* , mapfn = undefined, thisArg = undefined */) {
    var O = toObject(arrayLike);
    var C = typeof this == 'function' ? this : Array;
    var aLen = arguments.length;
    var mapfn = aLen > 1 ? arguments[1] : undefined;
    var mapping = mapfn !== undefined;
    var index = 0;
    var iterFn = getIterFn(O);
    var length, result, step, iterator;
    if (mapping) mapfn = ctx(mapfn, aLen > 2 ? arguments[2] : undefined, 2);
    // if object isn't iterable or it's array with default iterator - use simple case
    if (iterFn != undefined && !(C == Array && isArrayIter(iterFn))) {
      for (iterator = iterFn.call(O), result = new C(); !(step = iterator.next()).done; index++) {
        createProperty(result, index, mapping ? call(iterator, mapfn, [step.value, index], true) : step.value);
      }
    } else {
      length = toLength(O.length);
      for (result = new C(length); length > index; index++) {
        createProperty(result, index, mapping ? mapfn(O[index], index) : O[index]);
      }
    }
    result.length = index;
    return result;
  }
});


/***/ }),

/***/ "54a1":
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("6c1c");
__webpack_require__("1654");
module.exports = __webpack_require__("95d5");


/***/ }),

/***/ "5559":
/***/ (function(module, exports, __webpack_require__) {

var shared = __webpack_require__("dbdb")('keys');
var uid = __webpack_require__("62a0");
module.exports = function (key) {
  return shared[key] || (shared[key] = uid(key));
};


/***/ }),

/***/ "55dd":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var $export = __webpack_require__("5ca1");
var aFunction = __webpack_require__("d8e8");
var toObject = __webpack_require__("4bf8");
var fails = __webpack_require__("79e5");
var $sort = [].sort;
var test = [1, 2, 3];

$export($export.P + $export.F * (fails(function () {
  // IE8-
  test.sort(undefined);
}) || !fails(function () {
  // V8 bug
  test.sort(null);
  // Old WebKit
}) || !__webpack_require__("2f21")($sort)), 'Array', {
  // 22.1.3.25 Array.prototype.sort(comparefn)
  sort: function sort(comparefn) {
    return comparefn === undefined
      ? $sort.call(toObject(this))
      : $sort.call(toObject(this), aFunction(comparefn));
  }
});


/***/ }),

/***/ "584a":
/***/ (function(module, exports) {

var core = module.exports = { version: '2.6.2' };
if (typeof __e == 'number') __e = core; // eslint-disable-line no-undef


/***/ }),

/***/ "58ce":
/***/ (function(module, exports) {

// 7.1.4 ToInteger
var ceil = Math.ceil;
var floor = Math.floor;
module.exports = function (it) {
  return isNaN(it = +it) ? 0 : (it > 0 ? floor : ceil)(it);
};


/***/ }),

/***/ "5987":
/***/ (function(module, exports, __webpack_require__) {

/**
 * enigma.js v2.4.0
 * Copyright (c) 2018 QlikTech International AB
 * This library is licensed under MIT - See the LICENSE file for full details
 */

(function (global, factory) {
   true ? module.exports = factory() :
  undefined;
}(this, (function () { 'use strict';

  function _typeof(obj) {
    if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
      _typeof = function (obj) {
        return typeof obj;
      };
    } else {
      _typeof = function (obj) {
        return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
      };
    }

    return _typeof(obj);
  }

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  function _createClass(Constructor, protoProps, staticProps) {
    if (protoProps) _defineProperties(Constructor.prototype, protoProps);
    if (staticProps) _defineProperties(Constructor, staticProps);
    return Constructor;
  }

  function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function");
    }

    subClass.prototype = Object.create(superClass && superClass.prototype, {
      constructor: {
        value: subClass,
        writable: true,
        configurable: true
      }
    });
    if (superClass) _setPrototypeOf(subClass, superClass);
  }

  function _getPrototypeOf(o) {
    _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
      return o.__proto__ || Object.getPrototypeOf(o);
    };
    return _getPrototypeOf(o);
  }

  function _setPrototypeOf(o, p) {
    _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
      o.__proto__ = p;
      return o;
    };

    return _setPrototypeOf(o, p);
  }

  function _assertThisInitialized(self) {
    if (self === void 0) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }

    return self;
  }

  function _possibleConstructorReturn(self, call) {
    if (call && (typeof call === "object" || typeof call === "function")) {
      return call;
    }

    return _assertThisInitialized(self);
  }

  function _superPropBase(object, property) {
    while (!Object.prototype.hasOwnProperty.call(object, property)) {
      object = _getPrototypeOf(object);
      if (object === null) break;
    }

    return object;
  }

  function _get(target, property, receiver) {
    if (typeof Reflect !== "undefined" && Reflect.get) {
      _get = Reflect.get;
    } else {
      _get = function _get(target, property, receiver) {
        var base = _superPropBase(target, property);

        if (!base) return;
        var desc = Object.getOwnPropertyDescriptor(base, property);

        if (desc.get) {
          return desc.get.call(receiver);
        }

        return desc.value;
      };
    }

    return _get(target, property, receiver || target);
  }

  function _toConsumableArray(arr) {
    return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread();
  }

  function _arrayWithoutHoles(arr) {
    if (Array.isArray(arr)) {
      for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) arr2[i] = arr[i];

      return arr2;
    }
  }

  function _iterableToArray(iter) {
    if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter);
  }

  function _nonIterableSpread() {
    throw new TypeError("Invalid attempt to spread non-iterable instance");
  }

  /**
   * Utility functions
   */

  var util = {};

  util.isObject = function isObject(arg) {
    return typeof arg === 'object' && arg !== null;
  };

  util.isNumber = function isNumber(arg) {
    return typeof arg === 'number';
  };

  util.isUndefined = function isUndefined(arg) {
    return arg === void 0;
  };

  util.isFunction = function isFunction(arg){
    return typeof arg === 'function';
  };


  /**
   * EventEmitter class
   */

  function EventEmitter() {
    EventEmitter.init.call(this);
  }
  var nodeEventEmitter = EventEmitter;

  // Backwards-compat with node 0.10.x
  EventEmitter.EventEmitter = EventEmitter;

  EventEmitter.prototype._events = undefined;
  EventEmitter.prototype._maxListeners = undefined;

  // By default EventEmitters will print a warning if more than 10 listeners are
  // added to it. This is a useful default which helps finding memory leaks.
  EventEmitter.defaultMaxListeners = 10;

  EventEmitter.init = function() {
    this._events = this._events || {};
    this._maxListeners = this._maxListeners || undefined;
  };

  // Obviously not all Emitters should be limited to 10. This function allows
  // that to be increased. Set to zero for unlimited.
  EventEmitter.prototype.setMaxListeners = function(n) {
    if (!util.isNumber(n) || n < 0 || isNaN(n))
      throw TypeError('n must be a positive number');
    this._maxListeners = n;
    return this;
  };

  EventEmitter.prototype.emit = function(type) {
    var er, handler, len, args, i, listeners;

    if (!this._events)
      this._events = {};

    // If there is no 'error' event listener then throw.
    if (type === 'error' && !this._events.error) {
      er = arguments[1];
      if (er instanceof Error) {
        throw er; // Unhandled 'error' event
      } else {
        throw Error('Uncaught, unspecified "error" event.');
      }
      return false;
    }

    handler = this._events[type];

    if (util.isUndefined(handler))
      return false;

    if (util.isFunction(handler)) {
      switch (arguments.length) {
        // fast cases
        case 1:
          handler.call(this);
          break;
        case 2:
          handler.call(this, arguments[1]);
          break;
        case 3:
          handler.call(this, arguments[1], arguments[2]);
          break;
        // slower
        default:
          len = arguments.length;
          args = new Array(len - 1);
          for (i = 1; i < len; i++)
            args[i - 1] = arguments[i];
          handler.apply(this, args);
      }
    } else if (util.isObject(handler)) {
      len = arguments.length;
      args = new Array(len - 1);
      for (i = 1; i < len; i++)
        args[i - 1] = arguments[i];

      listeners = handler.slice();
      len = listeners.length;
      for (i = 0; i < len; i++)
        listeners[i].apply(this, args);
    }

    return true;
  };

  EventEmitter.prototype.addListener = function(type, listener) {
    var m;

    if (!util.isFunction(listener))
      throw TypeError('listener must be a function');

    if (!this._events)
      this._events = {};

    // To avoid recursion in the case that type === "newListener"! Before
    // adding it to the listeners, first emit "newListener".
    if (this._events.newListener)
      this.emit('newListener', type,
                util.isFunction(listener.listener) ?
                listener.listener : listener);

    if (!this._events[type])
      // Optimize the case of one listener. Don't need the extra array object.
      this._events[type] = listener;
    else if (util.isObject(this._events[type]))
      // If we've already got an array, just append.
      this._events[type].push(listener);
    else
      // Adding the second element, need to change to array.
      this._events[type] = [this._events[type], listener];

    // Check for listener leak
    if (util.isObject(this._events[type]) && !this._events[type].warned) {
      var m;
      if (!util.isUndefined(this._maxListeners)) {
        m = this._maxListeners;
      } else {
        m = EventEmitter.defaultMaxListeners;
      }

      if (m && m > 0 && this._events[type].length > m) {
        this._events[type].warned = true;

        if (util.isFunction(console.error)) {
          console.error('(node) warning: possible EventEmitter memory ' +
                        'leak detected. %d listeners added. ' +
                        'Use emitter.setMaxListeners() to increase limit.',
                        this._events[type].length);
        }
        if (util.isFunction(console.trace))
          console.trace();
      }
    }

    return this;
  };

  EventEmitter.prototype.on = EventEmitter.prototype.addListener;

  EventEmitter.prototype.once = function(type, listener) {
    if (!util.isFunction(listener))
      throw TypeError('listener must be a function');

    var fired = false;

    function g() {
      this.removeListener(type, g);

      if (!fired) {
        fired = true;
        listener.apply(this, arguments);
      }
    }

    g.listener = listener;
    this.on(type, g);

    return this;
  };

  // emits a 'removeListener' event iff the listener was removed
  EventEmitter.prototype.removeListener = function(type, listener) {
    var list, position, length, i;

    if (!util.isFunction(listener))
      throw TypeError('listener must be a function');

    if (!this._events || !this._events[type])
      return this;

    list = this._events[type];
    length = list.length;
    position = -1;

    if (list === listener ||
        (util.isFunction(list.listener) && list.listener === listener)) {
      delete this._events[type];
      if (this._events.removeListener)
        this.emit('removeListener', type, listener);

    } else if (util.isObject(list)) {
      for (i = length; i-- > 0;) {
        if (list[i] === listener ||
            (list[i].listener && list[i].listener === listener)) {
          position = i;
          break;
        }
      }

      if (position < 0)
        return this;

      if (list.length === 1) {
        list.length = 0;
        delete this._events[type];
      } else {
        list.splice(position, 1);
      }

      if (this._events.removeListener)
        this.emit('removeListener', type, listener);
    }

    return this;
  };

  EventEmitter.prototype.removeAllListeners = function(type) {
    var key, listeners;

    if (!this._events)
      return this;

    // not listening for removeListener, no need to emit
    if (!this._events.removeListener) {
      if (arguments.length === 0)
        this._events = {};
      else if (this._events[type])
        delete this._events[type];
      return this;
    }

    // emit removeListener for all listeners on all events
    if (arguments.length === 0) {
      for (key in this._events) {
        if (key === 'removeListener') continue;
        this.removeAllListeners(key);
      }
      this.removeAllListeners('removeListener');
      this._events = {};
      return this;
    }

    listeners = this._events[type];

    if (util.isFunction(listeners)) {
      this.removeListener(type, listeners);
    } else if (Array.isArray(listeners)) {
      // LIFO order
      while (listeners.length)
        this.removeListener(type, listeners[listeners.length - 1]);
    }
    delete this._events[type];

    return this;
  };

  EventEmitter.prototype.listeners = function(type) {
    var ret;
    if (!this._events || !this._events[type])
      ret = [];
    else if (util.isFunction(this._events[type]))
      ret = [this._events[type]];
    else
      ret = this._events[type].slice();
    return ret;
  };

  EventEmitter.listenerCount = function(emitter, type) {
    var ret;
    if (!emitter._events || !emitter._events[type])
      ret = 0;
    else if (util.isFunction(emitter._events[type]))
      ret = 1;
    else
      ret = emitter._events[type].length;
    return ret;
  };

  /**
  * @module EventEmitter
  * @private
  */

  var Events = {
    /**
    * Function used to add event handling to objects passed in.
    * @param {Object} obj Object instance that will get event handling.
    */
    mixin: function mixin(obj) {
      Object.keys(nodeEventEmitter.prototype).forEach(function (key) {
        obj[key] = nodeEventEmitter.prototype[key];
      });
      nodeEventEmitter.init(obj);
    }
  };

  var RPC_CLOSE_NORMAL = 1000;
  var RPC_CLOSE_MANUAL_SUSPEND = 4000;
  var cacheId = 0;
  /**
   * The QIX Engine session object
   */

  var Session =
  /*#__PURE__*/
  function () {
    /**
     * Handle opened state. This event is triggered whenever the websocket is connected and ready for
     * communication.
     * @event Session#opened
     * @type {Object}
     */

    /**
     * Handle closed state. This event is triggered when the underlying websocket is closed and
     * config.suspendOnClose is false.
     * @event Session#closed
     * @type {Object}
     */

    /**
     * Handle suspended state. This event is triggered in two cases (listed below). It is useful
     * in scenarios where you for example want to block interaction in your application until you
     * are resumed again. If config.suspendOnClose is true and there was a network disconnect
     * (socked closed) or if you ran session.suspend().
     * @event Session#suspended
     * @type {Object}
     * @param {Object} evt Event object.
     * @param {String} evt.initiator String indication what triggered the suspended state. Possible
     * values network, manual.
     */

    /**
     * Handle resumed state. This event is triggered when the session was properly resumed. It is
     * useful in scenarios where you for example can close blocking modal dialogs and allow the user
     * to interact with your application again.
     * @event Session#resumed
     * @type {Object}
     */

    /**
     * Handle all JSON-RPC notification event, 'notification:*. Or handle a specific JSON-RPC
     * notification event, 'notification:OnConnected'. These events depend on the product you use QIX
     * Engine from.
     * @event Session#notification
     * @type {Object}
     */

    /**
    * Handle websocket messages. Generally used in debugging purposes. traffic:* will handle all
    * websocket messages, traffic:sent will handle outgoing messages and traffic:received will handle
    * incoming messages.
    * @event Session#traffic
    * @type {Object}
    */
    function Session(options) {
      _classCallCheck(this, Session);

      var session = this;
      Object.assign(session, options);
      this.Promise = this.config.Promise;
      this.definition = this.config.definition;
      Events.mixin(session);
      cacheId += 1;
      session.id = cacheId;
      session.rpc.on('socket-error', session.onRpcError.bind(session));
      session.rpc.on('closed', session.onRpcClosed.bind(session));
      session.rpc.on('message', session.onRpcMessage.bind(session));
      session.rpc.on('notification', session.onRpcNotification.bind(session));
      session.rpc.on('traffic', session.onRpcTraffic.bind(session));
      session.on('closed', function () {
        return session.onSessionClosed();
      });
    }
    /**
    * Event handler for re-triggering error events from RPC.
    * @private
    * @emits socket-error
    * @param {Error} err Webocket error event.
    */


    _createClass(Session, [{
      key: "onRpcError",
      value: function onRpcError(err) {
        if (this.suspendResume.isSuspended) {
          return;
        }

        this.emit('socket-error', err);
      }
      /**
      * Event handler for the RPC close event.
      * @private
      * @emits Session#suspended
      * @emits Session#closed
      * @param {Event} evt WebSocket close event.
      */

    }, {
      key: "onRpcClosed",
      value: function onRpcClosed(evt) {
        var _this = this;

        if (this.suspendResume.isSuspended) {
          return;
        }

        if (evt.code === RPC_CLOSE_NORMAL || evt.code === RPC_CLOSE_MANUAL_SUSPEND) {
          return;
        }

        if (this.config.suspendOnClose) {
          this.suspendResume.suspend().then(function () {
            return _this.emit('suspended', {
              initiator: 'network'
            });
          });
        } else {
          this.emit('closed', evt);
        }
      }
      /**
      * Event handler for the RPC message event.
      * @private
      * @param {Object} response JSONRPC response.
      */

    }, {
      key: "onRpcMessage",
      value: function onRpcMessage(response) {
        var _this2 = this;

        if (this.suspendResume.isSuspended) {
          return;
        }

        if (response.change) {
          response.change.forEach(function (handle) {
            return _this2.emitHandleChanged(handle);
          });
        }

        if (response.close) {
          response.close.forEach(function (handle) {
            return _this2.emitHandleClosed(handle);
          });
        }
      }
      /**
      * Event handler for the RPC notification event.
      * @private
      * @emits Session#notification
      * @param {Object} response The JSONRPC notification.
      */

    }, {
      key: "onRpcNotification",
      value: function onRpcNotification(response) {
        this.emit('notification:*', response.method, response.params);
        this.emit("notification:".concat(response.method), response.params);
      }
      /**
      * Event handler for the RPC traffic event.
      * @private
      * @emits Session#traffic
      * @param {String} dir The traffic direction, sent or received.
      * @param {Object} data JSONRPC request/response/WebSocket message.
      * @param {Number} handle The associated handle.
      */

    }, {
      key: "onRpcTraffic",
      value: function onRpcTraffic(dir, data, handle) {
        this.emit('traffic:*', dir, data);
        this.emit("traffic:".concat(dir), data);
        var api = this.apis.getApi(handle);

        if (api) {
          api.emit('traffic:*', dir, data);
          api.emit("traffic:".concat(dir), data);
        }
      }
      /**
      * Event handler for cleaning up API instances when a session has been closed.
      * @private
      * @emits API#closed
      */

    }, {
      key: "onSessionClosed",
      value: function onSessionClosed() {
        this.apis.getApis().forEach(function (entry) {
          entry.api.emit('closed');
          entry.api.removeAllListeners();
        });
        this.apis.clear();
      }
      /**
       * Function used to get an API for a backend object.
       * @private
       * @param {Object} args Arguments used to create object API.
       * @param {Number} args.handle Handle of the backend object.
       * @param {String} args.id ID of the backend object.
       * @param {String} args.type QIX type of the backend object. Can for example
       *                           be "Doc" or "GenericVariable".
       * @param {String} args.genericType Custom type of the backend object, if defined in qInfo.
       * @returns {*} Returns the generated and possibly augmented API.
       */

    }, {
      key: "getObjectApi",
      value: function getObjectApi(args) {
        var handle = args.handle,
            id = args.id,
            type = args.type,
            genericType = args.genericType;
        var api = this.apis.getApi(handle);

        if (api) {
          return api;
        }

        var factory = this.definition.generate(type);
        api = factory(this, handle, id, genericType);
        this.apis.add(handle, api);
        return api;
      }
      /**
      * Establishes the websocket against the configured URL and returns the Global instance.
      * @emits Session#opened
      * @returns {Promise<Object>} Eventually resolved if the connection was successful.
      */

    }, {
      key: "open",
      value: function open() {
        var _this3 = this;

        if (!this.globalPromise) {
          var args = {
            handle: -1,
            id: 'Global',
            type: 'Global',
            genericType: 'Global'
          };
          this.globalPromise = this.rpc.open().then(function () {
            return _this3.getObjectApi(args);
          }).then(function (global) {
            _this3.emit('opened');

            return global;
          });
        }

        return this.globalPromise;
      }
      /**
      * Function used to send data on the RPC socket.
      * @param {Object} request The request to be sent. (data and some meta info)
      * @returns {Object} Returns a promise instance.
      */

    }, {
      key: "send",
      value: function send(request) {
        var _this4 = this;

        if (this.suspendResume.isSuspended) {
          return this.Promise.reject(new Error('Session suspended'));
        }

        request.id = this.rpc.createRequestId();
        var promise = this.intercept.executeRequests(this, this.Promise.resolve(request)).then(function (augmentedRequest) {
          var data = Object.assign({}, _this4.config.protocol, augmentedRequest); // the outKey value is used by multiple-out interceptor, at some point
          // we need to refactor that implementation and figure out how to transport
          // this value without hijacking the JSONRPC request object:

          delete data.outKey;

          var response = _this4.rpc.send(data);

          augmentedRequest.retry = function () {
            return _this4.send(request);
          };

          return _this4.intercept.executeResponses(_this4, response, augmentedRequest);
        });
        Session.addToPromiseChain(promise, 'requestId', request.id);
        return promise;
      }
      /**
      * Suspends the enigma.js session by closing the websocket and rejecting all method calls
      * until is has been resumed again.
      * @emits Session#suspended
      * @returns {Promise<Object>} Eventually resolved when the websocket has been closed.
      */

    }, {
      key: "suspend",
      value: function suspend() {
        var _this5 = this;

        return this.suspendResume.suspend().then(function () {
          return _this5.emit('suspended', {
            initiator: 'manual'
          });
        });
      }
      /**
      * Resumes a previously suspended enigma.js session by re-creating the websocket and,
      * if possible, re-open the document as well as refreshing the internal cashes. If successful,
      * changed events will be triggered on all generated APIs, and on the ones it was unable to
      * restore, the closed event will be triggered.
      * @emits Session#resumed
      * @param {Boolean} onlyIfAttached If true, resume only if the session was re-attached properly.
      * @returns {Promise<Object>} Eventually resolved when the websocket (and potentially the
      * previously opened document, and generated APIs) has been restored, rejected when it fails any
      * of those steps, or when onlyIfAttached is true and a new session was created.
      */

    }, {
      key: "resume",
      value: function resume(onlyIfAttached) {
        var _this6 = this;

        return this.suspendResume.resume(onlyIfAttached).then(function (value) {
          _this6.emit('resumed');

          return value;
        });
      }
      /**
      * Closes the websocket and cleans up internal caches, also triggers the closed event
      * on all generated APIs. Note that you have to manually invoke this when you want to
      * close a session and config.suspendOnClose is true.
      * @emits Session#closed
      * @returns {Promise<Object>} Eventually resolved when the websocket has been closed.
      */

    }, {
      key: "close",
      value: function close() {
        var _this7 = this;

        this.globalPromise = undefined;
        return this.rpc.close().then(function (evt) {
          return _this7.emit('closed', evt);
        });
      }
      /**
      * Given a handle, this function will emit the 'changed' event on the
      * corresponding API instance.
      * @private
      * @param {Number} handle The handle of the API instance.
      * @emits API#changed
      */

    }, {
      key: "emitHandleChanged",
      value: function emitHandleChanged(handle) {
        var api = this.apis.getApi(handle);

        if (api) {
          api.emit('changed');
        }
      }
      /**
      * Given a handle, this function will emit the 'closed' event on the
      * corresponding API instance.
      * @private
      * @param {Number} handle The handle of the API instance.
      * @emits API#closed
      */

    }, {
      key: "emitHandleClosed",
      value: function emitHandleClosed(handle) {
        var api = this.apis.getApi(handle);

        if (api) {
          api.emit('closed');
          api.removeAllListeners();
        }
      }
      /**
      * Function used to add info on the promise chain.
      * @private
      * @param {Promise<Object>} promise The promise to add info on.
      * @param {String} name The property to add info on.
      * @param {Any} value The info to add.
      */

    }], [{
      key: "addToPromiseChain",
      value: function addToPromiseChain(promise, name, value) {
        promise[name] = value;
        var then = promise.then;

        promise.then = function patchedThen() {
          for (var _len = arguments.length, params = new Array(_len), _key = 0; _key < _len; _key++) {
            params[_key] = arguments[_key];
          }

          var chain = then.apply(this, params);
          Session.addToPromiseChain(chain, name, value);
          return chain;
        };
      }
    }]);

    return Session;
  }();

  /**
  * Key-value cache
  * @private
  */
  var KeyValueCache =
  /*#__PURE__*/
  function () {
    function KeyValueCache() {
      _classCallCheck(this, KeyValueCache);

      this.entries = {};
    }
    /**
    * Adds an entry.
    * @private
    * @function KeyValueCache#add
    * @param {String} key The key representing an entry.
    * @param {*} entry The entry to be added.
    */


    _createClass(KeyValueCache, [{
      key: "add",
      value: function add(key, entry) {
        key += '';

        if (typeof this.entries[key] !== 'undefined') {
          throw new Error("Entry already defined with key ".concat(key));
        }

        this.entries[key] = entry;
      }
      /**
      * Sets an entry.
      * @private
      * @function KeyValueCache#set
      * @param {String} key The key representing an entry.
      * @param {*} entry The entry.
      */

    }, {
      key: "set",
      value: function set(key, entry) {
        key += '';
        this.entries[key] = entry;
      }
      /**
      * Removes an entry.
      * @private
      * @function KeyValueCache#remove
      * @param {String} key The key representing an entry.
      */

    }, {
      key: "remove",
      value: function remove(key) {
        delete this.entries[key];
      }
      /**
      * Gets an entry.
      * @private
      * @function KeyValueCache#get
      * @param {String} key The key representing an entry.
      * @returns {*} The entry for the key.
      */

    }, {
      key: "get",
      value: function get(key) {
        return this.entries[key];
      }
      /**
      * Gets a list of all entries.
      * @private
      * @function KeyValueCache#getAll
      * @returns {Array} The list of entries including its `key` and `value` properties.
      */

    }, {
      key: "getAll",
      value: function getAll() {
        var _this = this;

        return Object.keys(this.entries).map(function (key) {
          return {
            key: key,
            value: _this.entries[key]
          };
        });
      }
      /**
      * Gets a key for an entry.
      * @private
      * @function KeyValueCache#getKey
      * @param {*} entry The entry to locate the key for.
      * @returns {String} The key representing an entry.
      */

    }, {
      key: "getKey",
      value: function getKey(entry) {
        var _this2 = this;

        return Object.keys(this.entries).filter(function (key) {
          return _this2.entries[key] === entry;
        })[0];
      }
      /**
      * Clears the cache of all entries.
      * @private
      * @function KeyValueCache#clear
      */

    }, {
      key: "clear",
      value: function clear() {
        this.entries = {};
      }
    }]);

    return KeyValueCache;
  }();

  var hasOwnProperty$1 = Object.prototype.hasOwnProperty;
  /**
  * Returns the camelCase counterpart of a symbol.
  * @private
  * @param {String} symbol The symbol.
  * @return the camelCase counterpart.
  */

  function toCamelCase(symbol) {
    return symbol.substring(0, 1).toLowerCase() + symbol.substring(1);
  }
  /**
   * A facade function that allows parameters to be passed either by name
   * (through an object), or by position (through an array).
   * @private
   * @param {Function} base The function that is being overriden. Will be
   *                        called with parameters in array-form.
   * @param {Object} defaults Parameter list and it's default values.
   * @param {*} params The parameters.
   */


  function namedParamFacade(base, defaults) {
    for (var _len = arguments.length, params = new Array(_len > 2 ? _len - 2 : 0), _key = 2; _key < _len; _key++) {
      params[_key - 2] = arguments[_key];
    }

    if (params.length === 1 && _typeof(params[0]) === 'object' && !Array.isArray(params[0])) {
      var valid = Object.keys(params[0]).every(function (key) {
        return hasOwnProperty$1.call(defaults, key);
      });

      if (valid) {
        params = Object.keys(defaults).map(function (key) {
          return params[0][key] || defaults[key];
        });
      }
    }

    return base.apply(this, params);
  }
  /**
  * Qix schema definition.
  * @private
  */


  var Schema =
  /*#__PURE__*/
  function () {
    /**
    * Create a new schema instance.
    * @private
    * @param {Configuration} config The configuration for QIX.
    */
    function Schema(config) {
      _classCallCheck(this, Schema);

      this.config = config;
      this.Promise = config.Promise;
      this.schema = config.schema;
      this.mixins = new KeyValueCache();
      this.types = new KeyValueCache();
    }

    _createClass(Schema, [{
      key: "registerMixin",
      value: function registerMixin(_ref) {
        var _this = this;

        var types = _ref.types,
            type = _ref.type,
            extend = _ref.extend,
            override = _ref.override,
            init = _ref.init;

        if (!Array.isArray(types)) {
          types = [types];
        } // to support a single type


        if (type) {
          types.push(type);
        }

        var cached = {
          extend: extend,
          override: override,
          init: init
        };
        types.forEach(function (typeKey) {
          var entryList = _this.mixins.get(typeKey);

          if (entryList) {
            entryList.push(cached);
          } else {
            _this.mixins.add(typeKey, [cached]);
          }
        });
      }
      /**
      * Function used to generate a type definition.
      * @private
      * @param {String} type The type.
      * @returns {{create: Function, def: Object}} Returns an object with a definition
      *          of the type and a create factory.
      */

    }, {
      key: "generate",
      value: function generate(type) {
        var entry = this.types.get(type);

        if (entry) {
          return entry;
        }

        if (!this.schema.structs[type]) {
          throw new Error("".concat(type, " not found"));
        }

        var factory = this.generateApi(type, this.schema.structs[type]);
        this.types.add(type, factory);
        return factory;
      }
      /**
      * Function used to generate an API definition for a given type.
      * @private
      * @param {String} type The type to generate.
      * @param {Object} schema The schema describing the type.
      * @returns {{create: (function(session:Object, handle:Number, id:String,
      *          customKey:String)), def: Object}} Returns the API definition.
      */

    }, {
      key: "generateApi",
      value: function generateApi(type, schema) {
        var api = Object.create({});
        this.generateDefaultApi(api, schema); // Generate default

        this.mixinType(type, api); // Mixin default type

        this.mixinNamedParamFacade(api, schema); // Mixin named parameter support

        return function create(session, handle, id, customKey) {
          var _this2 = this;

          var instance = Object.create(api);
          Events.mixin(instance); // Always mixin event-emitter per instance

          Object.defineProperties(instance, {
            session: {
              enumerable: true,
              value: session
            },
            handle: {
              enumerable: true,
              value: handle,
              writable: true
            },
            id: {
              enumerable: true,
              value: id
            },
            type: {
              enumerable: true,
              value: type
            },
            genericType: {
              enumerable: true,
              value: customKey
            }
          });
          var mixinList = this.mixins.get(type) || [];

          if (customKey !== type) {
            this.mixinType(customKey, instance); // Mixin custom types

            mixinList = mixinList.concat(this.mixins.get(customKey) || []);
          }

          mixinList.forEach(function (mixin) {
            if (typeof mixin.init === 'function') {
              mixin.init({
                config: _this2.config,
                api: instance
              });
            }
          });
          return instance;
        }.bind(this);
      }
      /**
      * Function used to generate the methods with the right handlers to the object
      * API that is being generated.
      * @private
      * @param {Object} api The object API that is currently being generated.
      * @param {Object} schema The API definition.
      */

    }, {
      key: "generateDefaultApi",
      value: function generateDefaultApi(api, schema) {
        Object.keys(schema).forEach(function (method) {
          var out = schema[method].Out;
          var outKey = out.length === 1 ? out[0].Name : -1;
          var fnName = toCamelCase(method);

          api[fnName] = function generatedMethod() {
            for (var _len2 = arguments.length, params = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
              params[_key2] = arguments[_key2];
            }

            return this.session.send({
              handle: this.handle,
              method: method,
              params: params,
              outKey: outKey
            });
          };
        });
      }
      /**
      * Function used to add mixin methods to a specified API.
      * @private
      * @param {String} type Used to specify which mixin should be woven in.
      * @param {Object} api The object that will be woven.
      */

    }, {
      key: "mixinType",
      value: function mixinType(type, api) {
        var mixinList = this.mixins.get(type);

        if (mixinList) {
          mixinList.forEach(function (_ref2) {
            var _ref2$extend = _ref2.extend,
                extend = _ref2$extend === void 0 ? {} : _ref2$extend,
                _ref2$override = _ref2.override,
                override = _ref2$override === void 0 ? {} : _ref2$override;
            Object.keys(override).forEach(function (key) {
              if (typeof api[key] === 'function' && typeof override[key] === 'function') {
                var baseFn = api[key];

                api[key] = function wrappedFn() {
                  for (var _len3 = arguments.length, args = new Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
                    args[_key3] = arguments[_key3];
                  }

                  return override[key].apply(this, [baseFn.bind(this)].concat(args));
                };
              } else {
                throw new Error("No function to override. Type: ".concat(type, " function: ").concat(key));
              }
            });
            Object.keys(extend).forEach(function (key) {
              // handle overrides
              if (typeof api[key] === 'function' && typeof extend[key] === 'function') {
                throw new Error("Extend is not allowed for this mixin. Type: ".concat(type, " function: ").concat(key));
              } else {
                api[key] = extend[key];
              }
            });
          });
        }
      }
      /**
      * Function used to mixin the named parameter facade.
      * @private
      * @param {Object} api The object API that is currently being generated.
      * @param {Object} schema The API definition.
      */

    }, {
      key: "mixinNamedParamFacade",
      value: function mixinNamedParamFacade(api, schema) {
        Object.keys(schema).forEach(function (key) {
          var fnName = toCamelCase(key);
          var base = api[fnName];
          var defaults = schema[key].In.reduce(function (result, item) {
            result[item.Name] = item.DefaultValue;
            return result;
          }, {});

          api[fnName] = function namedParamWrapper() {
            for (var _len4 = arguments.length, params = new Array(_len4), _key4 = 0; _key4 < _len4; _key4++) {
              params[_key4] = arguments[_key4];
            }

            return namedParamFacade.apply(this, [base, defaults].concat(params));
          };
        });
      }
    }]);

    return Schema;
  }();

  /**
   * Helper class for handling RPC calls
   * @private
   */

  var RPCResolver =
  /*#__PURE__*/
  function () {
    function RPCResolver(id, handle, resolve, reject) {
      _classCallCheck(this, RPCResolver);

      Events.mixin(this);
      this.id = id;
      this.handle = handle;
      this.resolve = resolve;
      this.reject = reject;
    }

    _createClass(RPCResolver, [{
      key: "resolveWith",
      value: function resolveWith(data) {
        this.resolve(data);
        this.emit('resolved', this.id);
      }
    }, {
      key: "rejectWith",
      value: function rejectWith(err) {
        this.reject(err);
        this.emit('rejected', this.id);
      }
    }]);

    return RPCResolver;
  }();

  /**
  * This class handles remote procedure calls on a web socket.
  * @private
  */

  var RPC =
  /*#__PURE__*/
  function () {
    /**
    * Create a new RPC instance.
    * @private
    * @param {Object} options The configuration options for this class.
    * @param {Function} options.Promise The promise constructor to use.
    * @param {String} options.url The complete websocket URL used to connect.
    * @param {Function} options.createSocket The function callback to create a WebSocket.
    */
    function RPC(options) {
      _classCallCheck(this, RPC);

      Object.assign(this, options);
      Events.mixin(this);
      this.resolvers = {};
      this.requestId = 0;
      this.openedPromise = undefined;
    }
    /**
    * Opens a connection to the configured endpoint.
    * @private
    * @param {Boolean} force - ignores all previous and outstanding open calls if set to true.
    * @returns {Object} A promise instance.
    */


    _createClass(RPC, [{
      key: "open",
      value: function open() {
        var _this = this;

        var force = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

        if (!force && this.openedPromise) {
          return this.openedPromise;
        }

        try {
          this.socket = this.createSocket(this.url);
        } catch (err) {
          return this.Promise.reject(err);
        }

        this.socket.onopen = this.onOpen.bind(this);
        this.socket.onclose = this.onClose.bind(this);
        this.socket.onerror = this.onError.bind(this);
        this.socket.onmessage = this.onMessage.bind(this);
        this.openedPromise = new this.Promise(function (resolve, reject) {
          return _this.registerResolver('opened', null, resolve, reject);
        });
        this.closedPromise = new this.Promise(function (resolve, reject) {
          return _this.registerResolver('closed', null, resolve, reject);
        });
        return this.openedPromise;
      }
      /**
      * Resolves the open promise when a connection is successfully established.
      * @private
      */

    }, {
      key: "onOpen",
      value: function onOpen() {
        var _this2 = this;

        this.resolvers.opened.resolveWith(function () {
          return _this2.closedPromise;
        });
      }
      /**
      * Resolves the close promise when a connection is closed.
      * @private
      * @param {Object} event - The event describing close.
      */

    }, {
      key: "onClose",
      value: function onClose(event) {
        this.emit('closed', event);
        this.resolvers.closed.resolveWith(event);
        this.rejectAllOutstandingResolvers({
          code: -1,
          message: 'Socket closed'
        });
      }
      /**
      * Closes a connection.
      * @private
      * @param {Number} [code=1000] - The reason code for closing the connection.
      * @param {String} [reason=""] - The human readable string describing why the connection is closed.
      * @returns {Object} Returns a promise instance.
      */

    }, {
      key: "close",
      value: function close() {
        var code = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1000;
        var reason = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';

        if (this.socket) {
          this.socket.close(code, reason);
          this.socket = null;
        }

        return this.closedPromise;
      }
      /**
      * Emits an error event and rejects the open promise if an error is raised on the connection.
      * @private
      * @param {Object} event - The event describing the error.
      */

    }, {
      key: "onError",
      value: function onError(event) {
        if (this.resolvers.opened) {
          this.resolvers.opened.rejectWith(event);
        } else {
          // only emit errors after the initial open promise has been resolved,
          // this makes it possible to catch early websocket errors as well
          // as run-time ones:
          this.emit('socket-error', event);
        }

        this.rejectAllOutstandingResolvers({
          code: -1,
          message: 'Socket error'
        });
      }
      /**
      * Parses the onMessage event on the connection and resolve the promise for the request.
      * @private
      * @param {Object} event - The event describing the message.
      */

    }, {
      key: "onMessage",
      value: function onMessage(event) {
        var data = JSON.parse(event.data);
        var resolver = this.resolvers[data.id] || {};
        this.emit('traffic', 'received', data, resolver.handle);

        if (typeof data.id !== 'undefined') {
          this.emit('message', data);
          this.resolvers[data.id].resolveWith(data);
        } else {
          this.emit(data.params ? 'notification' : 'message', data);
        }
      }
      /**
      * Rejects all outstanding resolvers.
      * @private
      * @param {Object} reason - The reject reason.
      */

    }, {
      key: "rejectAllOutstandingResolvers",
      value: function rejectAllOutstandingResolvers(reason) {
        var _this3 = this;

        Object.keys(this.resolvers).forEach(function (id) {
          if (id === 'opened' || id === 'closed') {
            return; // "opened" and "closed" should not be handled here
          }

          var resolver = _this3.resolvers[id];
          resolver.rejectWith(reason);
        });
      }
      /**
      * Unregisters a resolver.
      * @private
      * @param {Number|String} id - The ID to unregister the resolver with.
      */

    }, {
      key: "unregisterResolver",
      value: function unregisterResolver(id) {
        var resolver = this.resolvers[id];
        resolver.removeAllListeners();
        delete this.resolvers[id];
      }
      /**
      * Registers a resolver.
      * @private
      * @param {Number|String} id - The ID to register the resolver with.
      * @param {Number} handle - The associated handle.
      * @returns {Function} The promise executor function.
      */

    }, {
      key: "registerResolver",
      value: function registerResolver(id, handle, resolve, reject) {
        var _this4 = this;

        var resolver = new RPCResolver(id, handle, resolve, reject);
        this.resolvers[id] = resolver;
        resolver.on('resolved', function (resolvedId) {
          return _this4.unregisterResolver(resolvedId);
        });
        resolver.on('rejected', function (rejectedId) {
          return _this4.unregisterResolver(rejectedId);
        });
      }
      /**
      * Sends data on the socket.
      * @private
      * @param {Object} data - The data to send.
      * @returns {Object} A promise instance.
      */

    }, {
      key: "send",
      value: function send(data) {
        var _this5 = this;

        if (!this.socket || this.socket.readyState !== this.socket.OPEN) {
          var error = new Error('Not connected');
          error.code = -1;
          return this.Promise.reject(error);
        }

        if (!data.id) {
          data.id = this.createRequestId();
        }

        data.jsonrpc = '2.0';
        return new this.Promise(function (resolve, reject) {
          _this5.socket.send(JSON.stringify(data));

          _this5.emit('traffic', 'sent', data, data.handle);

          return _this5.registerResolver(data.id, data.handle, resolve, reject);
        });
      }
    }, {
      key: "createRequestId",
      value: function createRequestId() {
        this.requestId += 1;
        return this.requestId;
      }
    }]);

    return RPC;
  }();

  var ON_ATTACHED_TIMEOUT_MS = 5000;
  var RPC_CLOSE_MANUAL_SUSPEND$1 = 4000;

  var SuspendResume =
  /*#__PURE__*/
  function () {
    /**
    * Creates a new SuspendResume instance.
    * @private
    * @param {Object} options The configuration option for this class.
    * @param {Promise<Object>} options.Promise The promise constructor to use.
    * @param {RPC} options.rpc The RPC instance to use when communicating towards Engine.
    * @param {ApiCache} options.apis The ApiCache instance to use.
    */
    function SuspendResume(options) {
      var _this = this;

      _classCallCheck(this, SuspendResume);

      Object.assign(this, options);
      this.isSuspended = false;
      this.rpc.on('traffic', function (dir, data) {
        if (dir === 'sent' && data.method === 'OpenDoc') {
          _this.openDocParams = data.params;
        }
      });
    }
    /**
    * Function used to restore the rpc connection.
    * @private
    * @param {Boolean} onlyIfAttached - if true, the returned promise will resolve
    *                                   only if the session can be re-attached.
    * @returns {Object} Returns a promise instance.
    */


    _createClass(SuspendResume, [{
      key: "restoreRpcConnection",
      value: function restoreRpcConnection(onlyIfAttached) {
        var _this2 = this;

        return this.reopen(ON_ATTACHED_TIMEOUT_MS).then(function (sessionState) {
          if (sessionState === 'SESSION_CREATED' && onlyIfAttached) {
            return _this2.Promise.reject(new Error('Not attached'));
          }

          return _this2.Promise.resolve();
        });
      }
      /**
      * Function used to restore the global API.
      * @private
      * @param {Object} changed - A list where the restored APIs will be added.
      * @returns {Object} Returns a promise instance.
      */

    }, {
      key: "restoreGlobal",
      value: function restoreGlobal(changed) {
        var global = this.apis.getApisByType('Global').pop();
        changed.push(global.api);
        return this.Promise.resolve();
      }
      /**
      * Function used to restore the doc API.
      * @private
      * @param {String} sessionState - The state of the session, attached or created.
      * @param {Array} closed - A list where the closed of APIs APIs will be added.
      * @param {Object} changed - A list where the restored APIs will be added.
      * @returns {Object} Returns a promise instance.
      */

    }, {
      key: "restoreDoc",
      value: function restoreDoc(closed, changed) {
        var _this3 = this;

        var doc = this.apis.getApisByType('Doc').pop();

        if (!doc) {
          return this.Promise.resolve();
        }

        return this.rpc.send({
          method: 'GetActiveDoc',
          handle: -1,
          params: []
        }).then(function (response) {
          if (response.error && _this3.openDocParams) {
            return _this3.rpc.send({
              method: 'OpenDoc',
              handle: -1,
              params: _this3.openDocParams
            });
          }

          return response;
        }).then(function (response) {
          if (response.error) {
            closed.push(doc.api);
            return _this3.Promise.resolve();
          }

          var handle = response.result.qReturn.qHandle;
          doc.api.handle = handle;
          changed.push(doc.api);
          return _this3.Promise.resolve(doc.api);
        });
      }
      /**
      * Function used to restore the APIs on the doc.
      * @private
      * @param {Object} doc - The doc API on which the APIs we want to restore exist.
      * @param {Array} closed - A list where the closed of APIs APIs will be added.
      * @param {Object} changed - A list where the restored APIs will be added.
      * @returns {Object} Returns a promise instance.
      */

    }, {
      key: "restoreDocObjects",
      value: function restoreDocObjects(doc, closed, changed) {
        var _this4 = this;

        var tasks = [];
        var apis = this.apis.getApis().map(function (entry) {
          return entry.api;
        }).filter(function (api) {
          return api.type !== 'Global' && api.type !== 'Doc';
        });

        if (!doc) {
          apis.forEach(function (api) {
            return closed.push(api);
          });
          return this.Promise.resolve();
        }

        apis.forEach(function (api) {
          var method = SuspendResume.buildGetMethodName(api.type);

          if (!method) {
            closed.push(api);
          } else {
            var request = _this4.rpc.send({
              method: method,
              handle: doc.handle,
              params: [api.id]
            }).then(function (response) {
              if (response.error || !response.result.qReturn.qHandle) {
                closed.push(api);
              } else {
                api.handle = response.result.qReturn.qHandle;
                changed.push(api);
              }
            });

            tasks.push(request);
          }
        });
        return this.Promise.all(tasks);
      }
      /**
      * Set the instance as suspended.
      * @private
      */

    }, {
      key: "suspend",
      value: function suspend() {
        this.isSuspended = true;
        return this.rpc.close(RPC_CLOSE_MANUAL_SUSPEND$1);
      }
      /**
      * Resumes a previously suspended RPC connection, and refreshes the API cache.
      *                                APIs unabled to be restored has their 'closed'
      *                                event triggered, otherwise 'changed'.
      * @private
      * @emits API#changed
      * @emits APIfunction@#closed
      * @param {Boolean} onlyIfAttached if true, resume only if the session was re-attached.
      * @returns {Promise<Object>} Eventually resolved if the RPC connection was successfully resumed,
      *                    otherwise rejected.
      */

    }, {
      key: "resume",
      value: function resume(onlyIfAttached) {
        var _this5 = this;

        var changed = [];
        var closed = [];
        return this.restoreRpcConnection(onlyIfAttached).then(function () {
          return _this5.restoreGlobal(changed);
        }).then(function () {
          return _this5.restoreDoc(closed, changed);
        }).then(function (doc) {
          return _this5.restoreDocObjects(doc, closed, changed);
        }).then(function () {
          _this5.isSuspended = false;

          _this5.apis.clear();

          closed.forEach(function (api) {
            api.emit('closed');
            api.removeAllListeners();
          });
          changed.forEach(function (api) {
            _this5.apis.add(api.handle, api);

            if (api.type !== 'Global') {
              api.emit('changed');
            }
          });
        }).catch(function (err) {
          return _this5.rpc.close().then(function () {
            return _this5.Promise.reject(err);
          });
        });
      }
      /**
      * Reopens the connection and waits for the OnConnected notification.
      * @private
      * @param {Number} timeout - The time to wait for the OnConnected notification.
      * @returns {Object} A promise containing the session state (SESSION_CREATED or SESSION_ATTACHED).
      */

    }, {
      key: "reopen",
      value: function reopen(timeout) {
        var _this6 = this;

        var timer;
        var notificationResolve;
        var notificationReceived = false;
        var notificationPromise = new this.Promise(function (resolve) {
          notificationResolve = resolve;
        });

        var waitForNotification = function waitForNotification() {
          if (!notificationReceived) {
            timer = setTimeout(function () {
              return notificationResolve('SESSION_CREATED');
            }, timeout);
          }

          return notificationPromise;
        };

        var onNotification = function onNotification(data) {
          if (data.method !== 'OnConnected') return;
          clearTimeout(timer);
          notificationResolve(data.params.qSessionState);
          notificationReceived = true;
        };

        this.rpc.on('notification', onNotification);
        return this.rpc.open(true).then(waitForNotification).then(function (state) {
          _this6.rpc.removeListener('notification', onNotification);

          return state;
        }).catch(function (err) {
          _this6.rpc.removeListener('notification', onNotification);

          return _this6.Promise.reject(err);
        });
      }
      /**
      * Function used to build the get method names for Doc APIs.
      * @private
      * @param {String} type - The API type.
      * @returns {String} Returns the get method name, or undefined if the type cannot be restored.
      */

    }], [{
      key: "buildGetMethodName",
      value: function buildGetMethodName(type) {
        if (type === 'Field' || type === 'Variable') {
          return null;
        }

        if (type === 'GenericVariable') {
          return 'GetVariableById';
        }

        return type.replace('Generic', 'Get');
      }
    }]);

    return SuspendResume;
  }();

  var SUCCESS_KEY = 'qSuccess';
  function deltaRequestInterceptor(session, request) {
    var delta = session.config.protocol.delta && request.outKey !== -1 && request.outKey !== SUCCESS_KEY;

    if (delta) {
      request.delta = delta;
    }

    return request;
  }

  /**
  * Response interceptor for generating APIs. Handles the quirks of engine not
  * returning an error when an object is missing.
  * @private
  * @param {Session} session - The session the intercept is being executed on.
  * @param {Object} request - The JSON-RPC request.
  * @param {Object} response - The response.
  * @returns {Object} - Returns the generated API
  */
  function apiInterceptor(session, request, response) {
    if (response.qHandle && response.qType) {
      return session.getObjectApi({
        handle: response.qHandle,
        type: response.qType,
        id: response.qGenericId,
        genericType: response.qGenericType
      });
    }

    if (response.qHandle === null && response.qType === null) {
      return session.config.Promise.reject(new Error('Object not found'));
    }

    return response;
  }

  var hasOwn = Object.prototype.hasOwnProperty;
  var toStr = Object.prototype.toString;
  var defineProperty = Object.defineProperty;
  var gOPD = Object.getOwnPropertyDescriptor;

  var isArray = function isArray(arr) {
  	if (typeof Array.isArray === 'function') {
  		return Array.isArray(arr);
  	}

  	return toStr.call(arr) === '[object Array]';
  };

  var isPlainObject = function isPlainObject(obj) {
  	if (!obj || toStr.call(obj) !== '[object Object]') {
  		return false;
  	}

  	var hasOwnConstructor = hasOwn.call(obj, 'constructor');
  	var hasIsPrototypeOf = obj.constructor && obj.constructor.prototype && hasOwn.call(obj.constructor.prototype, 'isPrototypeOf');
  	// Not own constructor property must be Object
  	if (obj.constructor && !hasOwnConstructor && !hasIsPrototypeOf) {
  		return false;
  	}

  	// Own properties are enumerated firstly, so to speed up,
  	// if last one is own, then all properties are own.
  	var key;
  	for (key in obj) { /**/ }

  	return typeof key === 'undefined' || hasOwn.call(obj, key);
  };

  // If name is '__proto__', and Object.defineProperty is available, define __proto__ as an own property on target
  var setProperty = function setProperty(target, options) {
  	if (defineProperty && options.name === '__proto__') {
  		defineProperty(target, options.name, {
  			enumerable: true,
  			configurable: true,
  			value: options.newValue,
  			writable: true
  		});
  	} else {
  		target[options.name] = options.newValue;
  	}
  };

  // Return undefined instead of __proto__ if '__proto__' is not an own property
  var getProperty = function getProperty(obj, name) {
  	if (name === '__proto__') {
  		if (!hasOwn.call(obj, name)) {
  			return void 0;
  		} else if (gOPD) {
  			// In early versions of node, obj['__proto__'] is buggy when obj has
  			// __proto__ as an own property. Object.getOwnPropertyDescriptor() works.
  			return gOPD(obj, name).value;
  		}
  	}

  	return obj[name];
  };

  var extend = function extend() {
  	var options, name, src, copy, copyIsArray, clone;
  	var target = arguments[0];
  	var i = 1;
  	var length = arguments.length;
  	var deep = false;

  	// Handle a deep copy situation
  	if (typeof target === 'boolean') {
  		deep = target;
  		target = arguments[1] || {};
  		// skip the boolean and the target
  		i = 2;
  	}
  	if (target == null || (typeof target !== 'object' && typeof target !== 'function')) {
  		target = {};
  	}

  	for (; i < length; ++i) {
  		options = arguments[i];
  		// Only deal with non-null/undefined values
  		if (options != null) {
  			// Extend the base object
  			for (name in options) {
  				src = getProperty(target, name);
  				copy = getProperty(options, name);

  				// Prevent never-ending loop
  				if (target !== copy) {
  					// Recurse if we're merging plain objects or arrays
  					if (deep && copy && (isPlainObject(copy) || (copyIsArray = isArray(copy)))) {
  						if (copyIsArray) {
  							copyIsArray = false;
  							clone = src && isArray(src) ? src : [];
  						} else {
  							clone = src && isPlainObject(src) ? src : {};
  						}

  						// Never move original objects, clone them
  						setProperty(target, { name: name, newValue: extend(deep, clone, copy) });

  					// Don't bring in undefined values
  					} else if (typeof copy !== 'undefined') {
  						setProperty(target, { name: name, newValue: copy });
  					}
  				}
  			}
  		}
  	}

  	// Return the modified object
  	return target;
  };

  var extend$1 = extend.bind(null, true);
  var JSONPatch = {};
  var isArray$1 = Array.isArray;

  function isObject(v) {
    return v != null && !Array.isArray(v) && _typeof(v) === 'object';
  }

  function isUndef(v) {
    return typeof v === 'undefined';
  }

  function isFunction(v) {
    return typeof v === 'function';
  }
  /**
  * Generate an exact duplicate (with no references) of a specific value.
  *
  * @private
  * @param {Object} The value to duplicate
  * @returns {Object} a unique, duplicated value
  */


  function generateValue(val) {
    if (val) {
      return extend$1({}, {
        val: val
      }).val;
    }

    return val;
  }
  /**
  * An additional type checker used to determine if the property is of internal
  * use or not a type that can be translated into JSON (like functions).
  *
  * @private
  * @param {Object} obj The object which has the property to check
  * @param {String} The property name to check
  * @returns {Boolean} Whether the property is deemed special or not
  */


  function isSpecialProperty(obj, key) {
    return isFunction(obj[key]) || key.substring(0, 2) === '$$' || key.substring(0, 1) === '_';
  }
  /**
  * Finds the parent object from a JSON-Pointer ("/foo/bar/baz" = "bar" is "baz" parent),
  * also creates the object structure needed.
  *
  * @private
  * @param {Object} data The root object to traverse through
  * @param {String} The JSON-Pointer string to use when traversing
  * @returns {Object} The parent object
  */


  function getParent(data, str) {
    var seperator = '/';
    var parts = str.substring(1).split(seperator).slice(0, -1);
    var numPart;
    parts.forEach(function (part, i) {
      if (i === parts.length) {
        return;
      }

      numPart = +part;
      var newPart = !isNaN(numPart) ? [] : {};
      data[numPart || part] = isUndef(data[numPart || part]) ? newPart : data[part];
      data = data[numPart || part];
    });
    return data;
  }
  /**
  * Cleans an object of all its properties, unless they're deemed special or
  * cannot be removed by configuration.
  *
  * @private
  * @param {Object} obj The object to clean
  */


  function emptyObject(obj) {
    Object.keys(obj).forEach(function (key) {
      var config = Object.getOwnPropertyDescriptor(obj, key);

      if (config.configurable && !isSpecialProperty(obj, key)) {
        delete obj[key];
      }
    });
  }
  /**
  * Compare an object with another, could be object, array, number, string, bool.
  * @private
  * @param {Object} a The first object to compare
  * @param {Object} a The second object to compare
  * @returns {Boolean} Whether the objects are identical
  */


  function compare(a, b) {
    var isIdentical = true;

    if (isObject(a) && isObject(b)) {
      if (Object.keys(a).length !== Object.keys(b).length) {
        return false;
      }

      Object.keys(a).forEach(function (key) {
        if (!compare(a[key], b[key])) {
          isIdentical = false;
        }
      });
      return isIdentical;
    }

    if (isArray$1(a) && isArray$1(b)) {
      if (a.length !== b.length) {
        return false;
      }

      for (var i = 0, l = a.length; i < l; i += 1) {
        if (!compare(a[i], b[i])) {
          return false;
        }
      }

      return true;
    }

    return a === b;
  }
  /**
  * Generates patches by comparing two arrays.
  *
  * @private
  * @param {Array} oldA The old (original) array, which will be patched
  * @param {Array} newA The new array, which will be used to compare against
  * @returns {Array} An array of patches (if any)
  */


  function patchArray(original, newA, basePath) {
    var patches = [];
    var oldA = original.slice();
    var tmpIdx = -1;

    function findIndex(a, id, idx) {
      if (a[idx] && isUndef(a[idx].qInfo)) {
        return null;
      }

      if (a[idx] && a[idx].qInfo.qId === id) {
        // shortcut if identical
        return idx;
      }

      for (var ii = 0, ll = a.length; ii < ll; ii += 1) {
        if (a[ii] && a[ii].qInfo.qId === id) {
          return ii;
        }
      }

      return -1;
    }

    if (compare(newA, oldA)) {
      // array is unchanged
      return patches;
    }

    if (!isUndef(newA[0]) && isUndef(newA[0].qInfo)) {
      // we cannot create patches without unique identifiers, replace array...
      patches.push({
        op: 'replace',
        path: basePath,
        value: newA
      });
      return patches;
    }

    for (var i = oldA.length - 1; i >= 0; i -= 1) {
      tmpIdx = findIndex(newA, oldA[i].qInfo && oldA[i].qInfo.qId, i);

      if (tmpIdx === -1) {
        patches.push({
          op: 'remove',
          path: "".concat(basePath, "/").concat(i)
        });
        oldA.splice(i, 1);
      } else {
        patches = patches.concat(JSONPatch.generate(oldA[i], newA[tmpIdx], "".concat(basePath, "/").concat(i)));
      }
    }

    for (var _i = 0, l = newA.length; _i < l; _i += 1) {
      tmpIdx = findIndex(oldA, newA[_i].qInfo && newA[_i].qInfo.qId);

      if (tmpIdx === -1) {
        patches.push({
          op: 'add',
          path: "".concat(basePath, "/").concat(_i),
          value: newA[_i]
        });
        oldA.splice(_i, 0, newA[_i]);
      } else if (tmpIdx !== _i) {
        patches.push({
          op: 'move',
          path: "".concat(basePath, "/").concat(_i),
          from: "".concat(basePath, "/").concat(tmpIdx)
        });
        oldA.splice(_i, 0, oldA.splice(tmpIdx, 1)[0]);
      }
    }

    return patches;
  }
  /**
  * Generate an array of JSON-Patch:es following the JSON-Patch Specification Draft.
  *
  * See [specification draft](http://tools.ietf.org/html/draft-ietf-appsawg-json-patch-10)
  *
  * Does NOT currently generate patches for arrays (will replace them)
  * @private
  * @param {Object} original The object to patch to
  * @param {Object} newData The object to patch from
  * @param {String} [basePath] The base path to use when generating the paths for
  *                            the patches (normally not used)
  * @returns {Array} An array of patches
  */


  JSONPatch.generate = function generate(original, newData, basePath) {
    basePath = basePath || '';
    var patches = [];
    Object.keys(newData).forEach(function (key) {
      var val = generateValue(newData[key]);
      var oldVal = original[key];
      var tmpPath = "".concat(basePath, "/").concat(key);

      if (compare(val, oldVal) || isSpecialProperty(newData, key)) {
        return;
      }

      if (isUndef(oldVal)) {
        // property does not previously exist
        patches.push({
          op: 'add',
          path: tmpPath,
          value: val
        });
      } else if (isObject(val) && isObject(oldVal)) {
        // we need to generate sub-patches for this, since it already exist
        patches = patches.concat(JSONPatch.generate(oldVal, val, tmpPath));
      } else if (isArray$1(val) && isArray$1(oldVal)) {
        patches = patches.concat(patchArray(oldVal, val, tmpPath));
      } else {
        // it's a simple property (bool, string, number)
        patches.push({
          op: 'replace',
          path: "".concat(basePath, "/").concat(key),
          value: val
        });
      }
    });
    Object.keys(original).forEach(function (key) {
      if (isUndef(newData[key]) && !isSpecialProperty(original, key)) {
        // this property does not exist anymore
        patches.push({
          op: 'remove',
          path: "".concat(basePath, "/").concat(key)
        });
      }
    });
    return patches;
  };
  /**
  * Apply a list of patches to an object.
  * @private
  * @param {Object} original The object to patch
  * @param {Array} patches The list of patches to apply
  */


  JSONPatch.apply = function apply(original, patches) {
    patches.forEach(function (patch) {
      var parent = getParent(original, patch.path);
      var key = patch.path.split('/').splice(-1)[0];
      var target = key && isNaN(+key) ? parent[key] : parent[+key] || parent;
      var from = patch.from ? patch.from.split('/').splice(-1)[0] : null;

      if (patch.path === '/') {
        parent = null;
        target = original;
      }

      if (patch.op === 'add' || patch.op === 'replace') {
        if (isArray$1(parent)) {
          // trust indexes from patches, so don't replace the index if it's an add
          if (key === '-') {
            key = parent.length;
          }

          parent.splice(+key, patch.op === 'add' ? 0 : 1, patch.value);
        } else if (isArray$1(target) && isArray$1(patch.value)) {
          var _target;

          var newValues = patch.value.slice(); // keep array reference if possible...

          target.length = 0;

          (_target = target).push.apply(_target, _toConsumableArray(newValues));
        } else if (isObject(target) && isObject(patch.value)) {
          // keep object reference if possible...
          emptyObject(target);
          extend$1(target, patch.value);
        } else if (!parent) {
          throw new Error('Patchee is not an object we can patch');
        } else {
          // simple value
          parent[key] = patch.value;
        }
      } else if (patch.op === 'move') {
        var oldParent = getParent(original, patch.from);

        if (isArray$1(parent)) {
          parent.splice(+key, 0, oldParent.splice(+from, 1)[0]);
        } else {
          parent[key] = oldParent[from];
          delete oldParent[from];
        }
      } else if (patch.op === 'remove') {
        if (isArray$1(parent)) {
          parent.splice(+key, 1);
        } else {
          delete parent[key];
        }
      }
    });
  };
  /**
  * Deep clone an object.
  * @private
  * @param {Object} obj The object to clone
  * @returns {Object} A new object identical to the `obj`
  */


  JSONPatch.clone = function clone(obj) {
    return extend$1({}, obj);
  };
  /**
  * Creates a JSON-patch.
  * @private
  * @param {String} op The operation of the patch. Available values: "add", "remove", "move"
  * @param {Object} [val] The value to set the `path` to. If `op` is `move`, `val`
  *                       is the "from JSON-path" path
  * @param {String} path The JSON-path for the property to change (e.g. "/qHyperCubeDef/columnOrder")
  * @returns {Object} A patch following the JSON-patch specification
  */


  JSONPatch.createPatch = function createPatch(op, val, path) {
    var patch = {
      op: op.toLowerCase(),
      path: path
    };

    if (patch.op === 'move') {
      patch.from = val;
    } else if (typeof val !== 'undefined') {
      patch.value = val;
    }

    return patch;
  };
  /**
  * Apply the differences of two objects (keeping references if possible).
  * Identical to running `JSONPatch.apply(original, JSONPatch.generate(original, newData));`
  * @private
  * @param {Object} original The object to update/patch
  * @param {Object} newData the object to diff against
  *
  * @example
  * var obj1 = { foo: [1,2,3], bar: { baz: true, qux: 1 } };
  * var obj2 = { foo: [4,5,6], bar: { baz: false } };
  * JSONPatch.updateObject(obj1, obj2);
  * // => { foo: [4,5,6], bar: { baz: false } };
  */


  JSONPatch.updateObject = function updateObject(original, newData) {
    if (!Object.keys(original).length) {
      extend$1(original, newData);
      return;
    }

    JSONPatch.apply(original, JSONPatch.generate(original, newData));
  };

  var sessions = {};
  /**
  * Function to make sure we release handle caches when they are closed.
  * @private
  * @param {Session} session The session instance to listen on.
  */

  var bindSession = function bindSession(session) {
    if (!sessions[session.id]) {
      var cache = {};
      sessions[session.id] = cache;
      session.on('traffic:received', function (data) {
        return data.close && data.close.forEach(function (handle) {
          return delete cache[handle];
        });
      });
      session.on('closed', function () {
        return delete sessions[session.id];
      });
    }
  };
  /**
  * Simple function that ensures the session events has been bound, and returns
  * either an existing key-value cache or creates one for the specified handle.
  * @private
  * @param {Session} session The session that owns the handle.
  * @param {Number} handle The object handle to retrieve the cache for.
  * @returns {KeyValueCache} The cache instance.
  */


  var getHandleCache = function getHandleCache(session, handle) {
    bindSession(session);
    var cache = sessions[session.id];

    if (!cache[handle]) {
      cache[handle] = new KeyValueCache();
    }

    return cache[handle];
  };
  /**
  * Function used to apply a list of patches and return the patched value.
  * @private
  * @param {Session} session The session.
  * @param {Number} handle The object handle.
  * @param {String} cacheId The cacheId.
  * @param {Array} patches The patches.
  * @returns {Object} Returns the patched value.
  */


  var patchValue = function patchValue(session, handle, cacheId, patches) {
    var cache = getHandleCache(session, handle);
    var entry = cache.get(cacheId);

    if (typeof entry === 'undefined') {
      entry = Array.isArray(patches[0].value) ? [] : {};
    }

    if (patches.length) {
      if (patches[0].path === '/' && _typeof(patches[0].value) !== 'object') {
        // 'plain' values on root path is not supported (no object reference),
        // so we simply store the value directly:
        entry = patches[0].value;
      } else {
        JSONPatch.apply(entry, patches);
      }

      cache.set(cacheId, entry);
    }

    return entry;
  };
  /**
  * Process delta interceptor.
  * @private
  * @param {Session} session The session the intercept is being executed on.
  * @param {Object} request The JSON-RPC request.
  * @param {Object} response The response.
  * @returns {Object} Returns the patched response
  */


  function deltaInterceptor(session, request, response) {
    var delta = response.delta,
        result = response.result;

    if (delta) {
      // when delta is on the response data is expected to be an array of patches:
      Object.keys(result).forEach(function (key) {
        if (!Array.isArray(result[key])) {
          throw new Error('Unexpected RPC response, expected array of patches');
        }

        result[key] = patchValue(session, request.handle, "".concat(request.method, "-").concat(key), result[key]);
      }); // return a cloned response object to avoid patched object references:

      return JSON.parse(JSON.stringify(response));
    }

    return response;
  } // export object reference for testing purposes:

  deltaInterceptor.sessions = sessions;

  /**
  * Process error interceptor.
  * @private
  * @param {Session} session - The session the intercept is being executed on.
  * @param {Object} request - The JSON-RPC request.
  * @param {Object} response - The response.
  * @returns {Object} - Returns the defined error for an error, else the response.
  */
  function errorInterceptor(session, request, response) {
    if (typeof response.error !== 'undefined') {
      var data = response.error;
      var error = new Error(data.message);
      error.code = data.code;
      error.parameter = data.parameter;
      return session.config.Promise.reject(error);
    }

    return response;
  }

  var RETURN_KEY = 'qReturn';
  /**
  * Picks out the result "out" parameter based on the QIX method+schema, with
  * some specific handling for some methods that breaks the predictable protocol.
  * @private
  * @param {Session} session - The session the intercept is being executed on.
  * @param {Object} request - The JSON-RPC request.
  * @param {Object} response - The response.
  * @returns {Object} - Returns the result property on the response
  */

  function outParamInterceptor(session, request, response) {
    if (request.method === 'CreateSessionApp' || request.method === 'CreateSessionAppFromApp') {
      // this method returns multiple out params that we need
      // to normalize before processing the response further:
      response[RETURN_KEY].qGenericId = response[RETURN_KEY].qGenericId || response.qSessionAppId;
    } else if (request.method === 'GetInteract') {
      // this method returns a qReturn value when it should only return
      // meta.outKey:
      delete response[RETURN_KEY];
    }

    if (hasOwnProperty.call(response, RETURN_KEY)) {
      return response[RETURN_KEY];
    }

    if (request.outKey !== -1) {
      return response[request.outKey];
    }

    return response;
  }

  /**
  * Process result interceptor.
  * @private
  * @param {Session} session - The session the intercept is being executed on.
  * @param {Object} request - The JSON-RPC request.
  * @param {Object} response - The response.
  * @returns {Object} - Returns the result property on the response
  */
  function resultInterceptor(session, request, response) {
    return response.result;
  }

  /**
   * Interceptors is a concept similar to mixins, but run on a lower level. The interceptor concept
   * can augment either the requests (i.e. before sent to QIX Engine), or the responses (i.e. after
   * QIX Engine has sent a response). The interceptor promises runs in parallel to the regular
   * promises used in enigma.js, which means that it can be really useful when you want to normalize
   * behaviors in your application.
   * @interface Interceptor
   */

  /**
    * @class InterceptorRequest
    * @implements {Interceptor}
    */

  /**
   * @class InterceptorResponse
   * @implements {Interceptor}
   */

  /**
   * This method is invoked when a request is about to be sent to QIX Engine.
   * @function InterceptorRequest#onFulfilled
   * @param {Session} session The session executing the interceptor.
   * @param {Object} request The JSON-RPC request that will be sent.
   */

  /**
   * This method is invoked when a previous interceptor has rejected the
   * promise, use this to handle for example errors before they are sent into mixins.
   * @function InterceptorResponse#onRejected
   * @param {Session} session The session executing the interceptor. You may use .retry() to retry
   * sending it to QIX Engine.
   * @param {Object} request The JSON-RPC request resulting in this error.
   * @param {Object} error Whatever the previous interceptor is rejected with.
   */

  /**
   * This method is invoked when a promise has been successfully resolved,
   * use this to modify the result or reject the promise chain before it is sent
   * to mixins.
   * @function InterceptorResponse#onFulfilled
   * @param {Session} session The session executing the interceptor.
   * @param {Object} request The JSON-RPC request resulting in this response.
   * @param {Object} result Whatever the previous interceptor is resolved with.
   */

  var Intercept =
  /*#__PURE__*/
  function () {
    /**
    * Create a new Intercept instance.
    * @private
    * @param {Object} options The configuration options for this class.
    * @param {Promise<Object>} options.Promise The promise constructor to use.
    * @param {ApiCache} options.apis The ApiCache instance to use.
    * @param {Array<Object>} [options.request] The additional request interceptors to use.
    * @param {Array<Object>} [options.response] The additional response interceptors to use.
    */
    function Intercept(options) {
      _classCallCheck(this, Intercept);

      Object.assign(this, options);
      this.request = [{
        onFulfilled: deltaRequestInterceptor
      }].concat(_toConsumableArray(this.request || []));
      this.response = [{
        onFulfilled: errorInterceptor
      }, {
        onFulfilled: deltaInterceptor
      }, {
        onFulfilled: resultInterceptor
      }, {
        onFulfilled: outParamInterceptor
      }].concat(_toConsumableArray(this.response || []), [{
        onFulfilled: apiInterceptor
      }]);
    }
    /**
    * Execute the request interceptor queue, each interceptor will get the result from
    * the previous interceptor.
    * @private
    * @param {Session} session The session instance to execute against.
    * @param {Promise<Object>} promise The promise to chain on to.
    * @returns {Promise<Object>}
    */


    _createClass(Intercept, [{
      key: "executeRequests",
      value: function executeRequests(session, promise) {
        var _this = this;

        return this.request.reduce(function (interception, interceptor) {
          var intercept = interceptor.onFulfilled && interceptor.onFulfilled.bind(_this, session);
          return interception.then(intercept);
        }, promise);
      }
      /**
      * Execute the response interceptor queue, each interceptor will get the result from
      * the previous interceptor.
      * @private
      * @param {Session} session The session instance to execute against.
      * @param {Promise<Object>} promise The promise to chain on to.
      * @param {Object} request The JSONRPC request object for the intercepted response.
      * @returns {Promise<Object>}
      */

    }, {
      key: "executeResponses",
      value: function executeResponses(session, promise, request) {
        var _this2 = this;

        return this.response.reduce(function (interception, interceptor) {
          return interception.then(interceptor.onFulfilled && interceptor.onFulfilled.bind(_this2, session, request), interceptor.onRejected && interceptor.onRejected.bind(_this2, session, request));
        }, promise);
      }
    }]);

    return Intercept;
  }();

  /**
  * API cache for instances of QIX types, e.g. GenericObject.
  * @private
  * @extends KeyValueCache
  */

  var ApiCache =
  /*#__PURE__*/
  function (_KeyValueCache) {
    _inherits(ApiCache, _KeyValueCache);

    function ApiCache() {
      _classCallCheck(this, ApiCache);

      return _possibleConstructorReturn(this, _getPrototypeOf(ApiCache).apply(this, arguments));
    }

    _createClass(ApiCache, [{
      key: "add",

      /**
      * Adds an API.
      * @private
      * @function ApiCache#add
      * @param {Number} handle - The handle for the API.
      * @param {*} api - The API.
      * @returns {{api: *}} The entry.
      */
      value: function add(handle, api) {
        var _this = this;

        var entry = {
          api: api
        };

        _get(_getPrototypeOf(ApiCache.prototype), "add", this).call(this, handle.toString(), entry);

        api.on('closed', function () {
          return _this.remove(handle);
        });
        return entry;
      }
      /**
      * Gets an API.
      * @private
      * @function ApiCache#getApi
      * @param {Number} handle - The handle for the API.
      * @returns {*} The API for the handle.
      */

    }, {
      key: "getApi",
      value: function getApi(handle) {
        var entry = typeof handle !== 'undefined' ? this.get(handle.toString()) : undefined;
        return entry && entry.api;
      }
      /**
      * Gets a list of APIs.
      * @private
      * @function ApiCache#getApis
      * @returns {Array} The list of entries including `handle` and `api` properties for each entry.
      */

    }, {
      key: "getApis",
      value: function getApis() {
        return _get(_getPrototypeOf(ApiCache.prototype), "getAll", this).call(this).map(function (entry) {
          return {
            handle: entry.key,
            api: entry.value.api
          };
        });
      }
      /**
      * Gets a list of APIs with a given type.
      * @private
      * @function ApiCache#getApisByType
      * @param {String} type - The type of APIs to get.
      * @returns {Array} The list of entries including `handle` and `api` properties for each entry.
      */

    }, {
      key: "getApisByType",
      value: function getApisByType(type) {
        return this.getApis().filter(function (entry) {
          return entry.api.type === type;
        });
      }
    }]);

    return ApiCache;
  }(KeyValueCache);

  /**
   * The enigma.js configuration object.
   * @interface Configuration
   * @property {Object} schema Object containing the specification for the API to generate.
   * Corresponds to a specific version of the QIX Engine API.
   * @property {String} url String containing a proper websocker URL to QIX Engine.
   * @property {Function} [createSocket] A function to use when instantiating the WebSocket,
   * mandatory for Node.js.
   * @property {Object} [Promise] ES6-compatible Promise library.
   * @property {Boolean} [suspendOnClose=false] Set to true if the session should be suspended
   * instead of closed when the websocket is closed.
   * @property {Array<Mixin>} [mixins=[]] Mixins to extend/augment the QIX Engine API. Mixins
   * are applied in the array order.
   * @property {Array} [requestInterceptors=[]] Interceptors for augmenting requests before they
   * are sent to QIX Engine. Interceptors are applied in the array order.
   * @property {Array} [responseInterceptors=[]] Interceptors for augmenting responses before they
   * are passed into mixins and end-users. Interceptors are applied in the array order.
   * @property {Object} [protocol={}] An object containing additional JSON-RPC request parameters.
   * @property {Boolean} [protocol.delta=true] Set to false to disable the use of the
   * bandwidth-reducing delta protocol.
   */

  /**
   * Mixin object to extend/augment the QIX Engine API
   * @interface Mixin
   * @property {String|Array<String>} types String or array of strings containing the API-types that
   * will be mixed in.
   * @property {Object} [extend] Object literal containing the methods that will be extended on the
   * specified API.
   * @property {Object} [override] Object literal containing the methods to override existing methods.
   * @property {Function} [init] Init function that, if defined, will run when an API is instantiated.
   * It runs with Promise and API object as parameters
   */

  /**
   * The API for generated APIs depends on the QIX Engine schema you pass into your Configuration,
   * and on what QIX struct the API has.
   * @interface API
   * @property {String} id Contains the unique identifier for this API.
   * @property {String} type Contains the schema class name for this API.
   * @property {String} genericType Corresponds to the qInfo.qType property on the generic object's
   * properties object.
   * @property {Session} session Contains a reference to the session that this API belongs to.
   * @property {Number} handle Contains the handle QIX Engine assigned to the API. Used interally in
   * enigma.js for caches and JSON-RPC requests.
   */

  /**
   * Handle changes on the API. The changed event is triggered whenever enigma.js or QIX Engine has
   * identified potential changes on the underlying properties or hypercubes and you should re-fetch
   * your data.
   * @event API#changed
   * @type {Object}
   */

  /**
   * Handle closed API. The closed event is triggered whenever QIX Engine considers an API closed.
   * It usually means that it no longer exist in the QIX Engine document or session.
   * @event API#closed
   * @type {Object}
   */

  /**
   * Handle JSON-RPC requests/responses for this API. Generally used in debugging purposes.
   * traffic:* will handle all websocket messages, traffic:sent will handle outgoing messages
   * and traffic:received will handle incoming messages.
   * @event API#traffic
   * @type {Object}
   */

  /**
  * Qix service.
  */

  var Qix =
  /*#__PURE__*/
  function () {
    function Qix() {
      _classCallCheck(this, Qix);
    }

    _createClass(Qix, null, [{
      key: "getSession",

      /**
      * Function used to get a session.
      * @private
      * @param {Configuration} config The configuration object for this session.
      * @returns {Session} Returns a session instance.
      */
      value: function getSession(config) {
        var createSocket = config.createSocket,
            Promise = config.Promise,
            requestInterceptors = config.requestInterceptors,
            responseInterceptors = config.responseInterceptors,
            url = config.url;
        var apis = new ApiCache();
        var intercept = new Intercept({
          apis: apis,
          Promise: Promise,
          request: requestInterceptors,
          response: responseInterceptors
        });
        var rpc = new RPC({
          createSocket: createSocket,
          Promise: Promise,
          url: url
        });
        var suspendResume = new SuspendResume({
          apis: apis,
          Promise: Promise,
          rpc: rpc
        });
        var session = new Session({
          apis: apis,
          config: config,
          intercept: intercept,
          rpc: rpc,
          suspendResume: suspendResume
        });
        return session;
      }
      /**
      * Function used to create a QIX session.
      * @param {Configuration} config The configuration object for the QIX session.
      * @returns {Session} Returns a new QIX session.
      */

    }, {
      key: "create",
      value: function create(config) {
        Qix.configureDefaults(config);
        config.mixins.forEach(function (mixin) {
          config.definition.registerMixin(mixin);
        });
        return Qix.getSession(config);
      }
      /**
      * Function used to configure defaults.
      * @private
      * @param {Configuration} config The configuration object for how to connect
      *                               and retrieve end QIX APIs.
      */

    }, {
      key: "configureDefaults",
      value: function configureDefaults(config) {
        if (!config) {
          throw new Error('You need to supply a configuration.');
        } // eslint-disable-next-line no-restricted-globals


        if (!config.Promise && typeof Promise === 'undefined') {
          throw new Error('Your environment has no Promise implementation. You must provide a Promise implementation in the config.');
        }

        if (typeof config.createSocket !== 'function' && typeof WebSocket === 'function') {
          // eslint-disable-next-line no-undef
          config.createSocket = function (url) {
            return new WebSocket(url);
          };
        }

        if (typeof config.suspendOnClose === 'undefined') {
          config.suspendOnClose = false;
        }

        config.protocol = config.protocol || {};
        config.protocol.delta = typeof config.protocol.delta !== 'undefined' ? config.protocol.delta : true; // eslint-disable-next-line no-restricted-globals

        config.Promise = config.Promise || Promise;
        config.mixins = config.mixins || [];
        config.definition = config.definition || new Schema(config);
      }
    }]);

    return Qix;
  }();

  return Qix;

})));
//# sourceMappingURL=enigma.js.map


/***/ }),

/***/ "59c4":
/***/ (function(module, exports, __webpack_require__) {

var dP = __webpack_require__("120e");
var createDesc = __webpack_require__("a3cc");
module.exports = __webpack_require__("7e81") ? function (object, key, value) {
  return dP.f(object, key, createDesc(1, value));
} : function (object, key, value) {
  object[key] = value;
  return object;
};


/***/ }),

/***/ "5b4e":
/***/ (function(module, exports, __webpack_require__) {

// false -> Array#indexOf
// true  -> Array#includes
var toIObject = __webpack_require__("36c3");
var toLength = __webpack_require__("b447");
var toAbsoluteIndex = __webpack_require__("0fc9");
module.exports = function (IS_INCLUDES) {
  return function ($this, el, fromIndex) {
    var O = toIObject($this);
    var length = toLength(O.length);
    var index = toAbsoluteIndex(fromIndex, length);
    var value;
    // Array#includes uses SameValueZero equality algorithm
    // eslint-disable-next-line no-self-compare
    if (IS_INCLUDES && el != el) while (length > index) {
      value = O[index++];
      // eslint-disable-next-line no-self-compare
      if (value != value) return true;
    // Array#indexOf ignores holes, Array#includes - not
    } else for (;length > index; index++) if (IS_INCLUDES || index in O) {
      if (O[index] === el) return IS_INCLUDES || index || 0;
    } return !IS_INCLUDES && -1;
  };
};


/***/ }),

/***/ "5bba":
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("9d98");
var $Object = __webpack_require__("584a").Object;
module.exports = function defineProperties(T, D) {
  return $Object.defineProperties(T, D);
};


/***/ }),

/***/ "5bf0":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var anObject = __webpack_require__("b597");
var toObject = __webpack_require__("9bce");
var toLength = __webpack_require__("b4df");
var toInteger = __webpack_require__("58ce");
var advanceStringIndex = __webpack_require__("dd44");
var regExpExec = __webpack_require__("d25d");
var max = Math.max;
var min = Math.min;
var floor = Math.floor;
var SUBSTITUTION_SYMBOLS = /\$([$&`']|\d\d?|<[^>]*>)/g;
var SUBSTITUTION_SYMBOLS_NO_NAMED = /\$([$&`']|\d\d?)/g;

var maybeToString = function (it) {
  return it === undefined ? it : String(it);
};

// @@replace logic
__webpack_require__("1fbc")('replace', 2, function (defined, REPLACE, $replace, maybeCallNative) {
  return [
    // `String.prototype.replace` method
    // https://tc39.github.io/ecma262/#sec-string.prototype.replace
    function replace(searchValue, replaceValue) {
      var O = defined(this);
      var fn = searchValue == undefined ? undefined : searchValue[REPLACE];
      return fn !== undefined
        ? fn.call(searchValue, O, replaceValue)
        : $replace.call(String(O), searchValue, replaceValue);
    },
    // `RegExp.prototype[@@replace]` method
    // https://tc39.github.io/ecma262/#sec-regexp.prototype-@@replace
    function (regexp, replaceValue) {
      var res = maybeCallNative($replace, regexp, this, replaceValue);
      if (res.done) return res.value;

      var rx = anObject(regexp);
      var S = String(this);
      var functionalReplace = typeof replaceValue === 'function';
      if (!functionalReplace) replaceValue = String(replaceValue);
      var global = rx.global;
      if (global) {
        var fullUnicode = rx.unicode;
        rx.lastIndex = 0;
      }
      var results = [];
      while (true) {
        var result = regExpExec(rx, S);
        if (result === null) break;
        results.push(result);
        if (!global) break;
        var matchStr = String(result[0]);
        if (matchStr === '') rx.lastIndex = advanceStringIndex(S, toLength(rx.lastIndex), fullUnicode);
      }
      var accumulatedResult = '';
      var nextSourcePosition = 0;
      for (var i = 0; i < results.length; i++) {
        result = results[i];
        var matched = String(result[0]);
        var position = max(min(toInteger(result.index), S.length), 0);
        var captures = [];
        // NOTE: This is equivalent to
        //   captures = result.slice(1).map(maybeToString)
        // but for some reason `nativeSlice.call(result, 1, result.length)` (called in
        // the slice polyfill when slicing native arrays) "doesn't work" in safari 9 and
        // causes a crash (https://pastebin.com/N21QzeQA) when trying to debug it.
        for (var j = 1; j < result.length; j++) captures.push(maybeToString(result[j]));
        var namedCaptures = result.groups;
        if (functionalReplace) {
          var replacerArgs = [matched].concat(captures, position, S);
          if (namedCaptures !== undefined) replacerArgs.push(namedCaptures);
          var replacement = String(replaceValue.apply(undefined, replacerArgs));
        } else {
          replacement = getSubstitution(matched, S, position, captures, namedCaptures, replaceValue);
        }
        if (position >= nextSourcePosition) {
          accumulatedResult += S.slice(nextSourcePosition, position) + replacement;
          nextSourcePosition = position + matched.length;
        }
      }
      return accumulatedResult + S.slice(nextSourcePosition);
    }
  ];

    // https://tc39.github.io/ecma262/#sec-getsubstitution
  function getSubstitution(matched, str, position, captures, namedCaptures, replacement) {
    var tailPos = position + matched.length;
    var m = captures.length;
    var symbols = SUBSTITUTION_SYMBOLS_NO_NAMED;
    if (namedCaptures !== undefined) {
      namedCaptures = toObject(namedCaptures);
      symbols = SUBSTITUTION_SYMBOLS;
    }
    return $replace.call(replacement, symbols, function (match, ch) {
      var capture;
      switch (ch.charAt(0)) {
        case '$': return '$';
        case '&': return matched;
        case '`': return str.slice(0, position);
        case "'": return str.slice(tailPos);
        case '<':
          capture = namedCaptures[ch.slice(1, -1)];
          break;
        default: // \d\d?
          var n = +ch;
          if (n === 0) return ch;
          if (n > m) {
            var f = floor(n / 10);
            if (f === 0) return ch;
            if (f <= m) return captures[f - 1] === undefined ? ch.charAt(1) : captures[f - 1] + ch.charAt(1);
            return ch;
          }
          capture = captures[n - 1];
      }
      return capture === undefined ? '' : capture;
    });
  }
});


/***/ }),

/***/ "5c95":
/***/ (function(module, exports, __webpack_require__) {

var hide = __webpack_require__("35e8");
module.exports = function (target, src, safe) {
  for (var key in src) {
    if (safe && target[key]) target[key] = src[key];
    else hide(target, key, src[key]);
  } return target;
};


/***/ }),

/***/ "5ca1":
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__("7726");
var core = __webpack_require__("8378");
var hide = __webpack_require__("32e9");
var redefine = __webpack_require__("2aba");
var ctx = __webpack_require__("9b43");
var PROTOTYPE = 'prototype';

var $export = function (type, name, source) {
  var IS_FORCED = type & $export.F;
  var IS_GLOBAL = type & $export.G;
  var IS_STATIC = type & $export.S;
  var IS_PROTO = type & $export.P;
  var IS_BIND = type & $export.B;
  var target = IS_GLOBAL ? global : IS_STATIC ? global[name] || (global[name] = {}) : (global[name] || {})[PROTOTYPE];
  var exports = IS_GLOBAL ? core : core[name] || (core[name] = {});
  var expProto = exports[PROTOTYPE] || (exports[PROTOTYPE] = {});
  var key, own, out, exp;
  if (IS_GLOBAL) source = name;
  for (key in source) {
    // contains in native
    own = !IS_FORCED && target && target[key] !== undefined;
    // export native or passed
    out = (own ? target : source)[key];
    // bind timers to global for call from export context
    exp = IS_BIND && own ? ctx(out, global) : IS_PROTO && typeof out == 'function' ? ctx(Function.call, out) : out;
    // extend global
    if (target) redefine(target, key, out, type & $export.U);
    // export
    if (exports[key] != out) hide(exports, key, exp);
    if (IS_PROTO && expProto[key] != out) expProto[key] = out;
  }
};
global.core = core;
// type bitmap
$export.F = 1;   // forced
$export.G = 2;   // global
$export.S = 4;   // static
$export.P = 8;   // proto
$export.B = 16;  // bind
$export.W = 32;  // wrap
$export.U = 64;  // safe
$export.R = 128; // real proto method for `library`
module.exports = $export;


/***/ }),

/***/ "5d58":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("d8d6");

/***/ }),

/***/ "5d73":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("469f");

/***/ }),

/***/ "62a0":
/***/ (function(module, exports) {

var id = 0;
var px = Math.random();
module.exports = function (key) {
  return 'Symbol('.concat(key === undefined ? '' : key, ')_', (++id + px).toString(36));
};


/***/ }),

/***/ "635a":
/***/ (function(module, exports) {

// https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
var global = module.exports = typeof window != 'undefined' && window.Math == Math
  ? window : typeof self != 'undefined' && self.Math == Math ? self
  // eslint-disable-next-line no-new-func
  : Function('return this')();
if (typeof __g == 'number') __g = global; // eslint-disable-line no-undef


/***/ }),

/***/ "63b6":
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__("e53d");
var core = __webpack_require__("584a");
var ctx = __webpack_require__("d864");
var hide = __webpack_require__("35e8");
var has = __webpack_require__("07e3");
var PROTOTYPE = 'prototype';

var $export = function (type, name, source) {
  var IS_FORCED = type & $export.F;
  var IS_GLOBAL = type & $export.G;
  var IS_STATIC = type & $export.S;
  var IS_PROTO = type & $export.P;
  var IS_BIND = type & $export.B;
  var IS_WRAP = type & $export.W;
  var exports = IS_GLOBAL ? core : core[name] || (core[name] = {});
  var expProto = exports[PROTOTYPE];
  var target = IS_GLOBAL ? global : IS_STATIC ? global[name] : (global[name] || {})[PROTOTYPE];
  var key, own, out;
  if (IS_GLOBAL) source = name;
  for (key in source) {
    // contains in native
    own = !IS_FORCED && target && target[key] !== undefined;
    if (own && has(exports, key)) continue;
    // export native or passed
    out = own ? target[key] : source[key];
    // prevent global pollution for namespaces
    exports[key] = IS_GLOBAL && typeof target[key] != 'function' ? source[key]
    // bind timers to global for call from export context
    : IS_BIND && own ? ctx(out, global)
    // wrap global constructors for prevent change them in library
    : IS_WRAP && target[key] == out ? (function (C) {
      var F = function (a, b, c) {
        if (this instanceof C) {
          switch (arguments.length) {
            case 0: return new C();
            case 1: return new C(a);
            case 2: return new C(a, b);
          } return new C(a, b, c);
        } return C.apply(this, arguments);
      };
      F[PROTOTYPE] = C[PROTOTYPE];
      return F;
    // make static versions for prototype methods
    })(out) : IS_PROTO && typeof out == 'function' ? ctx(Function.call, out) : out;
    // export proto methods to core.%CONSTRUCTOR%.methods.%NAME%
    if (IS_PROTO) {
      (exports.virtual || (exports.virtual = {}))[key] = out;
      // export proto methods to core.%CONSTRUCTOR%.prototype.%NAME%
      if (type & $export.R && expProto && !expProto[key]) hide(expProto, key, out);
    }
  }
};
// type bitmap
$export.F = 1;   // forced
$export.G = 2;   // global
$export.S = 4;   // static
$export.P = 8;   // proto
$export.B = 16;  // bind
$export.W = 32;  // wrap
$export.U = 64;  // safe
$export.R = 128; // real proto method for `library`
module.exports = $export;


/***/ }),

/***/ "656e":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// 25.4.1.5 NewPromiseCapability(C)
var aFunction = __webpack_require__("79aa");

function PromiseCapability(C) {
  var resolve, reject;
  this.promise = new C(function ($$resolve, $$reject) {
    if (resolve !== undefined || reject !== undefined) throw TypeError('Bad Promise constructor');
    resolve = $$resolve;
    reject = $$reject;
  });
  this.resolve = aFunction(resolve);
  this.reject = aFunction(reject);
}

module.exports.f = function (C) {
  return new PromiseCapability(C);
};


/***/ }),

/***/ "6718":
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__("e53d");
var core = __webpack_require__("584a");
var LIBRARY = __webpack_require__("b8e3");
var wksExt = __webpack_require__("ccb9");
var defineProperty = __webpack_require__("d9f6").f;
module.exports = function (name) {
  var $Symbol = core.Symbol || (core.Symbol = LIBRARY ? {} : global.Symbol || {});
  if (name.charAt(0) != '_' && !(name in $Symbol)) defineProperty($Symbol, name, { value: wksExt.f(name) });
};


/***/ }),

/***/ "67bb":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("f921");

/***/ }),

/***/ "696e":
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("c207");
__webpack_require__("1654");
__webpack_require__("6c1c");
__webpack_require__("24c5");
__webpack_require__("3c11");
__webpack_require__("43fc");
module.exports = __webpack_require__("584a").Promise;


/***/ }),

/***/ "69a8":
/***/ (function(module, exports) {

var hasOwnProperty = {}.hasOwnProperty;
module.exports = function (it, key) {
  return hasOwnProperty.call(it, key);
};


/***/ }),

/***/ "69d3":
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("6718")('asyncIterator');


/***/ }),

/***/ "6a99":
/***/ (function(module, exports, __webpack_require__) {

// 7.1.1 ToPrimitive(input [, PreferredType])
var isObject = __webpack_require__("d3f4");
// instead of the ES6 spec version, we didn't implement @@toPrimitive case
// and the second argument - flag - preferred type is a string
module.exports = function (it, S) {
  if (!isObject(it)) return it;
  var fn, val;
  if (S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  if (typeof (fn = it.valueOf) == 'function' && !isObject(val = fn.call(it))) return val;
  if (!S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  throw TypeError("Can't convert object to primitive value");
};


/***/ }),

/***/ "6abf":
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.7 / 15.2.3.4 Object.getOwnPropertyNames(O)
var $keys = __webpack_require__("e6f3");
var hiddenKeys = __webpack_require__("1691").concat('length', 'prototype');

exports.f = Object.getOwnPropertyNames || function getOwnPropertyNames(O) {
  return $keys(O, hiddenKeys);
};


/***/ }),

/***/ "6b4c":
/***/ (function(module, exports) {

var toString = {}.toString;

module.exports = function (it) {
  return toString.call(it).slice(8, -1);
};


/***/ }),

/***/ "6c1c":
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("c367");
var global = __webpack_require__("e53d");
var hide = __webpack_require__("35e8");
var Iterators = __webpack_require__("481b");
var TO_STRING_TAG = __webpack_require__("5168")('toStringTag');

var DOMIterables = ('CSSRuleList,CSSStyleDeclaration,CSSValueList,ClientRectList,DOMRectList,DOMStringList,' +
  'DOMTokenList,DataTransferItemList,FileList,HTMLAllCollection,HTMLCollection,HTMLFormElement,HTMLSelectElement,' +
  'MediaList,MimeTypeArray,NamedNodeMap,NodeList,PaintRequestList,Plugin,PluginArray,SVGLengthList,SVGNumberList,' +
  'SVGPathSegList,SVGPointList,SVGStringList,SVGTransformList,SourceBufferList,StyleSheetList,TextTrackCueList,' +
  'TextTrackList,TouchList').split(',');

for (var i = 0; i < DOMIterables.length; i++) {
  var NAME = DOMIterables[i];
  var Collection = global[NAME];
  var proto = Collection && Collection.prototype;
  if (proto && !proto[TO_STRING_TAG]) hide(proto, TO_STRING_TAG, NAME);
  Iterators[NAME] = Iterators.Array;
}


/***/ }),

/***/ "71c1":
/***/ (function(module, exports, __webpack_require__) {

var toInteger = __webpack_require__("3a38");
var defined = __webpack_require__("25eb");
// true  -> String#at
// false -> String#codePointAt
module.exports = function (TO_STRING) {
  return function (that, pos) {
    var s = String(defined(that));
    var i = toInteger(pos);
    var l = s.length;
    var a, b;
    if (i < 0 || i >= l) return TO_STRING ? '' : undefined;
    a = s.charCodeAt(i);
    return a < 0xd800 || a > 0xdbff || i + 1 === l || (b = s.charCodeAt(i + 1)) < 0xdc00 || b > 0xdfff
      ? TO_STRING ? s.charAt(i) : a
      : TO_STRING ? s.slice(i, i + 2) : (a - 0xd800 << 10) + (b - 0xdc00) + 0x10000;
  };
};


/***/ }),

/***/ "765d":
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("6718")('observable');


/***/ }),

/***/ "7726":
/***/ (function(module, exports) {

// https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
var global = module.exports = typeof window != 'undefined' && window.Math == Math
  ? window : typeof self != 'undefined' && self.Math == Math ? self
  // eslint-disable-next-line no-new-func
  : Function('return this')();
if (typeof __g == 'number') __g = global; // eslint-disable-line no-undef


/***/ }),

/***/ "774e":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("d2d5");

/***/ }),

/***/ "7782":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var LIBRARY = __webpack_require__("a6b3");
var $export = __webpack_require__("094c");
var redefine = __webpack_require__("3143");
var hide = __webpack_require__("59c4");
var Iterators = __webpack_require__("16f9");
var $iterCreate = __webpack_require__("aa24");
var setToStringTag = __webpack_require__("09e3");
var getPrototypeOf = __webpack_require__("0f30");
var ITERATOR = __webpack_require__("fe95")('iterator');
var BUGGY = !([].keys && 'next' in [].keys()); // Safari has buggy iterators w/o `next`
var FF_ITERATOR = '@@iterator';
var KEYS = 'keys';
var VALUES = 'values';

var returnThis = function () { return this; };

module.exports = function (Base, NAME, Constructor, next, DEFAULT, IS_SET, FORCED) {
  $iterCreate(Constructor, NAME, next);
  var getMethod = function (kind) {
    if (!BUGGY && kind in proto) return proto[kind];
    switch (kind) {
      case KEYS: return function keys() { return new Constructor(this, kind); };
      case VALUES: return function values() { return new Constructor(this, kind); };
    } return function entries() { return new Constructor(this, kind); };
  };
  var TAG = NAME + ' Iterator';
  var DEF_VALUES = DEFAULT == VALUES;
  var VALUES_BUG = false;
  var proto = Base.prototype;
  var $native = proto[ITERATOR] || proto[FF_ITERATOR] || DEFAULT && proto[DEFAULT];
  var $default = $native || getMethod(DEFAULT);
  var $entries = DEFAULT ? !DEF_VALUES ? $default : getMethod('entries') : undefined;
  var $anyNative = NAME == 'Array' ? proto.entries || $native : $native;
  var methods, key, IteratorPrototype;
  // Fix native
  if ($anyNative) {
    IteratorPrototype = getPrototypeOf($anyNative.call(new Base()));
    if (IteratorPrototype !== Object.prototype && IteratorPrototype.next) {
      // Set @@toStringTag to native iterators
      setToStringTag(IteratorPrototype, TAG, true);
      // fix for some old engines
      if (!LIBRARY && typeof IteratorPrototype[ITERATOR] != 'function') hide(IteratorPrototype, ITERATOR, returnThis);
    }
  }
  // fix Array#{values, @@iterator}.name in V8 / FF
  if (DEF_VALUES && $native && $native.name !== VALUES) {
    VALUES_BUG = true;
    $default = function values() { return $native.call(this); };
  }
  // Define iterator
  if ((!LIBRARY || FORCED) && (BUGGY || VALUES_BUG || !proto[ITERATOR])) {
    hide(proto, ITERATOR, $default);
  }
  // Plug for library
  Iterators[NAME] = $default;
  Iterators[TAG] = returnThis;
  if (DEFAULT) {
    methods = {
      values: DEF_VALUES ? $default : getMethod(VALUES),
      keys: IS_SET ? $default : getMethod(KEYS),
      entries: $entries
    };
    if (FORCED) for (key in methods) {
      if (!(key in proto)) redefine(proto, key, methods[key]);
    } else $export($export.P + $export.F * (BUGGY || VALUES_BUG), NAME, methods);
  }
  return methods;
};


/***/ }),

/***/ "78a3":
/***/ (function(module, exports, __webpack_require__) {

module.exports = !__webpack_require__("7e81") && !__webpack_require__("7eae")(function () {
  return Object.defineProperty(__webpack_require__("7d9c")('div'), 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),

/***/ "794b":
/***/ (function(module, exports, __webpack_require__) {

module.exports = !__webpack_require__("8e60") && !__webpack_require__("294c")(function () {
  return Object.defineProperty(__webpack_require__("1ec9")('div'), 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),

/***/ "795b":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("696e");

/***/ }),

/***/ "79aa":
/***/ (function(module, exports) {

module.exports = function (it) {
  if (typeof it != 'function') throw TypeError(it + ' is not a function!');
  return it;
};


/***/ }),

/***/ "79e5":
/***/ (function(module, exports) {

module.exports = function (exec) {
  try {
    return !!exec();
  } catch (e) {
    return true;
  }
};


/***/ }),

/***/ "7a1e":
/***/ (function(module, exports) {

var core = module.exports = { version: '2.6.1' };
if (typeof __e == 'number') __e = core; // eslint-disable-line no-undef


/***/ }),

/***/ "7cd6":
/***/ (function(module, exports, __webpack_require__) {

var classof = __webpack_require__("40c3");
var ITERATOR = __webpack_require__("5168")('iterator');
var Iterators = __webpack_require__("481b");
module.exports = __webpack_require__("584a").getIteratorMethod = function (it) {
  if (it != undefined) return it[ITERATOR]
    || it['@@iterator']
    || Iterators[classof(it)];
};


/***/ }),

/***/ "7d7b":
/***/ (function(module, exports, __webpack_require__) {

var anObject = __webpack_require__("e4ae");
var get = __webpack_require__("7cd6");
module.exports = __webpack_require__("584a").getIterator = function (it) {
  var iterFn = get(it);
  if (typeof iterFn != 'function') throw TypeError(it + ' is not iterable!');
  return anObject(iterFn.call(it));
};


/***/ }),

/***/ "7d9c":
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__("9691");
var document = __webpack_require__("635a").document;
// typeof document.createElement is 'object' in old IE
var is = isObject(document) && isObject(document.createElement);
module.exports = function (it) {
  return is ? document.createElement(it) : {};
};


/***/ }),

/***/ "7e81":
/***/ (function(module, exports, __webpack_require__) {

// Thank's IE8 for his funny defineProperty
module.exports = !__webpack_require__("7eae")(function () {
  return Object.defineProperty({}, 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),

/***/ "7e90":
/***/ (function(module, exports, __webpack_require__) {

var dP = __webpack_require__("d9f6");
var anObject = __webpack_require__("e4ae");
var getKeys = __webpack_require__("c3a1");

module.exports = __webpack_require__("8e60") ? Object.defineProperties : function defineProperties(O, Properties) {
  anObject(O);
  var keys = getKeys(Properties);
  var length = keys.length;
  var i = 0;
  var P;
  while (length > i) dP.f(O, P = keys[i++], Properties[P]);
  return O;
};


/***/ }),

/***/ "7eae":
/***/ (function(module, exports) {

module.exports = function (exec) {
  try {
    return !!exec();
  } catch (e) {
    return true;
  }
};


/***/ }),

/***/ "7f72":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var regexpFlags = __webpack_require__("c26b");

var nativeExec = RegExp.prototype.exec;
// This always refers to the native implementation, because the
// String#replace polyfill uses ./fix-regexp-well-known-symbol-logic.js,
// which loads this file before patching the method.
var nativeReplace = String.prototype.replace;

var patchedExec = nativeExec;

var LAST_INDEX = 'lastIndex';

var UPDATES_LAST_INDEX_WRONG = (function () {
  var re1 = /a/,
      re2 = /b*/g;
  nativeExec.call(re1, 'a');
  nativeExec.call(re2, 'a');
  return re1[LAST_INDEX] !== 0 || re2[LAST_INDEX] !== 0;
})();

// nonparticipating capturing group, copied from es5-shim's String#split patch.
var NPCG_INCLUDED = /()??/.exec('')[1] !== undefined;

var PATCH = UPDATES_LAST_INDEX_WRONG || NPCG_INCLUDED;

if (PATCH) {
  patchedExec = function exec(str) {
    var re = this;
    var lastIndex, reCopy, match, i;

    if (NPCG_INCLUDED) {
      reCopy = new RegExp('^' + re.source + '$(?!\\s)', regexpFlags.call(re));
    }
    if (UPDATES_LAST_INDEX_WRONG) lastIndex = re[LAST_INDEX];

    match = nativeExec.call(re, str);

    if (UPDATES_LAST_INDEX_WRONG && match) {
      re[LAST_INDEX] = re.global ? match.index + match[0].length : lastIndex;
    }
    if (NPCG_INCLUDED && match && match.length > 1) {
      // Fix browsers whose `exec` methods don't consistently return `undefined`
      // for NPCG, like IE8. NOTE: This doesn' work for /(.?)?/
      // eslint-disable-next-line no-loop-func
      nativeReplace.call(match[0], reCopy, function () {
        for (i = 1; i < arguments.length - 2; i++) {
          if (arguments[i] === undefined) match[i] = undefined;
        }
      });
    }

    return match;
  };
}

module.exports = patchedExec;


/***/ }),

/***/ "8378":
/***/ (function(module, exports) {

var core = module.exports = { version: '2.6.2' };
if (typeof __e == 'number') __e = core; // eslint-disable-line no-undef


/***/ }),

/***/ "8436":
/***/ (function(module, exports) {

module.exports = function () { /* empty */ };


/***/ }),

/***/ "85f2":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("454f");

/***/ }),

/***/ "86cc":
/***/ (function(module, exports, __webpack_require__) {

var anObject = __webpack_require__("cb7c");
var IE8_DOM_DEFINE = __webpack_require__("c69a");
var toPrimitive = __webpack_require__("6a99");
var dP = Object.defineProperty;

exports.f = __webpack_require__("9e1e") ? Object.defineProperty : function defineProperty(O, P, Attributes) {
  anObject(O);
  P = toPrimitive(P, true);
  anObject(Attributes);
  if (IE8_DOM_DEFINE) try {
    return dP(O, P, Attributes);
  } catch (e) { /* empty */ }
  if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported!');
  if ('value' in Attributes) O[P] = Attributes.value;
  return O;
};


/***/ }),

/***/ "8993":
/***/ (function(module, exports, __webpack_require__) {

var _Symbol$iterator = __webpack_require__("5d58");

var _Symbol = __webpack_require__("67bb");

function _typeof2(obj) { if (typeof _Symbol === "function" && typeof _Symbol$iterator === "symbol") { _typeof2 = function _typeof2(obj) { return typeof obj; }; } else { _typeof2 = function _typeof2(obj) { return obj && typeof _Symbol === "function" && obj.constructor === _Symbol && obj !== _Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof2(obj); }

function _typeof(obj) {
  if (typeof _Symbol === "function" && _typeof2(_Symbol$iterator) === "symbol") {
    module.exports = _typeof = function _typeof(obj) {
      return _typeof2(obj);
    };
  } else {
    module.exports = _typeof = function _typeof(obj) {
      return obj && typeof _Symbol === "function" && obj.constructor === _Symbol && obj !== _Symbol.prototype ? "symbol" : _typeof2(obj);
    };
  }

  return _typeof(obj);
}

module.exports = _typeof;

/***/ }),

/***/ "89a3":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

__webpack_require__("03dd");
var anObject = __webpack_require__("b597");
var $flags = __webpack_require__("c26b");
var DESCRIPTORS = __webpack_require__("7e81");
var TO_STRING = 'toString';
var $toString = /./[TO_STRING];

var define = function (fn) {
  __webpack_require__("3143")(RegExp.prototype, TO_STRING, fn, true);
};

// 21.2.5.14 RegExp.prototype.toString()
if (__webpack_require__("7eae")(function () { return $toString.call({ source: 'a', flags: 'b' }) != '/a/b'; })) {
  define(function toString() {
    var R = anObject(this);
    return '/'.concat(R.source, '/',
      'flags' in R ? R.flags : !DESCRIPTORS && R instanceof RegExp ? $flags.call(R) : undefined);
  });
// FF44- RegExp#toString has a wrong name
} else if ($toString.name != TO_STRING) {
  define(function toString() {
    return $toString.call(this);
  });
}


/***/ }),

/***/ "8aae":
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("32a6");
module.exports = __webpack_require__("584a").Object.keys;


/***/ }),

/***/ "8bbf":
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE__8bbf__;

/***/ }),

/***/ "8cc2":
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.2 / 15.2.3.5 Object.create(O [, Properties])
var anObject = __webpack_require__("b597");
var dPs = __webpack_require__("3e0f");
var enumBugKeys = __webpack_require__("2118");
var IE_PROTO = __webpack_require__("037a")('IE_PROTO');
var Empty = function () { /* empty */ };
var PROTOTYPE = 'prototype';

// Create object with fake `null` prototype: use iframe Object with cleared prototype
var createDict = function () {
  // Thrash, waste and sodomy: IE GC bug
  var iframe = __webpack_require__("7d9c")('iframe');
  var i = enumBugKeys.length;
  var lt = '<';
  var gt = '>';
  var iframeDocument;
  iframe.style.display = 'none';
  __webpack_require__("9561").appendChild(iframe);
  iframe.src = 'javascript:'; // eslint-disable-line no-script-url
  // createDict = iframe.contentWindow.Object;
  // html.removeChild(iframe);
  iframeDocument = iframe.contentWindow.document;
  iframeDocument.open();
  iframeDocument.write(lt + 'script' + gt + 'document.F=Object' + lt + '/script' + gt);
  iframeDocument.close();
  createDict = iframeDocument.F;
  while (i--) delete createDict[PROTOTYPE][enumBugKeys[i]];
  return createDict();
};

module.exports = Object.create || function create(O, Properties) {
  var result;
  if (O !== null) {
    Empty[PROTOTYPE] = anObject(O);
    result = new Empty();
    Empty[PROTOTYPE] = null;
    // add "__proto__" for Object.getPrototypeOf polyfill
    result[IE_PROTO] = O;
  } else result = createDict();
  return Properties === undefined ? result : dPs(result, Properties);
};


/***/ }),

/***/ "8dd0":
/***/ (function(module, exports, __webpack_require__) {

// 7.3.20 SpeciesConstructor(O, defaultConstructor)
var anObject = __webpack_require__("b597");
var aFunction = __webpack_require__("fc4a");
var SPECIES = __webpack_require__("fe95")('species');
module.exports = function (O, D) {
  var C = anObject(O).constructor;
  var S;
  return C === undefined || (S = anObject(C)[SPECIES]) == undefined ? D : aFunction(S);
};


/***/ }),

/***/ "8e60":
/***/ (function(module, exports, __webpack_require__) {

// Thank's IE8 for his funny defineProperty
module.exports = !__webpack_require__("294c")(function () {
  return Object.defineProperty({}, 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),

/***/ "8f60":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var create = __webpack_require__("a159");
var descriptor = __webpack_require__("aebd");
var setToStringTag = __webpack_require__("45f2");
var IteratorPrototype = {};

// 25.1.2.1.1 %IteratorPrototype%[@@iterator]()
__webpack_require__("35e8")(IteratorPrototype, __webpack_require__("5168")('iterator'), function () { return this; });

module.exports = function (Constructor, NAME, next) {
  Constructor.prototype = create(IteratorPrototype, { next: descriptor(1, next) });
  setToStringTag(Constructor, NAME + ' Iterator');
};


/***/ }),

/***/ "8feb":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("4910");

/***/ }),

/***/ "9003":
/***/ (function(module, exports, __webpack_require__) {

// 7.2.2 IsArray(argument)
var cof = __webpack_require__("6b4c");
module.exports = Array.isArray || function isArray(arg) {
  return cof(arg) == 'Array';
};


/***/ }),

/***/ "9138":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("35e8");


/***/ }),

/***/ "9306":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// 19.1.2.1 Object.assign(target, source, ...)
var getKeys = __webpack_require__("c3a1");
var gOPS = __webpack_require__("9aa9");
var pIE = __webpack_require__("355d");
var toObject = __webpack_require__("241e");
var IObject = __webpack_require__("335c");
var $assign = Object.assign;

// should work with symbols and should have deterministic property order (V8 bug)
module.exports = !$assign || __webpack_require__("294c")(function () {
  var A = {};
  var B = {};
  // eslint-disable-next-line no-undef
  var S = Symbol();
  var K = 'abcdefghijklmnopqrst';
  A[S] = 7;
  K.split('').forEach(function (k) { B[k] = k; });
  return $assign({}, A)[S] != 7 || Object.keys($assign({}, B)).join('') != K;
}) ? function assign(target, source) { // eslint-disable-line no-unused-vars
  var T = toObject(target);
  var aLen = arguments.length;
  var index = 1;
  var getSymbols = gOPS.f;
  var isEnum = pIE.f;
  while (aLen > index) {
    var S = IObject(arguments[index++]);
    var keys = getSymbols ? getKeys(S).concat(getSymbols(S)) : getKeys(S);
    var length = keys.length;
    var j = 0;
    var key;
    while (length > j) if (isEnum.call(S, key = keys[j++])) T[key] = S[key];
  } return T;
} : $assign;


/***/ }),

/***/ "935a":
/***/ (function(module, exports) {

module.exports = function (done, value) {
  return { value: value, done: !!done };
};


/***/ }),

/***/ "9427":
/***/ (function(module, exports, __webpack_require__) {

var $export = __webpack_require__("63b6");
// 19.1.2.2 / 15.2.3.5 Object.create(O [, Properties])
$export($export.S, 'Object', { create: __webpack_require__("a159") });


/***/ }),

/***/ "9541":
/***/ (function(module, exports, __webpack_require__) {

var dP = __webpack_require__("120e").f;
var FProto = Function.prototype;
var nameRE = /^\s*function ([^ (]*)/;
var NAME = 'name';

// 19.2.4.2 name
NAME in FProto || __webpack_require__("7e81") && dP(FProto, NAME, {
  configurable: true,
  get: function () {
    try {
      return ('' + this).match(nameRE)[1];
    } catch (e) {
      return '';
    }
  }
});


/***/ }),

/***/ "9561":
/***/ (function(module, exports, __webpack_require__) {

var document = __webpack_require__("635a").document;
module.exports = document && document.documentElement;


/***/ }),

/***/ "95d5":
/***/ (function(module, exports, __webpack_require__) {

var classof = __webpack_require__("40c3");
var ITERATOR = __webpack_require__("5168")('iterator');
var Iterators = __webpack_require__("481b");
module.exports = __webpack_require__("584a").isIterable = function (it) {
  var O = Object(it);
  return O[ITERATOR] !== undefined
    || '@@iterator' in O
    // eslint-disable-next-line no-prototype-builtins
    || Iterators.hasOwnProperty(classof(O));
};


/***/ }),

/***/ "9691":
/***/ (function(module, exports) {

module.exports = function (it) {
  return typeof it === 'object' ? it !== null : typeof it === 'function';
};


/***/ }),

/***/ "96cf":
/***/ (function(module, exports) {

/**
 * Copyright (c) 2014-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

!(function(global) {
  "use strict";

  var Op = Object.prototype;
  var hasOwn = Op.hasOwnProperty;
  var undefined; // More compressible than void 0.
  var $Symbol = typeof Symbol === "function" ? Symbol : {};
  var iteratorSymbol = $Symbol.iterator || "@@iterator";
  var asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator";
  var toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag";

  var inModule = typeof module === "object";
  var runtime = global.regeneratorRuntime;
  if (runtime) {
    if (inModule) {
      // If regeneratorRuntime is defined globally and we're in a module,
      // make the exports object identical to regeneratorRuntime.
      module.exports = runtime;
    }
    // Don't bother evaluating the rest of this file if the runtime was
    // already defined globally.
    return;
  }

  // Define the runtime globally (as expected by generated code) as either
  // module.exports (if we're in a module) or a new, empty object.
  runtime = global.regeneratorRuntime = inModule ? module.exports : {};

  function wrap(innerFn, outerFn, self, tryLocsList) {
    // If outerFn provided and outerFn.prototype is a Generator, then outerFn.prototype instanceof Generator.
    var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator;
    var generator = Object.create(protoGenerator.prototype);
    var context = new Context(tryLocsList || []);

    // The ._invoke method unifies the implementations of the .next,
    // .throw, and .return methods.
    generator._invoke = makeInvokeMethod(innerFn, self, context);

    return generator;
  }
  runtime.wrap = wrap;

  // Try/catch helper to minimize deoptimizations. Returns a completion
  // record like context.tryEntries[i].completion. This interface could
  // have been (and was previously) designed to take a closure to be
  // invoked without arguments, but in all the cases we care about we
  // already have an existing method we want to call, so there's no need
  // to create a new function object. We can even get away with assuming
  // the method takes exactly one argument, since that happens to be true
  // in every case, so we don't have to touch the arguments object. The
  // only additional allocation required is the completion record, which
  // has a stable shape and so hopefully should be cheap to allocate.
  function tryCatch(fn, obj, arg) {
    try {
      return { type: "normal", arg: fn.call(obj, arg) };
    } catch (err) {
      return { type: "throw", arg: err };
    }
  }

  var GenStateSuspendedStart = "suspendedStart";
  var GenStateSuspendedYield = "suspendedYield";
  var GenStateExecuting = "executing";
  var GenStateCompleted = "completed";

  // Returning this object from the innerFn has the same effect as
  // breaking out of the dispatch switch statement.
  var ContinueSentinel = {};

  // Dummy constructor functions that we use as the .constructor and
  // .constructor.prototype properties for functions that return Generator
  // objects. For full spec compliance, you may wish to configure your
  // minifier not to mangle the names of these two functions.
  function Generator() {}
  function GeneratorFunction() {}
  function GeneratorFunctionPrototype() {}

  // This is a polyfill for %IteratorPrototype% for environments that
  // don't natively support it.
  var IteratorPrototype = {};
  IteratorPrototype[iteratorSymbol] = function () {
    return this;
  };

  var getProto = Object.getPrototypeOf;
  var NativeIteratorPrototype = getProto && getProto(getProto(values([])));
  if (NativeIteratorPrototype &&
      NativeIteratorPrototype !== Op &&
      hasOwn.call(NativeIteratorPrototype, iteratorSymbol)) {
    // This environment has a native %IteratorPrototype%; use it instead
    // of the polyfill.
    IteratorPrototype = NativeIteratorPrototype;
  }

  var Gp = GeneratorFunctionPrototype.prototype =
    Generator.prototype = Object.create(IteratorPrototype);
  GeneratorFunction.prototype = Gp.constructor = GeneratorFunctionPrototype;
  GeneratorFunctionPrototype.constructor = GeneratorFunction;
  GeneratorFunctionPrototype[toStringTagSymbol] =
    GeneratorFunction.displayName = "GeneratorFunction";

  // Helper for defining the .next, .throw, and .return methods of the
  // Iterator interface in terms of a single ._invoke method.
  function defineIteratorMethods(prototype) {
    ["next", "throw", "return"].forEach(function(method) {
      prototype[method] = function(arg) {
        return this._invoke(method, arg);
      };
    });
  }

  runtime.isGeneratorFunction = function(genFun) {
    var ctor = typeof genFun === "function" && genFun.constructor;
    return ctor
      ? ctor === GeneratorFunction ||
        // For the native GeneratorFunction constructor, the best we can
        // do is to check its .name property.
        (ctor.displayName || ctor.name) === "GeneratorFunction"
      : false;
  };

  runtime.mark = function(genFun) {
    if (Object.setPrototypeOf) {
      Object.setPrototypeOf(genFun, GeneratorFunctionPrototype);
    } else {
      genFun.__proto__ = GeneratorFunctionPrototype;
      if (!(toStringTagSymbol in genFun)) {
        genFun[toStringTagSymbol] = "GeneratorFunction";
      }
    }
    genFun.prototype = Object.create(Gp);
    return genFun;
  };

  // Within the body of any async function, `await x` is transformed to
  // `yield regeneratorRuntime.awrap(x)`, so that the runtime can test
  // `hasOwn.call(value, "__await")` to determine if the yielded value is
  // meant to be awaited.
  runtime.awrap = function(arg) {
    return { __await: arg };
  };

  function AsyncIterator(generator) {
    function invoke(method, arg, resolve, reject) {
      var record = tryCatch(generator[method], generator, arg);
      if (record.type === "throw") {
        reject(record.arg);
      } else {
        var result = record.arg;
        var value = result.value;
        if (value &&
            typeof value === "object" &&
            hasOwn.call(value, "__await")) {
          return Promise.resolve(value.__await).then(function(value) {
            invoke("next", value, resolve, reject);
          }, function(err) {
            invoke("throw", err, resolve, reject);
          });
        }

        return Promise.resolve(value).then(function(unwrapped) {
          // When a yielded Promise is resolved, its final value becomes
          // the .value of the Promise<{value,done}> result for the
          // current iteration.
          result.value = unwrapped;
          resolve(result);
        }, function(error) {
          // If a rejected Promise was yielded, throw the rejection back
          // into the async generator function so it can be handled there.
          return invoke("throw", error, resolve, reject);
        });
      }
    }

    var previousPromise;

    function enqueue(method, arg) {
      function callInvokeWithMethodAndArg() {
        return new Promise(function(resolve, reject) {
          invoke(method, arg, resolve, reject);
        });
      }

      return previousPromise =
        // If enqueue has been called before, then we want to wait until
        // all previous Promises have been resolved before calling invoke,
        // so that results are always delivered in the correct order. If
        // enqueue has not been called before, then it is important to
        // call invoke immediately, without waiting on a callback to fire,
        // so that the async generator function has the opportunity to do
        // any necessary setup in a predictable way. This predictability
        // is why the Promise constructor synchronously invokes its
        // executor callback, and why async functions synchronously
        // execute code before the first await. Since we implement simple
        // async functions in terms of async generators, it is especially
        // important to get this right, even though it requires care.
        previousPromise ? previousPromise.then(
          callInvokeWithMethodAndArg,
          // Avoid propagating failures to Promises returned by later
          // invocations of the iterator.
          callInvokeWithMethodAndArg
        ) : callInvokeWithMethodAndArg();
    }

    // Define the unified helper method that is used to implement .next,
    // .throw, and .return (see defineIteratorMethods).
    this._invoke = enqueue;
  }

  defineIteratorMethods(AsyncIterator.prototype);
  AsyncIterator.prototype[asyncIteratorSymbol] = function () {
    return this;
  };
  runtime.AsyncIterator = AsyncIterator;

  // Note that simple async functions are implemented on top of
  // AsyncIterator objects; they just return a Promise for the value of
  // the final result produced by the iterator.
  runtime.async = function(innerFn, outerFn, self, tryLocsList) {
    var iter = new AsyncIterator(
      wrap(innerFn, outerFn, self, tryLocsList)
    );

    return runtime.isGeneratorFunction(outerFn)
      ? iter // If outerFn is a generator, return the full iterator.
      : iter.next().then(function(result) {
          return result.done ? result.value : iter.next();
        });
  };

  function makeInvokeMethod(innerFn, self, context) {
    var state = GenStateSuspendedStart;

    return function invoke(method, arg) {
      if (state === GenStateExecuting) {
        throw new Error("Generator is already running");
      }

      if (state === GenStateCompleted) {
        if (method === "throw") {
          throw arg;
        }

        // Be forgiving, per 25.3.3.3.3 of the spec:
        // https://people.mozilla.org/~jorendorff/es6-draft.html#sec-generatorresume
        return doneResult();
      }

      context.method = method;
      context.arg = arg;

      while (true) {
        var delegate = context.delegate;
        if (delegate) {
          var delegateResult = maybeInvokeDelegate(delegate, context);
          if (delegateResult) {
            if (delegateResult === ContinueSentinel) continue;
            return delegateResult;
          }
        }

        if (context.method === "next") {
          // Setting context._sent for legacy support of Babel's
          // function.sent implementation.
          context.sent = context._sent = context.arg;

        } else if (context.method === "throw") {
          if (state === GenStateSuspendedStart) {
            state = GenStateCompleted;
            throw context.arg;
          }

          context.dispatchException(context.arg);

        } else if (context.method === "return") {
          context.abrupt("return", context.arg);
        }

        state = GenStateExecuting;

        var record = tryCatch(innerFn, self, context);
        if (record.type === "normal") {
          // If an exception is thrown from innerFn, we leave state ===
          // GenStateExecuting and loop back for another invocation.
          state = context.done
            ? GenStateCompleted
            : GenStateSuspendedYield;

          if (record.arg === ContinueSentinel) {
            continue;
          }

          return {
            value: record.arg,
            done: context.done
          };

        } else if (record.type === "throw") {
          state = GenStateCompleted;
          // Dispatch the exception by looping back around to the
          // context.dispatchException(context.arg) call above.
          context.method = "throw";
          context.arg = record.arg;
        }
      }
    };
  }

  // Call delegate.iterator[context.method](context.arg) and handle the
  // result, either by returning a { value, done } result from the
  // delegate iterator, or by modifying context.method and context.arg,
  // setting context.delegate to null, and returning the ContinueSentinel.
  function maybeInvokeDelegate(delegate, context) {
    var method = delegate.iterator[context.method];
    if (method === undefined) {
      // A .throw or .return when the delegate iterator has no .throw
      // method always terminates the yield* loop.
      context.delegate = null;

      if (context.method === "throw") {
        if (delegate.iterator.return) {
          // If the delegate iterator has a return method, give it a
          // chance to clean up.
          context.method = "return";
          context.arg = undefined;
          maybeInvokeDelegate(delegate, context);

          if (context.method === "throw") {
            // If maybeInvokeDelegate(context) changed context.method from
            // "return" to "throw", let that override the TypeError below.
            return ContinueSentinel;
          }
        }

        context.method = "throw";
        context.arg = new TypeError(
          "The iterator does not provide a 'throw' method");
      }

      return ContinueSentinel;
    }

    var record = tryCatch(method, delegate.iterator, context.arg);

    if (record.type === "throw") {
      context.method = "throw";
      context.arg = record.arg;
      context.delegate = null;
      return ContinueSentinel;
    }

    var info = record.arg;

    if (! info) {
      context.method = "throw";
      context.arg = new TypeError("iterator result is not an object");
      context.delegate = null;
      return ContinueSentinel;
    }

    if (info.done) {
      // Assign the result of the finished delegate to the temporary
      // variable specified by delegate.resultName (see delegateYield).
      context[delegate.resultName] = info.value;

      // Resume execution at the desired location (see delegateYield).
      context.next = delegate.nextLoc;

      // If context.method was "throw" but the delegate handled the
      // exception, let the outer generator proceed normally. If
      // context.method was "next", forget context.arg since it has been
      // "consumed" by the delegate iterator. If context.method was
      // "return", allow the original .return call to continue in the
      // outer generator.
      if (context.method !== "return") {
        context.method = "next";
        context.arg = undefined;
      }

    } else {
      // Re-yield the result returned by the delegate method.
      return info;
    }

    // The delegate iterator is finished, so forget it and continue with
    // the outer generator.
    context.delegate = null;
    return ContinueSentinel;
  }

  // Define Generator.prototype.{next,throw,return} in terms of the
  // unified ._invoke helper method.
  defineIteratorMethods(Gp);

  Gp[toStringTagSymbol] = "Generator";

  // A Generator should always return itself as the iterator object when the
  // @@iterator function is called on it. Some browsers' implementations of the
  // iterator prototype chain incorrectly implement this, causing the Generator
  // object to not be returned from this call. This ensures that doesn't happen.
  // See https://github.com/facebook/regenerator/issues/274 for more details.
  Gp[iteratorSymbol] = function() {
    return this;
  };

  Gp.toString = function() {
    return "[object Generator]";
  };

  function pushTryEntry(locs) {
    var entry = { tryLoc: locs[0] };

    if (1 in locs) {
      entry.catchLoc = locs[1];
    }

    if (2 in locs) {
      entry.finallyLoc = locs[2];
      entry.afterLoc = locs[3];
    }

    this.tryEntries.push(entry);
  }

  function resetTryEntry(entry) {
    var record = entry.completion || {};
    record.type = "normal";
    delete record.arg;
    entry.completion = record;
  }

  function Context(tryLocsList) {
    // The root entry object (effectively a try statement without a catch
    // or a finally block) gives us a place to store values thrown from
    // locations where there is no enclosing try statement.
    this.tryEntries = [{ tryLoc: "root" }];
    tryLocsList.forEach(pushTryEntry, this);
    this.reset(true);
  }

  runtime.keys = function(object) {
    var keys = [];
    for (var key in object) {
      keys.push(key);
    }
    keys.reverse();

    // Rather than returning an object with a next method, we keep
    // things simple and return the next function itself.
    return function next() {
      while (keys.length) {
        var key = keys.pop();
        if (key in object) {
          next.value = key;
          next.done = false;
          return next;
        }
      }

      // To avoid creating an additional object, we just hang the .value
      // and .done properties off the next function object itself. This
      // also ensures that the minifier will not anonymize the function.
      next.done = true;
      return next;
    };
  };

  function values(iterable) {
    if (iterable) {
      var iteratorMethod = iterable[iteratorSymbol];
      if (iteratorMethod) {
        return iteratorMethod.call(iterable);
      }

      if (typeof iterable.next === "function") {
        return iterable;
      }

      if (!isNaN(iterable.length)) {
        var i = -1, next = function next() {
          while (++i < iterable.length) {
            if (hasOwn.call(iterable, i)) {
              next.value = iterable[i];
              next.done = false;
              return next;
            }
          }

          next.value = undefined;
          next.done = true;

          return next;
        };

        return next.next = next;
      }
    }

    // Return an iterator with no values.
    return { next: doneResult };
  }
  runtime.values = values;

  function doneResult() {
    return { value: undefined, done: true };
  }

  Context.prototype = {
    constructor: Context,

    reset: function(skipTempReset) {
      this.prev = 0;
      this.next = 0;
      // Resetting context._sent for legacy support of Babel's
      // function.sent implementation.
      this.sent = this._sent = undefined;
      this.done = false;
      this.delegate = null;

      this.method = "next";
      this.arg = undefined;

      this.tryEntries.forEach(resetTryEntry);

      if (!skipTempReset) {
        for (var name in this) {
          // Not sure about the optimal order of these conditions:
          if (name.charAt(0) === "t" &&
              hasOwn.call(this, name) &&
              !isNaN(+name.slice(1))) {
            this[name] = undefined;
          }
        }
      }
    },

    stop: function() {
      this.done = true;

      var rootEntry = this.tryEntries[0];
      var rootRecord = rootEntry.completion;
      if (rootRecord.type === "throw") {
        throw rootRecord.arg;
      }

      return this.rval;
    },

    dispatchException: function(exception) {
      if (this.done) {
        throw exception;
      }

      var context = this;
      function handle(loc, caught) {
        record.type = "throw";
        record.arg = exception;
        context.next = loc;

        if (caught) {
          // If the dispatched exception was caught by a catch block,
          // then let that catch block handle the exception normally.
          context.method = "next";
          context.arg = undefined;
        }

        return !! caught;
      }

      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        var record = entry.completion;

        if (entry.tryLoc === "root") {
          // Exception thrown outside of any try block that could handle
          // it, so set the completion value of the entire function to
          // throw the exception.
          return handle("end");
        }

        if (entry.tryLoc <= this.prev) {
          var hasCatch = hasOwn.call(entry, "catchLoc");
          var hasFinally = hasOwn.call(entry, "finallyLoc");

          if (hasCatch && hasFinally) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            } else if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else if (hasCatch) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            }

          } else if (hasFinally) {
            if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else {
            throw new Error("try statement without catch or finally");
          }
        }
      }
    },

    abrupt: function(type, arg) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc <= this.prev &&
            hasOwn.call(entry, "finallyLoc") &&
            this.prev < entry.finallyLoc) {
          var finallyEntry = entry;
          break;
        }
      }

      if (finallyEntry &&
          (type === "break" ||
           type === "continue") &&
          finallyEntry.tryLoc <= arg &&
          arg <= finallyEntry.finallyLoc) {
        // Ignore the finally entry if control is not jumping to a
        // location outside the try/catch block.
        finallyEntry = null;
      }

      var record = finallyEntry ? finallyEntry.completion : {};
      record.type = type;
      record.arg = arg;

      if (finallyEntry) {
        this.method = "next";
        this.next = finallyEntry.finallyLoc;
        return ContinueSentinel;
      }

      return this.complete(record);
    },

    complete: function(record, afterLoc) {
      if (record.type === "throw") {
        throw record.arg;
      }

      if (record.type === "break" ||
          record.type === "continue") {
        this.next = record.arg;
      } else if (record.type === "return") {
        this.rval = this.arg = record.arg;
        this.method = "return";
        this.next = "end";
      } else if (record.type === "normal" && afterLoc) {
        this.next = afterLoc;
      }

      return ContinueSentinel;
    },

    finish: function(finallyLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.finallyLoc === finallyLoc) {
          this.complete(entry.completion, entry.afterLoc);
          resetTryEntry(entry);
          return ContinueSentinel;
        }
      }
    },

    "catch": function(tryLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc === tryLoc) {
          var record = entry.completion;
          if (record.type === "throw") {
            var thrown = record.arg;
            resetTryEntry(entry);
          }
          return thrown;
        }
      }

      // The context.catch method must only be called with a location
      // argument that corresponds to a known catch block.
      throw new Error("illegal catch attempt");
    },

    delegateYield: function(iterable, resultName, nextLoc) {
      this.delegate = {
        iterator: values(iterable),
        resultName: resultName,
        nextLoc: nextLoc
      };

      if (this.method === "next") {
        // Deliberately forget the last sent value so that we don't
        // accidentally pass it on to the delegate.
        this.arg = undefined;
      }

      return ContinueSentinel;
    }
  };
})(
  // In sloppy mode, unbound `this` refers to the global object, fallback to
  // Function constructor if we're in global strict mode. That is sadly a form
  // of indirect eval which violates Content Security Policy.
  (function() {
    return this || (typeof self === "object" && self);
  })() || Function("return this")()
);


/***/ }),

/***/ "9a1a":
/***/ (function(module, exports, __webpack_require__) {

var toInteger = __webpack_require__("58ce");
var defined = __webpack_require__("0f31");
// true  -> String#at
// false -> String#codePointAt
module.exports = function (TO_STRING) {
  return function (that, pos) {
    var s = String(defined(that));
    var i = toInteger(pos);
    var l = s.length;
    var a, b;
    if (i < 0 || i >= l) return TO_STRING ? '' : undefined;
    a = s.charCodeAt(i);
    return a < 0xd800 || a > 0xdbff || i + 1 === l || (b = s.charCodeAt(i + 1)) < 0xdc00 || b > 0xdfff
      ? TO_STRING ? s.charAt(i) : a
      : TO_STRING ? s.slice(i, i + 2) : (a - 0xd800 << 10) + (b - 0xdc00) + 0x10000;
  };
};


/***/ }),

/***/ "9aa9":
/***/ (function(module, exports) {

exports.f = Object.getOwnPropertySymbols;


/***/ }),

/***/ "9b2f":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var anObject = __webpack_require__("b597");
var toLength = __webpack_require__("b4df");
var advanceStringIndex = __webpack_require__("dd44");
var regExpExec = __webpack_require__("d25d");

// @@match logic
__webpack_require__("1fbc")('match', 1, function (defined, MATCH, $match, maybeCallNative) {
  return [
    // `String.prototype.match` method
    // https://tc39.github.io/ecma262/#sec-string.prototype.match
    function match(regexp) {
      var O = defined(this);
      var fn = regexp == undefined ? undefined : regexp[MATCH];
      return fn !== undefined ? fn.call(regexp, O) : new RegExp(regexp)[MATCH](String(O));
    },
    // `RegExp.prototype[@@match]` method
    // https://tc39.github.io/ecma262/#sec-regexp.prototype-@@match
    function (regexp) {
      var res = maybeCallNative($match, regexp, this);
      if (res.done) return res.value;
      var rx = anObject(regexp);
      var S = String(this);
      if (!rx.global) return regExpExec(rx, S);
      var fullUnicode = rx.unicode;
      rx.lastIndex = 0;
      var A = [];
      var n = 0;
      var result;
      while ((result = regExpExec(rx, S)) !== null) {
        var matchStr = String(result[0]);
        A[n] = matchStr;
        if (matchStr === '') rx.lastIndex = advanceStringIndex(S, toLength(rx.lastIndex), fullUnicode);
        n++;
      }
      return n === 0 ? null : A;
    }
  ];
});


/***/ }),

/***/ "9b43":
/***/ (function(module, exports, __webpack_require__) {

// optional / simple context binding
var aFunction = __webpack_require__("d8e8");
module.exports = function (fn, that, length) {
  aFunction(fn);
  if (that === undefined) return fn;
  switch (length) {
    case 1: return function (a) {
      return fn.call(that, a);
    };
    case 2: return function (a, b) {
      return fn.call(that, a, b);
    };
    case 3: return function (a, b, c) {
      return fn.call(that, a, b, c);
    };
  }
  return function (/* ...args */) {
    return fn.apply(that, arguments);
  };
};


/***/ }),

/***/ "9bce":
/***/ (function(module, exports, __webpack_require__) {

// 7.1.13 ToObject(argument)
var defined = __webpack_require__("0f31");
module.exports = function (it) {
  return Object(defined(it));
};


/***/ }),

/***/ "9d06":
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "9d93":
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "9d98":
/***/ (function(module, exports, __webpack_require__) {

var $export = __webpack_require__("63b6");
// 19.1.2.3 / 15.2.3.7 Object.defineProperties(O, Properties)
$export($export.S + $export.F * !__webpack_require__("8e60"), 'Object', { defineProperties: __webpack_require__("7e90") });


/***/ }),

/***/ "9e1e":
/***/ (function(module, exports, __webpack_require__) {

// Thank's IE8 for his funny defineProperty
module.exports = !__webpack_require__("79e5")(function () {
  return Object.defineProperty({}, 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),

/***/ "9e99":
/***/ (function(module) {

module.exports = {"structs":{"Field":{"GetCardinal":{"In":[],"Out":[]},"GetAndMode":{"In":[],"Out":[]},"SelectValues":{"In":[{"Name":"qFieldValues","DefaultValue":[{"qText":"","qIsNumeric":false,"qNumber":0}]},{"Name":"qToggleMode","DefaultValue":false,"Optional":true},{"Name":"qSoftLock","DefaultValue":false,"Optional":true}],"Out":[]},"Select":{"In":[{"Name":"qMatch","DefaultValue":""},{"Name":"qSoftLock","DefaultValue":false,"Optional":true},{"Name":"qExcludedValuesMode","DefaultValue":0,"Optional":true}],"Out":[]},"ToggleSelect":{"In":[{"Name":"qMatch","DefaultValue":""},{"Name":"qSoftLock","DefaultValue":false,"Optional":true},{"Name":"qExcludedValuesMode","DefaultValue":0,"Optional":true}],"Out":[]},"ClearAllButThis":{"In":[{"Name":"qSoftLock","DefaultValue":false,"Optional":true}],"Out":[]},"SelectPossible":{"In":[{"Name":"qSoftLock","DefaultValue":false,"Optional":true}],"Out":[]},"SelectExcluded":{"In":[{"Name":"qSoftLock","DefaultValue":false,"Optional":true}],"Out":[]},"SelectAll":{"In":[{"Name":"qSoftLock","DefaultValue":false,"Optional":true}],"Out":[]},"Lock":{"In":[],"Out":[]},"Unlock":{"In":[],"Out":[]},"GetNxProperties":{"In":[],"Out":[{"Name":"qProperties"}]},"SetNxProperties":{"In":[{"Name":"qProperties","DefaultValue":{"qOneAndOnlyOne":false}}],"Out":[]},"SetAndMode":{"In":[{"Name":"qAndMode","DefaultValue":false}],"Out":[]},"SelectAlternative":{"In":[{"Name":"qSoftLock","DefaultValue":false,"Optional":true}],"Out":[]},"LowLevelSelect":{"In":[{"Name":"qValues","DefaultValue":[0]},{"Name":"qToggleMode","DefaultValue":false},{"Name":"qSoftLock","DefaultValue":false,"Optional":true}],"Out":[]},"Clear":{"In":[],"Out":[]}},"Variable":{"GetContent":{"In":[],"Out":[{"Name":"qContent"}]},"GetRawContent":{"In":[],"Out":[]},"SetContent":{"In":[{"Name":"qContent","DefaultValue":""},{"Name":"qUpdateMRU","DefaultValue":false}],"Out":[]},"ForceContent":{"In":[{"Name":"qs","DefaultValue":""},{"Name":"qd","DefaultValue":0}],"Out":[]},"GetNxProperties":{"In":[],"Out":[{"Name":"qProperties"}]},"SetNxProperties":{"In":[{"Name":"qProperties","DefaultValue":{"qName":"","qNumberPresentation":{"qType":0,"qnDec":0,"qUseThou":0,"qFmt":"","qDec":"","qThou":""},"qIncludeInBookmark":false,"qUsePredefListedValues":false,"qPreDefinedList":[""]}}],"Out":[]}},"GenericObject":{"GetLayout":{"In":[],"Out":[{"Name":"qLayout"}]},"GetListObjectData":{"In":[{"Name":"qPath","DefaultValue":""},{"Name":"qPages","DefaultValue":[{"qLeft":0,"qTop":0,"qWidth":0,"qHeight":0}]}],"Out":[{"Name":"qDataPages"}]},"GetHyperCubeData":{"In":[{"Name":"qPath","DefaultValue":""},{"Name":"qPages","DefaultValue":[{"qLeft":0,"qTop":0,"qWidth":0,"qHeight":0}]}],"Out":[{"Name":"qDataPages"}]},"GetHyperCubeReducedData":{"In":[{"Name":"qPath","DefaultValue":""},{"Name":"qPages","DefaultValue":[{"qLeft":0,"qTop":0,"qWidth":0,"qHeight":0}]},{"Name":"qZoomFactor","DefaultValue":0},{"Name":"qReductionMode","DefaultValue":0}],"Out":[{"Name":"qDataPages"}]},"GetHyperCubePivotData":{"In":[{"Name":"qPath","DefaultValue":""},{"Name":"qPages","DefaultValue":[{"qLeft":0,"qTop":0,"qWidth":0,"qHeight":0}]}],"Out":[{"Name":"qDataPages"}]},"GetHyperCubeStackData":{"In":[{"Name":"qPath","DefaultValue":""},{"Name":"qPages","DefaultValue":[{"qLeft":0,"qTop":0,"qWidth":0,"qHeight":0}]},{"Name":"qMaxNbrCells","DefaultValue":0,"Optional":true}],"Out":[{"Name":"qDataPages"}]},"GetHyperCubeContinuousData":{"In":[{"Name":"qPath","DefaultValue":""},{"Name":"qOptions","DefaultValue":{"qStart":0,"qEnd":0,"qNbrPoints":0,"qMaxNbrTicks":0,"qMaxNumberLines":0}},{"Name":"qReverseSort","DefaultValue":false,"Optional":true}],"Out":[{"Name":"qDataPages"},{"Name":"qAxisData"}]},"GetHyperCubeTreeData":{"In":[{"Name":"qPath","DefaultValue":""},{"Name":"qNodeOptions","DefaultValue":{"qMaxNbrOfNodes":0,"qTreeNodes":[{"qArea":{"qLeft":0,"qTop":0,"qWidth":0,"qHeight":0},"qAllValues":false}],"qTreeLevels":{"qLeft":0,"qDepth":0}},"Optional":true}],"Out":[{"Name":"qNodes"}]},"GetHyperCubeBinnedData":{"In":[{"Name":"qPath","DefaultValue":""},{"Name":"qPages","DefaultValue":[{"qLeft":0,"qTop":0,"qWidth":0,"qHeight":0}]},{"Name":"qViewport","DefaultValue":{"qWidth":0,"qHeight":0,"qZoomLevel":0}},{"Name":"qDataRanges","DefaultValue":[{"qLeft":0,"qTop":0,"qWidth":0,"qHeight":0}]},{"Name":"qMaxNbrCells","DefaultValue":0},{"Name":"qQueryLevel","DefaultValue":0},{"Name":"qBinningMethod","DefaultValue":0}],"Out":[{"Name":"qDataPages"}]},"ApplyPatches":{"In":[{"Name":"qPatches","DefaultValue":[{"qOp":0,"qPath":"","qValue":""}]},{"Name":"qSoftPatch","DefaultValue":false,"Optional":true}],"Out":[]},"ClearSoftPatches":{"In":[],"Out":[]},"SetProperties":{"In":[{"Name":"qProp","DefaultValue":{"qInfo":{"qId":"","qType":""},"qExtendsId":"","qMetaDef":{}}}],"Out":[]},"GetProperties":{"In":[],"Out":[{"Name":"qProp"}]},"GetEffectiveProperties":{"In":[],"Out":[{"Name":"qProp"}]},"SetFullPropertyTree":{"In":[{"Name":"qPropEntry","DefaultValue":{"qProperty":{"qInfo":{"qId":"","qType":""},"qExtendsId":"","qMetaDef":{}},"qChildren":[],"qEmbeddedSnapshotRef":null}}],"Out":[]},"GetFullPropertyTree":{"In":[],"Out":[{"Name":"qPropEntry"}]},"GetInfo":{"In":[],"Out":[{"Name":"qInfo"}]},"ClearSelections":{"In":[{"Name":"qPath","DefaultValue":""},{"Name":"qColIndices","DefaultValue":[0],"Optional":true}],"Out":[]},"ExportData":{"In":[{"Name":"qFileType","DefaultValue":0},{"Name":"qPath","DefaultValue":"","Optional":true},{"Name":"qFileName","DefaultValue":"","Optional":true},{"Name":"qExportState","DefaultValue":0,"Optional":true}],"Out":[{"Name":"qUrl"},{"Name":"qWarnings"}]},"SelectListObjectValues":{"In":[{"Name":"qPath","DefaultValue":""},{"Name":"qValues","DefaultValue":[0]},{"Name":"qToggleMode","DefaultValue":false},{"Name":"qSoftLock","DefaultValue":false,"Optional":true}],"Out":[{"Name":"qSuccess"}]},"SelectListObjectPossible":{"In":[{"Name":"qPath","DefaultValue":""},{"Name":"qSoftLock","DefaultValue":false,"Optional":true}],"Out":[{"Name":"qSuccess"}]},"SelectListObjectExcluded":{"In":[{"Name":"qPath","DefaultValue":""},{"Name":"qSoftLock","DefaultValue":false,"Optional":true}],"Out":[{"Name":"qSuccess"}]},"SelectListObjectAlternative":{"In":[{"Name":"qPath","DefaultValue":""},{"Name":"qSoftLock","DefaultValue":false,"Optional":true}],"Out":[{"Name":"qSuccess"}]},"SelectListObjectAll":{"In":[{"Name":"qPath","DefaultValue":""},{"Name":"qSoftLock","DefaultValue":false,"Optional":true}],"Out":[{"Name":"qSuccess"}]},"SelectListObjectContinuousRange":{"In":[{"Name":"qPath","DefaultValue":""},{"Name":"qRanges","DefaultValue":[{"qMin":0,"qMax":0,"qMinInclEq":false,"qMaxInclEq":false}]},{"Name":"qSoftLock","DefaultValue":false,"Optional":true}],"Out":[{"Name":"qSuccess"}]},"SearchListObjectFor":{"In":[{"Name":"qPath","DefaultValue":""},{"Name":"qMatch","DefaultValue":""}],"Out":[{"Name":"qSuccess"}]},"AbortListObjectSearch":{"In":[{"Name":"qPath","DefaultValue":""}],"Out":[]},"AcceptListObjectSearch":{"In":[{"Name":"qPath","DefaultValue":""},{"Name":"qToggleMode","DefaultValue":false},{"Name":"qSoftLock","DefaultValue":false,"Optional":true}],"Out":[]},"ExpandLeft":{"In":[{"Name":"qPath","DefaultValue":""},{"Name":"qRow","DefaultValue":0},{"Name":"qCol","DefaultValue":0},{"Name":"qAll","DefaultValue":false}],"Out":[]},"ExpandTop":{"In":[{"Name":"qPath","DefaultValue":""},{"Name":"qRow","DefaultValue":0},{"Name":"qCol","DefaultValue":0},{"Name":"qAll","DefaultValue":false}],"Out":[]},"CollapseLeft":{"In":[{"Name":"qPath","DefaultValue":""},{"Name":"qRow","DefaultValue":0},{"Name":"qCol","DefaultValue":0},{"Name":"qAll","DefaultValue":false}],"Out":[]},"CollapseTop":{"In":[{"Name":"qPath","DefaultValue":""},{"Name":"qRow","DefaultValue":0},{"Name":"qCol","DefaultValue":0},{"Name":"qAll","DefaultValue":false}],"Out":[]},"DrillUp":{"In":[{"Name":"qPath","DefaultValue":""},{"Name":"qDimNo","DefaultValue":0},{"Name":"qNbrSteps","DefaultValue":0}],"Out":[]},"Lock":{"In":[{"Name":"qPath","DefaultValue":""},{"Name":"qColIndices","DefaultValue":[0],"Optional":true}],"Out":[]},"Unlock":{"In":[{"Name":"qPath","DefaultValue":""},{"Name":"qColIndices","DefaultValue":[0],"Optional":true}],"Out":[]},"SelectHyperCubeValues":{"In":[{"Name":"qPath","DefaultValue":""},{"Name":"qDimNo","DefaultValue":0},{"Name":"qValues","DefaultValue":[0]},{"Name":"qToggleMode","DefaultValue":false}],"Out":[{"Name":"qSuccess"}]},"SelectHyperCubeCells":{"In":[{"Name":"qPath","DefaultValue":""},{"Name":"qRowIndices","DefaultValue":[0]},{"Name":"qColIndices","DefaultValue":[0]},{"Name":"qSoftLock","DefaultValue":false,"Optional":true},{"Name":"qDeselectOnlyOneSelected","DefaultValue":false,"Optional":true}],"Out":[{"Name":"qSuccess"}]},"SelectPivotCells":{"In":[{"Name":"qPath","DefaultValue":""},{"Name":"qSelections","DefaultValue":[{"qType":0,"qCol":0,"qRow":0}]},{"Name":"qSoftLock","DefaultValue":false,"Optional":true},{"Name":"qDeselectOnlyOneSelected","DefaultValue":false,"Optional":true}],"Out":[{"Name":"qSuccess"}]},"RangeSelectHyperCubeValues":{"In":[{"Name":"qPath","DefaultValue":""},{"Name":"qRanges","DefaultValue":[{"qRange":{"qMin":0,"qMax":0,"qMinInclEq":false,"qMaxInclEq":false},"qMeasureIx":0}]},{"Name":"qColumnsToSelect","DefaultValue":[0],"Optional":true},{"Name":"qOrMode","DefaultValue":false,"Optional":true},{"Name":"qDeselectOnlyOneSelected","DefaultValue":false,"Optional":true}],"Out":[{"Name":"qSuccess"}]},"MultiRangeSelectHyperCubeValues":{"In":[{"Name":"qPath","DefaultValue":""},{"Name":"qRanges","DefaultValue":[{"qRanges":[{"qRange":{"qMin":0,"qMax":0,"qMinInclEq":false,"qMaxInclEq":false},"qMeasureIx":0}],"qColumnsToSelect":[0]}]},{"Name":"qOrMode","DefaultValue":false,"Optional":true},{"Name":"qDeselectOnlyOneSelected","DefaultValue":false,"Optional":true}],"Out":[{"Name":"qSuccess"}]},"MultiRangeSelectTreeDataValues":{"In":[{"Name":"qPath","DefaultValue":""},{"Name":"qRanges","DefaultValue":[{"qRanges":[{"qRange":{"qMin":0,"qMax":0,"qMinInclEq":false,"qMaxInclEq":false},"qMeasureIx":0,"qDimensionIx":0}]}]},{"Name":"qOrMode","DefaultValue":false,"Optional":true},{"Name":"qDeselectOnlyOneSelected","DefaultValue":false,"Optional":true}],"Out":[{"Name":"qSuccess"}]},"SelectHyperCubeContinuousRange":{"In":[{"Name":"qPath","DefaultValue":""},{"Name":"qRanges","DefaultValue":[{"qRange":{"qMin":0,"qMax":0,"qMinInclEq":false,"qMaxInclEq":false},"qDimIx":0}]},{"Name":"qSoftLock","DefaultValue":false,"Optional":true}],"Out":[{"Name":"qSuccess"}]},"GetChild":{"In":[{"Name":"qId","DefaultValue":""}],"Out":[]},"GetChildInfos":{"In":[],"Out":[{"Name":"qInfos"}]},"CreateChild":{"In":[{"Name":"qProp","DefaultValue":{"qInfo":{"qId":"","qType":""},"qExtendsId":"","qMetaDef":{}}},{"Name":"qPropForThis","DefaultValue":null,"Optional":true}],"Out":[{"Name":"qInfo"}]},"DestroyChild":{"In":[{"Name":"qId","DefaultValue":""},{"Name":"qPropForThis","DefaultValue":null,"Optional":true}],"Out":[{"Name":"qSuccess"}]},"DestroyAllChildren":{"In":[{"Name":"qPropForThis","DefaultValue":null,"Optional":true}],"Out":[]},"SetChildArrayOrder":{"In":[{"Name":"qIds","DefaultValue":[""]}],"Out":[]},"GetLinkedObjects":{"In":[],"Out":[{"Name":"qItems"}]},"CopyFrom":{"In":[{"Name":"qFromId","DefaultValue":""}],"Out":[]},"BeginSelections":{"In":[{"Name":"qPaths","DefaultValue":[""]}],"Out":[]},"EndSelections":{"In":[{"Name":"qAccept","DefaultValue":false}],"Out":[]},"ResetMadeSelections":{"In":[],"Out":[]},"EmbedSnapshotObject":{"In":[{"Name":"qId","DefaultValue":""}],"Out":[]},"GetSnapshotObject":{"In":[],"Out":[]},"Publish":{"In":[],"Out":[]},"UnPublish":{"In":[],"Out":[]},"Approve":{"In":[],"Out":[]},"UnApprove":{"In":[],"Out":[]}},"GenericDimension":{"GetLayout":{"In":[],"Out":[{"Name":"qLayout"}]},"ApplyPatches":{"In":[{"Name":"qPatches","DefaultValue":[{"qOp":0,"qPath":"","qValue":""}]}],"Out":[]},"SetProperties":{"In":[{"Name":"qProp","DefaultValue":{"qInfo":{"qId":"","qType":""},"qDim":{"qGrouping":0,"qFieldDefs":[""],"qFieldLabels":[""],"qLabelExpression":""},"qMetaDef":{}}}],"Out":[]},"GetProperties":{"In":[],"Out":[{"Name":"qProp"}]},"GetInfo":{"In":[],"Out":[{"Name":"qInfo"}]},"GetDimension":{"In":[],"Out":[{"Name":"qDim"}]},"GetLinkedObjects":{"In":[],"Out":[{"Name":"qItems"}]},"Publish":{"In":[],"Out":[]},"UnPublish":{"In":[],"Out":[]},"Approve":{"In":[],"Out":[]},"UnApprove":{"In":[],"Out":[]}},"GenericBookmark":{"GetFieldValues":{"In":[{"Name":"qField","DefaultValue":""},{"Name":"qGetExcludedValues","DefaultValue":false},{"Name":"qDataPage","DefaultValue":{"qStartIndex":0,"qEndIndex":0}}],"Out":[{"Name":"qFieldValues"}]},"GetLayout":{"In":[],"Out":[{"Name":"qLayout"}]},"ApplyPatches":{"In":[{"Name":"qPatches","DefaultValue":[{"qOp":0,"qPath":"","qValue":""}]}],"Out":[]},"SetProperties":{"In":[{"Name":"qProp","DefaultValue":{"qInfo":{"qId":"","qType":""},"qMetaDef":{}}}],"Out":[]},"GetProperties":{"In":[],"Out":[{"Name":"qProp"}]},"GetInfo":{"In":[],"Out":[{"Name":"qInfo"}]},"Apply":{"In":[],"Out":[{"Name":"qSuccess"}]},"Publish":{"In":[],"Out":[]},"UnPublish":{"In":[],"Out":[]},"Approve":{"In":[],"Out":[]},"UnApprove":{"In":[],"Out":[]}},"GenericVariable":{"GetLayout":{"In":[],"Out":[{"Name":"qLayout"}]},"ApplyPatches":{"In":[{"Name":"qPatches","DefaultValue":[{"qOp":0,"qPath":"","qValue":""}]}],"Out":[]},"SetProperties":{"In":[{"Name":"qProp","DefaultValue":{"qInfo":{"qId":"","qType":""},"qMetaDef":{},"qName":"","qComment":"","qNumberPresentation":{"qType":0,"qnDec":0,"qUseThou":0,"qFmt":"","qDec":"","qThou":""},"qIncludeInBookmark":false,"qDefinition":""}}],"Out":[]},"GetProperties":{"In":[],"Out":[{"Name":"qProp"}]},"GetInfo":{"In":[],"Out":[{"Name":"qInfo"}]},"SetStringValue":{"In":[{"Name":"qVal","DefaultValue":""}],"Out":[]},"SetNumValue":{"In":[{"Name":"qVal","DefaultValue":0}],"Out":[]},"SetDualValue":{"In":[{"Name":"qText","DefaultValue":""},{"Name":"qNum","DefaultValue":0}],"Out":[]}},"GenericMeasure":{"GetLayout":{"In":[],"Out":[{"Name":"qLayout"}]},"ApplyPatches":{"In":[{"Name":"qPatches","DefaultValue":[{"qOp":0,"qPath":"","qValue":""}]}],"Out":[]},"SetProperties":{"In":[{"Name":"qProp","DefaultValue":{"qInfo":{"qId":"","qType":""},"qMeasure":{"qLabel":"","qDef":"","qGrouping":0,"qExpressions":[""],"qActiveExpression":0,"qLabelExpression":""},"qMetaDef":{}}}],"Out":[]},"GetProperties":{"In":[],"Out":[{"Name":"qProp"}]},"GetInfo":{"In":[],"Out":[{"Name":"qInfo"}]},"GetMeasure":{"In":[],"Out":[{"Name":"qMeasure"}]},"GetLinkedObjects":{"In":[],"Out":[{"Name":"qItems"}]},"Publish":{"In":[],"Out":[]},"UnPublish":{"In":[],"Out":[]},"Approve":{"In":[],"Out":[]},"UnApprove":{"In":[],"Out":[]}},"Doc":{"GetField":{"In":[{"Name":"qFieldName","DefaultValue":""},{"Name":"qStateName","DefaultValue":"","Optional":true}],"Out":[]},"GetFieldDescription":{"In":[{"Name":"qFieldName","DefaultValue":""}],"Out":[]},"GetVariable":{"In":[{"Name":"qName","DefaultValue":""}],"Out":[]},"GetLooselyCoupledVector":{"In":[],"Out":[{"Name":"qv"}]},"SetLooselyCoupledVector":{"In":[{"Name":"qv","DefaultValue":[0]}],"Out":[]},"Evaluate":{"In":[{"Name":"qExpression","DefaultValue":""}],"Out":[]},"EvaluateEx":{"In":[{"Name":"qExpression","DefaultValue":""}],"Out":[{"Name":"qValue"}]},"ClearAll":{"In":[{"Name":"qLockedAlso","DefaultValue":false,"Optional":true},{"Name":"qStateName","DefaultValue":"","Optional":true}],"Out":[]},"LockAll":{"In":[{"Name":"qStateName","DefaultValue":"","Optional":true}],"Out":[]},"UnlockAll":{"In":[{"Name":"qStateName","DefaultValue":"","Optional":true}],"Out":[]},"Back":{"In":[],"Out":[]},"Forward":{"In":[],"Out":[]},"CreateVariable":{"In":[{"Name":"qName","DefaultValue":""}],"Out":[]},"RemoveVariable":{"In":[{"Name":"qName","DefaultValue":""}],"Out":[]},"GetLocaleInfo":{"In":[],"Out":[]},"GetTablesAndKeys":{"In":[{"Name":"qWindowSize","DefaultValue":{"qcx":0,"qcy":0}},{"Name":"qNullSize","DefaultValue":{"qcx":0,"qcy":0}},{"Name":"qCellHeight","DefaultValue":0},{"Name":"qSyntheticMode","DefaultValue":false},{"Name":"qIncludeSysVars","DefaultValue":false}],"Out":[{"Name":"qtr"},{"Name":"qk"}]},"GetViewDlgSaveInfo":{"In":[],"Out":[]},"SetViewDlgSaveInfo":{"In":[{"Name":"qInfo","DefaultValue":{"qPos":{"qLeft":0,"qTop":0,"qWidth":0,"qHeight":0},"qCtlInfo":{"qInternalView":{"qTables":[{"qPos":{"qLeft":0,"qTop":0,"qWidth":0,"qHeight":0},"qCaption":""}],"qBroomPoints":[{"qPos":{"qx":0,"qy":0},"qTable":"","qFields":[""]}],"qConnectionPoints":[{"qPos":{"qx":0,"qy":0},"qFields":[""]}],"qZoomFactor":0},"qSourceView":{"qTables":[{"qPos":{"qLeft":0,"qTop":0,"qWidth":0,"qHeight":0},"qCaption":""}],"qBroomPoints":[{"qPos":{"qx":0,"qy":0},"qTable":"","qFields":[""]}],"qConnectionPoints":[{"qPos":{"qx":0,"qy":0},"qFields":[""]}],"qZoomFactor":0}},"qMode":0}}],"Out":[]},"GetEmptyScript":{"In":[{"Name":"qLocalizedMainSection","DefaultValue":"","Optional":true}],"Out":[]},"DoReload":{"In":[{"Name":"qMode","DefaultValue":0,"Optional":true},{"Name":"qPartial","DefaultValue":false,"Optional":true},{"Name":"qDebug","DefaultValue":false,"Optional":true}],"Out":[]},"GetScriptBreakpoints":{"In":[],"Out":[{"Name":"qBreakpoints"}]},"SetScriptBreakpoints":{"In":[{"Name":"qBreakpoints","DefaultValue":[{"qbufferName":"","qlineIx":0,"qEnabled":false}]}],"Out":[]},"GetScript":{"In":[],"Out":[{"Name":"qScript"}]},"GetTextMacros":{"In":[],"Out":[{"Name":"qMacros"}]},"SetFetchLimit":{"In":[{"Name":"qLimit","DefaultValue":0}],"Out":[]},"DoSave":{"In":[{"Name":"qFileName","DefaultValue":"","Optional":true}],"Out":[]},"GetTableData":{"In":[{"Name":"qOffset","DefaultValue":0},{"Name":"qRows","DefaultValue":0},{"Name":"qSyntheticMode","DefaultValue":false},{"Name":"qTableName","DefaultValue":""}],"Out":[{"Name":"qData"}]},"GetAppLayout":{"In":[],"Out":[{"Name":"qLayout"}]},"SetAppProperties":{"In":[{"Name":"qProp","DefaultValue":{"qTitle":"","qLastReloadTime":"","qMigrationHash":"","qSavedInProductVersion":"","qThumbnail":{"qUrl":""}}}],"Out":[]},"GetAppProperties":{"In":[],"Out":[{"Name":"qProp"}]},"GetLineage":{"In":[],"Out":[{"Name":"qLineage"}]},"CreateSessionObject":{"In":[{"Name":"qProp","DefaultValue":{"qInfo":{"qId":"","qType":""},"qExtendsId":"","qMetaDef":{}}}],"Out":[]},"DestroySessionObject":{"In":[{"Name":"qId","DefaultValue":""}],"Out":[{"Name":"qSuccess"}]},"CreateObject":{"In":[{"Name":"qProp","DefaultValue":{"qInfo":{"qId":"","qType":""},"qExtendsId":"","qMetaDef":{}}}],"Out":[{"Name":"qInfo"}]},"DestroyObject":{"In":[{"Name":"qId","DefaultValue":""}],"Out":[{"Name":"qSuccess"}]},"GetObject":{"In":[{"Name":"qId","DefaultValue":""}],"Out":[]},"GetObjects":{"In":[{"Name":"qOptions","DefaultValue":{"qTypes":[""],"qIncludeSessionObjects":false,"qData":{}}}],"Out":[{"Name":"qList"}]},"GetBookmarks":{"In":[{"Name":"qOptions","DefaultValue":{"qTypes":[""],"qData":{}}}],"Out":[{"Name":"qList"}]},"CloneObject":{"In":[{"Name":"qId","DefaultValue":""}],"Out":[{"Name":"qCloneId"}]},"CreateDraft":{"In":[{"Name":"qId","DefaultValue":""}],"Out":[{"Name":"qDraftId"}]},"CommitDraft":{"In":[{"Name":"qId","DefaultValue":""}],"Out":[]},"DestroyDraft":{"In":[{"Name":"qId","DefaultValue":""},{"Name":"qSourceId","DefaultValue":""}],"Out":[{"Name":"qSuccess"}]},"Undo":{"In":[],"Out":[{"Name":"qSuccess"}]},"Redo":{"In":[],"Out":[{"Name":"qSuccess"}]},"ClearUndoBuffer":{"In":[],"Out":[]},"CreateDimension":{"In":[{"Name":"qProp","DefaultValue":{"qInfo":{"qId":"","qType":""},"qDim":{"qGrouping":0,"qFieldDefs":[""],"qFieldLabels":[""],"qLabelExpression":""},"qMetaDef":{}}}],"Out":[{"Name":"qInfo"}]},"DestroyDimension":{"In":[{"Name":"qId","DefaultValue":""}],"Out":[{"Name":"qSuccess"}]},"GetDimension":{"In":[{"Name":"qId","DefaultValue":""}],"Out":[]},"CloneDimension":{"In":[{"Name":"qId","DefaultValue":""}],"Out":[{"Name":"qCloneId"}]},"CreateMeasure":{"In":[{"Name":"qProp","DefaultValue":{"qInfo":{"qId":"","qType":""},"qMeasure":{"qLabel":"","qDef":"","qGrouping":0,"qExpressions":[""],"qActiveExpression":0,"qLabelExpression":""},"qMetaDef":{}}}],"Out":[{"Name":"qInfo"}]},"DestroyMeasure":{"In":[{"Name":"qId","DefaultValue":""}],"Out":[{"Name":"qSuccess"}]},"GetMeasure":{"In":[{"Name":"qId","DefaultValue":""}],"Out":[]},"CloneMeasure":{"In":[{"Name":"qId","DefaultValue":""}],"Out":[{"Name":"qCloneId"}]},"CreateSessionVariable":{"In":[{"Name":"qProp","DefaultValue":{"qInfo":{"qId":"","qType":""},"qMetaDef":{},"qName":"","qComment":"","qNumberPresentation":{"qType":0,"qnDec":0,"qUseThou":0,"qFmt":"","qDec":"","qThou":""},"qIncludeInBookmark":false,"qDefinition":""}}],"Out":[]},"DestroySessionVariable":{"In":[{"Name":"qId","DefaultValue":""}],"Out":[{"Name":"qSuccess"}]},"CreateVariableEx":{"In":[{"Name":"qProp","DefaultValue":{"qInfo":{"qId":"","qType":""},"qMetaDef":{},"qName":"","qComment":"","qNumberPresentation":{"qType":0,"qnDec":0,"qUseThou":0,"qFmt":"","qDec":"","qThou":""},"qIncludeInBookmark":false,"qDefinition":""}}],"Out":[{"Name":"qInfo"}]},"DestroyVariableById":{"In":[{"Name":"qId","DefaultValue":""}],"Out":[{"Name":"qSuccess"}]},"DestroyVariableByName":{"In":[{"Name":"qName","DefaultValue":""}],"Out":[{"Name":"qSuccess"}]},"GetVariableById":{"In":[{"Name":"qId","DefaultValue":""}],"Out":[]},"GetVariableByName":{"In":[{"Name":"qName","DefaultValue":""}],"Out":[]},"MigrateVariables":{"In":[],"Out":[]},"MigrateDerivedFields":{"In":[],"Out":[]},"CheckExpression":{"In":[{"Name":"qExpr","DefaultValue":""},{"Name":"qLabels","DefaultValue":[""],"Optional":true}],"Out":[{"Name":"qErrorMsg"},{"Name":"qBadFieldNames"},{"Name":"qDangerousFieldNames"}]},"CheckNumberOrExpression":{"In":[{"Name":"qExpr","DefaultValue":""}],"Out":[{"Name":"qErrorMsg"},{"Name":"qBadFieldNames"}]},"AddAlternateState":{"In":[{"Name":"qStateName","DefaultValue":""}],"Out":[]},"RemoveAlternateState":{"In":[{"Name":"qStateName","DefaultValue":""}],"Out":[]},"CreateBookmark":{"In":[{"Name":"qProp","DefaultValue":{"qInfo":{"qId":"","qType":""},"qMetaDef":{}}}],"Out":[{"Name":"qInfo"}]},"DestroyBookmark":{"In":[{"Name":"qId","DefaultValue":""}],"Out":[{"Name":"qSuccess"}]},"GetBookmark":{"In":[{"Name":"qId","DefaultValue":""}],"Out":[]},"ApplyBookmark":{"In":[{"Name":"qId","DefaultValue":""}],"Out":[{"Name":"qSuccess"}]},"CloneBookmark":{"In":[{"Name":"qId","DefaultValue":""}],"Out":[{"Name":"qCloneId"}]},"AddFieldFromExpression":{"In":[{"Name":"qName","DefaultValue":""},{"Name":"qExpr","DefaultValue":""}],"Out":[{"Name":"qSuccess"}]},"GetFieldOnTheFlyByName":{"In":[{"Name":"qReadableName","DefaultValue":""}],"Out":[{"Name":"qName"}]},"GetAllInfos":{"In":[],"Out":[{"Name":"qInfos"}]},"Resume":{"In":[],"Out":[]},"AbortModal":{"In":[{"Name":"qAccept","DefaultValue":false}],"Out":[]},"Publish":{"In":[{"Name":"qStreamId","DefaultValue":""},{"Name":"qName","DefaultValue":"","Optional":true}],"Out":[]},"UnPublish":{"In":[],"Out":[]},"GetMatchingFields":{"In":[{"Name":"qTags","DefaultValue":[""]},{"Name":"qMatchingFieldMode","DefaultValue":0,"Optional":true}],"Out":[{"Name":"qFieldNames"}]},"FindMatchingFields":{"In":[{"Name":"qFieldName","DefaultValue":""},{"Name":"qTags","DefaultValue":[""]}],"Out":[{"Name":"qFieldNames"}]},"Scramble":{"In":[{"Name":"qFieldName","DefaultValue":""}],"Out":[]},"SaveObjects":{"In":[],"Out":[]},"GetAssociationScores":{"In":[{"Name":"qTable1","DefaultValue":""},{"Name":"qTable2","DefaultValue":""}],"Out":[{"Name":"qScore"}]},"GetMediaList":{"In":[],"Out":[{"Name":"qList"}]},"GetContentLibraries":{"In":[],"Out":[{"Name":"qList"}]},"GetLibraryContent":{"In":[{"Name":"qName","DefaultValue":""}],"Out":[{"Name":"qList"}]},"DoReloadEx":{"In":[{"Name":"qParams","DefaultValue":{"qMode":0,"qPartial":false,"qDebug":false},"Optional":true}],"Out":[{"Name":"qResult"}]},"BackCount":{"In":[],"Out":[]},"ForwardCount":{"In":[],"Out":[]},"ExportReducedData":{"In":[{"Name":"qOptions","DefaultValue":{"qBookmarkId":"","qExpires":0},"Optional":true}],"Out":[{"Name":"qDownloadInfo"}]},"SetScript":{"In":[{"Name":"qScript","DefaultValue":""}],"Out":[]},"CheckScriptSyntax":{"In":[],"Out":[{"Name":"qErrors"}]},"GetFavoriteVariables":{"In":[],"Out":[{"Name":"qNames"}]},"SetFavoriteVariables":{"In":[{"Name":"qNames","DefaultValue":[""]}],"Out":[]},"GetIncludeFileContent":{"In":[{"Name":"qPath","DefaultValue":""}],"Out":[{"Name":"qContent"}]},"CreateConnection":{"In":[{"Name":"qConnection","DefaultValue":{"qId":"","qName":"","qConnectionString":"","qType":"","qUserName":"","qPassword":"","qModifiedDate":"","qMeta":{"qName":""},"qLogOn":0}}],"Out":[{"Name":"qConnectionId"}]},"ModifyConnection":{"In":[{"Name":"qConnectionId","DefaultValue":""},{"Name":"qConnection","DefaultValue":{"qId":"","qName":"","qConnectionString":"","qType":"","qUserName":"","qPassword":"","qModifiedDate":"","qMeta":{"qName":""},"qLogOn":0}},{"Name":"qOverrideCredentials","DefaultValue":false,"Optional":true}],"Out":[]},"DeleteConnection":{"In":[{"Name":"qConnectionId","DefaultValue":""}],"Out":[]},"GetConnection":{"In":[{"Name":"qConnectionId","DefaultValue":""}],"Out":[{"Name":"qConnection"}]},"GetConnections":{"In":[],"Out":[{"Name":"qConnections"}]},"GetDatabaseInfo":{"In":[{"Name":"qConnectionId","DefaultValue":""}],"Out":[{"Name":"qInfo"}]},"GetDatabases":{"In":[{"Name":"qConnectionId","DefaultValue":""}],"Out":[{"Name":"qDatabases"}]},"GetDatabaseOwners":{"In":[{"Name":"qConnectionId","DefaultValue":""},{"Name":"qDatabase","DefaultValue":"","Optional":true}],"Out":[{"Name":"qOwners"}]},"GetDatabaseTables":{"In":[{"Name":"qConnectionId","DefaultValue":""},{"Name":"qDatabase","DefaultValue":"","Optional":true},{"Name":"qOwner","DefaultValue":"","Optional":true}],"Out":[{"Name":"qTables"}]},"GetDatabaseTableFields":{"In":[{"Name":"qConnectionId","DefaultValue":""},{"Name":"qDatabase","DefaultValue":"","Optional":true},{"Name":"qOwner","DefaultValue":"","Optional":true},{"Name":"qTable","DefaultValue":""}],"Out":[{"Name":"qFields"}]},"GetDatabaseTablePreview":{"In":[{"Name":"qConnectionId","DefaultValue":""},{"Name":"qDatabase","DefaultValue":"","Optional":true},{"Name":"qOwner","DefaultValue":"","Optional":true},{"Name":"qTable","DefaultValue":""},{"Name":"qConditions","DefaultValue":{"qType":0,"qWherePredicate":""},"Optional":true}],"Out":[{"Name":"qPreview"},{"Name":"qRowCount"}]},"GetFolderItemsForConnection":{"In":[{"Name":"qConnectionId","DefaultValue":""},{"Name":"qRelativePath","DefaultValue":"","Optional":true}],"Out":[{"Name":"qFolderItems"}]},"GuessFileType":{"In":[{"Name":"qConnectionId","DefaultValue":""},{"Name":"qRelativePath","DefaultValue":"","Optional":true}],"Out":[{"Name":"qDataFormat"}]},"GetFileTables":{"In":[{"Name":"qConnectionId","DefaultValue":""},{"Name":"qRelativePath","DefaultValue":"","Optional":true},{"Name":"qDataFormat","DefaultValue":{"qType":0,"qLabel":"","qQuote":"","qComment":"","qDelimiter":{"qName":"","qScriptCode":"","qNumber":0,"qIsMultiple":false},"qCodePage":0,"qHeaderSize":0,"qRecordSize":0,"qTabSize":0,"qIgnoreEOF":false,"qFixedWidthDelimiters":""}}],"Out":[{"Name":"qTables"}]},"GetFileTableFields":{"In":[{"Name":"qConnectionId","DefaultValue":""},{"Name":"qRelativePath","DefaultValue":"","Optional":true},{"Name":"qDataFormat","DefaultValue":{"qType":0,"qLabel":"","qQuote":"","qComment":"","qDelimiter":{"qName":"","qScriptCode":"","qNumber":0,"qIsMultiple":false},"qCodePage":0,"qHeaderSize":0,"qRecordSize":0,"qTabSize":0,"qIgnoreEOF":false,"qFixedWidthDelimiters":""}},{"Name":"qTable","DefaultValue":""}],"Out":[{"Name":"qFields"},{"Name":"qFormatSpec"}]},"GetFileTablePreview":{"In":[{"Name":"qConnectionId","DefaultValue":""},{"Name":"qRelativePath","DefaultValue":"","Optional":true},{"Name":"qDataFormat","DefaultValue":{"qType":0,"qLabel":"","qQuote":"","qComment":"","qDelimiter":{"qName":"","qScriptCode":"","qNumber":0,"qIsMultiple":false},"qCodePage":0,"qHeaderSize":0,"qRecordSize":0,"qTabSize":0,"qIgnoreEOF":false,"qFixedWidthDelimiters":""}},{"Name":"qTable","DefaultValue":""}],"Out":[{"Name":"qPreview"},{"Name":"qFormatSpec"}]},"GetFileTablesEx":{"In":[{"Name":"qConnectionId","DefaultValue":""},{"Name":"qRelativePath","DefaultValue":"","Optional":true},{"Name":"qDataFormat","DefaultValue":{"qType":0,"qLabel":"","qQuote":"","qComment":"","qDelimiter":{"qName":"","qScriptCode":"","qNumber":0,"qIsMultiple":false},"qCodePage":0,"qHeaderSize":0,"qRecordSize":0,"qTabSize":0,"qIgnoreEOF":false,"qFixedWidthDelimiters":""}}],"Out":[{"Name":"qTables"}]},"SendGenericCommandToCustomConnector":{"In":[{"Name":"qProvider","DefaultValue":""},{"Name":"qCommand","DefaultValue":""},{"Name":"qMethod","DefaultValue":""},{"Name":"qParameters","DefaultValue":[""]},{"Name":"qAppendConnection","DefaultValue":""}],"Out":[{"Name":"qResult"}]},"SearchSuggest":{"In":[{"Name":"qOptions","DefaultValue":{"qSearchFields":[""],"qContext":0,"qCharEncoding":0,"qAttributes":[""]}},{"Name":"qTerms","DefaultValue":[""]}],"Out":[{"Name":"qResult"}]},"SearchAssociations":{"In":[{"Name":"qOptions","DefaultValue":{"qSearchFields":[""],"qContext":0,"qCharEncoding":0,"qAttributes":[""]}},{"Name":"qTerms","DefaultValue":[""]},{"Name":"qPage","DefaultValue":{"qOffset":0,"qCount":0,"qMaxNbrFieldMatches":0,"qGroupOptions":[{"qGroupType":0,"qOffset":0,"qCount":0}],"qGroupItemOptions":[{"qGroupItemType":0,"qOffset":0,"qCount":0}]}}],"Out":[{"Name":"qResults"}]},"SelectAssociations":{"In":[{"Name":"qOptions","DefaultValue":{"qSearchFields":[""],"qContext":0,"qCharEncoding":0,"qAttributes":[""]}},{"Name":"qTerms","DefaultValue":[""]},{"Name":"qMatchIx","DefaultValue":0},{"Name":"qSoftLock","DefaultValue":null,"Optional":true}],"Out":[]},"SearchResults":{"In":[{"Name":"qOptions","DefaultValue":{"qSearchFields":[""],"qContext":0,"qCharEncoding":0,"qAttributes":[""]}},{"Name":"qTerms","DefaultValue":[""]},{"Name":"qPage","DefaultValue":{"qOffset":0,"qCount":0,"qMaxNbrFieldMatches":0,"qGroupOptions":[{"qGroupType":0,"qOffset":0,"qCount":0}],"qGroupItemOptions":[{"qGroupItemType":0,"qOffset":0,"qCount":0}]}}],"Out":[{"Name":"qResult"}]},"SearchObjects":{"In":[{"Name":"qOptions","DefaultValue":{"qAttributes":[""],"qCharEncoding":0}},{"Name":"qTerms","DefaultValue":[""]},{"Name":"qPage","DefaultValue":{"qOffset":0,"qCount":0,"qMaxNbrFieldMatches":0,"qGroupOptions":[{"qGroupType":0,"qOffset":0,"qCount":0}],"qGroupItemOptions":[{"qGroupItemType":0,"qOffset":0,"qCount":0}]}}],"Out":[{"Name":"qResult"}]},"GetScriptEx":{"In":[],"Out":[{"Name":"qScript"}]}},"Global":{"AbortRequest":{"In":[{"Name":"qRequestId","DefaultValue":0}],"Out":[]},"AbortAll":{"In":[],"Out":[]},"GetProgress":{"In":[{"Name":"qRequestId","DefaultValue":0}],"Out":[{"Name":"qProgressData"}]},"QvVersion":{"In":[],"Out":[]},"OSVersion":{"In":[],"Out":[]},"OSName":{"In":[],"Out":[]},"QTProduct":{"In":[],"Out":[]},"GetDocList":{"In":[],"Out":[{"Name":"qDocList"}]},"GetInteract":{"In":[{"Name":"qRequestId","DefaultValue":0}],"Out":[{"Name":"qDef"}]},"InteractDone":{"In":[{"Name":"qRequestId","DefaultValue":0},{"Name":"qDef","DefaultValue":{"qType":0,"qTitle":"","qMsg":"","qButtons":0,"qLine":"","qOldLineNr":0,"qNewLineNr":0,"qPath":"","qHidden":false,"qResult":0,"qInput":""}}],"Out":[]},"GetAuthenticatedUser":{"In":[],"Out":[]},"CreateDocEx":{"In":[{"Name":"qDocName","DefaultValue":""},{"Name":"qUserName","DefaultValue":"","Optional":true},{"Name":"qPassword","DefaultValue":"","Optional":true},{"Name":"qSerial","DefaultValue":"","Optional":true},{"Name":"qLocalizedScriptMainSection","DefaultValue":"","Optional":true}],"Out":[{"Name":"qDocId"}]},"GetActiveDoc":{"In":[],"Out":[]},"AllowCreateApp":{"In":[],"Out":[]},"CreateApp":{"In":[{"Name":"qAppName","DefaultValue":""},{"Name":"qLocalizedScriptMainSection","DefaultValue":"","Optional":true}],"Out":[{"Name":"qSuccess"},{"Name":"qAppId"}]},"DeleteApp":{"In":[{"Name":"qAppId","DefaultValue":""}],"Out":[{"Name":"qSuccess"}]},"IsDesktopMode":{"In":[],"Out":[]},"GetConfiguration":{"In":[],"Out":[{"Name":"qConfig"}]},"CancelRequest":{"In":[{"Name":"qRequestId","DefaultValue":0}],"Out":[]},"ShutdownProcess":{"In":[],"Out":[]},"ReloadExtensionList":{"In":[],"Out":[]},"ReplaceAppFromID":{"In":[{"Name":"qTargetAppId","DefaultValue":""},{"Name":"qSrcAppID","DefaultValue":""},{"Name":"qIds","DefaultValue":[""]}],"Out":[{"Name":"qSuccess"}]},"ReplaceAppFromPath":{"In":[{"Name":"qTargetAppId","DefaultValue":""},{"Name":"qSrcPath","DefaultValue":""},{"Name":"qIds","DefaultValue":[""]},{"Name":"qNoData","DefaultValue":false}],"Out":[]},"CopyApp":{"In":[{"Name":"qTargetAppId","DefaultValue":""},{"Name":"qSrcAppId","DefaultValue":""},{"Name":"qIds","DefaultValue":[""]}],"Out":[{"Name":"qSuccess"}]},"ImportApp":{"In":[{"Name":"qAppId","DefaultValue":""},{"Name":"qSrcPath","DefaultValue":""},{"Name":"qIds","DefaultValue":[""]}],"Out":[{"Name":"qSuccess"}]},"ImportAppEx":{"In":[{"Name":"qAppId","DefaultValue":""},{"Name":"qSrcPath","DefaultValue":""},{"Name":"qIds","DefaultValue":[""]},{"Name":"qExcludeConnections","DefaultValue":false}],"Out":[]},"ExportApp":{"In":[{"Name":"qTargetPath","DefaultValue":""},{"Name":"qSrcAppId","DefaultValue":""},{"Name":"qIds","DefaultValue":[""]},{"Name":"qNoData","DefaultValue":false,"Optional":true}],"Out":[{"Name":"qSuccess"}]},"PublishApp":{"In":[{"Name":"qAppId","DefaultValue":""},{"Name":"qName","DefaultValue":""},{"Name":"qStreamId","DefaultValue":""}],"Out":[]},"IsPersonalMode":{"In":[],"Out":[]},"GetUniqueID":{"In":[],"Out":[{"Name":"qUniqueID"}]},"OpenDoc":{"In":[{"Name":"qDocName","DefaultValue":""},{"Name":"qUserName","DefaultValue":"","Optional":true},{"Name":"qPassword","DefaultValue":"","Optional":true},{"Name":"qSerial","DefaultValue":"","Optional":true},{"Name":"qNoData","DefaultValue":false,"Optional":true}],"Out":[]},"CreateSessionApp":{"In":[],"Out":[{"Name":"qSessionAppId"}]},"CreateSessionAppFromApp":{"In":[{"Name":"qSrcAppId","DefaultValue":""}],"Out":[{"Name":"qSessionAppId"}]},"ProductVersion":{"In":[],"Out":[]},"GetAppEntry":{"In":[{"Name":"qAppID","DefaultValue":""}],"Out":[{"Name":"qEntry"}]},"ConfigureReload":{"In":[{"Name":"qCancelOnScriptError","DefaultValue":false},{"Name":"qUseErrorData","DefaultValue":false},{"Name":"qInteractOnError","DefaultValue":false}],"Out":[]},"CancelReload":{"In":[],"Out":[]},"GetBNF":{"In":[{"Name":"qBnfType","DefaultValue":0}],"Out":[{"Name":"qBnfDefs"}]},"GetFunctions":{"In":[{"Name":"qGroup","DefaultValue":0,"Optional":true}],"Out":[{"Name":"qFunctions"}]},"GetOdbcDsns":{"In":[],"Out":[{"Name":"qOdbcDsns"}]},"GetOleDbProviders":{"In":[],"Out":[{"Name":"qOleDbProviders"}]},"GetDatabasesFromConnectionString":{"In":[{"Name":"qConnection","DefaultValue":{"qId":"","qName":"","qConnectionString":"","qType":"","qUserName":"","qPassword":"","qModifiedDate":"","qMeta":{"qName":""},"qLogOn":0}}],"Out":[{"Name":"qDatabases"}]},"IsValidConnectionString":{"In":[{"Name":"qConnection","DefaultValue":{"qId":"","qName":"","qConnectionString":"","qType":"","qUserName":"","qPassword":"","qModifiedDate":"","qMeta":{"qName":""},"qLogOn":0}}],"Out":[]},"GetDefaultAppFolder":{"In":[],"Out":[{"Name":"qPath"}]},"GetMyDocumentsFolder":{"In":[],"Out":[{"Name":"qFolder"}]},"GetLogicalDriveStrings":{"In":[],"Out":[{"Name":"qDrives"}]},"GetFolderItemsForPath":{"In":[{"Name":"qPath","DefaultValue":""}],"Out":[{"Name":"qFolderItems"}]},"GetSupportedCodePages":{"In":[],"Out":[{"Name":"qCodePages"}]},"GetCustomConnectors":{"In":[{"Name":"qReloadList","DefaultValue":false,"Optional":true}],"Out":[{"Name":"qConnectors"}]},"GetStreamList":{"In":[],"Out":[{"Name":"qStreamList"}]},"EngineVersion":{"In":[],"Out":[{"Name":"qVersion"}]},"GetBaseBNF":{"In":[{"Name":"qBnfType","DefaultValue":0}],"Out":[{"Name":"qBnfDefs"},{"Name":"qBnfHash"}]},"GetBaseBNFHash":{"In":[{"Name":"qBnfType","DefaultValue":0}],"Out":[{"Name":"qBnfHash"}]},"GetBaseBNFString":{"In":[{"Name":"qBnfType","DefaultValue":0}],"Out":[{"Name":"qBnfStr"},{"Name":"qBnfHash"}]}}},"enums":{"LocalizedMessageCode":{"LOCMSG_SCRIPTEDITOR_EMPTY_MESSAGE":0,"LOCMSG_SCRIPTEDITOR_PROGRESS_SAVING_STARTED":1,"LOCMSG_SCRIPTEDITOR_PROGRESS_BYTES_LEFT":2,"LOCMSG_SCRIPTEDITOR_PROGRESS_STORING_TABLES":3,"LOCMSG_SCRIPTEDITOR_PROGRESS_QVD_ROWS_SO_FAR":4,"LOCMSG_SCRIPTEDITOR_PROGRESS_CONNECTED":5,"LOCMSG_SCRIPTEDITOR_PROGRESS_CONNECTING_TO":6,"LOCMSG_SCRIPTEDITOR_PROGRESS_CONNECT_FAILED":7,"LOCMSG_SCRIPTEDITOR_PROGRESS_QVD_ROWISH":8,"LOCMSG_SCRIPTEDITOR_PROGRESS_QVD_COLUMNAR":9,"LOCMSG_SCRIPTEDITOR_ERROR":10,"LOCMSG_SCRIPTEDITOR_DONE":11,"LOCMSG_SCRIPTEDITOR_LOAD_EXTERNAL_DATA":12,"LOCMSG_SCRIPTEDITOR_PROGRESS_OLD_QVD_ISLOADING":13,"LOCMSG_SCRIPTEDITOR_PROGRESS_QVC_LOADING":14,"LOCMSG_SCRIPTEDITOR_PROGRESS_QVD_BUFFERED":15,"LOCMSG_SCRIPTEDITOR_PROGRESS_QVC_PREPARING":16,"LOCMSG_SCRIPTEDITOR_PROGRESS_QVC_APPENDING":17,"LOCMSG_SCRIPTEDITOR_REMOVE_SYNTHETIC":18,"LOCMSG_SCRIPTEDITOR_PENDING_LINKEDTABLE_FETCHING":19,"LOCMSG_SCRIPTEDITOR_RELOAD":20,"LOCMSG_SCRIPTEDITOR_LINES_FETCHED":21,"LOCMSG_SCRIPTEDITOR_SEARCHINDEX_START":22,"LOCMSG_SCRIPTEDITOR_SEARCHINDEX_FIELD":23,"LOCMSG_SCRIPTEDITOR_SEARCHINDEX_SUCCESS":24,"LOCMSG_SCRIPTEDITOR_SEARCHINDEX_FAILURE":25,"LOCMSG_SCRIPTEDITOR_SEARCHINDEX_STARTABORT":26,"LOCMSG_SCRIPTEDITOR_SEARCHINDEX_ENDABORT":27,"LOCMSG_SCRIPTEDITOR_SEARCHINDEX_TIMEOUT":28,"LOCMSG_SCRIPTEDITOR_SEARCHINDEX_OUTOFMEMORY":29},"LocalizedErrorCode":{"LOCERR_INTERNAL_ERROR":-128,"LOCERR_GENERIC_UNKNOWN":-1,"LOCERR_GENERIC_OK":0,"LOCERR_GENERIC_NOT_SET":1,"LOCERR_GENERIC_NOT_FOUND":2,"LOCERR_GENERIC_ALREADY_EXISTS":3,"LOCERR_GENERIC_INVALID_PATH":4,"LOCERR_GENERIC_ACCESS_DENIED":5,"LOCERR_GENERIC_OUT_OF_MEMORY":6,"LOCERR_GENERIC_NOT_INITIALIZED":7,"LOCERR_GENERIC_INVALID_PARAMETERS":8,"LOCERR_GENERIC_EMPTY_PARAMETERS":9,"LOCERR_GENERIC_INTERNAL_ERROR":10,"LOCERR_GENERIC_CORRUPT_DATA":11,"LOCERR_GENERIC_MEMORY_INCONSISTENCY":12,"LOCERR_GENERIC_INVISIBLE_OWNER_ABORT":13,"LOCERR_GENERIC_PROHIBIT_VALIDATE":14,"LOCERR_GENERIC_ABORTED":15,"LOCERR_GENERIC_CONNECTION_LOST":16,"LOCERR_GENERIC_UNSUPPORTED_IN_PRODUCT_VERSION":17,"LOCERR_GENERIC_REST_CONNECTION_FAILURE":18,"LOCERR_GENERIC_MEMORY_LIMIT_REACHED":19,"LOCERR_HTTP_400":400,"LOCERR_HTTP_401":401,"LOCERR_HTTP_402":402,"LOCERR_HTTP_403":403,"LOCERR_HTTP_404":404,"LOCERR_HTTP_405":405,"LOCERR_HTTP_406":406,"LOCERR_HTTP_407":407,"LOCERR_HTTP_408":408,"LOCERR_HTTP_409":409,"LOCERR_HTTP_410":410,"LOCERR_HTTP_411":411,"LOCERR_HTTP_412":412,"LOCERR_HTTP_413":413,"LOCERR_HTTP_414":414,"LOCERR_HTTP_415":415,"LOCERR_HTTP_416":416,"LOCERR_HTTP_417":417,"LOCERR_HTTP_422":422,"LOCERR_HTTP_429":429,"LOCERR_HTTP_500":500,"LOCERR_HTTP_501":501,"LOCERR_HTTP_502":502,"LOCERR_HTTP_503":503,"LOCERR_HTTP_504":504,"LOCERR_HTTP_505":505,"LOCERR_HTTP_509":509,"LOCERR_HTTP_COULD_NOT_RESOLVE_HOST":700,"LOCERR_APP_ALREADY_EXISTS":1000,"LOCERR_APP_INVALID_NAME":1001,"LOCERR_APP_ALREADY_OPEN":1002,"LOCERR_APP_NOT_FOUND":1003,"LOCERR_APP_IMPORT_FAILED":1004,"LOCERR_APP_SAVE_FAILED":1005,"LOCERR_APP_CREATE_FAILED":1006,"LOCERR_APP_INVALID":1007,"LOCERR_APP_CONNECT_FAILED":1008,"LOCERR_APP_ALREADY_OPEN_IN_DIFFERENT_MODE":1009,"LOCERR_APP_MIGRATION_COULD_NOT_CONTACT_MIGRATION_SERVICE":1010,"LOCERR_APP_MIGRATION_COULD_NOT_START_MIGRATION":1011,"LOCERR_APP_MIGRATION_FAILURE":1012,"LOCERR_APP_SCRIPT_MISSING":1013,"LOCERR_APP_EXPORT_FAILED":1014,"LOCERR_CONNECTION_ALREADY_EXISTS":2000,"LOCERR_CONNECTION_NOT_FOUND":2001,"LOCERR_CONNECTION_FAILED_TO_LOAD":2002,"LOCERR_CONNECTION_FAILED_TO_IMPORT":2003,"LOCERR_CONNECTION_NAME_IS_INVALID":2004,"LOCERR_CONNECTOR_NO_FILE_STREAMING_SUPPORT":2300,"LOCERR_CONNECTOR_FILESIZE_EXCEEDED_BUFFER_SIZE":2301,"LOCERR_FILE_ACCESS_DENIED":3000,"LOCERR_FILE_NAME_INVALID":3001,"LOCERR_FILE_CORRUPT":3002,"LOCERR_FILE_NOT_FOUND":3003,"LOCERR_FILE_FORMAT_UNSUPPORTED":3004,"LOCERR_FILE_OPENED_IN_UNSUPPORTED_MODE":3005,"LOCERR_FILE_TABLE_NOT_FOUND":3006,"LOCERR_USER_ACCESS_DENIED":4000,"LOCERR_USER_IMPERSONATION_FAILED":4001,"LOCERR_SERVER_OUT_OF_SESSION_AND_USER_CALS":5000,"LOCERR_SERVER_OUT_OF_SESSION_CALS":5001,"LOCERR_SERVER_OUT_OF_USAGE_CALS":5002,"LOCERR_SERVER_OUT_OF_CALS":5003,"LOCERR_SERVER_OUT_OF_NAMED_CALS":5004,"LOCERR_SERVER_OFF_DUTY":5005,"LOCERR_SERVER_BUSY":5006,"LOCERR_SERVER_LICENSE_EXPIRED":5007,"LOCERR_SERVER_AJAX_DISABLED":5008,"LOCERR_HC_INVALID_OBJECT":6000,"LOCERR_HC_RESULT_TOO_LARGE":6001,"LOCERR_HC_INVALID_OBJECT_STATE":6002,"LOCERR_HC_MODAL_OBJECT_ERROR":6003,"LOCERR_CALC_INVALID_DEF":7000,"LOCERR_CALC_NOT_IN_LIB":7001,"LOCERR_CALC_HEAP_ERROR":7002,"LOCERR_CALC_TOO_LARGE":7003,"LOCERR_CALC_TIMEOUT":7004,"LOCERR_CALC_EVAL_CONDITION_FAILED":7005,"LOCERR_CALC_MIXED_LINKED_AGGREGATION":7006,"LOCERR_CALC_MISSING_LINKED":7007,"LOCERR_CALC_INVALID_COL_SORT":7008,"LOCERR_CALC_PAGES_TOO_LARGE":7009,"LOCERR_CALC_SEMANTIC_FIELD_NOT_ALLOWED":7010,"LOCERR_CALC_VALIDATION_STATE_INVALID":7011,"LOCERR_CALC_PIVOT_DIMENSIONS_ALREADY_EXISTS":7012,"LOCERR_CALC_MISSING_LINKED_FIELD":7013,"LOCERR_CALC_NOT_CALCULATED":7014,"LOCERR_LAYOUT_EXTENDS_INVALID_ID":8000,"LOCERR_LAYOUT_LINKED_OBJECT_NOT_FOUND":8001,"LOCERR_LAYOUT_LINKED_OBJECT_INVALID":8002,"LOCERR_PERSISTENCE_WRITE_FAILED":9000,"LOCERR_PERSISTENCE_READ_FAILED":9001,"LOCERR_PERSISTENCE_DELETE_FAILED":9002,"LOCERR_PERSISTENCE_NOT_FOUND":9003,"LOCERR_PERSISTENCE_UNSUPPORTED_VERSION":9004,"LOCERR_PERSISTENCE_MIGRATION_FAILED_READ_ONLY":9005,"LOCERR_PERSISTENCE_MIGRATION_CANCELLED":9006,"LOCERR_PERSISTENCE_MIGRATION_BACKUP_FAILED":9007,"LOCERR_PERSISTENCE_DISK_FULL":9008,"LOCERR_PERSISTENCE_NOT_SUPPORTED_FOR_SESSION_APP":9009,"LOCERR_PERSISTENCE_SYNC_SET_CHUNK_INVALID_PARAMETERS":9510,"LOCERR_PERSISTENCE_SYNC_GET_CHUNK_INVALID_PARAMETERS":9511,"LOCERR_SCRIPT_DATASOURCE_ACCESS_DENIED":10000,"LOCERR_RELOAD_IN_PROGRESS":11000,"LOCERR_RELOAD_TABLE_X_NOT_FOUND":11001,"LOCERR_RELOAD_UNKNOWN_STATEMENT":11002,"LOCERR_RELOAD_EXPECTED_SOMETHING_FOUND_UNKNOWN":11003,"LOCERR_RELOAD_EXPECTED_NOTHING_FOUND_UNKNOWN":11004,"LOCERR_RELOAD_EXPECTED_ONE_OF_1_TOKENS_FOUND_UNKNOWN":11005,"LOCERR_RELOAD_EXPECTED_ONE_OF_2_TOKENS_FOUND_UNKNOWN":11006,"LOCERR_RELOAD_EXPECTED_ONE_OF_3_TOKENS_FOUND_UNKNOWN":11007,"LOCERR_RELOAD_EXPECTED_ONE_OF_4_TOKENS_FOUND_UNKNOWN":11008,"LOCERR_RELOAD_EXPECTED_ONE_OF_5_TOKENS_FOUND_UNKNOWN":11009,"LOCERR_RELOAD_EXPECTED_ONE_OF_6_TOKENS_FOUND_UNKNOWN":11010,"LOCERR_RELOAD_EXPECTED_ONE_OF_7_TOKENS_FOUND_UNKNOWN":11011,"LOCERR_RELOAD_EXPECTED_ONE_OF_8_OR_MORE_TOKENS_FOUND_UNKNOWN":11012,"LOCERR_RELOAD_FIELD_X_NOT_FOUND":11013,"LOCERR_RELOAD_MAPPING_TABLE_X_NOT_FOUND":11014,"LOCERR_RELOAD_LIB_CONNECTION_X_NOT_FOUND":11015,"LOCERR_RELOAD_NAME_ALREADY_TAKEN":11016,"LOCERR_RELOAD_WRONG_FILE_FORMAT_DIF":11017,"LOCERR_RELOAD_WRONG_FILE_FORMAT_BIFF":11018,"LOCERR_RELOAD_WRONG_FILE_FORMAT_ENCRYPTED":11019,"LOCERR_RELOAD_OPEN_FILE_ERROR":11020,"LOCERR_RELOAD_AUTO_GENERATE_COUNT":11021,"LOCERR_RELOAD_PE_ILLEGAL_PREFIX_COMB":11022,"LOCERR_RELOAD_MATCHING_CONTROL_STATEMENT_ERROR":11023,"LOCERR_RELOAD_MATCHING_LIBPATH_X_NOT_FOUND":11024,"LOCERR_RELOAD_MATCHING_LIBPATH_X_INVALID":11025,"LOCERR_RELOAD_MATCHING_LIBPATH_X_OUTSIDE":11026,"LOCERR_RELOAD_NO_QUALIFIED_PATH_FOR_FILE":11027,"LOCERR_RELOAD_MODE_STATEMENT_ONLY_FOR_LIB_PATHS":11028,"LOCERR_RELOAD_INCONSISTENT_USE_OF_SEMANTIC_FIELDS":11029,"LOCERR_RELOAD_NO_OPEN_DATABASE":11030,"LOCERR_RELOAD_AGGREGATION_REQUIRED_BY_GROUP_BY":11031,"LOCERR_RELOAD_CONNECT_MUST_USE_LIB_PREFIX_IN_THIS_MODE":11032,"LOCERR_RELOAD_ODBC_CONNECT_FAILED":11033,"LOCERR_RELOAD_OLEDB_CONNECT_FAILED":11034,"LOCERR_RELOAD_CUSTOM_CONNECT_FAILED":11035,"LOCERR_RELOAD_ODBC_READ_FAILED":11036,"LOCERR_RELOAD_OLEDB_READ_FAILED":11037,"LOCERR_RELOAD_CUSTOM_READ_FAILED":11038,"LOCERR_RELOAD_BINARY_LOAD_PROHIBITED":11039,"LOCERR_RELOAD_CONNECTOR_START_FAILED":11040,"LOCERR_RELOAD_CONNECTOR_NOT_RESPONDING":11041,"LOCERR_RELOAD_CONNECTOR_REPLY_ERROR":11042,"LOCERR_RELOAD_CONNECTOR_CONNECT_ERROR":11043,"LOCERR_RELOAD_CONNECTOR_NOT_FOUND_ERROR":11044,"LOCERR_RELOAD_INPUT_FIELD_WITH_DUPLICATE_KEYS":11045,"LOCERR_RELOAD_CONCATENATE_LOAD_NO_PREVIOUS_TABLE":11046,"LOCERR_PERSONAL_NEW_VERSION_AVAILABLE":12000,"LOCERR_PERSONAL_VERSION_EXPIRED":12001,"LOCERR_PERSONAL_SECTION_ACCESS_DETECTED":12002,"LOCERR_PERSONAL_APP_DELETION_FAILED":12003,"LOCERR_USER_AUTHENTICATION_FAILURE":12004,"LOCERR_EXPORT_OUT_OF_MEMORY":13000,"LOCERR_EXPORT_NO_DATA":13001,"LOCERR_SYNC_INVALID_OFFSET":14000,"LOCERR_SEARCH_TIMEOUT":15000,"LOCERR_DIRECT_DISCOVERY_LINKED_EXPRESSION_FAIL":16000,"LOCERR_DIRECT_DISCOVERY_ROWCOUNT_OVERFLOW":16001,"LOCERR_DIRECT_DISCOVERY_EMPTY_RESULT":16002,"LOCERR_DIRECT_DISCOVERY_DB_CONNECTION_FAILED":16003,"LOCERR_DIRECT_DISCOVERY_MEASURE_NOT_ALLOWED":16004,"LOCERR_DIRECT_DISCOVERY_DETAIL_NOT_ALLOWED":16005,"LOCERR_DIRECT_DISCOVERY_NOT_SYNTH_CIRCULAR_ALLOWED":16006,"LOCERR_DIRECT_DISCOVERY_ONLY_ONE_DD_TABLE_ALLOWED":16007,"LOCERR_DIRECT_DISCOVERY_DB_AUTHORIZATION_FAILED":16008,"LOCERR_SMART_LOAD_TABLE_NOT_FOUND":17000,"LOCERR_SMART_LOAD_TABLE_DUPLICATED":17001,"LOCERR_VARIABLE_NO_NAME":18000,"LOCERR_VARIABLE_DUPLICATE_NAME":18001,"LOCERR_VARIABLE_INCONSISTENCY":18002,"LOCERR_MEDIA_LIBRARY_LIST_FAILED":19000,"LOCERR_MEDIA_LIBRARY_CONTENT_FAILED":19001,"LOCERR_MEDIA_BUNDLING_FAILED":19002,"LOCERR_MEDIA_UNBUNDLING_FAILED":19003,"LOCERR_MEDIA_LIBRARY_NOT_FOUND":19004,"LOCERR_FEATURE_DISABLED":20000,"LOCERR_JSON_RPC_INVALID_REQUEST":-32600,"LOCERR_JSON_RPC_METHOD_NOT_FOUND":-32601,"LOCERR_JSON_RPC_INVALID_PARAMETERS":-32602,"LOCERR_JSON_RPC_INTERNAL_ERROR":-32603,"LOCERR_JSON_RPC_PARSE_ERROR":-32700,"LOCERR_MQ_SOCKET_CONNECT_FAILURE":33000,"LOCERR_MQ_SOCKET_OPEN_FAILURE":33001,"LOCERR_MQ_PROTOCOL_NO_RESPONE":33002,"LOCERR_MQ_PROTOCOL_LIBRARY_EXCEPTION":33003,"LOCERR_MQ_PROTOCOL_CONNECTION_CLOSED":33004,"LOCERR_MQ_PROTOCOL_CHANNEL_CLOSED":33005,"LOCERR_MQ_PROTOCOL_UNKNOWN_ERROR":33006,"LOCERR_MQ_PROTOCOL_INVALID_STATUS":33007,"LOCERR_EXTENGINE_GRPC_STATUS_OK":22000,"LOCERR_EXTENGINE_GRPC_STATUS_CANCELLED":22001,"LOCERR_EXTENGINE_GRPC_STATUS_UNKNOWN":22002,"LOCERR_EXTENGINE_GRPC_STATUS_INVALID_ARGUMENT":22003,"LOCERR_EXTENGINE_GRPC_STATUS_DEADLINE_EXCEEDED":22004,"LOCERR_EXTENGINE_GRPC_STATUS_NOT_FOUND":22005,"LOCERR_EXTENGINE_GRPC_STATUS_ALREADY_EXISTS":22006,"LOCERR_EXTENGINE_GRPC_STATUS_PERMISSION_DENIED":22007,"LOCERR_EXTENGINE_GRPC_STATUS_RESOURCE_EXHAUSTED":22008,"LOCERR_EXTENGINE_GRPC_STATUS_FAILED_PRECONDITION":22009,"LOCERR_EXTENGINE_GRPC_STATUS_ABORTED":22010,"LOCERR_EXTENGINE_GRPC_STATUS_OUT_OF_RANGE":22011,"LOCERR_EXTENGINE_GRPC_STATUS_UNIMPLEMENTED":22012,"LOCERR_EXTENGINE_GRPC_STATUS_INTERNAL":22013,"LOCERR_EXTENGINE_GRPC_STATUS_UNAVAILABLE":22014,"LOCERR_EXTENGINE_GRPC_STATUS_DATA_LOSS":22015,"LOCERR_EXTENGINE_GRPC_STATUS_UNAUTHENTICATED":22016,"LOCERR_LXW_INVALID_OBJ":23001,"LOCERR_LXW_INVALID_FILE":23002,"LOCERR_LXW_INVALID_SHEET":23003,"LOCERR_LXW_INVALID_EXPORT_RANGE":23004,"LOCERR_LXW_ERROR":23005,"LOCERR_LXW_ERROR_MEMORY_MALLOC_FAILED":23006,"LOCERR_LXW_ERROR_CREATING_XLSX_FILE":23007,"LOCERR_LXW_ERROR_CREATING_TMPFILE":23008,"LOCERR_LXW_ERROR_ZIP_FILE_OPERATION":23009,"LOCERR_LXW_ERROR_ZIP_FILE_ADD":23010,"LOCERR_LXW_ERROR_ZIP_CLOSE":23011,"LOCERR_LXW_ERROR_NULL_PARAMETER_IGNORED":23012,"LOCERR_LXW_ERROR_MAX_STRING_LENGTH_EXCEEDED":23013,"LOCERR_LXW_ERROR_255_STRING_LENGTH_EXCEEDED":23014,"LOCERR_LXW_ERROR_SHARED_STRING_INDEX_NOT_FOUND":23015,"LOCERR_LXW_ERROR_WORKSHEET_INDEX_OUT_OF_RANGE":23016,"LOCERR_LXW_ERROR_WORKSHEET_MAX_NUMBER_URLS_EXCEEDED":23017,"LOCERR_CURL_UNSUPPORTED_PROTOCOL":30000,"LOCERR_CURL_COULDNT_RESOLVE_PROXY":30001,"LOCERR_CURL_COULDNT_CONNECT":30002,"LOCERR_CURL_REMOTE_ACCESS_DENIED":30003,"LOCERR_CURL_FTP_ACCEPT_FAILED":30004,"LOCERR_CURL_FTP_ACCEPT_TIMEOUT":30005,"LOCERR_CURL_FTP_CANT_GET_HOST":30006,"LOCERR_CURL_PARTIAL_FILE":30007,"LOCERR_CURL_QUOTE_ERROR":30008,"LOCERR_CURL_WRITE_ERROR":30009,"LOCERR_CURL_UPLOAD_FAILED":30010,"LOCERR_CURL_OUT_OF_MEMORY":30011,"LOCERR_CURL_OPERATION_TIMEDOUT":30012,"LOCERR_CURL_FTP_COULDNT_USE_REST":30013,"LOCERR_CURL_HTTP_POST_ERROR":30014,"LOCERR_CURL_SSL_CONNECT_ERROR":30015,"LOCERR_CURL_FILE_COULDNT_READ_FILE":30016,"LOCERR_CURL_LDAP_CANNOT_BIND":30017,"LOCERR_CURL_LDAP_SEARCH_FAILED":30018,"LOCERR_CURL_TOO_MANY_REDIRECTS":30019,"LOCERR_CURL_PEER_FAILED_VERIFICATION":30020,"LOCERR_CURL_GOT_NOTHING":30021,"LOCERR_CURL_SSL_ENGINE_NOTFOUND":30022,"LOCERR_CURL_SSL_ENGINE_SETFAILED":30023,"LOCERR_CURL_SSL_CERTPROBLEM":30024,"LOCERR_CURL_SSL_CIPHER":30025,"LOCERR_CURL_SSL_CACERT":30026,"LOCERR_CURL_BAD_CONTENT_ENCODING":30027,"LOCERR_CURL_LDAP_INVALID_URL":30028,"LOCERR_CURL_USE_SSL_FAILED":30029,"LOCERR_CURL_SSL_ENGINE_INITFAILED":30030,"LOCERR_CURL_LOGIN_DENIED":30031,"LOCERR_CURL_TFTP_NOTFOUND":30032,"LOCERR_CURL_TFTP_ILLEGAL":30033,"LOCERR_CURL_SSH":30034},"LocalizedWarningCode":{"LOCWARN_PERSONAL_RELOAD_REQUIRED":0,"LOCWARN_PERSONAL_VERSION_EXPIRES_SOON":1,"LOCWARN_EXPORT_DATA_TRUNCATED":1000,"LOCWARN_COULD_NOT_OPEN_ALL_OBJECTS":2000},"GrpType":{"GRP_NX_NONE":0,"GRP_NX_HIEARCHY":1,"GRP_NX_COLLECTION":2},"ExportFileType":{"EXPORT_CSV_C":0,"EXPORT_CSV_T":1,"EXPORT_OOXML":2},"ExportState":{"EXPORT_POSSIBLE":0,"EXPORT_ALL":1},"DimCellType":{"NX_DIM_CELL_VALUE":0,"NX_DIM_CELL_EMPTY":1,"NX_DIM_CELL_NORMAL":2,"NX_DIM_CELL_TOTAL":3,"NX_DIM_CELL_OTHER":4,"NX_DIM_CELL_AGGR":5,"NX_DIM_CELL_PSEUDO":6,"NX_DIM_CELL_ROOT":7,"NX_DIM_CELL_NULL":8,"NX_DIM_CELL_GENERATED":9},"StackElemType":{"NX_STACK_CELL_NORMAL":0,"NX_STACK_CELL_TOTAL":1,"NX_STACK_CELL_OTHER":2,"NX_STACK_CELL_SUM":3,"NX_STACK_CELL_VALUE":4,"NX_STACK_CELL_PSEUDO":5},"SortIndicatorType":{"NX_SORT_INDICATE_NONE":0,"NX_SORT_INDICATE_ASC":1,"NX_SORT_INDICATE_DESC":2},"DimensionType":{"NX_DIMENSION_TYPE_DISCRETE":0,"NX_DIMENSION_TYPE_NUMERIC":1,"NX_DIMENSION_TYPE_TIME":2},"FieldSelectionMode":{"SELECTION_MODE_NORMAL":0,"SELECTION_MODE_AND":1,"SELECTION_MODE_NOT":2},"FrequencyMode":{"NX_FREQUENCY_NONE":0,"NX_FREQUENCY_VALUE":1,"NX_FREQUENCY_PERCENT":2,"NX_FREQUENCY_RELATIVE":3},"DataReductionMode":{"DATA_REDUCTION_NONE":0,"DATA_REDUCTION_ONEDIM":1,"DATA_REDUCTION_SCATTERED":2,"DATA_REDUCTION_CLUSTERED":3,"DATA_REDUCTION_STACKED":4},"HypercubeMode":{"DATA_MODE_STRAIGHT":0,"DATA_MODE_PIVOT":1,"DATA_MODE_PIVOT_STACK":2,"DATA_MODE_TREE":3},"PatchOperationType":{"Add":0,"Remove":1,"Replace":2},"SelectionCellType":{"NX_CELL_DATA":0,"NX_CELL_TOP":1,"NX_CELL_LEFT":2},"MatchingFieldMode":{"MATCHINGFIELDMODE_MATCH_ALL":0,"MATCHINGFIELDMODE_MATCH_ONE":1},"SessionState":{"SESSION_CREATED":0,"SESSION_ATTACHED":1,"SESSION_ERROR_NO_LICENSE":2,"SESSION_ERROR_LICENSE_RENEW":3},"ReloadState":{"RELOAD_PAUSED":0,"RELOAD_STARTED":1,"RELOAD_ABORTED":2},"QrsChangeType":{"QRS_CHANGE_UNDEFINED":0,"QRS_CHANGE_ADD":1,"QRS_CHANGE_UPDATE":2,"QRS_CHANGE_DELETE":3},"ExtEngineDataType":{"NX_EXT_DATATYPE_STRING":0,"NX_EXT_DATATYPE_DOUBLE":1,"NX_EXT_DATATYPE_BOTH":2},"ExtEngineFunctionType":{"NX_EXT_FUNCTIONTYPE_SCALAR":0,"NX_EXT_FUNCTIONTYPE_AGGR":1,"NX_EXT_FUNCTIONTYPE_TENSOR":2},"ExtEngineMsgType":{"NX_EXT_MSGTYPE_FUNCTION_CALL":1,"NX_EXT_MSGTYPE_SCRIPT_CALL":2,"NX_EXT_MSGTYPE_RETURN_VALUE":3,"NX_EXT_MSGTYPE_RETURN_MULTIPLE":4,"NX_EXT_MSGTYPE_RETURN_ERROR":5}},"version":"12.170.2"};

/***/ }),

/***/ "a159":
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.2 / 15.2.3.5 Object.create(O [, Properties])
var anObject = __webpack_require__("e4ae");
var dPs = __webpack_require__("7e90");
var enumBugKeys = __webpack_require__("1691");
var IE_PROTO = __webpack_require__("5559")('IE_PROTO');
var Empty = function () { /* empty */ };
var PROTOTYPE = 'prototype';

// Create object with fake `null` prototype: use iframe Object with cleared prototype
var createDict = function () {
  // Thrash, waste and sodomy: IE GC bug
  var iframe = __webpack_require__("1ec9")('iframe');
  var i = enumBugKeys.length;
  var lt = '<';
  var gt = '>';
  var iframeDocument;
  iframe.style.display = 'none';
  __webpack_require__("32fc").appendChild(iframe);
  iframe.src = 'javascript:'; // eslint-disable-line no-script-url
  // createDict = iframe.contentWindow.Object;
  // html.removeChild(iframe);
  iframeDocument = iframe.contentWindow.document;
  iframeDocument.open();
  iframeDocument.write(lt + 'script' + gt + 'document.F=Object' + lt + '/script' + gt);
  iframeDocument.close();
  createDict = iframeDocument.F;
  while (i--) delete createDict[PROTOTYPE][enumBugKeys[i]];
  return createDict();
};

module.exports = Object.create || function create(O, Properties) {
  var result;
  if (O !== null) {
    Empty[PROTOTYPE] = anObject(O);
    result = new Empty();
    Empty[PROTOTYPE] = null;
    // add "__proto__" for Object.getPrototypeOf polyfill
    result[IE_PROTO] = O;
  } else result = createDict();
  return Properties === undefined ? result : dPs(result, Properties);
};


/***/ }),

/***/ "a1f5":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var addToUnscopables = __webpack_require__("1daf");
var step = __webpack_require__("935a");
var Iterators = __webpack_require__("16f9");
var toIObject = __webpack_require__("531a");

// 22.1.3.4 Array.prototype.entries()
// 22.1.3.13 Array.prototype.keys()
// 22.1.3.29 Array.prototype.values()
// 22.1.3.30 Array.prototype[@@iterator]()
module.exports = __webpack_require__("7782")(Array, 'Array', function (iterated, kind) {
  this._t = toIObject(iterated); // target
  this._i = 0;                   // next index
  this._k = kind;                // kind
// 22.1.5.2.1 %ArrayIteratorPrototype%.next()
}, function () {
  var O = this._t;
  var kind = this._k;
  var index = this._i++;
  if (!O || index >= O.length) {
    this._t = undefined;
    return step(1);
  }
  if (kind == 'keys') return step(0, index);
  if (kind == 'values') return step(0, O[index]);
  return step(0, [index, O[index]]);
}, 'values');

// argumentsList[@@iterator] is %ArrayProto_values% (9.4.4.6, 9.4.4.7)
Iterators.Arguments = Iterators.Array;

addToUnscopables('keys');
addToUnscopables('values');
addToUnscopables('entries');


/***/ }),

/***/ "a21f":
/***/ (function(module, exports, __webpack_require__) {

var core = __webpack_require__("584a");
var $JSON = core.JSON || (core.JSON = { stringify: JSON.stringify });
module.exports = function stringify(it) { // eslint-disable-line no-unused-vars
  return $JSON.stringify.apply($JSON, arguments);
};


/***/ }),

/***/ "a22a":
/***/ (function(module, exports, __webpack_require__) {

var ctx = __webpack_require__("d864");
var call = __webpack_require__("b0dc");
var isArrayIter = __webpack_require__("3702");
var anObject = __webpack_require__("e4ae");
var toLength = __webpack_require__("b447");
var getIterFn = __webpack_require__("7cd6");
var BREAK = {};
var RETURN = {};
var exports = module.exports = function (iterable, entries, fn, that, ITERATOR) {
  var iterFn = ITERATOR ? function () { return iterable; } : getIterFn(iterable);
  var f = ctx(fn, that, entries ? 2 : 1);
  var index = 0;
  var length, step, iterator, result;
  if (typeof iterFn != 'function') throw TypeError(iterable + ' is not iterable!');
  // fast case for arrays with default iterator
  if (isArrayIter(iterFn)) for (length = toLength(iterable.length); length > index; index++) {
    result = entries ? f(anObject(step = iterable[index])[0], step[1]) : f(iterable[index]);
    if (result === BREAK || result === RETURN) return result;
  } else for (iterator = iterFn.call(iterable); !(step = iterator.next()).done;) {
    result = call(iterator, f, step.value, entries);
    if (result === BREAK || result === RETURN) return result;
  }
};
exports.BREAK = BREAK;
exports.RETURN = RETURN;


/***/ }),

/***/ "a28d":
/***/ (function(module, exports, __webpack_require__) {

// getting tag from 19.1.3.6 Object.prototype.toString()
var cof = __webpack_require__("dc45");
var TAG = __webpack_require__("fe95")('toStringTag');
// ES3 wrong here
var ARG = cof(function () { return arguments; }()) == 'Arguments';

// fallback for IE11 Script Access Denied error
var tryGet = function (it, key) {
  try {
    return it[key];
  } catch (e) { /* empty */ }
};

module.exports = function (it) {
  var O, T, B;
  return it === undefined ? 'Undefined' : it === null ? 'Null'
    // @@toStringTag case
    : typeof (T = tryGet(O = Object(it), TAG)) == 'string' ? T
    // builtinTag case
    : ARG ? cof(O)
    // ES3 arguments fallback
    : (B = cof(O)) == 'Object' && typeof O.callee == 'function' ? 'Arguments' : B;
};


/***/ }),

/***/ "a3c3":
/***/ (function(module, exports, __webpack_require__) {

// 19.1.3.1 Object.assign(target, source)
var $export = __webpack_require__("63b6");

$export($export.S + $export.F, 'Object', { assign: __webpack_require__("9306") });


/***/ }),

/***/ "a3cc":
/***/ (function(module, exports) {

module.exports = function (bitmap, value) {
  return {
    enumerable: !(bitmap & 1),
    configurable: !(bitmap & 2),
    writable: !(bitmap & 4),
    value: value
  };
};


/***/ }),

/***/ "a4bb":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("8aae");

/***/ }),

/***/ "a522":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("ea03");

/***/ }),

/***/ "a6b3":
/***/ (function(module, exports) {

module.exports = false;


/***/ }),

/***/ "a745":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("f410");

/***/ }),

/***/ "aa24":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var create = __webpack_require__("8cc2");
var descriptor = __webpack_require__("a3cc");
var setToStringTag = __webpack_require__("09e3");
var IteratorPrototype = {};

// 25.1.2.1.1 %IteratorPrototype%[@@iterator]()
__webpack_require__("59c4")(IteratorPrototype, __webpack_require__("fe95")('iterator'), function () { return this; });

module.exports = function (Constructor, NAME, next) {
  Constructor.prototype = create(IteratorPrototype, { next: descriptor(1, next) });
  setToStringTag(Constructor, NAME + ' Iterator');
};


/***/ }),

/***/ "aba2":
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__("e53d");
var macrotask = __webpack_require__("4178").set;
var Observer = global.MutationObserver || global.WebKitMutationObserver;
var process = global.process;
var Promise = global.Promise;
var isNode = __webpack_require__("6b4c")(process) == 'process';

module.exports = function () {
  var head, last, notify;

  var flush = function () {
    var parent, fn;
    if (isNode && (parent = process.domain)) parent.exit();
    while (head) {
      fn = head.fn;
      head = head.next;
      try {
        fn();
      } catch (e) {
        if (head) notify();
        else last = undefined;
        throw e;
      }
    } last = undefined;
    if (parent) parent.enter();
  };

  // Node.js
  if (isNode) {
    notify = function () {
      process.nextTick(flush);
    };
  // browsers with MutationObserver, except iOS Safari - https://github.com/zloirock/core-js/issues/339
  } else if (Observer && !(global.navigator && global.navigator.standalone)) {
    var toggle = true;
    var node = document.createTextNode('');
    new Observer(flush).observe(node, { characterData: true }); // eslint-disable-line no-new
    notify = function () {
      node.data = toggle = !toggle;
    };
  // environments with maybe non-completely correct, but existent Promise
  } else if (Promise && Promise.resolve) {
    // Promise.resolve without an argument throws an error in LG WebOS 2
    var promise = Promise.resolve(undefined);
    notify = function () {
      promise.then(flush);
    };
  // for other environments - macrotask based on:
  // - setImmediate
  // - MessageChannel
  // - window.postMessag
  // - onreadystatechange
  // - setTimeout
  } else {
    notify = function () {
      // strange IE + webpack dev server bug - use .call(global)
      macrotask.call(global, flush);
    };
  }

  return function (fn) {
    var task = { fn: fn, next: undefined };
    if (last) last.next = task;
    if (!head) {
      head = task;
      notify();
    } last = task;
  };
};


/***/ }),

/***/ "aebd":
/***/ (function(module, exports) {

module.exports = function (bitmap, value) {
  return {
    enumerable: !(bitmap & 1),
    configurable: !(bitmap & 2),
    writable: !(bitmap & 4),
    value: value
  };
};


/***/ }),

/***/ "b0dc":
/***/ (function(module, exports, __webpack_require__) {

// call something on iterator step with safe closing on error
var anObject = __webpack_require__("e4ae");
module.exports = function (iterator, fn, value, entries) {
  try {
    return entries ? fn(anObject(value)[0], value[1]) : fn(value);
  // 7.4.6 IteratorClose(iterator, completion)
  } catch (e) {
    var ret = iterator['return'];
    if (ret !== undefined) anObject(ret.call(iterator));
    throw e;
  }
};


/***/ }),

/***/ "b447":
/***/ (function(module, exports, __webpack_require__) {

// 7.1.15 ToLength
var toInteger = __webpack_require__("3a38");
var min = Math.min;
module.exports = function (it) {
  return it > 0 ? min(toInteger(it), 0x1fffffffffffff) : 0; // pow(2, 53) - 1 == 9007199254740991
};


/***/ }),

/***/ "b4df":
/***/ (function(module, exports, __webpack_require__) {

// 7.1.15 ToLength
var toInteger = __webpack_require__("58ce");
var min = Math.min;
module.exports = function (it) {
  return it > 0 ? min(toInteger(it), 0x1fffffffffffff) : 0; // pow(2, 53) - 1 == 9007199254740991
};


/***/ }),

/***/ "b597":
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__("9691");
module.exports = function (it) {
  if (!isObject(it)) throw TypeError(it + ' is not an object!');
  return it;
};


/***/ }),

/***/ "b5ef":
/***/ (function(module, exports, __webpack_require__) {

// 7.1.1 ToPrimitive(input [, PreferredType])
var isObject = __webpack_require__("9691");
// instead of the ES6 spec version, we didn't implement @@toPrimitive case
// and the second argument - flag - preferred type is a string
module.exports = function (it, S) {
  if (!isObject(it)) return it;
  var fn, val;
  if (S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  if (typeof (fn = it.valueOf) == 'function' && !isObject(val = fn.call(it))) return val;
  if (!S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  throw TypeError("Can't convert object to primitive value");
};


/***/ }),

/***/ "b658":
/***/ (function(module, exports, __webpack_require__) {

var core = __webpack_require__("7a1e");
var global = __webpack_require__("635a");
var SHARED = '__core-js_shared__';
var store = global[SHARED] || (global[SHARED] = {});

(module.exports = function (key, value) {
  return store[key] || (store[key] = value !== undefined ? value : {});
})('versions', []).push({
  version: core.version,
  mode: __webpack_require__("a6b3") ? 'pure' : 'global',
  copyright: '© 2018 Denis Pushkarev (zloirock.ru)'
});


/***/ }),

/***/ "b8e3":
/***/ (function(module, exports) {

module.exports = true;


/***/ }),

/***/ "b98f":
/***/ (function(module, exports, __webpack_require__) {

// optional / simple context binding
var aFunction = __webpack_require__("fc4a");
module.exports = function (fn, that, length) {
  aFunction(fn);
  if (that === undefined) return fn;
  switch (length) {
    case 1: return function (a) {
      return fn.call(that, a);
    };
    case 2: return function (a, b) {
      return fn.call(that, a, b);
    };
    case 3: return function (a, b, c) {
      return fn.call(that, a, b, c);
    };
  }
  return function (/* ...args */) {
    return fn.apply(that, arguments);
  };
};


/***/ }),

/***/ "bc13":
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__("e53d");
var navigator = global.navigator;

module.exports = navigator && navigator.userAgent || '';


/***/ }),

/***/ "be13":
/***/ (function(module, exports) {

// 7.2.1 RequireObjectCoercible(argument)
module.exports = function (it) {
  if (it == undefined) throw TypeError("Can't call method on  " + it);
  return it;
};


/***/ }),

/***/ "bf0b":
/***/ (function(module, exports, __webpack_require__) {

var pIE = __webpack_require__("355d");
var createDesc = __webpack_require__("aebd");
var toIObject = __webpack_require__("36c3");
var toPrimitive = __webpack_require__("1bc3");
var has = __webpack_require__("07e3");
var IE8_DOM_DEFINE = __webpack_require__("794b");
var gOPD = Object.getOwnPropertyDescriptor;

exports.f = __webpack_require__("8e60") ? gOPD : function getOwnPropertyDescriptor(O, P) {
  O = toIObject(O);
  P = toPrimitive(P, true);
  if (IE8_DOM_DEFINE) try {
    return gOPD(O, P);
  } catch (e) { /* empty */ }
  if (has(O, P)) return createDesc(!pIE.f.call(O, P), O[P]);
};


/***/ }),

/***/ "bf90":
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.6 Object.getOwnPropertyDescriptor(O, P)
var toIObject = __webpack_require__("36c3");
var $getOwnPropertyDescriptor = __webpack_require__("bf0b").f;

__webpack_require__("ce7e")('getOwnPropertyDescriptor', function () {
  return function getOwnPropertyDescriptor(it, key) {
    return $getOwnPropertyDescriptor(toIObject(it), key);
  };
});


/***/ }),

/***/ "c207":
/***/ (function(module, exports) {



/***/ }),

/***/ "c259":
/***/ (function(module, exports, __webpack_require__) {

var $iterators = __webpack_require__("a1f5");
var getKeys = __webpack_require__("d0af");
var redefine = __webpack_require__("3143");
var global = __webpack_require__("635a");
var hide = __webpack_require__("59c4");
var Iterators = __webpack_require__("16f9");
var wks = __webpack_require__("fe95");
var ITERATOR = wks('iterator');
var TO_STRING_TAG = wks('toStringTag');
var ArrayValues = Iterators.Array;

var DOMIterables = {
  CSSRuleList: true, // TODO: Not spec compliant, should be false.
  CSSStyleDeclaration: false,
  CSSValueList: false,
  ClientRectList: false,
  DOMRectList: false,
  DOMStringList: false,
  DOMTokenList: true,
  DataTransferItemList: false,
  FileList: false,
  HTMLAllCollection: false,
  HTMLCollection: false,
  HTMLFormElement: false,
  HTMLSelectElement: false,
  MediaList: true, // TODO: Not spec compliant, should be false.
  MimeTypeArray: false,
  NamedNodeMap: false,
  NodeList: true,
  PaintRequestList: false,
  Plugin: false,
  PluginArray: false,
  SVGLengthList: false,
  SVGNumberList: false,
  SVGPathSegList: false,
  SVGPointList: false,
  SVGStringList: false,
  SVGTransformList: false,
  SourceBufferList: false,
  StyleSheetList: true, // TODO: Not spec compliant, should be false.
  TextTrackCueList: false,
  TextTrackList: false,
  TouchList: false
};

for (var collections = getKeys(DOMIterables), i = 0; i < collections.length; i++) {
  var NAME = collections[i];
  var explicit = DOMIterables[NAME];
  var Collection = global[NAME];
  var proto = Collection && Collection.prototype;
  var key;
  if (proto) {
    if (!proto[ITERATOR]) hide(proto, ITERATOR, ArrayValues);
    if (!proto[TO_STRING_TAG]) hide(proto, TO_STRING_TAG, NAME);
    Iterators[NAME] = ArrayValues;
    if (explicit) for (key in $iterators) if (!proto[key]) redefine(proto, key, $iterators[key], true);
  }
}


/***/ }),

/***/ "c26b":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// 21.2.5.3 get RegExp.prototype.flags
var anObject = __webpack_require__("b597");
module.exports = function () {
  var that = anObject(this);
  var result = '';
  if (that.global) result += 'g';
  if (that.ignoreCase) result += 'i';
  if (that.multiline) result += 'm';
  if (that.unicode) result += 'u';
  if (that.sticky) result += 'y';
  return result;
};


/***/ }),

/***/ "c367":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var addToUnscopables = __webpack_require__("8436");
var step = __webpack_require__("50ed");
var Iterators = __webpack_require__("481b");
var toIObject = __webpack_require__("36c3");

// 22.1.3.4 Array.prototype.entries()
// 22.1.3.13 Array.prototype.keys()
// 22.1.3.29 Array.prototype.values()
// 22.1.3.30 Array.prototype[@@iterator]()
module.exports = __webpack_require__("30f1")(Array, 'Array', function (iterated, kind) {
  this._t = toIObject(iterated); // target
  this._i = 0;                   // next index
  this._k = kind;                // kind
// 22.1.5.2.1 %ArrayIteratorPrototype%.next()
}, function () {
  var O = this._t;
  var kind = this._k;
  var index = this._i++;
  if (!O || index >= O.length) {
    this._t = undefined;
    return step(1);
  }
  if (kind == 'keys') return step(0, index);
  if (kind == 'values') return step(0, O[index]);
  return step(0, [index, O[index]]);
}, 'values');

// argumentsList[@@iterator] is %ArrayProto_values% (9.4.4.6, 9.4.4.7)
Iterators.Arguments = Iterators.Array;

addToUnscopables('keys');
addToUnscopables('values');
addToUnscopables('entries');


/***/ }),

/***/ "c3a1":
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.14 / 15.2.3.14 Object.keys(O)
var $keys = __webpack_require__("e6f3");
var enumBugKeys = __webpack_require__("1691");

module.exports = Object.keys || function keys(O) {
  return $keys(O, enumBugKeys);
};


/***/ }),

/***/ "c575":
/***/ (function(module, exports) {

// 7.2.9 SameValue(x, y)
module.exports = Object.is || function is(x, y) {
  // eslint-disable-next-line no-self-compare
  return x === y ? x !== 0 || 1 / x === 1 / y : x != x && y != y;
};


/***/ }),

/***/ "c59a":
/***/ (function(module, exports, __webpack_require__) {

// 7.2.8 IsRegExp(argument)
var isObject = __webpack_require__("9691");
var cof = __webpack_require__("dc45");
var MATCH = __webpack_require__("fe95")('match');
module.exports = function (it) {
  var isRegExp;
  return isObject(it) && ((isRegExp = it[MATCH]) !== undefined ? !!isRegExp : cof(it) == 'RegExp');
};


/***/ }),

/***/ "c69a":
/***/ (function(module, exports, __webpack_require__) {

module.exports = !__webpack_require__("9e1e") && !__webpack_require__("79e5")(function () {
  return Object.defineProperty(__webpack_require__("230e")('div'), 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),

/***/ "c8bb":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("54a1");

/***/ }),

/***/ "ca5a":
/***/ (function(module, exports) {

var id = 0;
var px = Math.random();
module.exports = function (key) {
  return 'Symbol('.concat(key === undefined ? '' : key, ')_', (++id + px).toString(36));
};


/***/ }),

/***/ "cb7c":
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__("d3f4");
module.exports = function (it) {
  if (!isObject(it)) throw TypeError(it + ' is not an object!');
  return it;
};


/***/ }),

/***/ "ccb9":
/***/ (function(module, exports, __webpack_require__) {

exports.f = __webpack_require__("5168");


/***/ }),

/***/ "cd03":
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.15 Object.preventExtensions(O)
var isObject = __webpack_require__("f772");
var meta = __webpack_require__("ebfd").onFreeze;

__webpack_require__("ce7e")('preventExtensions', function ($preventExtensions) {
  return function preventExtensions(it) {
    return $preventExtensions && isObject(it) ? $preventExtensions(meta(it)) : it;
  };
});


/***/ }),

/***/ "cd78":
/***/ (function(module, exports, __webpack_require__) {

var anObject = __webpack_require__("e4ae");
var isObject = __webpack_require__("f772");
var newPromiseCapability = __webpack_require__("656e");

module.exports = function (C, x) {
  anObject(C);
  if (isObject(x) && x.constructor === C) return x;
  var promiseCapability = newPromiseCapability.f(C);
  var resolve = promiseCapability.resolve;
  resolve(x);
  return promiseCapability.promise;
};


/***/ }),

/***/ "ce7e":
/***/ (function(module, exports, __webpack_require__) {

// most Object methods by ES6 should accept primitives
var $export = __webpack_require__("63b6");
var core = __webpack_require__("584a");
var fails = __webpack_require__("294c");
module.exports = function (KEY, exec) {
  var fn = (core.Object || {})[KEY] || Object[KEY];
  var exp = {};
  exp[KEY] = exec(fn);
  $export($export.S + $export.F * fails(function () { fn(1); }), 'Object', exp);
};


/***/ }),

/***/ "d0af":
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.14 / 15.2.3.14 Object.keys(O)
var $keys = __webpack_require__("008c");
var enumBugKeys = __webpack_require__("2118");

module.exports = Object.keys || function keys(O) {
  return $keys(O, enumBugKeys);
};


/***/ }),

/***/ "d25d":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var classof = __webpack_require__("a28d");
var builtinExec = RegExp.prototype.exec;

 // `RegExpExec` abstract operation
// https://tc39.github.io/ecma262/#sec-regexpexec
module.exports = function (R, S) {
  var exec = R.exec;
  if (typeof exec === 'function') {
    var result = exec.call(R, S);
    if (typeof result !== 'object') {
      throw new TypeError('RegExp exec method returned something other than an Object or null');
    }
    return result;
  }
  if (classof(R) !== 'RegExp') {
    throw new TypeError('RegExp#exec called on incompatible receiver');
  }
  return builtinExec.call(R, S);
};


/***/ }),

/***/ "d2d5":
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("1654");
__webpack_require__("549b");
module.exports = __webpack_require__("584a").Array.from;


/***/ }),

/***/ "d3f4":
/***/ (function(module, exports) {

module.exports = function (it) {
  return typeof it === 'object' ? it !== null : typeof it === 'function';
};


/***/ }),

/***/ "d5ca":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("4bbc");

/***/ }),

/***/ "d847":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("5bba");

/***/ }),

/***/ "d864":
/***/ (function(module, exports, __webpack_require__) {

// optional / simple context binding
var aFunction = __webpack_require__("79aa");
module.exports = function (fn, that, length) {
  aFunction(fn);
  if (that === undefined) return fn;
  switch (length) {
    case 1: return function (a) {
      return fn.call(that, a);
    };
    case 2: return function (a, b) {
      return fn.call(that, a, b);
    };
    case 3: return function (a, b, c) {
      return fn.call(that, a, b, c);
    };
  }
  return function (/* ...args */) {
    return fn.apply(that, arguments);
  };
};


/***/ }),

/***/ "d8d6":
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("1654");
__webpack_require__("6c1c");
module.exports = __webpack_require__("ccb9").f('iterator');


/***/ }),

/***/ "d8e8":
/***/ (function(module, exports) {

module.exports = function (it) {
  if (typeof it != 'function') throw TypeError(it + ' is not a function!');
  return it;
};


/***/ }),

/***/ "d9f6":
/***/ (function(module, exports, __webpack_require__) {

var anObject = __webpack_require__("e4ae");
var IE8_DOM_DEFINE = __webpack_require__("794b");
var toPrimitive = __webpack_require__("1bc3");
var dP = Object.defineProperty;

exports.f = __webpack_require__("8e60") ? Object.defineProperty : function defineProperty(O, P, Attributes) {
  anObject(O);
  P = toPrimitive(P, true);
  anObject(Attributes);
  if (IE8_DOM_DEFINE) try {
    return dP(O, P, Attributes);
  } catch (e) { /* empty */ }
  if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported!');
  if ('value' in Attributes) O[P] = Attributes.value;
  return O;
};


/***/ }),

/***/ "dbdb":
/***/ (function(module, exports, __webpack_require__) {

var core = __webpack_require__("584a");
var global = __webpack_require__("e53d");
var SHARED = '__core-js_shared__';
var store = global[SHARED] || (global[SHARED] = {});

(module.exports = function (key, value) {
  return store[key] || (store[key] = value !== undefined ? value : {});
})('versions', []).push({
  version: core.version,
  mode: __webpack_require__("b8e3") ? 'pure' : 'global',
  copyright: '© 2019 Denis Pushkarev (zloirock.ru)'
});


/***/ }),

/***/ "dc45":
/***/ (function(module, exports) {

var toString = {}.toString;

module.exports = function (it) {
  return toString.call(it).slice(8, -1);
};


/***/ }),

/***/ "dc62":
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("9427");
var $Object = __webpack_require__("584a").Object;
module.exports = function create(P, D) {
  return $Object.create(P, D);
};


/***/ }),

/***/ "dd08":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SenseFieldView_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("0b69");
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SenseFieldView_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SenseFieldView_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */
 /* unused harmony default export */ var _unused_webpack_default_export = (_node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SenseFieldView_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "dd44":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var at = __webpack_require__("9a1a")(true);

 // `AdvanceStringIndex` abstract operation
// https://tc39.github.io/ecma262/#sec-advancestringindex
module.exports = function (S, index, unicode) {
  return index + (unicode ? at(S, index).length : 1);
};


/***/ }),

/***/ "e265":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("ed33");

/***/ }),

/***/ "e4ae":
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__("f772");
module.exports = function (it) {
  if (!isObject(it)) throw TypeError(it + ' is not an object!');
  return it;
};


/***/ }),

/***/ "e53d":
/***/ (function(module, exports) {

// https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
var global = module.exports = typeof window != 'undefined' && window.Math == Math
  ? window : typeof self != 'undefined' && self.Math == Math ? self
  // eslint-disable-next-line no-new-func
  : Function('return this')();
if (typeof __g == 'number') __g = global; // eslint-disable-line no-undef


/***/ }),

/***/ "e5b8":
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("1271");
var $Object = __webpack_require__("584a").Object;
module.exports = function getOwnPropertyNames(it) {
  return $Object.getOwnPropertyNames(it);
};


/***/ }),

/***/ "e6f3":
/***/ (function(module, exports, __webpack_require__) {

var has = __webpack_require__("07e3");
var toIObject = __webpack_require__("36c3");
var arrayIndexOf = __webpack_require__("5b4e")(false);
var IE_PROTO = __webpack_require__("5559")('IE_PROTO');

module.exports = function (object, names) {
  var O = toIObject(object);
  var i = 0;
  var result = [];
  var key;
  for (key in O) if (key != IE_PROTO) has(O, key) && result.push(key);
  // Don't enum bug & hidden keys
  while (names.length > i) if (has(O, key = names[i++])) {
    ~arrayIndexOf(result, key) || result.push(key);
  }
  return result;
};


/***/ }),

/***/ "ea03":
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("43f8");
module.exports = __webpack_require__("584a").Object.isExtensible;


/***/ }),

/***/ "ead6":
/***/ (function(module, exports, __webpack_require__) {

// Works with __proto__ only. Old v8 can't work with null proto objects.
/* eslint-disable no-proto */
var isObject = __webpack_require__("f772");
var anObject = __webpack_require__("e4ae");
var check = function (O, proto) {
  anObject(O);
  if (!isObject(proto) && proto !== null) throw TypeError(proto + ": can't set as prototype!");
};
module.exports = {
  set: Object.setPrototypeOf || ('__proto__' in {} ? // eslint-disable-line
    function (test, buggy, set) {
      try {
        set = __webpack_require__("d864")(Function.call, __webpack_require__("bf0b").f(Object.prototype, '__proto__').set, 2);
        set(test, []);
        buggy = !(test instanceof Array);
      } catch (e) { buggy = true; }
      return function setPrototypeOf(O, proto) {
        check(O, proto);
        if (buggy) O.__proto__ = proto;
        else set(O, proto);
        return O;
      };
    }({}, false) : undefined),
  check: check
};


/***/ }),

/***/ "eada":
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("cd03");
module.exports = __webpack_require__("584a").Object.preventExtensions;


/***/ }),

/***/ "ebfd":
/***/ (function(module, exports, __webpack_require__) {

var META = __webpack_require__("62a0")('meta');
var isObject = __webpack_require__("f772");
var has = __webpack_require__("07e3");
var setDesc = __webpack_require__("d9f6").f;
var id = 0;
var isExtensible = Object.isExtensible || function () {
  return true;
};
var FREEZE = !__webpack_require__("294c")(function () {
  return isExtensible(Object.preventExtensions({}));
});
var setMeta = function (it) {
  setDesc(it, META, { value: {
    i: 'O' + ++id, // object ID
    w: {}          // weak collections IDs
  } });
};
var fastKey = function (it, create) {
  // return primitive with prefix
  if (!isObject(it)) return typeof it == 'symbol' ? it : (typeof it == 'string' ? 'S' : 'P') + it;
  if (!has(it, META)) {
    // can't set metadata to uncaught frozen object
    if (!isExtensible(it)) return 'F';
    // not necessary to add metadata
    if (!create) return 'E';
    // add missing metadata
    setMeta(it);
  // return object ID
  } return it[META].i;
};
var getWeak = function (it, create) {
  if (!has(it, META)) {
    // can't set metadata to uncaught frozen object
    if (!isExtensible(it)) return true;
    // not necessary to add metadata
    if (!create) return false;
    // add missing metadata
    setMeta(it);
  // return hash weak collections IDs
  } return it[META].w;
};
// add metadata on freeze-family methods calling
var onFreeze = function (it) {
  if (FREEZE && meta.NEED && isExtensible(it) && !has(it, META)) setMeta(it);
  return it;
};
var meta = module.exports = {
  KEY: META,
  NEED: false,
  fastKey: fastKey,
  getWeak: getWeak,
  onFreeze: onFreeze
};


/***/ }),

/***/ "ed33":
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("014b");
module.exports = __webpack_require__("584a").Object.getOwnPropertySymbols;


/***/ }),

/***/ "f201":
/***/ (function(module, exports, __webpack_require__) {

// 7.3.20 SpeciesConstructor(O, defaultConstructor)
var anObject = __webpack_require__("e4ae");
var aFunction = __webpack_require__("79aa");
var SPECIES = __webpack_require__("5168")('species');
module.exports = function (O, D) {
  var C = anObject(O).constructor;
  var S;
  return C === undefined || (S = anObject(C)[SPECIES]) == undefined ? D : aFunction(S);
};


/***/ }),

/***/ "f410":
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("1af6");
module.exports = __webpack_require__("584a").Array.isArray;


/***/ }),

/***/ "f499":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("a21f");

/***/ }),

/***/ "f682":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var isRegExp = __webpack_require__("c59a");
var anObject = __webpack_require__("b597");
var speciesConstructor = __webpack_require__("8dd0");
var advanceStringIndex = __webpack_require__("dd44");
var toLength = __webpack_require__("b4df");
var callRegExpExec = __webpack_require__("d25d");
var regexpExec = __webpack_require__("7f72");
var $min = Math.min;
var $push = [].push;
var $SPLIT = 'split';
var LENGTH = 'length';
var LAST_INDEX = 'lastIndex';

// eslint-disable-next-line no-empty
var SUPPORTS_Y = !!(function () { try { return new RegExp('x', 'y'); } catch (e) {} })();

// @@split logic
__webpack_require__("1fbc")('split', 2, function (defined, SPLIT, $split, maybeCallNative) {
  var internalSplit;
  if (
    'abbc'[$SPLIT](/(b)*/)[1] == 'c' ||
    'test'[$SPLIT](/(?:)/, -1)[LENGTH] != 4 ||
    'ab'[$SPLIT](/(?:ab)*/)[LENGTH] != 2 ||
    '.'[$SPLIT](/(.?)(.?)/)[LENGTH] != 4 ||
    '.'[$SPLIT](/()()/)[LENGTH] > 1 ||
    ''[$SPLIT](/.?/)[LENGTH]
  ) {
    // based on es5-shim implementation, need to rework it
    internalSplit = function (separator, limit) {
      var string = String(this);
      if (separator === undefined && limit === 0) return [];
      // If `separator` is not a regex, use native split
      if (!isRegExp(separator)) return $split.call(string, separator, limit);
      var output = [];
      var flags = (separator.ignoreCase ? 'i' : '') +
                  (separator.multiline ? 'm' : '') +
                  (separator.unicode ? 'u' : '') +
                  (separator.sticky ? 'y' : '');
      var lastLastIndex = 0;
      var splitLimit = limit === undefined ? 4294967295 : limit >>> 0;
      // Make `global` and avoid `lastIndex` issues by working with a copy
      var separatorCopy = new RegExp(separator.source, flags + 'g');
      var match, lastIndex, lastLength;
      while (match = regexpExec.call(separatorCopy, string)) {
        lastIndex = separatorCopy[LAST_INDEX];
        if (lastIndex > lastLastIndex) {
          output.push(string.slice(lastLastIndex, match.index));
          if (match[LENGTH] > 1 && match.index < string[LENGTH]) $push.apply(output, match.slice(1));
          lastLength = match[0][LENGTH];
          lastLastIndex = lastIndex;
          if (output[LENGTH] >= splitLimit) break;
        }
        if (separatorCopy[LAST_INDEX] === match.index) separatorCopy[LAST_INDEX]++; // Avoid an infinite loop
      }
      if (lastLastIndex === string[LENGTH]) {
        if (lastLength || !separatorCopy.test('')) output.push('');
      } else output.push(string.slice(lastLastIndex));
      return output[LENGTH] > splitLimit ? output.slice(0, splitLimit) : output;
    };
  // Chakra, V8
  } else if ('0'[$SPLIT](undefined, 0)[LENGTH]) {
    internalSplit = function (separator, limit) {
      return separator === undefined && limit === 0 ? [] : $split.call(this, separator, limit);
    };
  } else {
    internalSplit = $split;
  }

  return [
    // `String.prototype.split` method
    // https://tc39.github.io/ecma262/#sec-string.prototype.split
    function split(separator, limit) {
      var O = defined(this);
      var splitter = separator == undefined ? undefined : separator[SPLIT];
      return splitter !== undefined
        ? splitter.call(separator, O, limit)
        : internalSplit.call(String(O), separator, limit);
    },
    // `RegExp.prototype[@@split]` method
    // https://tc39.github.io/ecma262/#sec-regexp.prototype-@@split
    //
    // NOTE: This cannot be properly polyfilled in engines that don't support
    // the 'y' flag.
    function (regexp, limit) {
      var res = maybeCallNative(internalSplit, regexp, this, limit, internalSplit !== $split);
      if (res.done) return res.value;

      var rx = anObject(regexp);
      var S = String(this);
      var C = speciesConstructor(rx, RegExp);

      var unicodeMatching = rx.unicode;
      var flags = (rx.ignoreCase ? 'i' : '') +
                    (rx.multiline ? 'm' : '') +
                    (rx.unicode ? 'u' : '') +
                    (SUPPORTS_Y ? 'y' : 'g');

      // ^(? + rx + ) is needed, in combination with some S slicing, to
      // simulate the 'y' flag.
      var splitter = new C(SUPPORTS_Y ? rx : '^(?:' + rx.source + ')', flags);
      var lim = limit === undefined ? 0xffffffff : limit >>> 0;
      if (lim === 0) return [];
      if (S.length === 0) return callRegExpExec(splitter, S) === null ? [S] : [];
      var p = 0;
      var q = 0;
      var A = [];
      while (q < S.length) {
        splitter.lastIndex = SUPPORTS_Y ? q : 0;
        var z = callRegExpExec(splitter, SUPPORTS_Y ? S : S.slice(q));
        var e;
        if (
          z === null ||
          (e = $min(toLength(splitter.lastIndex + (SUPPORTS_Y ? 0 : q)), S.length)) === p
        ) {
          q = advanceStringIndex(S, q, unicodeMatching);
        } else {
          A.push(S.slice(p, q));
          if (A.length === lim) return A;
          for (var i = 1; i <= z.length - 1; i++) {
            A.push(z[i]);
            if (A.length === lim) return A;
          }
          q = p = e;
        }
      }
      A.push(S.slice(p));
      return A;
    }
  ];
});


/***/ }),

/***/ "f6fd":
/***/ (function(module, exports) {

// document.currentScript polyfill by Adam Miller

// MIT license

(function(document){
  var currentScript = "currentScript",
      scripts = document.getElementsByTagName('script'); // Live NodeList collection

  // If browser needs currentScript polyfill, add get currentScript() to the document object
  if (!(currentScript in document)) {
    Object.defineProperty(document, currentScript, {
      get: function(){

        // IE 6-10 supports script readyState
        // IE 10+ support stack trace
        try { throw new Error(); }
        catch (err) {

          // Find the second match for the "at" string to get file src url from stack.
          // Specifically works with the format of stack traces in IE.
          var i, res = ((/.*at [^\(]*\((.*):.+:.+\)$/ig).exec(err.stack) || [false])[1];

          // For all scripts on the page, if src matches or if ready state is interactive, return the script tag
          for(i in scripts){
            if(scripts[i].src == res || scripts[i].readyState == "interactive"){
              return scripts[i];
            }
          }

          // If no match, return null
          return null;
        }
      }
    });
  }
})(document);


/***/ }),

/***/ "f772":
/***/ (function(module, exports) {

module.exports = function (it) {
  return typeof it === 'object' ? it !== null : typeof it === 'function';
};


/***/ }),

/***/ "f921":
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("014b");
__webpack_require__("c207");
__webpack_require__("69d3");
__webpack_require__("765d");
module.exports = __webpack_require__("584a").Symbol;


/***/ }),

/***/ "fa99":
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("0293");
module.exports = __webpack_require__("584a").Object.getPrototypeOf;


/***/ }),

/***/ "fb15":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/@vue/cli-service/lib/commands/build/setPublicPath.js
// This file is imported into lib/wc client bundles.

if (typeof window !== 'undefined') {
  if (true) {
    __webpack_require__("f6fd")
  }

  var i
  if ((i = window.document.currentScript) && (i = i.src.match(/(.+\/)[^/]+\.js(\?.*)?$/))) {
    __webpack_require__.p = i[1] // eslint-disable-line
  }
}

// Indicate to webpack that this file can be concatenated
/* harmony default export */ var setPublicPath = (null);

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"11c17465-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/SenseFieldView.vue?vue&type=template&id=017a151e&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{attrs:{"id":"SenseFieldView"}},[(_vm.fieldsExtracted)?_c('div',{staticClass:"SenseFieldView"},[_c('div',{staticClass:"fields-list"},_vm._l((_vm.qlik.currentSelectionFields),function(field){return _c('qlik-field-placeholder',{key:field.qField,attrs:{"fieldName":field.qField,"filterVisible":"true","qDoc":_vm.qDoc,"qType":"selections-object"},on:{"fieldVisible":_vm.fieldVisible}})}),1),_c('search',{on:{"clearSearch":_vm.clearSearch,"filterChange":_vm.filterChange}}),_c('div',{staticClass:"fields-list"},_vm._l((_vm.allFields),function(field){return _c('qlik-field-placeholder',{key:field.key,attrs:{"fieldName":field.qName,"filterVisible":field.visible,"qDoc":_vm.qDoc,"qType":"all-fields-object","debugMode":"false"},on:{"fieldVisible":_vm.fieldVisible}})}),1)],1):_vm._e()])}
var staticRenderFns = []


// CONCATENATED MODULE: ./src/SenseFieldView.vue?vue&type=template&id=017a151e&

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/get-iterator.js
var get_iterator = __webpack_require__("5d73");
var get_iterator_default = /*#__PURE__*/__webpack_require__.n(get_iterator);

// EXTERNAL MODULE: ./node_modules/core-js/modules/es6.array.sort.js
var es6_array_sort = __webpack_require__("55dd");

// EXTERNAL MODULE: ./node_modules/regenerator-runtime/runtime.js
var runtime = __webpack_require__("96cf");

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/promise.js
var promise = __webpack_require__("795b");
var promise_default = /*#__PURE__*/__webpack_require__.n(promise);

// CONCATENATED MODULE: ./node_modules/@babel/runtime-corejs2/helpers/esm/asyncToGenerator.js


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) {
  try {
    var info = gen[key](arg);
    var value = info.value;
  } catch (error) {
    reject(error);
    return;
  }

  if (info.done) {
    resolve(value);
  } else {
    promise_default.a.resolve(value).then(_next, _throw);
  }
}

function _asyncToGenerator(fn) {
  return function () {
    var self = this,
        args = arguments;
    return new promise_default.a(function (resolve, reject) {
      var gen = fn.apply(self, args);

      function _next(value) {
        asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value);
      }

      function _throw(err) {
        asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err);
      }

      _next(undefined);
    });
  };
}
// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js
var define_property = __webpack_require__("85f2");
var define_property_default = /*#__PURE__*/__webpack_require__.n(define_property);

// CONCATENATED MODULE: ./node_modules/@babel/runtime-corejs2/helpers/esm/defineProperty.js

function _defineProperty(obj, key, value) {
  if (key in obj) {
    define_property_default()(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"11c17465-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/QlikFieldPlaceholder.vue?vue&type=template&id=3736f87e&scoped=true&
var QlikFieldPlaceholdervue_type_template_id_3736f87e_scoped_true_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return (_vm.filterVisible)?_c('div',{staticClass:"qlik-field-placeholder"},[(!_vm.loaded)?_c('div',{staticClass:"loader"},[_vm._v("TEST")]):_vm._e(),_c('div',{staticClass:"test"},[(_vm.loaded)?_c('qlik-field',{attrs:{"clickAway":false,"showHeader":true,"showArrow":false,"qDoc":_vm.qDoc,"debugMode":false,"fieldName":_vm.fieldName,"qType":_vm.qType}}):_vm._e()],1)]):_vm._e()}
var QlikFieldPlaceholdervue_type_template_id_3736f87e_scoped_true_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/QlikFieldPlaceholder.vue?vue&type=template&id=3736f87e&scoped=true&

// EXTERNAL MODULE: ../sense-field-vue/dist/sense-field-vue.common.js
var sense_field_vue_common = __webpack_require__("2348");
var sense_field_vue_common_default = /*#__PURE__*/__webpack_require__.n(sense_field_vue_common);

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/QlikFieldPlaceholder.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
 // import tempData from "../temp/fieldsdata.js";

/* harmony default export */ var QlikFieldPlaceholdervue_type_script_lang_js_ = ({
  name: "QlikFieldPlaceholder",
  components: {
    QlikField: sense_field_vue_common_default.a
  },
  props: ["fieldName", "filterVisible", "qDoc", "qType"],
  directives: {},
  data: function data() {
    return {
      isVisible: false,
      loaded: true
    };
  },
  methods: {},
  mounted: function mounted() {},
  watch: {}
});
// CONCATENATED MODULE: ./src/components/QlikFieldPlaceholder.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_QlikFieldPlaceholdervue_type_script_lang_js_ = (QlikFieldPlaceholdervue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/components/QlikFieldPlaceholder.vue?vue&type=style&index=0&id=3736f87e&scoped=true&lang=css&
var QlikFieldPlaceholdervue_type_style_index_0_id_3736f87e_scoped_true_lang_css_ = __webpack_require__("1922");

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () { injectStyles.call(this, this.$root.$options.shadowRoot) }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}

// CONCATENATED MODULE: ./src/components/QlikFieldPlaceholder.vue






/* normalize component */

var component = normalizeComponent(
  components_QlikFieldPlaceholdervue_type_script_lang_js_,
  QlikFieldPlaceholdervue_type_template_id_3736f87e_scoped_true_render,
  QlikFieldPlaceholdervue_type_template_id_3736f87e_scoped_true_staticRenderFns,
  false,
  null,
  "3736f87e",
  null
  
)

/* harmony default export */ var QlikFieldPlaceholder = (component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"11c17465-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Search.vue?vue&type=template&id=d0c1e11e&scoped=true&
var Searchvue_type_template_id_d0c1e11e_scoped_true_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"search"},[_c('div',{staticClass:"search-inner"},[_c('div',{staticClass:"lui-search"},[_c('span',{staticClass:"lui-icon lui-icon--search lui-search__search-icon"}),_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.filter),expression:"filter"}],staticClass:"lui-search__input",attrs:{"maxlength":"255","spellcheck":"false","type":"text","placeholder":"Field search"},domProps:{"value":(_vm.filter)},on:{"keyup":_vm.checkForEsc,"input":function($event){if($event.target.composing){ return; }_vm.filter=$event.target.value}}}),_vm._m(0)])])])}
var Searchvue_type_template_id_d0c1e11e_scoped_true_staticRenderFns = [function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('button',{staticClass:"lui-search__clear-button"},[_c('span',{staticClass:"lui-icon lui-icon--small lui-icon--close"})])}]


// CONCATENATED MODULE: ./src/components/Search.vue?vue&type=template&id=d0c1e11e&scoped=true&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Search.vue?vue&type=script&lang=js&


//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var Searchvue_type_script_lang_js_ = ({
  name: "Search",
  props: {},
  data: function data() {
    return {
      filter: ""
    };
  },
  methods: {
    checkForEsc: function checkForEsc(e) {
      var _this = this;

      if (e.keyCode == 27) {
        // _this.filter = "";
        _this.$emit("clearSearch");
      }
    }
  },
  watch: {
    filter: function () {
      var _filter = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee(newVal, oldVal) {
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                this.$emit("filterChange", newVal);

              case 1:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function filter(_x, _x2) {
        return _filter.apply(this, arguments);
      }

      return filter;
    }()
  }
});
// CONCATENATED MODULE: ./src/components/Search.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_Searchvue_type_script_lang_js_ = (Searchvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/components/Search.vue?vue&type=style&index=0&id=d0c1e11e&scoped=true&lang=css&
var Searchvue_type_style_index_0_id_d0c1e11e_scoped_true_lang_css_ = __webpack_require__("3a9d");

// CONCATENATED MODULE: ./src/components/Search.vue






/* normalize component */

var Search_component = normalizeComponent(
  components_Searchvue_type_script_lang_js_,
  Searchvue_type_template_id_d0c1e11e_scoped_true_render,
  Searchvue_type_template_id_d0c1e11e_scoped_true_staticRenderFns,
  false,
  null,
  "d0c1e11e",
  null
  
)

/* harmony default export */ var Search = (Search_component.exports);
// EXTERNAL MODULE: ./node_modules/enigma.js/enigma.js
var enigma = __webpack_require__("5987");
var enigma_default = /*#__PURE__*/__webpack_require__.n(enigma);

// EXTERNAL MODULE: ./node_modules/enigma.js/schemas/12.170.2.json
var _12_170_2 = __webpack_require__("9e99");

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/SenseFieldView.vue?vue&type=script&lang=js&






var _name$components$prop;

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

 // import Loader from "./components/Loader.vue";
// import tempFieldsData from "./temp/fields.js";




function compare(a, b) {
  if (a.qName < b.qName) return -1;
  if (a.qName > b.qName) return 1;
  return 0;
}

/* harmony default export */ var SenseFieldViewvue_type_script_lang_js_ = (_name$components$prop = {
  name: "SenseFieldView",
  components: {
    QlikFieldPlaceholder: QlikFieldPlaceholder,
    Search: Search // Loader

  },
  props: {// allFields: {
    //   type: Array,
    //   required: false,
    //   default: function() {
    //     return [
    //       {
    //         qName: "CategoryName",
    //         qCardinal: 5,
    //         qTags: ["$ascii", "$text"],
    //         qSrcTables: ["Categories"]
    //       }
    //     ];
    //   }
    // },
    // selectionFields: {
    //   type: Array,
    //   required: false,
    //   default: function() {
    //     return [
    //       {
    //         qName: "CategoryID",
    //         qCardinal: 5,
    //         qTags: ["$key", "$numeric", "$integer"],
    //         qSrcTables: ["Categories", "Products"],
    //         visible: true,
    //         values: 1
    //       }
    //     ];
    //   }
    // }
  }
}, _defineProperty(_name$components$prop, "props", {
  isDebug: {
    type: Boolean,
    required: false,
    default: true
  },
  qDoc: {
    type: Object,
    required: false
  },
  allFields1: {
    type: Array,
    required: false,
    default: function _default() {
      return [{
        qName: "CategoryName",
        qCardinal: 5,
        qTags: ["$ascii", "$text"],
        qSrcTables: ["Categories"]
      }];
    }
  },
  selectionFields: {
    type: Array,
    required: false,
    default: function _default() {
      return [{
        qName: "CategoryID",
        qCardinal: 5,
        qTags: ["$key", "$numeric", "$integer"],
        qSrcTables: ["Categories", "Products"],
        visible: true
      }];
    }
  }
}), _defineProperty(_name$components$prop, "data", function data() {
  var _ref;

  return _ref = {
    // fields: [],
    // filter: "",
    // qFields: [],
    // qfieldsCurrentSelections: [],
    // global: {},
    // session: {},
    // qDoc: {},
    fieldsExtracted: true,
    fields: [],
    filter: "",
    // qFields: [],
    // qfieldsCurrentSelections: [],
    // global: {},
    // session: {},
    qDocData: {},
    allFields: []
  }, _defineProperty(_ref, "fieldsExtracted", true), _defineProperty(_ref, "qlik", {
    session: {},
    qDoc: {},
    currentSelections: {},
    currentSelectionFields: []
  }), _ref;
}), _defineProperty(_name$components$prop, "methods", {
  fieldVisible: function () {
    var _fieldVisible = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee(fieldName) {
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, this);
    }));

    function fieldVisible(_x) {
      return _fieldVisible.apply(this, arguments);
    }

    return fieldVisible;
  }(),
  clearSearch: function clearSearch() {
    this.filter = "";
  },
  filterChange: function filterChange(value) {
    this.filter = value;
  },
  getCurrentSelections: function () {
    var _getCurrentSelections = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee3() {
      var sessionList, sessionObj, selections;
      return regeneratorRuntime.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              sessionList = {
                qInfo: {
                  qId: "",
                  qType: "SessionLists"
                },
                qSelectionObjectDef: {}
              };
              _context3.next = 3;
              return this.qDocData.createSessionObject(sessionList);

            case 3:
              sessionObj = _context3.sent;
              _context3.next = 6;
              return sessionObj.getLayout();

            case 6:
              selections = _context3.sent;
              console.log(selections);
              sessionObj.on("changed",
              /*#__PURE__*/
              _asyncToGenerator(
              /*#__PURE__*/
              regeneratorRuntime.mark(function _callee2() {
                var newData;
                return regeneratorRuntime.wrap(function _callee2$(_context2) {
                  while (1) {
                    switch (_context2.prev = _context2.next) {
                      case 0:
                        _context2.next = 2;
                        return sessionObj.getLayout();

                      case 2:
                        newData = _context2.sent;
                        // self.qlik.currentSelectionFields = newData.qSelectionObject.qSelections
                        console.log(newData.qSelectionObject.qSelections);

                      case 4:
                      case "end":
                        return _context2.stop();
                    }
                  }
                }, _callee2, this);
              })));
              return _context3.abrupt("return", selections);

            case 10:
            case "end":
              return _context3.stop();
          }
        }
      }, _callee3, this);
    }));

    function getCurrentSelections() {
      return _getCurrentSelections.apply(this, arguments);
    }

    return getCurrentSelections;
  }()
}), _defineProperty(_name$components$prop, "mounted", function () {
  var _mounted = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee5() {
    var self, qTables, session, global, qsDocPath, qDoc, sessionList, sessionObj, selections, fields, _iteratorNormalCompletion, _didIteratorError, _iteratorError, _iterator, _step, table, _iteratorNormalCompletion2, _didIteratorError2, _iteratorError2, _iterator2, _step2, field, sorted;

    return regeneratorRuntime.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            self = this;
            qTables = {};
            this.qDocData = this.qDoc;

            if (!self.isDebug) {
              _context5.next = 16;
              break;
            }

            session = enigma_default.a.create({
              schema: _12_170_2,
              url: "ws://localhost:19076/app/engineData",
              createSocket: function createSocket(url) {
                return new WebSocket(url);
              }
            });
            _context5.next = 7;
            return session.open();

          case 7:
            global = _context5.sent;
            qsDocPath = "/data/Consumer Sales.qvf";
            _context5.next = 11;
            return global.openDoc(qsDocPath);

          case 11:
            qDoc = _context5.sent;
            self.qDoc = qDoc;
            _context5.next = 15;
            return qDoc.getTablesAndKeys({}, {}, 0, true, false);

          case 15:
            qTables = _context5.sent;

          case 16:
            sessionList = {
              qInfo: {
                qType: "SessionLists"
              },
              qSelectionObjectDef: {}
            }; // let qTables = await qDoc.getTablesAndKeys({}, {}, 0, true, false);

            _context5.next = 19;
            return this.qDocData.createSessionObject(sessionList);

          case 19:
            sessionObj = _context5.sent;
            _context5.next = 22;
            return sessionObj.getLayout();

          case 22:
            selections = _context5.sent;
            self.qlik.currentSelectionFields = selections.qSelectionObject.qSelections; // console.log(selections);

            sessionObj.on("changed",
            /*#__PURE__*/
            function () {
              var _ref3 = _asyncToGenerator(
              /*#__PURE__*/
              regeneratorRuntime.mark(function _callee4(d) {
                var newData;
                return regeneratorRuntime.wrap(function _callee4$(_context4) {
                  while (1) {
                    switch (_context4.prev = _context4.next) {
                      case 0:
                        _context4.next = 2;
                        return sessionObj.getLayout();

                      case 2:
                        newData = _context4.sent;
                        self.qlik.currentSelectionFields = newData.qSelectionObject.qSelections;
                        console.log(newData.qSelectionObject.qSelections); // console.log(newData);

                      case 5:
                      case "end":
                        return _context4.stop();
                    }
                  }
                }, _callee4, this);
              }));

              return function (_x2) {
                return _ref3.apply(this, arguments);
              };
            }());
            self.qlik.currentSelections = selections;
            fields = [];
            _iteratorNormalCompletion = true;
            _didIteratorError = false;
            _iteratorError = undefined;
            _context5.prev = 30;
            _iterator = get_iterator_default()(qTables.qtr);

          case 32:
            if (_iteratorNormalCompletion = (_step = _iterator.next()).done) {
              _context5.next = 56;
              break;
            }

            table = _step.value;
            _iteratorNormalCompletion2 = true;
            _didIteratorError2 = false;
            _iteratorError2 = undefined;
            _context5.prev = 37;

            for (_iterator2 = get_iterator_default()(table.qFields); !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
              field = _step2.value;
              field.table = table.qName;
              field.key = field.qName + "" + table.qName;
              field.values = {
                qDataPages: [{
                  qMatrix: [[{
                    qText: "3",
                    qNum: 3,
                    qElemNumber: 0,
                    qState: "S"
                  }]]
                }]
              };
              field.visible = true;

              if (field.qKeyType == "NOT_KEY") {
                fields.push(field);
              }
            }

            _context5.next = 45;
            break;

          case 41:
            _context5.prev = 41;
            _context5.t0 = _context5["catch"](37);
            _didIteratorError2 = true;
            _iteratorError2 = _context5.t0;

          case 45:
            _context5.prev = 45;
            _context5.prev = 46;

            if (!_iteratorNormalCompletion2 && _iterator2.return != null) {
              _iterator2.return();
            }

          case 48:
            _context5.prev = 48;

            if (!_didIteratorError2) {
              _context5.next = 51;
              break;
            }

            throw _iteratorError2;

          case 51:
            return _context5.finish(48);

          case 52:
            return _context5.finish(45);

          case 53:
            _iteratorNormalCompletion = true;
            _context5.next = 32;
            break;

          case 56:
            _context5.next = 62;
            break;

          case 58:
            _context5.prev = 58;
            _context5.t1 = _context5["catch"](30);
            _didIteratorError = true;
            _iteratorError = _context5.t1;

          case 62:
            _context5.prev = 62;
            _context5.prev = 63;

            if (!_iteratorNormalCompletion && _iterator.return != null) {
              _iterator.return();
            }

          case 65:
            _context5.prev = 65;

            if (!_didIteratorError) {
              _context5.next = 68;
              break;
            }

            throw _iteratorError;

          case 68:
            return _context5.finish(65);

          case 69:
            return _context5.finish(62);

          case 70:
            sorted = fields.sort(compare); // _this.qFields = sorted;

            self.allFields = sorted; // self.allFields.push(sorted[0]);

          case 72:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5, this, [[30, 58, 62, 70], [37, 41, 45, 53], [46,, 48, 52], [63,, 65, 69]]);
  }));

  function mounted() {
    return _mounted.apply(this, arguments);
  }

  return mounted;
}()), _defineProperty(_name$components$prop, "asyncComputed", {
  getAllFields1: function () {
    var _getAllFields = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee6() {
      return regeneratorRuntime.wrap(function _callee6$(_context6) {
        while (1) {
          switch (_context6.prev = _context6.next) {
            case 0:
            case "end":
              return _context6.stop();
          }
        }
      }, _callee6, this);
    }));

    function getAllFields1() {
      return _getAllFields.apply(this, arguments);
    }

    return getAllFields1;
  }() // getFields: function() {
  //   let _this = this;
  //   let currentSelectionFields = _this.qfieldsCurrentSelections.map(function(
  //     f
  //   ) {
  //     return f.qName;
  //   });
  //   let result = [];
  //   if (_this.filter.length > 0) {
  //     result = _this.qFields.filter(function(f) {
  //       return (
  //         f.qName.toLowerCase().indexOf(_this.filter.toLowerCase()) !== -1 &&
  //         currentSelectionFields.indexOf(f.qName) == -1 &&
  //         !f.qIsHidden
  //       );
  //     });
  //   } else {
  //     result = _this.qFields.filter(function(f) {
  //       if (!f.values) {
  //         // console.log('added')
  //         f.values = {
  //           qDataPages: [
  //             {
  //               qMatrix: []
  //             }
  //           ]
  //         };
  //       }
  //       // console.log('init vals', f.qName)
  //       return currentSelectionFields.indexOf(f.qName) == -1 && !f.qIsHidden;
  //     });
  //   }
  //   // console.log(result)
  //   return result;
  // },
  // getCurrentSelectionFields: function() {
  //   let _this = this;
  //   // return tempFieldsData.currentSelections;
  //   return _this.qfields;
  // }

}), _defineProperty(_name$components$prop, "watch", {}), _name$components$prop);
// CONCATENATED MODULE: ./src/SenseFieldView.vue?vue&type=script&lang=js&
 /* harmony default export */ var src_SenseFieldViewvue_type_script_lang_js_ = (SenseFieldViewvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/SenseFieldView.vue?vue&type=style&index=0&lang=css&
var SenseFieldViewvue_type_style_index_0_lang_css_ = __webpack_require__("dd08");

// CONCATENATED MODULE: ./src/SenseFieldView.vue






/* normalize component */

var SenseFieldView_component = normalizeComponent(
  src_SenseFieldViewvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* harmony default export */ var SenseFieldView = (SenseFieldView_component.exports);
// CONCATENATED MODULE: ./node_modules/@vue/cli-service/lib/commands/build/entry-lib.js


/* harmony default export */ var entry_lib = __webpack_exports__["default"] = (SenseFieldView);



/***/ }),

/***/ "fc4a":
/***/ (function(module, exports) {

module.exports = function (it) {
  if (typeof it != 'function') throw TypeError(it + ' is not a function!');
  return it;
};


/***/ }),

/***/ "fde4":
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("bf90");
var $Object = __webpack_require__("584a").Object;
module.exports = function getOwnPropertyDescriptor(it, key) {
  return $Object.getOwnPropertyDescriptor(it, key);
};


/***/ }),

/***/ "fe95":
/***/ (function(module, exports, __webpack_require__) {

var store = __webpack_require__("b658")('wks');
var uid = __webpack_require__("11f1");
var Symbol = __webpack_require__("635a").Symbol;
var USE_SYMBOL = typeof Symbol == 'function';

var $exports = module.exports = function (name) {
  return store[name] || (store[name] =
    USE_SYMBOL && Symbol[name] || (USE_SYMBOL ? Symbol : uid)('Symbol.' + name));
};

$exports.store = store;


/***/ })

/******/ })["default"];
});
//# sourceMappingURL=sense-field-view.umd.js.map