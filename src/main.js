import Vue from 'vue'
import SenseFieldView from './SenseFieldView.vue'
import AsyncComputed from 'vue-async-computed'

Vue.use(AsyncComputed)
// import VueObserveVisibility from 'vue-observe-visibility'
// Vue.use(VueObserveVisibility)

// import { ObserveVisibility } from 'vue-observe-visibility'

// Vue.directive('observe-visibility', ObserveVisibility)

Vue.config.productionTip = false

import QlikFieldStyles from "sense-field-vue/dist/sense-field-vue.css";

new Vue({
  render: h => h(SenseFieldView),
}).$mount('#SenseFieldView')
