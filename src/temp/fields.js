const fields = {
    "currentSelections": [{
        "qIsHidden": true,
        "qIsSystem": true,
        "qName": "$Field",
        "qCardinal": 30,
        "qTags": [
            "$key",
            "$hidden",
            "$system",
            "$ascii",
            "$text"
        ],
        "qSrcTables": []
    },
    {
        "qIsHidden": true,
        "qIsSystem": true,
        "qName": "$Table",
        "qCardinal": 7,
        "qTags": [
            "$key",
            "$hidden",
            "$system",
            "$ascii",
            "$text"
        ],
        "qSrcTables": []
    }
    ],
    "allFields": [
        {
        "qIsHidden": true,
        "qIsSystem": true,
        "qName": "$Field",
        "qCardinal": 30,
        "qTags": [
            "$key",
            "$hidden",
            "$system",
            "$ascii",
            "$text"
        ],
        "qSrcTables": []
    },
    {
        "qIsHidden": true,
        "qIsSystem": true,
        "qName": "$Table",
        "qCardinal": 7,
        "qTags": [
            "$key",
            "$hidden",
            "$system",
            "$ascii",
            "$text"
        ],
        "qSrcTables": []
    },
    {
        "qIsHidden": true,
        "qIsSystem": true,
        "qName": "$Rows",
        "qCardinal": 7,
        "qTags": [
            "$hidden",
            "$system",
            "$numeric",
            "$integer"
        ],
        "qSrcTables": []
    },
    {
        "qIsHidden": true,
        "qIsSystem": true,
        "qName": "$Fields",
        "qCardinal": 5,
        "qTags": [
            "$hidden",
            "$system",
            "$numeric",
            "$integer"
        ],
        "qSrcTables": []
    },
    {
        "qIsHidden": true,
        "qIsSystem": true,
        "qName": "$FieldNo",
        "qCardinal": 11,
        "qTags": [
            "$hidden",
            "$system",
            "$numeric",
            "$integer"
        ],
        "qSrcTables": []
    },
    {
        "qIsHidden": true,
        "qIsSystem": true,
        "qName": "$Info",
        "qCardinal": 1,
        "qTags": [
            "$hidden",
            "$system",
            "$ascii",
            "$text"
        ],
        "qSrcTables": []
    },
    {
        "qName": "CategoryID",
        "qCardinal": 5,
        "qTags": [
            "$key",
            "$numeric",
            "$integer"
        ],
        "qSrcTables": [
            "Categories",
            "Products"
        ]
    },
    {
        "qName": "CategoryName",
        "qCardinal": 5,
        "qTags": [
            "$ascii",
            "$text"
        ],
        "qSrcTables": [
            "Categories"
        ]
    },
    {
        "qName": "Address",
        "qCardinal": 1,
        "qTags": [
            "$text"
        ],
        "qSrcTables": [
            "Customers"
        ]
    },
    {
        "qName": "City",
        "qCardinal": 1,
        "qTags": [
            "$text"
        ],
        "qSrcTables": [
            "Customers"
        ]
    },
    {
        "qName": "ContactName",
        "qCardinal": 1,
        "qTags": [
            "$text"
        ],
        "qSrcTables": [
            "Customers"
        ]
    },
    {
        "qName": "ContactTitle",
        "qCardinal": 1,
        "qTags": [
            "$ascii",
            "$text"
        ],
        "qSrcTables": [
            "Customers"
        ]
    },
    {
        "qName": "Country",
        "qCardinal": 1,
        "qTags": [
            "$ascii",
            "$text"
        ],
        "qSrcTables": [
            "Customers"
        ]
    },
    {
        "qName": "Customer",
        "qCardinal": 1,
        "qTags": [
            "$text"
        ],
        "qSrcTables": [
            "Customers"
        ]
    },
    {
        "qName": "CustomerID",
        "qCardinal": 1,
        "qTags": [
            "$key",
            "$ascii",
            "$text"
        ],
        "qSrcTables": [
            "Customers",
            "Orders"
        ]
    },
    {
        "qName": "Fax",
        "qCardinal": 1,
        "qTags": [],
        "qSrcTables": [
            "Customers"
        ]
    },
    {
        "qName": "Phone",
        "qCardinal": 1,
        "qTags": [],
        "qSrcTables": [
            "Customers"
        ]
    },
    {
        "qName": "PostalCode",
        "qCardinal": 1,
        "qTags": [
            "$numeric",
            "$integer"
        ],
        "qSrcTables": [
            "Customers"
        ]
    },
    {
        "qName": "Region",
        "qCardinal": 0,
        "qTags": [
            "$text"
        ],
        "qSrcTables": [
            "Customers"
        ]
    },
    {
        "qName": "OrderID",
        "qCardinal": 17,
        "qTags": [
            "$key",
            "$numeric",
            "$integer"
        ],
        "qSrcTables": [
            "Order_Details",
            "Orders"
        ]
    },
    {
        "qName": "ProductID",
        "qCardinal": 10,
        "qTags": [
            "$key",
            "$numeric",
            "$integer"
        ],
        "qSrcTables": [
            "Order_Details",
            "Products"
        ]
    },
    {
        "qName": "Quantity",
        "qCardinal": 16,
        "qTags": [
            "$numeric",
            "$integer"
        ],
        "qSrcTables": [
            "Order_Details"
        ]
    },
    {
        "qName": "UnitPrice",
        "qCardinal": 14,
        "qTags": [
            "$numeric"
        ],
        "qSrcTables": [
            "Order_Details"
        ]
    },
    {
        "qName": "OrderTotal",
        "qCardinal": 30,
        "qTags": [
            "$numeric"
        ],
        "qSrcTables": [
            "Order_Details"
        ]
    },
    {
        "qName": "OrderDetailCounter",
        "qCardinal": 1,
        "qTags": [
            "$numeric",
            "$integer"
        ],
        "qSrcTables": [
            "Order_Details"
        ]
    },
    {
        "qName": "OrderDate",
        "qCardinal": 16,
        "qTags": [
            "$numeric",
            "$integer",
            "$timestamp",
            "$date"
        ],
        "qSrcTables": [
            "Orders"
        ]
    },
    {
        "qName": "OrderYear",
        "qCardinal": 2,
        "qTags": [
            "$numeric",
            "$integer"
        ],
        "qSrcTables": [
            "Orders"
        ]
    },
    {
        "qName": "OrderQuarter",
        "qCardinal": 6,
        "qTags": [
            "$numeric",
            "$integer",
            "$timestamp",
            "$date"
        ],
        "qSrcTables": [
            "Orders"
        ]
    },
    {
        "qName": "OrderMonth",
        "qCardinal": 10,
        "qTags": [
            "$numeric",
            "$integer"
        ],
        "qSrcTables": [
            "Orders"
        ]
    },
    {
        "qName": "OrderDay",
        "qCardinal": 12,
        "qTags": [
            "$numeric",
            "$integer"
        ],
        "qSrcTables": [
            "Orders"
        ]
    },
    {
        "qName": "ShipperID",
        "qCardinal": 3,
        "qTags": [
            "$key",
            "$numeric",
            "$integer"
        ],
        "qSrcTables": [
            "Orders",
            "Shippers"
        ]
    },
    {
        "qName": "OrderCounter",
        "qCardinal": 1,
        "qTags": [
            "$numeric",
            "$integer"
        ],
        "qSrcTables": [
            "Orders"
        ]
    },
    {
        "qName": "ProductName",
        "qCardinal": 10,
        "qTags": [
            "$text"
        ],
        "qSrcTables": [
            "Products"
        ]
    },
    {
        "qName": "SupplierID",
        "qCardinal": 9,
        "qTags": [
            "$key",
            "$numeric",
            "$integer"
        ],
        "qSrcTables": [
            "Products",
            "Suppliers"
        ]
    },
    {
        "qName": "Shipper",
        "qCardinal": 3,
        "qTags": [
            "$ascii",
            "$text"
        ],
        "qSrcTables": [
            "Shippers"
        ]
    },
    {
        "qName": "Supplier",
        "qCardinal": 9,
        "qTags": [
            "$text"
        ],
        "qSrcTables": [
            "Suppliers"
        ]
    }
    ]
}

export default fields
